CREATE TABLE `QuoteAmount` (
  `QuoteAmount_ID` INT NOT NULL,
  `FeatureType` VARCHAR(45) NOT NULL,
  `FeatureValue` VARCHAR(45) NOT NULL,
  `PriceModifier` INT NULL,
  PRIMARY KEY (`QuoteAmount_ID`)
);

DELETE FROM QuoteAmount;

INSERT INTO `QuoteAmount` (`QuoteAmount_ID`, `FeatureType`, `FeatureValue`, `PriceModifier`) VALUES(1,'TYPE','Compact Car' ,'+100.00');
INSERT INTO `QuoteAmount` (`QuoteAmount_ID`, `FeatureType`, `FeatureValue`, `PriceModifier`) VALUES(2,'TYPE','Small SUV' ,'+150');
INSERT INTO `QuoteAmount` (`QuoteAmount_ID`, `FeatureType`, `FeatureValue`, `PriceModifier`) VALUES(3,'RIMS','Steel' ,'0');
INSERT INTO `QuoteAmount` (`QuoteAmount_ID`, `FeatureType`, `FeatureValue`, `PriceModifier`) VALUES(4,'RIMS','Aluminum' ,'+10');
INSERT INTO `QuoteAmount` (`QuoteAmount_ID`, `FeatureType`, `FeatureValue`, `PriceModifier`) VALUES(4,'RIMS','No wheels' ,'-40');
INSERT INTO `QuoteAmount` (`QuoteAmount_ID`, `FeatureType`, `FeatureValue`, `PriceModifier`) VALUES(5,'HAS_TITLE','Yes' ,'0');
INSERT INTO `QuoteAmount` (`QuoteAmount_ID`, `FeatureType`, `FeatureValue`, `PriceModifier`) VALUES(6,'HAS_TITLE','No' ,'-10');
INSERT INTO `QuoteAmount` (`QuoteAmount_ID`, `FeatureType`, `FeatureValue`, `PriceModifier`) VALUES(7,'TRANSPORT','Hillsborough' ,'0');
INSERT INTO `QuoteAmount` (`QuoteAmount_ID`, `FeatureType`, `FeatureValue`, `PriceModifier`) VALUES(8,'TRANSPORT','Pinellas' ,'0');
INSERT INTO `QuoteAmount` (`QuoteAmount_ID`, `FeatureType`, `FeatureValue`, `PriceModifier`) VALUES(9,'TRANSPORT','Pasco' ,'-20');
INSERT INTO `QuoteAmount` (`QuoteAmount_ID`, `FeatureType`, `FeatureValue`, `PriceModifier`) VALUES(10,'TRANSPORT','Hernando' ,'-20');
INSERT INTO `QuoteAmount` (`QuoteAmount_ID`, `FeatureType`, `FeatureValue`, `PriceModifier`) VALUES(11,'TRANSPORT','Polk' ,'-20');
INSERT INTO `QuoteAmount` (`QuoteAmount_ID`, `FeatureType`, `FeatureValue`, `PriceModifier`) VALUES(12,'SELF_DELIVERY','Yes' ,'+40');
INSERT INTO `QuoteAmount` (`QuoteAmount_ID`, `FeatureType`, `FeatureValue`, `PriceModifier`) VALUES(13,'SELF_DELIVERY','No' ,'0');

