CREATE TABLE `Application` (
  `Application_ID` INT NOT NULL,
  `Code` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`Application_ID`),
  UNIQUE INDEX `Code_UNIQUE` (`Code` ASC));

INSERT INTO `Application`(`Application_ID`,`Code`)VALUES(1,'VEHICLE_QUOTE_TOOL');

CREATE TABLE `User_UserRole_Application` (
  `User_UserRole_Application_ID` INT NOT NULL AUTO_INCREMENT,
  `User_ID` INT NOT NULL,
  `UserRole_ID` INT NOT NULL,
  `Application_Code` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`User_UserRole_Application_ID`),
  UNIQUE INDEX `UNIQUE_KEY` (`User_ID` ASC, `UserRole_ID` ASC, `Application_Code` ASC));

  INSERT INTO `UserRole`(`UserRole_ID`,`Description`)VALUES(3,'Vendor');

ALTER TABLE `User` 
CHANGE COLUMN `Email` `Email` VARCHAR(150) NOT NULL ;

ALTER TABLE `Transaction` 
ADD COLUMN `CreatedBy_User_ID` INT NULL;
