TRUNCATE TABLE `QuoteAmount`

INSERT INTO `QuoteAmount` (`QuoteAmount_ID`,`FeatureType`,`FeatureValue`,`PriceModifier`) VALUES (2,'RIMS','No Wheels',-40);
INSERT INTO `QuoteAmount` (`QuoteAmount_ID`,`FeatureType`,`FeatureValue`,`PriceModifier`) VALUES (3,'RIMS','Steel',0);
INSERT INTO `QuoteAmount` (`QuoteAmount_ID`,`FeatureType`,`FeatureValue`,`PriceModifier`) VALUES (4,'RIMS','Aluminum',10);
INSERT INTO `QuoteAmount` (`QuoteAmount_ID`,`FeatureType`,`FeatureValue`,`PriceModifier`) VALUES (5,'HAS_TITLE','Yes',10);
INSERT INTO `QuoteAmount` (`QuoteAmount_ID`,`FeatureType`,`FeatureValue`,`PriceModifier`) VALUES (6,'HAS_TITLE','No',-10);
INSERT INTO `QuoteAmount` (`QuoteAmount_ID`,`FeatureType`,`FeatureValue`,`PriceModifier`) VALUES (7,'LOCATION','Hillsborough',0);
INSERT INTO `QuoteAmount` (`QuoteAmount_ID`,`FeatureType`,`FeatureValue`,`PriceModifier`) VALUES (8,'LOCATION','Pinellas',0);
INSERT INTO `QuoteAmount` (`QuoteAmount_ID`,`FeatureType`,`FeatureValue`,`PriceModifier`) VALUES (9,'LOCATION','Pasco',-20);
INSERT INTO `QuoteAmount` (`QuoteAmount_ID`,`FeatureType`,`FeatureValue`,`PriceModifier`) VALUES (10,'LOCATION','Hernando',-20);
INSERT INTO `QuoteAmount` (`QuoteAmount_ID`,`FeatureType`,`FeatureValue`,`PriceModifier`) VALUES (11,'LOCATION','Polk',-20);
INSERT INTO `QuoteAmount` (`QuoteAmount_ID`,`FeatureType`,`FeatureValue`,`PriceModifier`) VALUES (12,'SELF_DELIVERY','Yes',40);
INSERT INTO `QuoteAmount` (`QuoteAmount_ID`,`FeatureType`,`FeatureValue`,`PriceModifier`) VALUES (13,'SELF_DELIVERY','No',0);
INSERT INTO `QuoteAmount` (`QuoteAmount_ID`,`FeatureType`,`FeatureValue`,`PriceModifier`) VALUES (14,'HAS_KEYS','Yes',0);
INSERT INTO `QuoteAmount` (`QuoteAmount_ID`,`FeatureType`,`FeatureValue`,`PriceModifier`) VALUES (15,'HAS_KEYS','No',-20);
INSERT INTO `QuoteAmount` (`QuoteAmount_ID`,`FeatureType`,`FeatureValue`,`PriceModifier`) VALUES (16,'TYPE','Compact Car',100);
INSERT INTO `QuoteAmount` (`QuoteAmount_ID`,`FeatureType`,`FeatureValue`,`PriceModifier`) VALUES (17,'TYPE','Midsize Car',160);
INSERT INTO `QuoteAmount` (`QuoteAmount_ID`,`FeatureType`,`FeatureValue`,`PriceModifier`) VALUES (18,'TYPE','Fullsize Car',180);
INSERT INTO `QuoteAmount` (`QuoteAmount_ID`,`FeatureType`,`FeatureValue`,`PriceModifier`) VALUES (19,'TYPE','Pickup',220);
INSERT INTO `QuoteAmount` (`QuoteAmount_ID`,`FeatureType`,`FeatureValue`,`PriceModifier`) VALUES (20,'TYPE','Mini Van',200);
INSERT INTO `QuoteAmount` (`QuoteAmount_ID`,`FeatureType`,`FeatureValue`,`PriceModifier`) VALUES (21,'TYPE','Small SUV',200);
INSERT INTO `QuoteAmount` (`QuoteAmount_ID`,`FeatureType`,`FeatureValue`,`PriceModifier`) VALUES (22,'TYPE','Fullsize SUV',220);