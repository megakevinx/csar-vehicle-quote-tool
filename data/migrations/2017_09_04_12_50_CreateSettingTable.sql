CREATE TABLE `Setting` (
  `Setting_ID` INT NOT NULL,
  `Code` VARCHAR(45) NOT NULL,
  `Value` VARCHAR(200) NOT NULL,
  PRIMARY KEY (`Setting_ID`),
  UNIQUE INDEX `Code_UNIQUE` (`Code` ASC));

INSERT INTO `Setting`(`Setting_ID`,`Code`,`Value`)
VALUES(1,'VEHICLE_DROP_OFF_ADDRESS','123 Main St Tampa, FL 33503');


INSERT INTO `Setting`(`Setting_ID`,`Code`,`Value`)
VALUES(2,'CSAR_CONTACT_PHONE_NUMBER','555-555-5555');

