CREATE TABLE `QuoteAmountOverride` (
  `QuoteAmountOverride_ID` INT NOT NULL AUTO_INCREMENT,
  `VehicleYear` VARCHAR(4) NOT NULL,
  `VehicleMake` VARCHAR(25) NOT NULL,
  `VehicleModel` VARCHAR(25) NOT NULL,
  `Quote` DECIMAL(10,2) NOT NULL,
  PRIMARY KEY (`QuoteAmountOverride_ID`));
