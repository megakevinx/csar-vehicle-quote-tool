ALTER TABLE `QuoteAmountOverride` 
ADD UNIQUE INDEX `UK_QuoteAmountOverride` (`VehicleYear` ASC, `VehicleMake` ASC, `VehicleModel` ASC);
