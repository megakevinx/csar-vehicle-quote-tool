ALTER TABLE `QuoteAmountOverride` 
ADD COLUMN `VehicleYearComparer` CHAR(6) NOT NULL;

ALTER TABLE `QuoteAmountOverride` 
DROP INDEX `UK_QuoteAmountOverride` ,
ADD UNIQUE INDEX `UK_QuoteAmountOverride` (`VehicleYear` ASC, `VehicleMake` ASC, `VehicleModel` ASC, `VehicleYearComparer` ASC);

UPDATE `QuoteAmountOverride` SET `VehicleYearComparer` = 'equals'
