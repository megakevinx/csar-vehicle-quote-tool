INSERT INTO `VehicleType` (`VehicleType_ID`,`Description`) VALUES (8, 'Midsize SUV');
INSERT INTO `VehicleType` (`VehicleType_ID`,`Description`) VALUES (9, 'Small Pickup');

TRUNCATE TABLE `QuoteAmount`;

INSERT INTO `QuoteAmount` (`QuoteAmount_ID`, `FeatureType`, `FeatureValue`, `PriceModifier`) VALUES
(1, 'TYPE', 'Compact Car', 100),
(2, 'TYPE', 'Midsize Car', 150),
(3, 'TYPE', 'Fullsize Car', 180),
(4, 'TYPE', 'Small Pickup', 190),
(5, 'TYPE', 'Pickup', 200),
(6, 'TYPE', 'Mini Van', 180),
(7, 'TYPE', 'Small SUV', 140),
(8, 'TYPE', 'Midsize SUV', 160),
(9, 'TYPE', 'Fullsize SUV', 180),
(10, 'RIMS', 'No Wheels', -40),
(11, 'RIMS', 'Steel', 0),
(12, 'RIMS', 'Aluminum', 0),
(13, 'HAS_TITLE', 'Yes', 0),
(14, 'HAS_TITLE', 'No', -10),
(15, 'LOCATION', 'Hillsborough', 0),
(16, 'LOCATION', 'Pinellas', 0),
(17, 'LOCATION', 'Pasco', -25),
(18, 'LOCATION', 'Hernando', -40),
(19, 'LOCATION', 'Polk', -40),
(20, 'SELF_DELIVERY', 'Yes', 40),
(21, 'SELF_DELIVERY', 'No', 0),
(22, 'HAS_KEYS', 'Yes', 0),
(23, 'HAS_KEYS', 'No', -20);

INSERT INTO `QuoteRule` (`VehicleType_ID`,`HasTitle`,`Area_ID`,`QuoteAmountMin`,`QuoteAmountMax`) VALUES
(8,0,1,160.00,160.00),
(8,1,1,160.00,160.00),
(8,0,2,160.00,160.00),
(8,1,2,160.00,160.00),
(8,0,3,160.00,160.00),
(8,1,3,160.00,160.00),
(8,0,4,160.00,160.00),
(8,1,4,160.00,160.00),
(8,0,5,160.00,160.00),
(8,1,5,160.00,160.00);

INSERT INTO `QuoteRule` (`VehicleType_ID`,`HasTitle`,`Area_ID`,`QuoteAmountMin`,`QuoteAmountMax`) VALUES
(9,0,1,240.00,240.00),
(9,1,1,240.00,240.00),
(9,0,2,240.00,240.00),
(9,1,2,240.00,240.00),
(9,0,3,240.00,240.00),
(9,1,3,240.00,240.00),
(9,0,4,240.00,240.00),
(9,1,4,240.00,240.00),
(9,0,5,240.00,240.00),
(9,1,5,240.00,240.00);