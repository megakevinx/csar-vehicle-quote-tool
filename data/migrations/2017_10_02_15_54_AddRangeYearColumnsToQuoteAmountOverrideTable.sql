ALTER TABLE `QuoteAmountOverride`
ADD COLUMN `FromVehicleYear` VARCHAR(4) NOT NULL;

ALTER TABLE `QuoteAmountOverride` 
ADD COLUMN `ToVehicleYear` VARCHAR(4) NOT NULL;

UPDATE `QuoteAmountOverride` 
SET `FromVehicleYear` = `VehicleYear`, `ToVehicleYear` = `VehicleYear`
WHERE `VehicleYearComparer` = 'equals'

UPDATE `QuoteAmountOverride`
SET `FromVehicleYear` = `VehicleYear`, `ToVehicleYear` = 'Any'
WHERE `VehicleYearComparer` = 'newer'

UPDATE `QuoteAmountOverride`
SET `FromVehicleYear` = 'Any', `ToVehicleYear` = `VehicleYear`
WHERE `VehicleYearComparer` = 'older'

ALTER TABLE `QuoteAmountOverride` 
DROP COLUMN `VehicleYearComparer`,
DROP COLUMN `VehicleYear`,
DROP INDEX `UK_QuoteAmountOverride`;

ALTER TABLE `QuoteAmountOverride`
ADD UNIQUE INDEX `UK_QuoteAmountOverride` (`FromVehicleYear` ASC, `ToVehicleYear` ASC, `VehicleMake` ASC, `VehicleModel` ASC);

