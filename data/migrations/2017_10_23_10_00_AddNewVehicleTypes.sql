UPDATE `VehicleType` SET `Description` = 'Large Car' WHERE `VehicleType_ID` = 3;
UPDATE `VehicleType` SET `Description` = 'Large Truck' WHERE `VehicleType_ID` = 4;
UPDATE `VehicleType` SET `Description` = 'Midsize Minivan' WHERE `VehicleType_ID` = 5;
UPDATE `VehicleType` SET `Description` = 'Compact SUV' WHERE `VehicleType_ID` = 6;
UPDATE `VehicleType` SET `Description` = 'Large SUV' WHERE `VehicleType_ID`= 7;
UPDATE `VehicleType` SET `Description` = 'Compact Truck' WHERE `VehicleType_ID` = 9;

INSERT INTO `VehicleType` (`VehicleType_ID`, `Description`) VALUES (10, 'Midsize Truck');
INSERT INTO `VehicleType` (`VehicleType_ID`, `Description`) VALUES (11, 'Compact Van');
INSERT INTO `VehicleType` (`VehicleType_ID`, `Description`) VALUES (12, 'Midsize Van');
INSERT INTO `VehicleType` (`VehicleType_ID`, `Description`) VALUES (13, 'Large Van');
INSERT INTO `VehicleType` (`VehicleType_ID`, `Description`) VALUES (14, 'Compact Minivan');
INSERT INTO `VehicleType` (`VehicleType_ID`, `Description`) VALUES (15, 'Large Minivan');

ALTER TABLE `VehicleType` ADD COLUMN `Sequence` INT NOT NULL;

UPDATE `VehicleType` SET `Sequence`='1' WHERE `VehicleType_ID`='1';
UPDATE `VehicleType` SET `Sequence`='2' WHERE `VehicleType_ID`='2';
UPDATE `VehicleType` SET `Sequence`='3' WHERE `VehicleType_ID`='3';
UPDATE `VehicleType` SET `Sequence`='4' WHERE `VehicleType_ID`='9';
UPDATE `VehicleType` SET `Sequence`='5' WHERE `VehicleType_ID`='10';
UPDATE `VehicleType` SET `Sequence`='6' WHERE `VehicleType_ID`='4';
UPDATE `VehicleType` SET `Sequence`='7' WHERE `VehicleType_ID`='6';
UPDATE `VehicleType` SET `Sequence`='8' WHERE `VehicleType_ID`='8';
UPDATE `VehicleType` SET `Sequence`='9' WHERE `VehicleType_ID`='7';
UPDATE `VehicleType` SET `Sequence`='10' WHERE `VehicleType_ID`='11';
UPDATE `VehicleType` SET `Sequence`='11' WHERE `VehicleType_ID`='12';
UPDATE `VehicleType` SET `Sequence`='12' WHERE `VehicleType_ID`='13';
UPDATE `VehicleType` SET `Sequence`='13' WHERE `VehicleType_ID`='14';
UPDATE `VehicleType` SET `Sequence`='14' WHERE `VehicleType_ID`='5';
UPDATE `VehicleType` SET `Sequence`='15' WHERE `VehicleType_ID`='15';


UPDATE `QuoteAmount` SET `FeatureValue`='Large Truck' WHERE `QuoteAmount_ID`='5';
UPDATE `QuoteAmount` SET `FeatureValue`='Midsize Minivan' WHERE `QuoteAmount_ID`='6';
UPDATE `QuoteAmount` SET `FeatureValue`='Large Car' WHERE `QuoteAmount_ID`='3';
UPDATE `QuoteAmount` SET `FeatureValue`='Large SUV' WHERE `QuoteAmount_ID`='9';
UPDATE `QuoteAmount` SET `FeatureValue`='Compact Truck' WHERE `QuoteAmount_ID`='4';
UPDATE `QuoteAmount` SET `FeatureValue`='Compact SUV' WHERE `QuoteAmount_ID`='7';


INSERT INTO `QuoteAmount` (`QuoteAmount_ID`,`FeatureType`,`FeatureValue`,`PriceModifier`)
VALUES (24, 'TYPE', 'Midsize Truck', 195);

INSERT INTO `QuoteAmount` (`QuoteAmount_ID`,`FeatureType`,`FeatureValue`,`PriceModifier`)
VALUES (25, 'TYPE', 'Compact Van', 170);
INSERT INTO `QuoteAmount` (`QuoteAmount_ID`,`FeatureType`,`FeatureValue`,`PriceModifier`)
VALUES (26, 'TYPE', 'Midsize Van', 180);
INSERT INTO `QuoteAmount` (`QuoteAmount_ID`,`FeatureType`,`FeatureValue`,`PriceModifier`)
VALUES (27, 'TYPE', 'Large Van', 190);

INSERT INTO `QuoteAmount` (`QuoteAmount_ID`,`FeatureType`,`FeatureValue`,`PriceModifier`)
VALUES (28, 'TYPE', 'Compact Minivan', 170);
INSERT INTO `QuoteAmount` (`QuoteAmount_ID`,`FeatureType`,`FeatureValue`,`PriceModifier`)
VALUES (29, 'TYPE', 'Large Minivan', 190);
