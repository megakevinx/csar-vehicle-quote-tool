ALTER TABLE `User` 
ADD COLUMN `PasswordResetToken` VARCHAR(40) NULL,
ADD COLUMN `PasswordResetTokenCreationDate` DATETIME NULL;
