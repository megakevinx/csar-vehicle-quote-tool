/* Run CarQuery's script before this. It creates a new tbl_02_models table with the data */

/* Create and populate our own table */
CREATE TABLE VehicleModelSpec
(
    VehicleMake_ID VARCHAR(32),
    VehicleModel_ID VARCHAR(64),
    Year VARCHAR(4),
    BodyStyle VARCHAR(64),
    WeightInKg INT,
    LengthInMm INT,
    WidthInMm INT,
    HeightInMm INT
)
AS
SELECT
    model_make_id as VehicleMake_ID,
    model_name as VehicleModel_ID,
    model_year as Year,
    model_body as BodyStyle,
    model_weight_kg as WeightInKg,
    model_length_mm as LengthInMm,
    model_width_mm as WidthInMm,
    model_height_mm as HeightInMm
FROM
    tbl_02_models;

/* Add indexes */
ALTER TABLE `VehicleModelSpec` 
ADD INDEX `IX_VehicleMake_ID` (`VehicleMake_ID` ASC),
ADD INDEX `IX_VehicleModel_ID` (`VehicleModel_ID` ASC),
ADD INDEX `IX_Year` (`Year` ASC);

/* Normalize data so that it plays nicely with our previous data */

UPDATE VehicleModelSpec SET VehicleMake_ID = 'alfa romeo' WHERE VehicleMake_ID = 'alfa-romeo';
UPDATE VehicleModelSpec SET VehicleMake_ID = 'alfa romeo' WHERE VehicleMake_ID = 'Alfa Romeo';
UPDATE VehicleModelSpec SET VehicleMake_ID = 'aston martin' WHERE VehicleMake_ID = 'aston-martin';
UPDATE VehicleModelSpec SET VehicleMake_ID = 'aston martin' WHERE VehicleMake_ID = 'Aston Martin';
UPDATE VehicleModelSpec SET VehicleMake_ID = 'land rover' WHERE VehicleMake_ID = 'land-rover';
UPDATE VehicleModelSpec SET VehicleMake_ID = 'land rover' WHERE VehicleMake_ID = 'Land Rover';

UPDATE VehicleModelSpec SET VehicleModel_ID = LOWER(VehicleModel_ID);
UPDATE VehicleModelSpec SET VehicleMake_ID = LOWER(VehicleMake_ID);
UPDATE VehicleModelSpec SET VehicleModel_ID = 'rs 3' WHERE VehicleMake_ID = 'audi' AND VehicleModel_ID = 'rs3';
UPDATE VehicleModelSpec SET VehicleModel_ID = 'rs 4' WHERE VehicleMake_ID = 'audi' AND VehicleModel_ID = 'rs4';
UPDATE VehicleModelSpec SET VehicleModel_ID = 'rs 5' WHERE VehicleMake_ID = 'audi' AND VehicleModel_ID = 'rs5';
UPDATE VehicleModelSpec SET VehicleModel_ID = 'rs 6' WHERE VehicleMake_ID = 'audi' AND VehicleModel_ID = 'rs6';
UPDATE VehicleModelSpec SET VehicleModel_ID = '200sx' WHERE VehicleMake_ID = 'nissan' AND VehicleModel_ID = '200 sx silvia';
UPDATE VehicleModelSpec SET VehicleModel_ID = 'town and country' WHERE VehicleMake_ID = 'chrysler' AND VehicleModel_ID = 'town & country';
