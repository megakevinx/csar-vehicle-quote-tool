define('app/apiclients/AnalyticsApiClient',
    ['jquery'],
    function($) {
        function AnalyticsApiClient() {

            function reportGetQuoteClicked() {
                gtag('event', 'conversion', {
                    'send_to': 'AW-811722817/d-8ECMu-o38QwdCHgwM',
                    'event_callback': function() {
                        console.log("[Get Quote] Conversion Reported");
                    }
                });
            }

            function reportIWantToSellClicked() {
                gtag('event', 'conversion', {
                    'send_to': 'AW-811722817/lczHCLSXm38QwdCHgwM',
                    'transaction_id': "",
                    'event_callback': function() {
                        console.log("[I Want To Sell] Conversion Reported");
                    }
                });
            }

            return {
                reportGetQuoteClicked: reportGetQuoteClicked,
                reportIWantToSellClicked: reportIWantToSellClicked
            };
        }

        return AnalyticsApiClient;
    }
);

