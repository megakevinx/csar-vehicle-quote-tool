define('app/apiclients/ConfigurationApiClient',
    ['jquery'],
    function($) {
        function ConfigurationApiClient() {

            function getConfigurationData(params) {

                var onSuccess = params.onSuccess;
                var onFail = params.onFail;

                var settings = {
                    "async": true,
                    "url": "api/configuration",
                    "method": "GET"
                };

                console.log("GET request sent to: " + settings.url);

                $.ajax(settings).done(onSuccess).fail(onFail);
            }

            function submitConfigurationData(params) {

                var appSettings = params.appSettings;
                var onSuccess = params.onSuccess;
                var onFail = params.onFail;

                var settings = {
                    "async": true,
                    "url": "api/configuration",
                    "method": "POST",
                    "headers": {
                        "content-type": "application/json"
                    },
                    "data": JSON.stringify(appSettings)
                };

                console.log("POST request sent to: " + settings.url);

                $.ajax(settings).done(onSuccess).fail(onFail);
            }

            function submitQuoteCalculationRules(params) {

                var quoteCalculationRules = params.quoteCalculationRules;
                var onSuccess = params.onSuccess;
                var onFail = params.onFail;

                var settings = {
                    "async": true,
                    "url": "api/quote-calculation-rules",
                    "method": "POST",
                    "headers": {
                        "content-type": "application/json"
                    },
                    "data": JSON.stringify(quoteCalculationRules)
                };

                console.log("POST request sent to: " + settings.url);

                $.ajax(settings).done(onSuccess).fail(onFail);
            }

            function deleteQuoteOverride(params) {

                var quoteOverrideId = params.quoteOverrideId;
                var onSuccess = params.onSuccess;
                var onFail = params.onFail;

                var settings = {
                    "async": true,
                    "url": "api/quote-override/" + quoteOverrideId,
                    "method": "DELETE"
                };

                console.log("DELETE request sent to: " + settings.url);

                $.ajax(settings).done(onSuccess).fail(onFail);
            }

            function createQuoteOverride(params) {

                var quoteOverrideData = params.quoteOverrideData;
                var onSuccess = params.onSuccess;
                var onFail = params.onFail;

                var settings = {
                    "async": true,
                    "url": "api/quote-override",
                    "method": "POST",
                    "headers": {
                        "content-type": "application/json"
                    },
                    "data": JSON.stringify(quoteOverrideData)
                };

                console.log("POST request sent to: " + settings.url);

                $.ajax(settings).done(onSuccess).fail(onFail);
            }

            function changeBasePrice(params) {

                var basePriceChange = params.basePriceChange;
                var onSuccess = params.onSuccess;
                var onFail = params.onFail;

                var settings = {
                    "async": true,
                    "url": "api/quote-calculation-rules/change",
                    "method": "POST",
                    "headers": {
                        "content-type": "application/json"
                    },
                    "data": JSON.stringify({ basePriceChange: basePriceChange })
                };

                console.log("POST request sent to: " + settings.url);

                $.ajax(settings).done(onSuccess).fail(onFail);
            }

            return {
                getConfigurationData: getConfigurationData,
                submitConfigurationData: submitConfigurationData,
                submitQuoteCalculationRules: submitQuoteCalculationRules,
                deleteQuoteOverride: deleteQuoteOverride,
                createQuoteOverride: createQuoteOverride,
                changeBasePrice: changeBasePrice
            };
        }

        return ConfigurationApiClient;
    }
);

