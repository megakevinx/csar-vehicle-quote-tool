define('app/apiclients/FaqApiClient',
['jquery'],
function($) {
    function FaqApiClient() {

        function updateFaq(params) {

            var faqData = params.faqData;
            var onSuccess = params.onSuccess;
            var onFail = params.onFail;

            var settings = {
                "async": true,
                "url": "api/faq/" + faqData.id,
                "method": "PUT",
                "headers": {
                    "content-type": "application/json"
                },
                "data": JSON.stringify(faqData)
            };

            console.log("PUT request sent to: " + settings.url);

            $.ajax(settings).done(onSuccess).fail(onFail);
        }

        function deleteFaq(params) {

            var faqData = params.faqData;
            var onSuccess = params.onSuccess;
            var onFail = params.onFail;

            var settings = {
                "async": true,
                "url": "api/faq/" + faqData.id,
                "method": "DELETE",
                "headers": {
                    "content-type": "application/json"
                }
            };

            console.log("DELETE request sent to: " + settings.url);

            $.ajax(settings).done(onSuccess).fail(onFail);
        }

        function createFaq(params) {

            var faqData = params.faqData;
            var onSuccess = params.onSuccess;
            var onFail = params.onFail;

            var settings = {
                "async": true,
                "url": "api/faq",
                "method": "POST",
                "headers": {
                    "content-type": "application/json"
                },
                "data": JSON.stringify(faqData)
            };

            console.log("POST request sent to: " + settings.url);

            $.ajax(settings).done(onSuccess).fail(onFail);
        }

        return {
            updateFaq: updateFaq,
            deleteFaq: deleteFaq,
            createFaq: createFaq
        };
    }

    return FaqApiClient;
}
);
