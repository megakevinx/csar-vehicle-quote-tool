define('app/apiclients/LocationApiClient',
    ['jquery'],
    function($) {
        function LocationApiClient() {

            function isZipCodeValid(params) {

                var zipCode = params.zipCode;
                var onSuccess = params.onSuccess;
                var onFail = params.onFail;

                var settings = {
                    "async": true,
                    "url": "api/zip-validation/" + zipCode,
                    "method": "GET"
                };

                console.log("GET request sent to: " + settings.url);

                $.ajax(settings).done(onSuccess).fail(onFail);
            }

            return {
                isZipCodeValid: isZipCodeValid
            };
        }

        return LocationApiClient;
    }
);

