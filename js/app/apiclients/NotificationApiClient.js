define('app/apiclients/NotificationApiClient',
    ['jquery'],
    function($) {
        function NotificationApiClient() {

            function notifyCallbackVendor(params) {

                var referenceNumber = params.referenceNumber;
                var onSuccess = params.onSuccess;
                var onFail = params.onFail;

                var settings = {
                    "async": true,
                    "url": "api/notification",
                    "method": "POST",
                    "headers": {
                        "content-type": "application/json"
                    },
                    "data": JSON.stringify({
                        referenceNumber: referenceNumber
                    })
                };

                console.log("POST request sent to: " + settings.url);

                $.ajax(settings).done(onSuccess).fail(onFail);
            }

            function notifyCallbackCustomer(params) {

                var referenceNumber = params.referenceNumber;
                var onSuccess = params.onSuccess;
                var onFail = params.onFail;

                var settings = {
                    "async": true,
                    "url": "api/notification/callback-customer",
                    "method": "POST",
                    "headers": {
                        "content-type": "application/json"
                    },
                    "data": JSON.stringify({
                        referenceNumber: referenceNumber
                    })
                };

                console.log("POST request sent to: " + settings.url);

                $.ajax(settings).done(onSuccess).fail(onFail);
            }

            function notifyStillShoppingAround(params) {

                var referenceNumber = params.referenceNumber;
                var onSuccess = params.onSuccess;
                var onFail = params.onFail;

                var settings = {
                    "async": true,
                    "url": "api/notification/still-shopping",
                    "method": "POST",
                    "headers": {
                        "content-type": "application/json"
                    },
                    "data": JSON.stringify({
                        referenceNumber: referenceNumber
                    })
                };

                console.log("POST request sent to: " + settings.url);

                $.ajax(settings).done(onSuccess).fail(onFail);
            }

            function notifyNotReadyToSell(params) {

                var referenceNumber = params.referenceNumber;
                var onSuccess = params.onSuccess;
                var onFail = params.onFail;

                var settings = {
                    "async": true,
                    "url": "api/notification/not-ready",
                    "method": "POST",
                    "headers": {
                        "content-type": "application/json"
                    },
                    "data": JSON.stringify({
                        referenceNumber: referenceNumber
                    })
                };

                console.log("POST request sent to: " + settings.url);

                $.ajax(settings).done(onSuccess).fail(onFail);
            }

            return {
                notifyCallbackVendor: notifyCallbackVendor,
                notifyCallbackCustomer: notifyCallbackCustomer,
                notifyStillShoppingAround: notifyStillShoppingAround,
                notifyNotReadyToSell: notifyNotReadyToSell,
            };
        }

        return NotificationApiClient;
    }
);

