define('app/apiclients/QuoteApiClient',
    ['jquery'],
    function($) {
        function QuoteApiClient() {

            function getQuote(params) {

                var vehicleData = params.vehicleData;
                var onSuccess = params.onSuccess;
                var onFail = params.onFail;

                var settings = {
                    "async": true,
                    "url": "api/quote",
                    "method": "POST",
                    "headers": {
                        "content-type": "application/json"
                    },
                    "data": JSON.stringify(vehicleData)
                    //"data": $.param(vehicleData)
                };

                console.log("POST request sent to: " + settings.url);

                $.ajax(settings).done(onSuccess).fail(onFail);
            }

            function getCustomerQuote(params) {

                var quoteData = params.quoteData;
                var onSuccess = params.onSuccess;
                var onFail = params.onFail;

                var settings = {
                    "async": true,
                    "url": "api/quote/customer",
                    "method": "POST",
                    "headers": {
                        "content-type": "application/json"
                    },
                    "data": JSON.stringify(quoteData)
                    //"data": $.param(vehicleData)
                };

                console.log("POST request sent to: " + settings.url);

                $.ajax(settings).done(onSuccess).fail(onFail);
            }

            function calculateQuote(params) {

                var vehicleData = params.vehicleData;
                var onSuccess = params.onSuccess;
                var onFail = params.onFail;

                var settings = {
                    "async": true,
                    "url": "api/quote/calculate",
                    "method": "GET",
                    "headers": {
                        "content-type": "application/json"
                    },
                    //"data": JSON.stringify(vehicleData)
                    "data": $.param(vehicleData)
                };

                console.log("POST request sent to: " + settings.url);

                $.ajax(settings).done(onSuccess).fail(onFail);
            };

            function notifyCallbackVendor(params) {

                var referenceNumber = params.referenceNumber;
                var onSuccess = params.onSuccess;
                var onFail = params.onFail;

                var settings = {
                    "async": true,
                    "url": "api/notification",
                    "method": "POST",
                    "headers": {
                        "content-type": "application/json"
                    },
                    "data": JSON.stringify({
                        referenceNumber: referenceNumber
                    })
                };

                console.log("POST request sent to: " + settings.url);

                $.ajax(settings).done(onSuccess).fail(onFail);
            }

            function submitPickupInfo(params) {

                var pickupInfo = params.pickupInfo;
                var onSuccess = params.onSuccess;
                var onFail = params.onFail;

                var settings = {
                    "async": true,
                    "url": "api/quote/" + pickupInfo.referenceNumber,
                    "method": "PUT",
                    "headers": {
                        "content-type": "application/json"
                    },
                    "data": JSON.stringify(pickupInfo)
                    //"data": $.param(vehicleData)
                };

                console.log("PUT request sent to: " + settings.url);

                $.ajax(settings).done(onSuccess).fail(onFail);
            }

            function submitLocationInfo(params) {

                var locationInfo = params.locationInfo;
                var onSuccess = params.onSuccess;
                var onFail = params.onFail;

                var settings = {
                    "async": true,
                    "url": "api/quote/location/" + locationInfo.referenceNumber,
                    "method": "PUT",
                    "headers": {
                        "content-type": "application/json"
                    },
                    "data": JSON.stringify(locationInfo)
                    //"data": $.param(vehicleData)
                };

                console.log("PUT request sent to: " + settings.url);

                $.ajax(settings).done(onSuccess).fail(onFail);
            }

            return {
                getQuote: getQuote,
                getCustomerQuote: getCustomerQuote,
                calculateQuote: calculateQuote,
                submitPickupInfo: submitPickupInfo,
                submitLocationInfo: submitLocationInfo,
                notifyCallbackVendor: notifyCallbackVendor
            };
        }

        return QuoteApiClient;
    }
);

