define('app/apiclients/VehicleMakeApiClient',
    ['jquery'],
    function($) {
        function VehicleMakeApiClient() {

            function getVehicleMakes(params) {

                var onSuccess = params.onSuccess;
                var onFail = params.onFail;

                var settings = {
                    "async": true,
                    "url": "api/vehicle/makes",
                    "method": "GET",
                    "headers": {
                        "content-type": "application/json"
                    },
                    // "data": $.param(vehicleData)
                };

                console.log("GET request sent to: " + settings.url);

                $.ajax(settings).done(onSuccess).fail(onFail);
            }

            function getVehicleModels(params) {

                var vehicleMake = params.vehicleMake;
                var vehicleYear = params.vehicleYear;
                var onSuccess = params.onSuccess;
                var onFail = params.onFail;

                var settings = {
                    "async": true,
                    "url": "api/vehicle/models",
                    "method": "GET",
                    "headers": {
                        "content-type": "application/json"
                    },
                    "data": $.param({
                        "make": vehicleMake,
                        "year": vehicleYear
                    })
                };

                console.log("GET request sent to: " + settings.url);

                $.ajax(settings).done(onSuccess).fail(onFail);
            }

            function getVehicleModelsForAllYears(params) {

                var vehicleMake = params.vehicleMake;
                var onSuccess = params.onSuccess;
                var onFail = params.onFail;

                var settings = {
                    "async": true,
                    "url": "api/vehicle/models",
                    "method": "GET",
                    "headers": {
                        "content-type": "application/json"
                    },
                    "data": $.param({
                        "make": vehicleMake
                    })
                };

                console.log("GET request sent to: " + settings.url);

                $.ajax(settings).done(onSuccess).fail(onFail);
            }

            function getVehicleStyleInfo(params) {

                var vehicleYear = params.vehicleYear;
                var vehicleMake = params.vehicleMake;
                var vehicleModel = params.vehicleModel;
                var onSuccess = params.onSuccess;
                var onFail = params.onFail;

                var settings = {
                    "async": true,
                    "url": "api/vehicle/type",
                    "method": "GET",
                    "headers": {
                        "content-type": "application/json"
                    },
                    "data": $.param({
                        "year": vehicleYear,
                        "make": vehicleMake,
                        "model": vehicleModel
                    })
                };

                console.log("GET request sent to: " + settings.url);

                $.ajax(settings).done(onSuccess).fail(onFail);
            }

            return {
                getVehicleMakes: getVehicleMakes,
                getVehicleModels: getVehicleModels,
                getVehicleModelsForAllYears: getVehicleModelsForAllYears,
                getVehicleStyleInfo: getVehicleStyleInfo
            };
        }

        return VehicleMakeApiClient;
    }
);

