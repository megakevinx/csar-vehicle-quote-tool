define('app/validators/AppSettingsValidator',
['jquery', 'jquery.validate', 'additional-methods'],
function($, $validate, $validateAdditionalMethods) {

    function AppSettingsValidator() {

        var self = this;

        var formSelector = "form#app_settings_form";
        var errorMessagesContainer = 'div#app_settings_validation_error_container';

        function trimNormalizer(value) {
            return $.trim(value);
        }

        var validationOptions = {
            rules: {
                vehicle_drop_off_address: {
                    required: true,
                    normalizer: trimNormalizer
                },
                contact_phone_number: {
                    phoneUS: true,
                    required: true
                }
            },

            messages: {
                vehicle_drop_off_address: {
                    required: "- Please specify the Vehicle Drop Off Address"
                },
                contact_phone_number: {
                    phoneUS: "- Please make sure that the Contact Phone Number is valid",
                    required: "- Please specify the Contact Phone Number"
                }
            },

            errorElement : 'div',
            errorLabelContainer: errorMessagesContainer
        };

        function formIsValid() {
            var form = $(formSelector);
            form.validate(validationOptions);
            var isValid = form.valid();

            return isValid;
        }

        return {
            formIsValid: formIsValid
        };
    }

    return AppSettingsValidator;
}
);
