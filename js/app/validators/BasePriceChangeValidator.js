define('app/validators/BasePriceChangeValidator',
['jquery', 'jquery.validate', 'additional-methods'],
function($, $validate, $validateAdditionalMethods) {

    function BasePriceChangeValidator() {

        var self = this;

        var formSelector = "form#base_price_change_form";
        var errorMessagesContainer = 'div#base_price_change_validation_error_container';

        var validationOptions = {
            rules: {
                base_price_change: {
                    required: true,
                    max: 100, // field in db allows only three digits left of dot
                    min: -100
                }
            },

            messages: {
                base_price_change: {
                    required: "- Please specify the amount",
                    currency: "- Please make sure that the amount is a valid number",
                    max: "- The amount must be below 100",
                    min: "- The amount must be above -100"
                }
            },

            errorElement : 'div',
            errorLabelContainer: errorMessagesContainer
        };

        function formIsValid() {

            var form = $(formSelector);
            form.validate(validationOptions);
            var isValid = form.valid();

            return isValid;
        }

        return {
            formIsValid: formIsValid
        };
    }

    return BasePriceChangeValidator;
});
