define('app/validators/BasicVehicleInfoValidator',
    ['jquery', 'jquery.validate'],
    function($, $validate) {

        function BasicVehicleInfoValidator() {

            var self = this;

            var formSelector = "form#basic_vehicle_info_form";
            var errorMessagesContainer = 'div#basic_vehicle_info_validation_error_container';

            var validationOptions = {
                rules: {
                    vehicle_year: {
                        required: true
                    },
                    vehicle_make: {
                        required: true
                    },
                    vehicle_model: {
                        required: true
                    },
                    vehicle_type: {
                        required: true
                    }
                },

                messages: {
                    vehicle_year: {
                        required: "- Please select the Vehicle year"
                    },
                    vehicle_make: {
                        required: "- Please select the vehicle make"
                    },
                    vehicle_model: {
                        required: "- Please select the vehicle model"
                    },
                    vehicle_type: {
                        required: "- Please select the vehicle type"
                    }
                },

                errorElement : 'div',
                errorLabelContainer: errorMessagesContainer
            };

            function formIsValid() {
                var form = $(formSelector);
                form.validate(validationOptions);
                var isValid = form.valid();

                return isValid;
            }

            return {
                formIsValid: formIsValid
            };
        }

        return BasicVehicleInfoValidator;
    }
);
