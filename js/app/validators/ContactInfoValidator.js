define('app/validators/ContactInfoValidator',
['jquery', 'jquery.validate', 'additional-methods'],
function($, $validate, $validateAdditionalMethods) {

    function ContactInfoValidator() {

        var self = this;

        var formSelector = "form#contact_info_form";
        var errorMessagesContainer = 'div#contact_info_validation_error_container';

        function trimNormalizer(value) {
            return $.trim(value);
        }

        var validationOptions = {
            rules: {
                seller_name: {
                    required: true,
                    normalizer: trimNormalizer
                },
                seller_phone_number: {
                    phoneUS: true
                },
                seller_email: {
                    required: {
                        depends: function(elem) {
                            return !($("[name=seller_phone_number]").val());
                        }
                    },
                    email: true
                }
            },

            messages: {
                seller_name: {
                    required: "- Please specify the seller's full name"
                },
                seller_phone_number: {
                    phoneUS: "- Please make sure that the seller's phone number is valid"
                },
                seller_email: {
                    required: "- Please specify either the seller's phone number or Email",
                    email: "- Please make sure that the seller's Email is valid"
                }
            },

            errorElement : 'div',
            errorLabelContainer: errorMessagesContainer
        };

        function formIsValid() {

            var form = $(formSelector);
            form.validate(validationOptions);
            var isValid = form.valid();

            return isValid;
        }

        return {
            formIsValid: formIsValid
        };
    }

    return ContactInfoValidator;
});
