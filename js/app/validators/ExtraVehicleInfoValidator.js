define('app/validators/ExtraVehicleInfoValidator',
    ['jquery', 'jquery.validate', 'additional-methods'],
    function($, $validate, $validateAdditionalMethods) {

        function ExtraVehicleInfoValidator() {

            var self = this;

            var formSelector = "form#extra_vehicle_info_form";
            var errorMessagesContainer = 'div#extra_vehicle_info_validation_error_container';

            var context = null;

            $.validator.addMethod(
                "zipcode",
                function(value, element) {
                    var re = /(^\d{5}$)/;
                    return this.optional(element) || re.test(value);
                },
                "The zipcode is not valid"
            );

            var validationOptions = {
                rules: {
                    vehicle_has_keys: {
                        required: true
                    },
                    vehicle_has_title: {
                        required: true
                    },
                    wheels_description: {
                        required: true
                    },
                    vehicle_will_be_delivered: {
                        required: true
                    },
                    vehicle_zip_code: {
                        required: true,
                        zipcode: true
                    }
                },

                messages: {
                    vehicle_has_keys: {
                        required: "- Please specify whether the vehicle has keys"
                    },
                    vehicle_has_title: {
                        required: "- Please specify whether the vehicle has title"
                    },
                    wheels_description: {
                        required: "- Please specify the description of the wheels of the vehicle"
                    },
                    vehicle_will_be_delivered: {
                        required: "- Please specify whether the vehicle will be delivered to us"
                    },
                    vehicle_zip_code: {
                        required: "- Please specify the zip code where the vehicle is located",
                        zipcode: "- Please make sure that the zip code is valid"
                    }
                },

                errorElement : 'div',
                errorLabelContainer: errorMessagesContainer
            };

            function configure(configParams) {
                context = configParams;
            }

            function formIsValid() {

                var form = $(formSelector);
                form.validate(validationOptions);
                var isValid = form.valid();

                return isValid;
            }

            return {
                formIsValid: formIsValid,
                configure: configure
            };
        }

        return ExtraVehicleInfoValidator;
    }
);
