define('app/validators/FaqValidator',
['jquery', 'jquery.validate', 'additional-methods'],
function($, $validate, $validateAdditionalMethods) {

    function FaqValidator() {

        var self = this;

        var formSelector = "form#create_faq_form";
        var errorMessagesContainer = 'div#faqs_validation_error_container';

        var validationOptions = {
            rules: {
                question: {
                    required: true
                },
                answer: {
                    required: true
                }
            },

            messages: {
                question: {
                    required: "- Please specify a question"
                },
                answer: {
                    required: "- Please specify an answer"
                }
            },

            errorElement : 'div',
            errorLabelContainer: errorMessagesContainer
        };

        function formIsValid() {

            var form = $(formSelector);
            form.validate(validationOptions);
            var isValid = form.valid();

            return isValid;
        }

        return {
            formIsValid: formIsValid
        };
    }

    return FaqValidator;
}
);
