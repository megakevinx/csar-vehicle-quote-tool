define('app/validators/LocationInfoValidator',
['jquery', 'jquery.validate', 'additional-methods'],
function($, $validate, $validateAdditionalMethods) {

    function LocationInfoValidator() {

        var self = this;

        var formSelector = "form#location_info_form";
        var errorMessagesContainer = 'div#location_info_validation_error_container';

        function trimNormalizer(value) {
            return $.trim(value);
        }

        var validationOptions = {
            rules: {
                vehicle_street_address: {
                    required: true,
                    normalizer: trimNormalizer
                },
                vehicle_city: {
                    required: true,
                    normalizer: trimNormalizer
                },
            },

            messages: {
                vehicle_street_address: {
                    required: "- Please specify the street address where the vehicle is located"
                },
                vehicle_city: {
                    required: "- Please specify the city where the vehicle is located"
                },
            },

            errorElement : 'div',
            errorLabelContainer: errorMessagesContainer
        };

        function configure(configParams) {
            context = configParams;
        }

        function formIsValid() {

            var form = $(formSelector);
            form.validate(validationOptions);
            var isValid = form.valid();

            return isValid;
        }

        return {
            formIsValid: formIsValid
        };
    }

    return LocationInfoValidator;
});
