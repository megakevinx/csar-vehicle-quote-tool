define('app/validators/PickupInfoValidator',
    ['jquery', 'jquery.validate', 'additional-methods'],
    function($, $validate, $validateAdditionalMethods) {

        function PickupInfoValidator() {

            var self = this;

            var formSelector = "form#pickup_info_form";
            var errorMessagesContainer = 'div#pickup_info_validation_error_container';

            function trimNormalizer(value) {
                return $.trim(value);
            }

            var validationOptions = {
                rules: {
                    vehicle_street_address: {
                        required: true,
                        normalizer: trimNormalizer
                    },
                    vehicle_city: {
                        required: true,
                        normalizer: trimNormalizer
                    },

                    seller_name: {
                        required: true,
                        normalizer: trimNormalizer
                    },
                    seller_phone_number: {
                        phoneUS: true
                    },
                    seller_email: {
                        required: {
                            depends: function(elem) {
                                return !($("[name=seller_phone_number]").val());
                            }
                        },
                        email: true
                    },
                    buyer_payout: {
                        required: true,
                        currency: ["$", false],
                        max: 999.99 // field in db allows only three digits left of dot
                    }
                },

                messages: {
                    vehicle_street_address: {
                        required: "- Please specify the street address where the vehicle is located"
                    },
                    vehicle_city: {
                        required: "- Please specify the city where the vehicle is located"
                    },

                    seller_name: {
                        required: "- Please specify the seller's full name"
                    },
                    seller_phone_number: {
                        phoneUS: "- Please make sure that the seller's phone number is valid"
                    },
                    seller_email: {
                        required: "- Please specify either the seller's phone number or Email",
                        email: "- Please make sure that the seller's Email is valid"
                    },

                    buyer_payout: {
                        required: "- Please specify the payout",
                        currency: "- Please make sure that the payout is a valid number",
                        max: "- The buyout price must be below 1000.00"
                    }
                },

                errorElement : 'div',
                errorLabelContainer: errorMessagesContainer
            };

            function formIsValid() {

                var form = $(formSelector);
                form.validate(validationOptions);
                var isValid = form.valid();

                return isValid;
            }

            return {
                formIsValid: formIsValid
            };
        }

        return PickupInfoValidator;
    }
);
