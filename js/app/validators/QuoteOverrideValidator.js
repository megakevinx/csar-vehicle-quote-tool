define('app/validators/QuoteOverrideValidator',
['jquery', 'jquery.validate', 'additional-methods'],
function($, $validate, $validateAdditionalMethods) {

    function QuoteOverrideValidator() {

        var self = this;

        var formSelector = "form#quote_override_form";
        var errorMessagesContainer = 'div#quote_override_validation_error_container';

        $.validator.addMethod('less_than_or_equal_to', function (value, element, param) {
            var paramElem = $("[name=" + param + "]");

            if ($(element).val() != "Any" && $(paramElem).val() != "Any") {
                return $(element).val() <= $(paramElem).val();
            }
            else {
                return true;
            }
        });

        var validationOptions = {
            rules: {
                from_vehicle_year: {
                    required: true,
                    less_than_or_equal_to: 'to_vehicle_year'
                },
                to_vehicle_year: {
                    required: true
                },
                vehicle_make: {
                    required: true
                },
                vehicle_model: {
                    required: true
                },
                override_quote: {
                    required: true,
                    currency: ["$", false],
                    max: 999.99, // field in db allows only three digits left of dot
                    min: 0
                }
            },

            messages: {
                from_vehicle_year: {
                    required: "- Please select the Vehicle Year lower limit",
                    less_than_or_equal_to: "- The Vehicle Year lower limit must be less than or equal to the upper limit."
                },
                to_vehicle_year: {
                    required: "- Please select the Vehicle Year upper limit"
                },
                vehicle_make: {
                    required: "- Please select the Vehicle Make"
                },
                vehicle_model: {
                    required: "- Please select the Vehicle Model"
                },
                override_quote: {
                    required: "- Please specify the Quote",
                    currency: "- Please make sure that the Quote is a valid money amount",
                    max: "- The Quote must be below 1000.00",
                    min: "- The Quote can't be lower than 0"
                }
            },

            errorElement : 'div',
            errorLabelContainer: errorMessagesContainer
        };

        function formIsValid() {

            var form = $(formSelector);
            form.validate(validationOptions);
            var isValid = form.valid();

            return isValid;
        }

        return {
            formIsValid: formIsValid
        };
    }

    return QuoteOverrideValidator;
}
);
