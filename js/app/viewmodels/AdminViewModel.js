define(
    [
        'knockout', 'jquery', 'lodash', 'bootstrap',
        'app/alerts/AlertsManager', 'app/apiclients/ConfigurationApiClient', 'app/apiclients/VehicleMakeApiClient', 'app/apiclients/FaqApiClient'
    ],

    function (ko, $, _, bootstrap,
        AlertsManager, ConfigurationApiClient, VehicleMakeApiClient, FaqApiClient) {

        function AdminViewModel(vehicleDropOffAddress, contactPhoneNumber, quoteCalculationRules, quoteOverrides, vehicleYears, vehicleMakes, faqs,
            alertsManager, configurationApiClient, vehicleMakeApiClient, faqApiClient) {

            var self = this;

            // Initial config data
            self.vehicleDropOffAddress = vehicleDropOffAddress;
            self.contactPhoneNumber = contactPhoneNumber;
            self.quoteCalculationRules = quoteCalculationRules;
            self.quoteOverrides = quoteOverrides;
            self.vehicleYears = vehicleYears;
            self.vehicleMakes = vehicleMakes;
            self.faqs = faqs;

            // Api Clients
            self.alertsManager = alertsManager;
            self.configurationApiClient = configurationApiClient;
            self.vehicleMakeApiClient = vehicleMakeApiClient;
            self.faqApiClient = faqApiClient;
        }

        $(document).ready(function() {

            ko.components.register('app-settings', {
                viewModel: { require: 'app/viewmodels/components/AppSettingsComponent' },
                template: { require: 'text!app/viewmodels/templates/app-settings-template.html' }
            });

            ko.components.register('quote-calculation-rules', {
                viewModel: { require: 'app/viewmodels/components/QuoteCalculationRulesComponent' },
                template: { require: 'text!app/viewmodels/templates/quote-calculation-rules-template.html' }
            });

            ko.components.register('quote-overrides', {
                viewModel: { require: 'app/viewmodels/components/QuoteOverridesComponent' },
                template: { require: 'text!app/viewmodels/templates/quote-overrides-template.html' }
            });

            ko.components.register('faqs-manager', {
                viewModel: { require: 'app/viewmodels/components/FaqsManagerComponent' },
                template: { require: 'text!app/viewmodels/templates/faqs-manager-template.html' }
            });

            $("div.validation-error-alert a.close").click(function(eventObject) {
                $(eventObject.target.parentElement).hide();
            });

            var alertsManager = new AlertsManager();
            var configurationApiClient = new ConfigurationApiClient();
            var vehicleMakeApiClient = new VehicleMakeApiClient();
            var faqApiClient = new FaqApiClient();

            configurationApiClient.getConfigurationData({
                onSuccess: function (response, textStatus, jqXHR) {

                    var vehicleDropOffAddress = response.vehicleDropOffAddress;
                    var contactPhoneNumber = response.contactPhoneNumber;
                    var quoteCalculationRules = response.quoteCalculationRules;
                    var quoteOverrides = response.quoteOverrides;
                    var vehicleYears = response.vehicleYears;
                    var faqs = response.faqs;

                    vehicleMakeApiClient.getVehicleMakes({
                        onSuccess: function (response, textStatus, jqXHR) {

                            var vehicleMakes = _.map(response.makes, function(value, index) {
                                return { id: value.name.toLowerCase(), name: value.name };
                            });

                            var viewModel = new AdminViewModel(
                                vehicleDropOffAddress, contactPhoneNumber, quoteCalculationRules, quoteOverrides, vehicleYears, vehicleMakes, faqs,
                                alertsManager, configurationApiClient, vehicleMakeApiClient, faqApiClient
                            );

                            ko.applyBindings(viewModel);

                            $("#overlay").fadeOut();
                        },
                        onFail: alertsManager.onAjaxRequestFail
                    });
                },
                onFail: alertsManager.onAjaxRequestFail
            });

         });
    }
);