define(
    [
        'knockout', 'jquery', 'lodash', 'bootstrap',
        'app/alerts/AlertsManager',
        'app/apiclients/ConfigurationApiClient', 'app/apiclients/VehicleMakeApiClient', 'app/apiclients/QuoteApiClient', 'app/apiclients/LocationApiClient', 'app/apiclients/NotificationApiClient',
        'app/apiclients/AnalyticsApiClient',
        'app/validators/BasicVehicleInfoValidator', 'app/validators/ExtraVehicleInfoValidator', 'app/validators/ContactInfoValidator', 'app/validators/LocationInfoValidator',
        'app/wizard/StepNavigator'
    ],

    function (ko, $, _, bootstrap,
        AlertsManager,
        ConfigurationApiClient, VehicleMakeApiClient, QuoteApiClient, LocationApiClient, NotificationApiClient, AnalyticsApiClient,
        BasicVehicleInfoValidator, ExtraVehicleInfoValidator, ContactInfoValidator, LocationInfoValidator,
        StepNavigator) {

        function CustomerViewModel(vehicleYears, vehicleMakes, vehicleTypes, vehicleDropOffAddress, contactPhoneNumber,
            vehicleMakeApiClient, quoteApiClient, locationApiClient, notificationApiClient, analyticsApiClient, alertsManager,
            basicVehicleInfoValidator, extraVehicleInfoValidator, contactInfoValidator, locationInfoValidator) {

            var self = this;

            self.alertsManager = alertsManager;

            self.vehicleMakeApiClient = vehicleMakeApiClient;
            self.locationApiClient = locationApiClient;
            self.quoteApiClient = quoteApiClient;
            self.analyticsApiClient = analyticsApiClient;

            self.vehicleYears = vehicleYears;
            self.vehicleMakes = vehicleMakes;
            self.vehicleTypes = vehicleTypes;

            // Fields for basicVehicleInfo step
            self.selectedVehicleYear = ko.observable();
            self.selectedVehicleMake = ko.observable();
            self.selectedVehicleModel = ko.observable();
            self.selectedVehicleTypeId = ko.observable();

            // Fields for extraVehicleInfo step
            self.vehicleHasKeys = ko.observable();
            self.vehicleHasTitle = ko.observable();
            self.selectedVehicleWheelsDescription = ko.observable();
            self.vehicleWillBeDelivered = ko.observable("no");
            self.vehicleZipCode = ko.observable();

            self.vehicleDropOffAddress = vehicleDropOffAddress;

            // Fields for ContactInfo step
            self.sellerName = ko.observable();
            self.sellerPhoneNumber = ko.observable();
            self.sellerEmail = ko.observable();

            // Fields for PickupInfo step
            self.vehicleStreetAddress = ko.observable();
            self.vehicleCity = ko.observable();

            // TODO: remove
            self.buyerPayout = ko.observable(0.00);

            // Fields for quoteCreated step
            self.calculatedQuote = ko.observable();
            self.referenceNumber = ko.observable();

            self.contactPhoneNumber = contactPhoneNumber;

            // Internal configuration
            self.isLoadingRequest = ko.observable(false);

            self.serverValidationErrorAlertContainerId = "#extra_vehicle_info_server_validation_error_container";
            self.serverValidationErrorAlertId = "extra_vehicle_info_server_validation_error_alert";
            self.zipCodeInvalidErrorMessage = "- We don't offer our services in the vehicle's location";

            // Wizard steps navigation
            var steps = {
                basicVehicleInfo: "BASIC_VEHICLE_INFO",
                extraVehicleInfo: "EXTRA_VEHICLE_INFO",
                contactInfo: "CONTACT_INFORMATION",

                quoteCreated: "QUOTE_CREATED",

                quoteAccepted: "QUOTE_ACCEPTED",
                willCallBack: "WILL_CALL_BACK",
                notReady: "NOT_READY",
                stillShopping: "STILL_SHOPPING"
            };

            // Validators configuration
            var validatorsByStep = [];
            validatorsByStep[steps.basicVehicleInfo] = basicVehicleInfoValidator;
            validatorsByStep[steps.extraVehicleInfo] = extraVehicleInfoValidator;
            validatorsByStep[steps.contactInfo] = contactInfoValidator;
            validatorsByStep[steps.quoteCreated] = locationInfoValidator;

            var stepNavigator = new StepNavigator({
                stepsSequence: [steps.basicVehicleInfo, steps.extraVehicleInfo, steps.contactInfo, steps.quoteCreated],
                stepsWithNavigationForward: [steps.basicVehicleInfo, steps.extraVehicleInfo],
                stepsWithNavigationBackward: [steps.extraVehicleInfo, steps.contactInfo],
                validatorsByStep: validatorsByStep,
                initialStep: steps.basicVehicleInfo,

                onStepAdvanceValidate: function (onSuccessCallback) {
                    if (stepNavigator.currentStep() === steps.extraVehicleInfo) {

                        self.isLoadingRequest(true);

                        locationApiClient.isZipCodeValid({
                            zipCode: self.vehicleZipCode(),
                            onSuccess: function (response, textStatus, jqXHR) {

                                self.isLoadingRequest(false);

                                if (response.isValid) {
                                    alertsManager.dismiss(self.serverValidationErrorAlertId);
                                    onSuccessCallback();
                                }
                                else {
                                    alertsManager.showDanger(
                                        $(self.serverValidationErrorAlertContainerId),
                                        self.zipCodeInvalidErrorMessage,
                                        self.serverValidationErrorAlertId
                                    );
                                }
                            },
                            onFail: function (jqXHR, textStatus, error) {
                                self.isLoadingRequest(false);
                                alertsManager.onAjaxRequestFail(jqXHR, textStatus, error);
                            }
                        });
                    }
                    else {
                        onSuccessCallback();
                    }
                }
            });

            self.shouldDisplayPreviousStepButton = stepNavigator.shouldDisplayPreviousStepButton;
            self.shouldDisplayNextStepButton = stepNavigator.shouldDisplayNextStepButton;
            self.shouldDisplayStepButtonsContainer = stepNavigator.shouldDisplayStepButtonsContainer;

            self.goToPreviousStepOnClick = stepNavigator.goToPreviousStepOnClick;
            self.goToNextStepOnClick = stepNavigator.goToNextStepOnClick;

            self.currentStep = ko.observable(steps.basicVehicleInfo);

            // Wizard step display/traversal logic
            self.shouldDisplayBasicVehicleInfoStep = ko.pureComputed(function() {
                return (stepNavigator.currentStep() === steps.basicVehicleInfo);
            });

            self.shouldDisplayExtraVehicleInfoStep = ko.pureComputed(function() {
                return (stepNavigator.currentStep() === steps.extraVehicleInfo);
            });

            self.shouldDisplayContactInfoStep = ko.pureComputed(function() {
                return (stepNavigator.currentStep() === steps.contactInfo);
            });

            self.shouldDisplayQuoteCreatedStep = ko.pureComputed(function() {
                return (stepNavigator.currentStep() === steps.quoteCreated);
            });

            self.shouldDisplayQuoteAcceptedStep = ko.pureComputed(function() {
                return (stepNavigator.currentStep() === steps.quoteAccepted);
            });

            self.shouldDisplayWillCallBackStep = ko.pureComputed(function() {
                return (stepNavigator.currentStep() === steps.willCallBack);
            });

            self.shouldDisplayNotReadyStep = ko.pureComputed(function() {
                return (stepNavigator.currentStep() === steps.notReady);
            });

            self.shouldDisplayStillShoppingStep = ko.pureComputed(function() {
                return (stepNavigator.currentStep() === steps.stillShopping);
            });

            self.getQuoteOnClick = function () {

                var validator = validatorsByStep[stepNavigator.currentStep()];

                if (!validator.formIsValid()) {
                    console.log("form invalid");
                    return;
                }

                self.analyticsApiClient.reportGetQuoteClicked();

                var quoteData = {
                    vehicleYear: self.selectedVehicleYear(),
                    vehicleMake: self.selectedVehicleMake(),
                    vehicleModel: self.selectedVehicleModel(),
                    vehicleTypeId: self.selectedVehicleTypeId(),

                    vehicleHasKeys: self.vehicleHasKeys(),
                    vehicleHasTitle: self.vehicleHasTitle(),
                    vehicleWheelsDescription: self.selectedVehicleWheelsDescription(),
                    vehicleWillBeDelivered: self.vehicleWillBeDelivered(),
                    vehicleZipCode: self.vehicleZipCode(),

                    sellerName: self.sellerName(),
                    sellerPhoneNumber: self.sellerPhoneNumber(),
                    sellerEmail: self.sellerEmail(),
                };

                self.isLoadingRequest(true);

                quoteApiClient.getCustomerQuote({

                    quoteData: quoteData,

                    onSuccess: function (response, textStatus, jqXHR) {

                        self.calculatedQuote(response.quote);
                        self.referenceNumber(response.referenceNumber);

                        stepNavigator.showStep(steps.quoteCreated);

                        console.log(response);

                        self.isLoadingRequest(false);
                    },
                    onFail: function (jqXHR, textStatus, error) {
                        if (jqXHR.status == 400) {
                            alertsManager.showDanger(
                                $(self.serverValidationErrorAlertContainerId),
                                self.zipCodeInvalidErrorMessage,
                                self.serverValidationErrorAlertId
                            );
                            console.log(jqXHR.responseJSON);
                        }
                        else {
                            alertsManager.onAjaxRequestFail(jqXHR, textStatus, error);
                        }

                        self.isLoadingRequest(false);
                    }
                });
            };

            self.submitLocationInfoOnClick = function () {

                var validator = validatorsByStep[stepNavigator.currentStep()];

                if (!validator.formIsValid()) {
                    console.log("form invalid");
                    return;
                }

                self.analyticsApiClient.reportIWantToSellClicked();

                var locationInfo = {
                    referenceNumber: self.referenceNumber(),

                    vehicleStreetAddress: self.vehicleStreetAddress(),
                    vehicleCity: self.vehicleCity(),
                };

                self.isLoadingRequest(true);

                quoteApiClient.submitLocationInfo({

                    locationInfo: locationInfo,

                    onSuccess: function (response, textStatus, jqXHR) {
                        stepNavigator.showStep(steps.quoteAccepted);
                        console.log(response);
                        self.isLoadingRequest(false);
                    },
                    onFail: function (jqXHR, textStatus, error) {
                        alertsManager.onAjaxRequestFail(jqXHR, textStatus, error);
                        self.isLoadingRequest(false);
                    }
                });
            };

            function createNotification(notifyCallback, stepToShow) {
                self.isLoadingRequest(true);

                notifyCallback({
                    referenceNumber: self.referenceNumber(),

                    onSuccess: function (response, textStatus, jqXHR) {
                        console.log(response);
                        stepNavigator.showStep(stepToShow);
                        self.isLoadingRequest(false);
                    },
                    onFail: function (jqXHR, textStatus, error) {
                        alertsManager.onAjaxRequestFail(jqXHR, textStatus, error);
                        self.isLoadingRequest(false);
                    }
                });
            }

            self.callMeBackOnClick = function () {
                createNotification(notificationApiClient.notifyCallbackCustomer, steps.willCallBack);
            };

            self.shoppingAroundOnClick = function() {
                createNotification(notificationApiClient.notifyStillShoppingAround, steps.stillShopping);
            };

            self.notReadyToSellOnClick = function() {
                createNotification(notificationApiClient.notifyNotReadyToSell, steps.notReady);
            };

            self.getAnotherQuoteOnClick = function() {
                location.reload(true);
            };
        }

        $(document).ready(function () {

            ko.components.register('basic-vehicle-info', {
                viewModel: { require: 'app/viewmodels/components/BasicVehicleInfoComponent' },
                template: { require: 'text!app/viewmodels/templates/basic-vehicle-info-template.html' }
            });

            ko.components.register('extra-vehicle-info', {
                viewModel: { require: 'app/viewmodels/components/ExtraVehicleInfoComponent' },
                template: { require: 'text!app/viewmodels/templates/extra-vehicle-info-template.html' }
            });

            ko.components.register('contact-info', {
                viewModel: { require: 'app/viewmodels/components/ContactInfoComponent' },
                template: { require: 'text!app/viewmodels/templates/contact-info-template.html' }
            });

            ko.components.register('customer-quote-created', {
                viewModel: { require: 'app/viewmodels/components/CustomerQuoteCreatedComponent' },
                template: { require: 'text!app/viewmodels/templates/customer-quote-created-template.html' }
            });

            ko.components.register('pickup-info', {
                viewModel: { require: 'app/viewmodels/components/PickupInfoComponent' },
                template: { require: 'text!app/viewmodels/templates/pickup-info-template.html' }
            });

            ko.components.register('transaction-updated', {
                viewModel: { require: 'app/viewmodels/components/TransactionUpdatedComponent' },
                template: { require: 'text!app/viewmodels/templates/transaction-updated-template.html' }
            });

            ko.components.register('will-call-back', {
                viewModel: { require: 'app/viewmodels/components/WillCallBackComponent' },
                template: { require: 'text!app/viewmodels/templates/will-call-back-template.html' }
            });

            ko.components.register('ref-number-confirmation', {
                viewModel: { require: 'app/viewmodels/components/RefNumberConfirmationComponent' },
                template: { require: 'text!app/viewmodels/templates/ref-number-confirmation-template.html' }
            });

            $(document).on("keypress", "form", function(event) { 
                return event.keyCode != 13;
            });

            $("div.validation-error-alert a.close").click(function(eventObject) {
                $(eventObject.target.parentElement).hide();
            });

            var alertsManager = new AlertsManager();
            var configurationApiClient = new ConfigurationApiClient();
            var vehicleMakeApiClient = new VehicleMakeApiClient();
            var quoteApiClient = new QuoteApiClient();
            var locationApiClient = new LocationApiClient();
            var notificationApiClient = new NotificationApiClient();
            var analyticsApiClient = new AnalyticsApiClient();

            var basicVehicleInfoValidator = new BasicVehicleInfoValidator();
            var extraVehicleInfoValidator = new ExtraVehicleInfoValidator();
            var contactInfoValidator = new ContactInfoValidator();
            var locationInfoValidator = new LocationInfoValidator();

            configurationApiClient.getConfigurationData({
                onSuccess: function (response, textStatus, jqXHR) {

                    var vehicleYears = response.vehicleYears;
                    var vehicleTypes = response.vehicleTypes;

                    var vehicleDropOffAddress = response.vehicleDropOffAddress;
                    var contactPhoneNumber = response.contactPhoneNumber;

                    vehicleMakeApiClient.getVehicleMakes({
                        onSuccess: function (response, textStatus, jqXHR) {

                            var vehicleMakes = _.map(response.makes, function(value, index) {
                                return { id: value.name.toLowerCase(), name: value.name };
                            });

                            var viewModel = new CustomerViewModel(vehicleYears, vehicleMakes, vehicleTypes, vehicleDropOffAddress, contactPhoneNumber,
                                vehicleMakeApiClient, quoteApiClient, locationApiClient, notificationApiClient, analyticsApiClient, alertsManager,
                                basicVehicleInfoValidator, extraVehicleInfoValidator, contactInfoValidator, locationInfoValidator);
                            ko.applyBindings(viewModel);

                            $("#overlay").fadeOut();
                        },
                        onFail: alertsManager.onAjaxRequestFail
                    });
                },
                onFail: alertsManager.onAjaxRequestFail
            });
        });
    }
);