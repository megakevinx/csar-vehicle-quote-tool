define(
    [
        'knockout', 'jquery', 'lodash', 'bootstrap',
        'app/alerts/AlertsManager',
        'app/apiclients/ConfigurationApiClient', 'app/apiclients/VehicleMakeApiClient', 'app/apiclients/QuoteApiClient',
        'app/validators/BasicVehicleInfoValidator', 'app/validators/ExtraVehicleInfoValidator',
        'app/wizard/StepNavigator'
    ],

    function (ko, $, _, bootstrap,
        AlertsManager,
        ConfigurationApiClient, VehicleMakeApiClient, QuoteApiClient,
        BasicVehicleInfoValidator, ExtraVehicleInfoValidator,
        StepNavigator) {

        function SalesViewModel(vehicleYears, vehicleMakes, vehicleTypes, vehicleDropOffAddress,
            vehicleMakeApiClient, quoteApiClient, alertsManager,
            basicVehicleInfoValidator, extraVehicleInfoValidator) {

            var self = this;

            self.alertsManager = alertsManager;

            self.vehicleMakeApiClient = vehicleMakeApiClient;

            self.vehicleYears = vehicleYears;
            self.vehicleMakes = vehicleMakes;
            self.vehicleTypes = vehicleTypes;

            // Fields for basicVehicleInfo step
            self.selectedVehicleYear = ko.observable();
            self.selectedVehicleMake = ko.observable();
            self.selectedVehicleModel = ko.observable();
            self.selectedVehicleTypeId = ko.observable();

            // Fields for extraVehicleInfo step
            self.vehicleHasKeys = ko.observable();
            self.vehicleHasTitle = ko.observable();
            self.selectedVehicleWheelsDescription = ko.observable();
            self.vehicleWillBeDelivered = ko.observable("no");
            self.vehicleZipCode = ko.observable();

            self.vehicleDropOffAddress = vehicleDropOffAddress;

            // Fields for quoteCalculated step
            self.calculatedQuote = ko.observable();

            // Internal configuration
            self.isLoadingRequest = ko.observable(false);

            // Wizard steps navigation
            var steps = {
                basicVehicleInfo: "BASIC_VEHICLE_INFO",
                extraVehicleInfo: "EXTRA_VEHICLE_INFO",
                quoteCalculated: "QUOTE_CALCULATED"
            };

            var validatorsByStep = [];
            validatorsByStep[steps.basicVehicleInfo] = basicVehicleInfoValidator;
            validatorsByStep[steps.extraVehicleInfo] = extraVehicleInfoValidator;
            validatorsByStep[steps.quoteCalculated] = {
                formIsValid: function () {
                    return true;
                }
            };

            var stepNavigator = new StepNavigator({
                stepsSequence: [steps.basicVehicleInfo, steps.extraVehicleInfo, steps.quoteCalculated],
                stepsWithNavigationForward: [steps.basicVehicleInfo],
                stepsWithNavigationBackward: [steps.extraVehicleInfo],
                validatorsByStep: validatorsByStep,
                initialStep: steps.basicVehicleInfo
            });

            self.shouldDisplayPreviousStepButton = stepNavigator.shouldDisplayPreviousStepButton;
            self.shouldDisplayNextStepButton = stepNavigator.shouldDisplayNextStepButton;
            self.shouldDisplayStepButtonsContainer = stepNavigator.shouldDisplayStepButtonsContainer;
            
            self.goToPreviousStepOnClick = stepNavigator.goToPreviousStepOnClick;
            self.goToNextStepOnClick = stepNavigator.goToNextStepOnClick;

            self.shouldDisplayBasicVehicleInfoStep = ko.pureComputed(function() {
                return (stepNavigator.currentStep() === steps.basicVehicleInfo);
            });

            self.shouldDisplayExtraVehicleInfoStep = ko.pureComputed(function() {
                return (stepNavigator.currentStep() === steps.extraVehicleInfo);
            });

            self.shouldDisplayQuoteCalculatedStep = ko.pureComputed(function() {
                return (stepNavigator.currentStep() === steps.quoteCalculated);
            });

            self.serverValidationErrorAlertContainerId = "#extra_vehicle_info_server_validation_error_container";
            self.serverValidationErrorAlertId = "extra_vehicle_info_server_validation_error_alert";
            self.zipCodeInvalidErrorMessage = "- We don't offer our services in the vehicle's location";

            self.getQuoteOnClick = function () {

                var validator = validatorsByStep[stepNavigator.currentStep()];

                if (!validator.formIsValid()) {
                    console.log("form invalid");
                    return;
                }

                var vehicleData = {
                    vehicleYear: self.selectedVehicleYear(),
                    vehicleMake: self.selectedVehicleMake(),
                    vehicleModel: self.selectedVehicleModel(),
                    vehicleTypeId: self.selectedVehicleTypeId(),

                    vehicleHasKeys: self.vehicleHasKeys(),
                    vehicleHasTitle: self.vehicleHasTitle(),
                    vehicleWheelsDescription: self.selectedVehicleWheelsDescription(),
                    vehicleWillBeDelivered: self.vehicleWillBeDelivered(),
                    vehicleZipCode: self.vehicleZipCode(),
                };

                self.isLoadingRequest(true);

                quoteApiClient.calculateQuote({

                    vehicleData: vehicleData,

                    onSuccess: function (response, textStatus, jqXHR) {
                        console.log(response);

                        self.calculatedQuote(response.quote);

                        stepNavigator.showStep(steps.quoteCalculated);

                        self.isLoadingRequest(false);
                    },
                    onFail: function (jqXHR, textStatus, error) {
                        if (jqXHR.status == 400) {
                            console.log(jqXHR.responseJSON);
                            alertsManager.showDanger(
                                $(self.serverValidationErrorAlertContainerId),
                                self.zipCodeInvalidErrorMessage,
                                self.serverValidationErrorAlertId
                            );
                        }
                        else {
                            alertsManager.onAjaxRequestFail(jqXHR, textStatus, error);
                        }

                        self.isLoadingRequest(false);
                    }
                });
            };
        }

        $(document).ready(function () {

            ko.components.register('basic-vehicle-info', {
                viewModel: { require: 'app/viewmodels/components/BasicVehicleInfoComponent' },
                template: { require: 'text!app/viewmodels/templates/basic-vehicle-info-template.html' }
            });

            ko.components.register('extra-vehicle-info', {
                viewModel: { require: 'app/viewmodels/components/ExtraVehicleInfoComponent' },
                template: { require: 'text!app/viewmodels/templates/extra-vehicle-info-template.html' }
            });

            ko.components.register('quote-calculated', {
                viewModel: { require: 'app/viewmodels/components/QuoteCalculatedComponent' },
                template: { require: 'text!app/viewmodels/templates/quote-calculated-template.html' }
            });

            $(document).on("keypress", "form", function(event) { 
                return event.keyCode != 13;
            });

            $("div.validation-error-alert a.close").click(function(eventObject) {
                $(eventObject.target.parentElement).hide();
            });

            var alertsManager = new AlertsManager();
            var configurationApiClient = new ConfigurationApiClient();
            var vehicleMakeApiClient = new VehicleMakeApiClient();
            var quoteApiClient = new QuoteApiClient();

            var basicVehicleInfoValidator = new BasicVehicleInfoValidator();
            var extraVehicleInfoValidator = new ExtraVehicleInfoValidator();

            configurationApiClient.getConfigurationData({
                onSuccess: function (response, textStatus, jqXHR) {

                    var vehicleYears = response.vehicleYears;
                    var vehicleTypes = response.vehicleTypes;

                    var vehicleDropOffAddress = response.vehicleDropOffAddress;

                    vehicleMakeApiClient.getVehicleMakes({
                        onSuccess: function (response, textStatus, jqXHR) {

                            var vehicleMakes = _.map(response.makes, function(value, index) {
                                return { id: value.name.toLowerCase(), name: value.name };
                            });

                            var viewModel = new SalesViewModel(vehicleYears, vehicleMakes, vehicleTypes, vehicleDropOffAddress,
                                vehicleMakeApiClient, quoteApiClient, alertsManager,
                                basicVehicleInfoValidator, extraVehicleInfoValidator);
                            ko.applyBindings(viewModel);

                            $("#overlay").fadeOut();
                        },
                        onFail: alertsManager.onAjaxRequestFail
                    });
                },
                onFail: alertsManager.onAjaxRequestFail
            });
        });
    }
);