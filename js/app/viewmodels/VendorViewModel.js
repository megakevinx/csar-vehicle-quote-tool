define(
    [
        'knockout', 'jquery', 'lodash', 'bootstrap',
        'app/alerts/AlertsManager',
        'app/apiclients/ConfigurationApiClient', 'app/apiclients/VehicleMakeApiClient', 'app/apiclients/QuoteApiClient',
        'app/validators/BasicVehicleInfoValidator', 'app/validators/ExtraVehicleInfoValidator', 'app/validators/PickupInfoValidator',
        'app/wizard/StepNavigator'
    ],

    function (ko, $, _, bootstrap,
        AlertsManager,
        ConfigurationApiClient, VehicleMakeApiClient, QuoteApiClient,
        BasicVehicleInfoValidator, ExtraVehicleInfoValidator, PickupInfoValidator,
        StepNavigator) {

        function VendorViewModel(vehicleYears, vehicleMakes, vehicleTypes, vehicleDropOffAddress, contactPhoneNumber,
            vehicleMakeApiClient, quoteApiClient, alertsManager,
            basicVehicleInfoValidator, extraVehicleInfoValidator, pickupInfoValidator) {

            var self = this;

            self.alertsManager = alertsManager;

            self.vehicleMakeApiClient = vehicleMakeApiClient;
            self.quoteApiClient = quoteApiClient;

            self.vehicleYears = vehicleYears;
            self.vehicleMakes = vehicleMakes;
            self.vehicleTypes = vehicleTypes;

            // Fields for basicVehicleInfo step
            self.selectedVehicleYear = ko.observable();
            self.selectedVehicleMake = ko.observable();
            self.selectedVehicleModel = ko.observable();
            self.selectedVehicleTypeId = ko.observable();

            // Fields for extraVehicleInfo step
            self.vehicleHasKeys = ko.observable();
            self.vehicleHasTitle = ko.observable();
            self.selectedVehicleWheelsDescription = ko.observable();
            self.vehicleWillBeDelivered = ko.observable("no");
            self.vehicleZipCode = ko.observable();

            self.vehicleDropOffAddress = vehicleDropOffAddress;

            // Fields for PickupInfo step
            self.vehicleStreetAddress = ko.observable();
            self.vehicleCity = ko.observable();

            self.sellerName = ko.observable();
            self.sellerPhoneNumber = ko.observable();
            self.sellerEmail = ko.observable();

            self.buyerPayout = ko.observable(0.00);

            // Fields for quoteCreated step
            self.calculatedQuote = ko.observable();
            self.referenceNumber = ko.observable();

            self.contactPhoneNumber = contactPhoneNumber;

            // Internal configuration
            self.isLoadingRequest = ko.observable(false);

            // Wizard steps navigation
            var steps = {
                basicVehicleInfo: "BASIC_VEHICLE_INFO",
                extraVehicleInfo: "EXTRA_VEHICLE_INFO",
                quoteCreated: "QUOTE_CREATED",
                pickupInfo: "PICKUP_INFORMATION",
                quoteUpdated: "QUOTE_UPDATED",
                willCallBack: "WILL_CALL_BACK"
            };

            // Validators configuration
            var validatorsByStep = [];
            validatorsByStep[steps.basicVehicleInfo] = basicVehicleInfoValidator;
            validatorsByStep[steps.extraVehicleInfo] = extraVehicleInfoValidator;
            validatorsByStep[steps.quoteCreated] = {
                formIsValid: function () {
                    return true;
                }
            };

            validatorsByStep[steps.pickupInfo] = pickupInfoValidator;
            // validatorsByStep[steps.quoteUpdated] = dummyValidator;

            var stepNavigator = new StepNavigator({
                stepsSequence: [steps.basicVehicleInfo, steps.extraVehicleInfo, steps.quoteCreated, steps.pickupInfo, steps.quoteUpdated],
                stepsWithNavigationForward: [steps.basicVehicleInfo],
                stepsWithNavigationBackward: [steps.extraVehicleInfo, steps.pickupInfo],
                validatorsByStep: validatorsByStep,
                initialStep: steps.basicVehicleInfo
            });

            self.shouldDisplayPreviousStepButton = stepNavigator.shouldDisplayPreviousStepButton;
            self.shouldDisplayNextStepButton = stepNavigator.shouldDisplayNextStepButton;
            self.shouldDisplayStepButtonsContainer = stepNavigator.shouldDisplayStepButtonsContainer;
            
            self.goToPreviousStepOnClick = stepNavigator.goToPreviousStepOnClick;
            self.goToNextStepOnClick = stepNavigator.goToNextStepOnClick;

            self.currentStep = ko.observable(steps.basicVehicleInfo);

            // Wizard step display/traversal logic
            self.shouldDisplayBasicVehicleInfoStep = ko.pureComputed(function() {
                return (stepNavigator.currentStep() === steps.basicVehicleInfo);
            });

            self.shouldDisplayExtraVehicleInfoStep = ko.pureComputed(function() {
                return (stepNavigator.currentStep() === steps.extraVehicleInfo);
            });

            self.shouldDisplayQuoteCreatedStep = ko.pureComputed(function() {
                return (stepNavigator.currentStep() === steps.quoteCreated);
            });

            self.shouldDisplayPickupInfoStep = ko.pureComputed(function() {
                return (stepNavigator.currentStep() === steps.pickupInfo);
            });

            self.shouldDisplayQuoteUpdatedStep = ko.pureComputed(function() {
                return (stepNavigator.currentStep() === steps.quoteUpdated);
            });

            self.shouldDisplayWillCallBackStep = ko.pureComputed(function() {
                return (stepNavigator.currentStep() === steps.willCallBack);
            });

            self.serverValidationErrorAlertContainerId = "#extra_vehicle_info_server_validation_error_container";
            self.serverValidationErrorAlertId = "extra_vehicle_info_server_validation_error_alert";
            self.zipCodeInvalidErrorMessage = "- We don't offer our services in the vehicle's location";

            self.notifyCallbackVendorOnSuccess = function() {
                stepNavigator.showStep(steps.willCallBack);
            };

            self.getQuoteOnClick = function () {

                var validator = validatorsByStep[stepNavigator.currentStep()];

                if (!validator.formIsValid()) {
                    console.log("form invalid");
                    return;
                }

                var vehicleData = {
                    vehicleYear: self.selectedVehicleYear(),
                    vehicleMake: self.selectedVehicleMake(),
                    vehicleModel: self.selectedVehicleModel(),
                    vehicleTypeId: self.selectedVehicleTypeId(),

                    vehicleHasKeys: self.vehicleHasKeys(),
                    vehicleHasTitle: self.vehicleHasTitle(),
                    vehicleWheelsDescription: self.selectedVehicleWheelsDescription(),
                    vehicleWillBeDelivered: self.vehicleWillBeDelivered(),
                    vehicleZipCode: self.vehicleZipCode(),
                };

                self.isLoadingRequest(true);

                quoteApiClient.getQuote({

                    vehicleData: vehicleData,

                    onSuccess: function (response, textStatus, jqXHR) {

                        self.calculatedQuote(response.quote);
                        self.referenceNumber(response.referenceNumber);

                        stepNavigator.showStep(steps.quoteCreated);

                        console.log(response);

                        self.isLoadingRequest(false);
                    },
                    onFail: function (jqXHR, textStatus, error) {
                        if (jqXHR.status == 400) {
                            alertsManager.showDanger(
                                $(self.serverValidationErrorAlertContainerId),
                                self.zipCodeInvalidErrorMessage,
                                self.serverValidationErrorAlertId
                            );
                            console.log(jqXHR.responseJSON);
                        }
                        else {
                            alertsManager.onAjaxRequestFail(jqXHR, textStatus, error);
                        }

                        self.isLoadingRequest(false);
                    }
                });
            };

            self.getAnotherQuoteOnClick = function() {
                location.reload(true);
            };

            self.submitPickupInfoOnClick = function () {

                var validator = validatorsByStep[stepNavigator.currentStep()];

                if (!validator.formIsValid()) {
                    console.log("form invalid");
                    return;
                }

                var pickupInfo = {
                    referenceNumber: self.referenceNumber(),

                    vehicleStreetAddress: self.vehicleStreetAddress(),
                    vehicleCity: self.vehicleCity(),

                    sellerName: self.sellerName(),
                    sellerPhoneNumber: self.sellerPhoneNumber(),
                    sellerEmail: self.sellerEmail(),

                    buyerPayout: self.buyerPayout()
                };

                self.isLoadingRequest(true);

                quoteApiClient.submitPickupInfo({

                    pickupInfo: pickupInfo,

                    onSuccess: function (response, textStatus, jqXHR) {
                        stepNavigator.showStep(steps.quoteUpdated);
                        console.log(response);
                        self.isLoadingRequest(false);
                    },
                    onFail: function (jqXHR, textStatus, error) {
                        alertsManager.onAjaxRequestFail(jqXHR, textStatus, error);
                        self.isLoadingRequest(false);
                    }
                });
            };
        }

        $(document).ready(function () {

            ko.components.register('basic-vehicle-info', {
                viewModel: { require: 'app/viewmodels/components/BasicVehicleInfoComponent' },
                template: { require: 'text!app/viewmodels/templates/basic-vehicle-info-template.html' }
            });

            ko.components.register('extra-vehicle-info', {
                viewModel: { require: 'app/viewmodels/components/ExtraVehicleInfoComponent' },
                template: { require: 'text!app/viewmodels/templates/extra-vehicle-info-template.html' }
            });

            ko.components.register('transaction-created', {
                viewModel: { require: 'app/viewmodels/components/TransactionCreatedComponent' },
                template: { require: 'text!app/viewmodels/templates/transaction-created-template.html' }
            });

            ko.components.register('pickup-info', {
                viewModel: { require: 'app/viewmodels/components/PickupInfoComponent' },
                template: { require: 'text!app/viewmodels/templates/pickup-info-template.html' }
            });

            ko.components.register('transaction-updated', {
                viewModel: { require: 'app/viewmodels/components/TransactionUpdatedComponent' },
                template: { require: 'text!app/viewmodels/templates/transaction-updated-template.html' }
            });

            ko.components.register('will-call-back', {
                viewModel: { require: 'app/viewmodels/components/WillCallBackComponent' },
                template: { require: 'text!app/viewmodels/templates/will-call-back-template.html' }
            });

            $(document).on("keypress", "form", function(event) { 
                return event.keyCode != 13;
            });

            $("div.validation-error-alert a.close").click(function(eventObject) {
                $(eventObject.target.parentElement).hide();
            });

            var alertsManager = new AlertsManager();
            var configurationApiClient = new ConfigurationApiClient();
            var vehicleMakeApiClient = new VehicleMakeApiClient();
            var quoteApiClient = new QuoteApiClient();

            var basicVehicleInfoValidator = new BasicVehicleInfoValidator();
            var extraVehicleInfoValidator = new ExtraVehicleInfoValidator();
            var pickupInfoValidator = new PickupInfoValidator();

            configurationApiClient.getConfigurationData({
                onSuccess: function (response, textStatus, jqXHR) {

                    var vehicleYears = response.vehicleYears;
                    var vehicleTypes = response.vehicleTypes;

                    var vehicleDropOffAddress = response.vehicleDropOffAddress;
                    var contactPhoneNumber = response.contactPhoneNumber;

                    vehicleMakeApiClient.getVehicleMakes({
                        onSuccess: function (response, textStatus, jqXHR) {

                            var vehicleMakes = _.map(response.makes, function(value, index) {
                                return { id: value.name.toLowerCase(), name: value.name };
                            });

                            var viewModel = new VendorViewModel(vehicleYears, vehicleMakes, vehicleTypes, vehicleDropOffAddress, contactPhoneNumber,
                                vehicleMakeApiClient, quoteApiClient, alertsManager,
                                basicVehicleInfoValidator, extraVehicleInfoValidator, pickupInfoValidator);
                            ko.applyBindings(viewModel);

                            $("#overlay").fadeOut();
                        },
                        onFail: alertsManager.onAjaxRequestFail
                    });
                },
                onFail: alertsManager.onAjaxRequestFail
            });
        });
    }
);