define('app/viewmodels/components/AppSettingsComponent',
[
    'knockout', 'jquery', 'lodash', 'bootstrap',
    'app/alerts/AlertsManager', 'app/apiclients/ConfigurationApiClient', 'app/validators/AppSettingsValidator'
],
function (ko, $, _, bootstrap,
    AlertsManager, ConfigurationApiClient, AppSettingsValidator) {

    function AppSettingsComponent(params) {

        $("div.validation-error-alert a.close").click(function(eventObject) {
            $(eventObject.target.parentElement).hide();
        });

        var self = this;

        var alertsManager = params.alertsManager;
        var configurationApiClient = params.configurationApiClient;

        var appSettingsValidator = new AppSettingsValidator();

        self.isLoadingRequest = ko.observable(false);

        self.vehicleDropOffAddress = ko.observable(params.vehicleDropOffAddress);
        self.contactPhoneNumber = ko.observable(params.contactPhoneNumber);

        self.saveAppSettingsOnclick = function() {
            
            if (!appSettingsValidator.formIsValid()) {
                console.log("form invalid");
                return;
            }

            var appSettings = {
                vehicleDropOffAddress: self.vehicleDropOffAddress(),
                contactPhoneNumber: self.contactPhoneNumber()
            };

            self.isLoadingRequest(true);

            configurationApiClient.submitConfigurationData({

                appSettings: appSettings,

                onSuccess: function (response, textStatus, jqXHR) {
                    console.log(response);

                    alertsManager.showSuccess(
                        $("#app_settings_alerts_container"),
                        "The Application Settings were saved successfully"
                    );

                    self.isLoadingRequest(false);
                },
                onFail: function (jqXHR, textStatus, error) {
                    alertsManager.onAjaxRequestFail(jqXHR, textStatus, error);
                    self.isLoadingRequest(false);
                }
            });
        };
    }

    return AppSettingsComponent;
});