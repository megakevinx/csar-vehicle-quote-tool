define('app/viewmodels/components/BasicVehicleInfoComponent',
[
    'knockout', 'jquery', 'lodash', 'bootstrap'
],
function (ko, $, _, bootstrap) {

    function BasicVehicleInfoComponent(params) {

        $("div.validation-error-alert a.close").click(function(eventObject) {
            $(eventObject.target.parentElement).hide();
        });

        var self = this;

        var vehicleMakeApiClient = params.vehicleMakeApiClient;
        var alertsManager = params.alertsManager;

        self.vehicleYears = params.vehicleYears;
        self.vehicleMakes = params.vehicleMakes;
        self.vehicleTypes = params.vehicleTypes;

        self.vehicleModels = ko.observableArray([]);

        self.selectedVehicleYear = params.selectedVehicleYear;
        self.selectedVehicleMake = params.selectedVehicleMake;
        self.selectedVehicleModel = params.selectedVehicleModel;
        self.selectedVehicleTypeId = params.selectedVehicleTypeId;

        self.isLoadingVehicleType = ko.observable(false);
        var vehicleTypeNotFoundAlertId = "VEHICLE_TYPE_NOT_FOUND";

        //self.shouldAllowVehicleTypeSelection = ko.observable(false);

        self.vehicleYearOnSelect = function() {
            loadVehicleModels();
        };

        self.vehicleMakeOnSelect = function() {
            loadVehicleModels();
        };

        function loadVehicleModels() {
            if (self.selectedVehicleYear() && self.selectedVehicleMake()) {
                vehicleMakeApiClient.getVehicleModels({

                    vehicleMake: self.selectedVehicleMake(),
                    vehicleYear: self.selectedVehicleYear(),

                    onSuccess: function (response) {
                        var models = _.map(response.models, function(value) {
                            return { id: value.name.toLowerCase(), name: value.name };
                        });

                        self.vehicleModels(models);
                    },
                    onFail: alertsManager.onAjaxRequestFail
                });
            }
        }

        self.vehicleModelOnSelect = function() {

            if (self.selectedVehicleYear() && self.selectedVehicleMake() && self.selectedVehicleModel()) {

                self.isLoadingVehicleType(true);

                vehicleMakeApiClient.getVehicleStyleInfo({

                    vehicleYear: self.selectedVehicleYear(),
                    vehicleMake: self.selectedVehicleMake(),
                    vehicleModel: self.selectedVehicleModel(),

                    onSuccess: function (response) {
                        var vehicleType = response.vehicleType;

                        self.selectedVehicleTypeId(vehicleType.id);

                        alertsManager.dismiss(vehicleTypeNotFoundAlertId);

                        self.isLoadingVehicleType(false);
                    },
                    onFail: function (jqXHR, textStatus, error) {
                        console.log({
                            response: jqXHR.responseText,
                            status: jqXHR.status
                        });

                        self.selectedVehicleTypeId(null);

                        var searchQuery = "https://www.google.com/search?q=" + self.selectedVehicleMake() + " " + self.selectedVehicleModel() + " size wikipedia";
                        var searchLink = "<a target='_blank' href='" + searchQuery + "'>try asking Google</a>";

                        alertsManager.showWarning(
                            $("#vehicle_type_error_container"),
                            "Sorry! We could not figure out the Vehicle Type. You can " + searchLink + ".",
                            vehicleTypeNotFoundAlertId
                        );

                        self.isLoadingVehicleType(false);
                    }
                });
            }
        }

        var vehicleTypePictureMap = [];

        vehicleTypePictureMap["Compact Car"] = "img/compact-car.png";
        vehicleTypePictureMap["Midsize Car"] = "img/midsize-car.png";
        vehicleTypePictureMap["Large Car"] = "img/large-car.png";

        vehicleTypePictureMap["Compact Truck"] = "img/compact-truck.png";
        vehicleTypePictureMap["Midsize Truck"] = "img/midsize-truck.png";
        vehicleTypePictureMap["Large Truck"] = "img/large-truck.png";

        vehicleTypePictureMap["Compact SUV"] = "img/compact-suv.png";
        vehicleTypePictureMap["Midsize SUV"] = "img/midsize-suv.png";
        vehicleTypePictureMap["Large SUV"] = "img/large-suv.png";

        vehicleTypePictureMap["Compact Van"] = "img/compact-van.png";
        vehicleTypePictureMap["Midsize Van"] = "img/midsize-van.png";
        vehicleTypePictureMap["Large Van"] = "img/large-van.png";

        vehicleTypePictureMap["Compact Minivan"] = "img/compact-minivan.png";
        vehicleTypePictureMap["Midsize Minivan"] = "img/midsize-minivan.png";
        vehicleTypePictureMap["Large Minivan"] = "img/large-minivan.png";

        self.vehicleTypePicture = ko.pureComputed(function() {
            if (self.selectedVehicleTypeId()) {
                var selectedVehicleType = _.find(self.vehicleTypes, function(t) {
                    return t.id == self.selectedVehicleTypeId()
                });

                return vehicleTypePictureMap[selectedVehicleType.description];
            }
            else {
                return "img/no-vehicle-type.png";
            }
        });
    }

    return BasicVehicleInfoComponent;
});