define('app/viewmodels/components/ContactInfoComponent',
[
    'knockout', 'jquery', 'lodash', 'bootstrap'
],
function (ko, $, _, bootstrap) {

    function ContactInfoComponent(params) {

        $("div.validation-error-alert a.close").click(function(eventObject) {
            $(eventObject.target.parentElement).hide();
        });

        var self = this;

        self.sellerName = params.sellerName;
        self.sellerPhoneNumber = params.sellerPhoneNumber;
        self.sellerEmail = params.sellerEmail;
    }

    return ContactInfoComponent;
});