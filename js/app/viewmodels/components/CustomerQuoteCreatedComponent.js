define('app/viewmodels/components/CustomerQuoteCreatedComponent',
[
    'knockout', 'jquery', 'lodash', 'bootstrap'
],
function (ko, $, _, bootstrap) {

    function CustomerQuoteCreatedComponent(params) {

        var self = this;

        self.isLoadingRequest = params.isLoadingRequest;

        self.calculatedQuote = params.calculatedQuote;
        self.referenceNumber = params.referenceNumber;
        self.contactPhoneNumber = params.contactPhoneNumber;

        self.vehicleStreetAddress = params.vehicleStreetAddress;
        self.vehicleCity = params.vehicleCity;

        self.submitLocationInfoOnClick = params.submitLocationInfoOnClick;
        self.callMeBackOnClick = params.callMeBackOnClick;
        self.notReadyToSellOnClick = params.notReadyToSellOnClick;
        self.shoppingAroundOnClick = params.shoppingAroundOnClick;
        self.getAnotherQuoteOnClick = params.getAnotherQuoteOnClick;
    }

    return CustomerQuoteCreatedComponent;
});