define('app/viewmodels/components/ExtraVehicleInfoComponent',
[
    'knockout', 'jquery', 'lodash', 'bootstrap'
],
function (ko, $, _, bootstrap) {

    function ExtraVehicleInfoComponent(params) {

        $("div.validation-error-alert a.close").click(function(eventObject) {
            $(eventObject.target.parentElement).hide();
        });

        var self = this;

        self.vehicleHasKeys = params.vehicleHasKeys;
        self.vehicleHasTitle = params.vehicleHasTitle;
        self.selectedVehicleWheelsDescription = params.selectedVehicleWheelsDescription;
        self.vehicleWillBeDelivered = params.vehicleWillBeDelivered;
        self.vehicleZipCode = params.vehicleZipCode;

        self.vehicleDropOffAddress = params.vehicleDropOffAddress;

        self.vehicleWheelsDescriptions = [
            { value: "steel", description: "Steel" },
            { value: "aluminum", description: "Aluminum" },
            { value: "no wheels", description: "No wheels" },
        ];

        self.shouldDisplayDropOffAddress = ko.pureComputed(function() {
            return self.vehicleWillBeDelivered() == "yes";
        });
    }

    return ExtraVehicleInfoComponent;
});