define('app/viewmodels/components/FaqsManagerComponent',
[
    'knockout', 'jquery', 'lodash', 'bootstrap',
    'app/validators/FaqValidator'
],
function (ko, $, _, bootstrap, FaqValidator) {

    function FaqsManagerComponent(params) {

        $("div.validation-error-alert a.close").click(function(eventObject) {
            $(eventObject.target.parentElement).hide();
        });

        var self = this;

        var alertsManager = params.alertsManager;
        var faqApiClient = params.faqApiClient;

        var faqValidator = new FaqValidator();

        self.isLoadingRequest = ko.observable(false);

        self.newQuestion = ko.observable();
        self.newAnswer = ko.observable();

        self.saveNewFaqOnclick = function() {

            if (!faqValidator.formIsValid()) {
                console.log("form invalid");
                return;
            }

            var faqData = {
                question: self.newQuestion(),
                answer: self.newAnswer()
            };

            self.isLoadingRequest(true);

            faqApiClient.createFaq({

                faqData: faqData,

                onSuccess: function (response, textStatus, jqXHR) {
                    console.log(response);

                    self.loadFaqs(response.faqs);

                    alertsManager.showSuccess(
                        $("#faqs_alerts_container"),
                        "The new FAQ has been created successfully"
                    );

                    self.isLoadingRequest(false);
                },
                onFail: function (jqXHR, textStatus, error) {
                    self.isLoadingRequest(false);
                    alertsManager.onAjaxRequestFail(jqXHR, textStatus, error);
                }
            });
        };

        function FaqViewModel(faq, parent) {

            var self = this;
            //var parent = parent;

            self.isLoadingRequest = ko.observable(false);
            self.isValid = ko.observable(true);

            self.id = ko.observable(faq.id);
            self.question = ko.observable(faq.question);
            self.answer = ko.observable(faq.answer);

            self.saveOnClick = function() {

                if (!self.question() || !self.answer()) {

                    self.isValid(false);

                    alertsManager.showDanger(
                        $("#faqs_alerts_container"),
                        "A question and answer are required in order to save a FAQ."
                    );

                    return;
                }
                else {
                    self.isValid(true);
                }

                var faqData = {
                    id: self.id(),
                    question: self.question(),
                    answer: self.answer()
                };

                self.isLoadingRequest(true);

                faqApiClient.updateFaq({

                    faqData: faqData,

                    onSuccess: function (response) {

                        console.log(response);

                        self.question(response.updatedFaq.question);
                        self.answer(response.updatedFaq.answer);

                        alertsManager.showSuccess(
                            $("#faqs_alerts_container"),
                            "The FAQ has been saved successfully"
                        );

                        self.isLoadingRequest(false);
                    },
                    onFail: function (jqXHR, textStatus, error) {

                        if (jqXHR.responseJSON.message) {
                            alertsManager.showDanger(
                                $("#faqs_alerts_container"),
                                jqXHR.responseJSON.message
                            );
                        }
                        else {
                            alertsManager.onAjaxRequestFail(jqXHR, textStatus, error);
                        }

                        self.isLoadingRequest(false);
                    }
                });
            };

            self.deleteOnClick = function () {

                if (!confirm("Are you sure you want to delete this FAQ?")) {
                    return;
                }

                var faqData = {
                    id: self.id()
                };

                self.isLoadingRequest(true);

                faqApiClient.deleteFaq({

                    faqData: faqData,

                    onSuccess: function (response) {

                        console.log(response);

                        parent.loadFaqs(response.faqs);

                        alertsManager.showSuccess(
                            $("#faqs_alerts_container"),
                            "The FAQ has been deleted successfully"
                        );

                        self.isLoadingRequest(false);
                    },
                    onFail: function (jqXHR, textStatus, error) {
                        alertsManager.onAjaxRequestFail(jqXHR, textStatus, error);
                        self.isLoadingRequest(false);
                    }
                });
            };
        }

        self.faqs = ko.observableArray([]);

        self.loadFaqs = function(faqs) {

            self.faqs.removeAll();

            faqs = _.map(faqs, function(faq) {
                return new FaqViewModel(faq, self);
            });

            _.forEach(faqs, function(faq) {
                self.faqs.push(faq);
            });
        };

        self.loadFaqs(params.faqs);
    }

    return FaqsManagerComponent;
});