define('app/viewmodels/components/PickupInfoComponent',
[
    'knockout', 'jquery', 'lodash', 'bootstrap'
],
function (ko, $, _, bootstrap) {

    function PickupInfoComponent(params) {

        $("div.validation-error-alert a.close").click(function(eventObject) {
            $(eventObject.target.parentElement).hide();
        });

        var self = this;

        self.vehicleStreetAddress = params.vehicleStreetAddress;
        self.vehicleCity = params.vehicleCity;
        self.sellerName = params.sellerName;
        self.sellerPhoneNumber = params.sellerPhoneNumber;
        self.sellerEmail = params.sellerEmail;
        self.calculatedQuote = params.calculatedQuote;
        self.buyerPayout = params.buyerPayout;

        self.isLoadingRequest = params.isLoadingRequest;
        self.submitPickupInfoOnClick = params.submitPickupInfoOnClick;

        self.buyerProfit = ko.pureComputed(function() {
            if (!isNaN(self.buyerPayout())) {
                return (self.calculatedQuote() - self.buyerPayout()).toFixed(2);
            }
            else {
                return 0.00;
            }
        });

        self.profitStatus = ko.pureComputed(function() {
            if (self.buyerProfit() == 0) {
                return "label-warning";
            }
            else if (self.buyerProfit() > 0) {
                return "label-success";
            }
            else if (self.buyerProfit() < 0) {
                return "label-danger";
            }
        });
    }

    return PickupInfoComponent;
});