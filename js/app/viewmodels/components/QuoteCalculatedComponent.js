define('app/viewmodels/components/QuoteCalculatedComponent',
[
    'knockout', 'jquery', 'lodash', 'bootstrap'
],
function (ko, $, _, bootstrap) {

    function QuoteCalculatedComponent(params) {

        var self = this;

        self.calculatedQuote = params.calculatedQuote;

        self.quantityInInventory = ko.pureComputed(function() {
            return _.random(30, 70);
        });

        self.getAnotherQuoteOnClick = function() {
            location.reload(true);
        };
    }

    return QuoteCalculatedComponent;
});