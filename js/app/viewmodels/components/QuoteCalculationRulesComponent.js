define('app/viewmodels/components/QuoteCalculationRulesComponent',
[
    'knockout', 'jquery', 'lodash', 'bootstrap',
    'app/alerts/AlertsManager', 'app/apiclients/ConfigurationApiClient', 'app/validators/BasePriceChangeValidator'
],
function (ko, $, _, bootstrap,
    AlertsManager, ConfigurationApiClient, BasePriceChangeValidator) {

    function QuoteCalculationRulesComponent(params) {

        $("div.validation-error-alert a.close").click(function(eventObject) {
            $(eventObject.target.parentElement).hide();
        });

        var self = this;

        var alertsManager = params.alertsManager;
        var configurationApiClient = params.configurationApiClient;

        var basePriceChangeValidator = new BasePriceChangeValidator();

        self.isLoadingRequest = ko.observable(false);

        self.basePriceChange = ko.observable(0);

        self.quoteCalculationRules = ko.observableArray([]);

        function loadQuoteCalculationRules(rules) {

            rules = _.map(rules, function(rule) {
                return {
                    id: ko.observable(rule.id),
                    featureType: ko.observable(rule.featureType),
                    featureValue: ko.observable(rule.featureValue),
                    priceModifier: ko.observable(rule.priceModifier),

                    getPriceModifierFieldClass: function(priceModifier) {
                        if (priceModifier() > 0) {
                            return "has-info";
                        }
                        else if (priceModifier() < 0) {
                            return "has-warning";
                        }
                    }
                };
            });

            self.quoteCalculationRules.removeAll();

            _.forEach(rules, function(rule) {
                self.quoteCalculationRules.push(rule);
            });
        }

        loadQuoteCalculationRules(params.quoteCalculationRules);

        self.changeBasePriceOnclick = function() {

            if (!basePriceChangeValidator.formIsValid()) {
                console.log("form invalid");
                return;
            }

            self.isLoadingRequest(true);

            configurationApiClient.changeBasePrice({

                basePriceChange: self.basePriceChange(),

                onSuccess: function (response, textStatus, jqXHR) {
                    console.log(response);

                    loadQuoteCalculationRules(response.quoteCalculationRules);

                    alertsManager.showSuccess(
                        $("#base_price_change_alerts_container"),
                        "All of the Base Prices were changed successfully"
                    );

                    self.isLoadingRequest(false);
                },
                onFail: function (jqXHR, textStatus, error) {
                    alertsManager.onAjaxRequestFail(jqXHR, textStatus, error);
                    self.isLoadingRequest(false);
                }
            });
        };

        function quoteCalculationRulesAreValid() {

            var invalidRules = _.filter(self.quoteCalculationRules(), function(r) {
                return !$.isNumeric(r.priceModifier());
            });

            if (_.size(invalidRules) > 0) {

                var ruleIdsAsString = _.join(_.map(invalidRules, function(r) { return r.id() }), ', ');

                alertsManager.showDanger(
                    $("#quote_calculation_rules_alerts_container"),
                    "Please make sure that Rule(s) [" + ruleIdsAsString + "] have a valid Price Modifier"
                );

                return false;
            }
            else {
                return true;
            }
        }

        self.saveQuoteCalculationRulesOnclick = function() {

            if (!quoteCalculationRulesAreValid()) {
                console.log("form invalid");
                return;
            }

            var quoteCalculationRules = _.map(self.quoteCalculationRules(), function(rule) {
                return {
                    id: rule.id(),
                    priceModifier: rule.priceModifier()
                };
            });

            self.isLoadingRequest(true);

            configurationApiClient.submitQuoteCalculationRules({

                quoteCalculationRules: quoteCalculationRules,

                onSuccess: function (response, textStatus, jqXHR) {
                    console.log(response);

                    alertsManager.showSuccess(
                        $("#quote_calculation_rules_alerts_container"),
                        "The Quote Calculation Rules were saved successfully"
                    );

                    self.isLoadingRequest(false);
                },
                onFail: function (jqXHR, textStatus, error) {
                    alertsManager.onAjaxRequestFail(jqXHR, textStatus, error);
                    self.isLoadingRequest(false);
                }
            });
        };
    }

    return QuoteCalculationRulesComponent;
});