define('app/viewmodels/components/QuoteOverridesComponent',
[
    'knockout', 'jquery', 'lodash', 'bootstrap',
    'app/alerts/AlertsManager', 'app/apiclients/ConfigurationApiClient', 'app/apiclients/VehicleMakeApiClient', 'app/validators/QuoteOverrideValidator'
],
function (ko, $, _, bootstrap,
    AlertsManager, ConfigurationApiClient, VehicleMakeApiClient, QuoteOverrideValidator) {

    function QuoteOverridesComponent(params) {

        $("div.validation-error-alert a.close").click(function(eventObject) {
            $(eventObject.target.parentElement).hide();
        });

        var self = this;

        var alertsManager = params.alertsManager;
        var configurationApiClient = params.configurationApiClient;
        var vehicleMakeApiClient = params.vehicleMakeApiClient;

        var quoteOverrideValidator = new QuoteOverrideValidator();

        self.isLoadingRequest = ko.observable(false);

        function QuoteOverrideViewModel(override, parent) {

            var self = this;

            self.id = ko.observable(override.id);
            self.fromVehicleYear = ko.observable(override.fromVehicleYear);
            self.toVehicleYear = ko.observable(override.toVehicleYear);
            self.vehicleMake = ko.observable(override.vehicleMake);
            self.vehicleModel = ko.observable(override.vehicleModel);
            self.quote = ko.observable(override.quote);

            self.deleteOnClick = function(quoteOverride) {

                parent.isLoadingRequest(true);

                configurationApiClient.deleteQuoteOverride({

                    quoteOverrideId: quoteOverride.id(),

                    onSuccess: function (response, textStatus, jqXHR) {

                        console.log(response);

                        loadQuoteOverrides(response.quoteOverrides);

                        alertsManager.showSuccess(
                            $("#quote_override_alerts_container"),
                            "The Quote Override was deleted successfully"
                        );

                        parent.isLoadingRequest(false);
                    },
                    onFail: function (jqXHR, textStatus, error) {
                        alertsManager.onAjaxRequestFail(jqXHR, textStatus, error);
                        parent.isLoadingRequest(false);
                    }
                });
            };
        }

        self.quoteOverrides = ko.observableArray([]);

        function loadQuoteOverrides(overrides) {

            overrides = _.map(overrides, function(override) {
                return new QuoteOverrideViewModel(override, self);
            });

            self.quoteOverrides.removeAll();

            _.forEach(overrides, function(override) {
                self.quoteOverrides.push(override);
            });
        }

        loadQuoteOverrides(params.quoteOverrides);

        self.vehicleYears = _.concat('Any', params.vehicleYears);
        self.vehicleMakes = params.vehicleMakes;
        self.vehicleModels = ko.observableArray([]);

        self.selectedFromVehicleYear = ko.observable();
        self.selectedToVehicleYear = ko.observable();
        self.selectedVehicleMake = ko.observable();
        self.selectedVehicleModel = ko.observable();
        self.overrideQuote = ko.observable();

        self.vehicleMakeOnSelect = function() {
            loadVehicleModels();
        };

        function loadVehicleModels() {
            if(self.selectedVehicleMake()) {

                var getModelsParams = {
                    vehicleMake: self.selectedVehicleMake(),

                    onSuccess: function (response, textStatus, jqXHR) {
                        var models = _.map(response.models, function(value, index) {
                            return { id: value.name.toLowerCase(), name: value.name };
                        });

                        self.vehicleModels(_.concat({ id: 'Any', name: 'Any' }, models));
                    },
                    onFail: alertsManager.onAjaxRequestFail
                };

                vehicleMakeApiClient.getVehicleModelsForAllYears(getModelsParams);
            }
        }

        var serverValidationErrorAlertContainerId = "#quote_override_server_validation_error_container";
        var serverValidationErrorAlertId = "quote_override_server_validation_error_alert";
        var duplicatedQuoteOverrideErrorMessage = "There already exists an identical override";

        self.saveQuoteOverrideOnclick = function() {

            if (!quoteOverrideValidator.formIsValid()) {
                console.log("form invalid");
                return;
            }

            var quoteOverrideData = {
                fromVehicleYear: self.selectedFromVehicleYear(),
                toVehicleYear: self.selectedToVehicleYear(),
                vehicleMake: self.selectedVehicleMake(),
                vehicleModel: self.selectedVehicleModel(),
                quote: self.overrideQuote()
            };

            self.isLoadingRequest(true);

            configurationApiClient.createQuoteOverride({

                quoteOverrideData: quoteOverrideData,

                onSuccess: function (response, textStatus, jqXHR) {
                    console.log(response);

                    loadQuoteOverrides(response.quoteOverrides);

                    alertsManager.showSuccess(
                        $("#quote_override_alerts_container"),
                        "The Quote Override was created successfully"
                    );

                    self.isLoadingRequest(false);
                },
                onFail: function (jqXHR, textStatus, error) {
                    if (jqXHR.status == 400) {
                        alertsManager.showDanger(
                            $(serverValidationErrorAlertContainerId),
                            duplicatedQuoteOverrideErrorMessage,
                            serverValidationErrorAlertId
                        );
                        console.log(jqXHR.responseJSON);
                    }
                    else {
                        alertsManager.onAjaxRequestFail(jqXHR, textStatus, error);
                    }

                    self.isLoadingRequest(false);
                }
            });
        };

    }

    return QuoteOverridesComponent;
});