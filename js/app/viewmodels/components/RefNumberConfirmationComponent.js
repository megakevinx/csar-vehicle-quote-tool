define('app/viewmodels/components/RefNumberConfirmationComponent',
[
    'knockout', 'jquery', 'lodash', 'bootstrap'
],
function (ko, $, _, bootstrap) {

    function RefNumberConfirmationComponent(params) {

        var self = this;

        self.referenceNumber = params.referenceNumber;
        self.getAnotherQuoteOnClick = params.getAnotherQuoteOnClick;
    }

    return RefNumberConfirmationComponent;
});