define('app/viewmodels/components/TransactionCreatedComponent',
[
    'knockout', 'jquery', 'lodash', 'bootstrap'
],
function (ko, $, _, bootstrap) {

    function TransactionCreatedComponent(params) {

        var self = this;

        var alertsManager = params.alertsManager;
        var quoteApiClient = params.quoteApiClient;

        self.calculatedQuote = params.calculatedQuote;
        self.referenceNumber = params.referenceNumber;
        self.contactPhoneNumber = params.contactPhoneNumber;
        self.isLoadingRequest = params.isLoadingRequest;

        self.goToNextStepOnClick = params.goToNextStepOnClick;
        self.getAnotherQuoteOnClick = params.getAnotherQuoteOnClick;
        var notifyCallbackVendorOnSuccess = params.notifyCallbackVendorOnSuccess;

        self.callMeBackOnClick = function () {
            self.isLoadingRequest(true);

            quoteApiClient.notifyCallbackVendor({

                referenceNumber: self.referenceNumber(),

                onSuccess: function (response, textStatus, jqXHR) {
                    console.log(response);
                    notifyCallbackVendorOnSuccess();
                    self.isLoadingRequest(false);
                },
                onFail: function (jqXHR, textStatus, error) {
                    self.isLoadingRequest(false);
                    alertsManager.onAjaxRequestFail(jqXHR, textStatus, error);
                }
            });
        };
    }

    return TransactionCreatedComponent;
});