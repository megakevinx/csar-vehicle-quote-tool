define('app/viewmodels/components/TransactionUpdatedComponent',
[
    'knockout', 'jquery', 'lodash', 'bootstrap'
],
function (ko, $, _, bootstrap) {

    function TransactionUpdatedComponent(params) {

        var self = this;

        self.referenceNumber = params.referenceNumber;
        self.getAnotherQuoteOnClick = params.getAnotherQuoteOnClick;
    }

    return TransactionUpdatedComponent;
});