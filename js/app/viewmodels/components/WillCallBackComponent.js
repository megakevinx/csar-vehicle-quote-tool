define('app/viewmodels/components/WillCallBackComponent',
[
    'knockout', 'jquery', 'lodash', 'bootstrap'
],
function (ko, $, _, bootstrap) {

    function WillCallBackComponent(params) {

        var self = this;

        self.referenceNumber = params.referenceNumber;
        self.getAnotherQuoteOnClick = params.getAnotherQuoteOnClick;
    }

    return WillCallBackComponent;
});