define('app/wizard/StepNavigator',
[
    'knockout', 'jquery', 'lodash'
],
function(ko, $, _) {
    
    function StepNavigator(params) {

        var stepsSequence = params.stepsSequence;
        var stepsWithNavigationForward = params.stepsWithNavigationForward;
        var stepsWithNavigationBackward = params.stepsWithNavigationBackward;
        var validatorsByStep = params.validatorsByStep;

        var onStepAdvanceValidate = params.onStepAdvanceValidate;

        var currentStep = ko.observable(params.initialStep);

        // Wizard step display/traversal logic
        var shouldDisplayPreviousStepButton = ko.pureComputed(function() {
            return _.includes(stepsWithNavigationBackward, currentStep());
        });

        var shouldDisplayNextStepButton = ko.pureComputed(function() {
            return _.includes(stepsWithNavigationForward, currentStep());
        });

        var shouldDisplayStepButtonsContainer = ko.pureComputed(function() {
            return (shouldDisplayPreviousStepButton() || shouldDisplayNextStepButton());
        });

        function goToPreviousStepOnClick() {

            var currentStepIndex = _.indexOf(stepsSequence, currentStep());
            var previousStep = stepsSequence[currentStepIndex - 1];

            showStep(previousStep);
        }

        function afterOnStepAdvanceValidate() {
            var currentStepIndex = _.indexOf(stepsSequence, currentStep());
            var nextStep = stepsSequence[currentStepIndex + 1];

            showStep(nextStep);
        }

        function goToNextStepOnClick() {

            var validator = validatorsByStep[currentStep()];

            if (validator.formIsValid()) {

                if (onStepAdvanceValidate) {
                    onStepAdvanceValidate(afterOnStepAdvanceValidate);
                }
                else {
                    afterOnStepAdvanceValidate();
                }
            }
        }

        function showStep(stepToShow) {
            window.scrollTo(0, 0);
            currentStep(stepToShow);
        }
        
        return {
            currentStep: currentStep,
            shouldDisplayPreviousStepButton: shouldDisplayPreviousStepButton,
            shouldDisplayNextStepButton: shouldDisplayNextStepButton,
            shouldDisplayStepButtonsContainer: shouldDisplayStepButtonsContainer,

            goToPreviousStepOnClick: goToPreviousStepOnClick,
            goToNextStepOnClick: goToNextStepOnClick,
            showStep: showStep
        };
    }

    return StepNavigator;
});