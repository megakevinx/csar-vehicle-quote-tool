<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'router' => [
        'routes' => [
            'home' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'vendor' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/vendors',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'vendor',
                    ],
                ],
            ],
            'sales' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/sales',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'sales',
                    ],
                ],
            ],
            'customer' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/customer',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'customer',
                    ],
                ],
            ],
            'coverage' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/coverage[/:action]',
                    'defaults' => [
                        'controller' => Controller\CoverageController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'admin' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/admin',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'admin',
                    ],
                ],
            ],
            'aboutus' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/about-us',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'aboutUs',
                    ],
                ],
            ],
            'terms' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/terms',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'terms',
                    ],
                ],
            ],
            'privacy' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/privacy',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'privacy',
                    ],
                ],
            ],
            'faq' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/faq',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'faq',
                    ],
                ],
            ],

            'api_faq' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/api/faq[/:id]',
                    'defaults' => [
                        'controller' => Controller\FaqController::class,
                    ],
                ],
            ],
            'vehicle_type' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/api/vehicle_type[/:id]',
                    'defaults' => [
                        'controller' => Controller\VehicleTypeController::class,
                    ],
                ],
            ],
            'vehicle' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/api/vehicle/:action',
                    'defaults' => [
                        'controller' => Controller\VehicleController::class,
                    ],
                ],
            ],
            'configuration' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/api/configuration',
                    'defaults' => [
                        'controller' => Controller\ConfigurationController::class,
                    ],
                ],
            ],
            'quote' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/api/quote[/:id]',
                    'defaults' => [
                        'controller' => Controller\QuoteController::class,
                    ],
                ],
            ],
            'quote-calculate' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/api/quote/calculate',
                    'defaults' => [
                        'controller' => Controller\QuoteController::class,
                        'action' => 'calculate'
                    ],
                ],
            ],
            'quote-customer' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/api/quote/customer',
                    'defaults' => [
                        'controller' => Controller\QuoteController::class,
                        'action' => 'customer'
                    ],
                ],
            ],
            'quote-location' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/api/quote/location/:id',
                    'defaults' => [
                        'controller' => Controller\QuoteController::class,
                        'action' => 'location'
                    ],
                ],
            ],
            'notification' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/api/notification',
                    'defaults' => [
                        'controller' => Controller\NotificationController::class,
                    ],
                ],
            ],
            'notification-callback-customer' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/api/notification/callback-customer',
                    'defaults' => [
                        'controller' => Controller\NotificationController::class,
                        'action' => 'callback-customer'
                    ],
                ],
            ],
            'notification-still-shopping' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/api/notification/still-shopping',
                    'defaults' => [
                        'controller' => Controller\NotificationController::class,
                        'action' => 'still-shopping'
                    ],
                ],
            ],
            'notification-not-ready' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/api/notification/not-ready',
                    'defaults' => [
                        'controller' => Controller\NotificationController::class,
                        'action' => 'not-ready'
                    ],
                ],
            ],
            'zip-validation' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/api/zip-validation/:id',
                    'defaults' => [
                        'controller' => Controller\ZipValidationController::class,
                    ],
                ],
            ],
            'quote-calculation-rules' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/api/quote-calculation-rules',
                    'defaults' => [
                        'controller' => Controller\QuoteCalculationRuleController::class,
                    ],
                ],
            ],
            'quote-calculation-rules-change' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/api/quote-calculation-rules/change',
                    'defaults' => [
                        'controller' => Controller\QuoteCalculationRuleController::class,
                        'action' => 'change'
                    ],
                ],
            ],
            'quote-override' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/api/quote-override[/:id]',
                    'defaults' => [
                        'controller' => Controller\QuoteOverrideController::class,
                    ],
                ],
            ],

            'login' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/login',
                    'defaults' => [
                        'controller' => Controller\AuthController::class,
                        'action'     => 'login',
                    ],
                ],
            ],
            'logout' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/logout',
                    'defaults' => [
                        'controller' => Controller\AuthController::class,
                        'action'     => 'logout',
                    ],
                ],
            ],
            'register' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/register',
                    'defaults' => [
                        'controller' => Controller\AuthController::class,
                        'action'     => 'register',
                    ],
                ],
            ],
            'register-success' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/register-success',
                    'defaults' => [
                        'controller' => Controller\AuthController::class,
                        'action'     => 'registerSuccess',
                    ],
                ],
            ],
            'reset-password' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/reset-password',
                    'defaults' => [
                        'controller' => Controller\AuthController::class,
                        'action'     => 'resetPassword',
                    ],
                ],
            ],
            'password-reset-success' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/password-reset-success',
                    'defaults' => [
                        'controller' => Controller\AuthController::class,
                        'action'     => 'passwordResetSuccess',
                    ],
                ],
            ],
            'set-password' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/set-password',
                    'defaults' => [
                        'controller' => Controller\AuthController::class,
                        'action'     => 'setPassword',
                    ],
                ],
            ],
            'set-password-success' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/set-password-success',
                    'defaults' => [
                        'controller' => Controller\AuthController::class,
                        'action'     => 'setPasswordSuccess',
                    ],
                ],
            ],
        ],
    ],

    'controllers' => [
        'factories' => [
        ],
    ],

    'view_manager' => [
        // 'base_path' => '/public/',
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
];
