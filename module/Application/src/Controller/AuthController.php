<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
//use Zend\Authentication\Result;
use Zend\Uri\Uri;

use Application\Controller\Form\LoginForm;
use Application\Controller\Form\UserForm;
use Application\Controller\Form\PasswordResetForm;
use Application\Controller\Form\PasswordChangeForm;

use Application\Service\AuthService;
use Application\Service\NotificationService;

class AuthController extends AbstractActionController
{
    private $authService;
    private $notificationService;

    public function __construct(AuthService $authService, NotificationService $notificationService)
    {
        $this->authService = $authService;
        $this->notificationService = $notificationService;
    }

    public function loginAction()
    {
        // Create login form
        $form = new LoginForm(); 

        // Store login status.
        $isLoginError = false;

        // Check if user has submitted the form
        if ($this->getRequest()->isPost())
        {
            // Fill in the form with POST data
            $data = $this->params()->fromPost();

            $form->setData($data);

            // Validate form
            if($form->isValid())
            {
                // Get filtered and validated data
                $data = $form->getData();

                // Perform login attempt.
                $result = $this->authService->login($data['email'], $data['password'], $data['remember_me']);

                // Check result.
                if ($result->isSuccess)
                {
                    // TODO: Add more redirects for each role
                    if ($result->identity->role === 'Vendor')
                    {
                        return $this->redirect()->toRoute('vendor');
                    }
                    else if ($result->identity->role === 'StreetSalesTeam')
                    {
                        return $this->redirect()->toRoute('sales');
                    }
                    else if ($result->identity->role === 'Administrator')
                    {
                        return $this->redirect()->toRoute('admin');
                    }
                    else
                    {
                        $isLoginError = true;
                    }
                }
                else
                {
                    $isLoginError = true;
                }
            }
            else
            {
                $isLoginError = true;
            }
        }

        return new ViewModel([
            'form' => $form,
            'isLoginError' => $isLoginError
        ]);
    }
    
    /**
     * The "logout" action performs logout operation.
     */
    public function logoutAction() 
    {
        if ($this->identity())
        {
            $this->authService->logout();
        }

        return $this->redirect()->toRoute('login');
    }

    public function registerAction()
    {
        // Create user form
        $form = new UserForm();

        // Check if user has submitted the form
        if ($this->getRequest()->isPost())
        {
            // Fill in the form with POST data
            $data = $this->params()->fromPost();

            $form->setData($data);

            // Validate form
            if ($form->isValid())
            {
                // Get filtered and validated data
                $data = $form->getData();

                // Add user.
                $result = $this->authService->register($data);

                if ($result->isSuccess) {
                    return $this->redirect()->toRoute('register-success');
                }
                else
                {
                    return new ViewModel([
                        'form' => $form,
                        'isError' => true,
                        'errorMessage' => $result->errorMessage
                    ]);
                }
            }
            else
            {
                return new ViewModel([
                    'form' => $form,
                    'isError' => true,
                    'errorMessage' => null
                ]);
            }
        }

        return new ViewModel([
            'form' => $form,
            'isError' => false
        ]);
    }

    public function registerSuccessAction()
    {
        return new ViewModel();
    }

    /**
     * This action displays the "Reset Password" page.
     */
    public function resetPasswordAction()
    {
        // Create form
        $form = new PasswordResetForm();

        // Check if user has submitted the form
        if ($this->getRequest()->isPost()) {

            // Fill in the form with POST data
            $data = $this->params()->fromPost();

            $form->setData($data);

            // Validate form
            if ($form->isValid()) {

                $data = $form->getData($data);

                $httpHost = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : 'localhost';
                $setPasswordUrl = $httpHost . $this->url()->fromRoute('set-password');

                $result = $this->authService->beginResetPassword($data);

                if ($result->isSuccess) {
                    $this->notificationService->createPasswordResetNotification($result->updatedUser, $setPasswordUrl);
                    return $this->redirect()->toRoute('password-reset-success');
                }
                else {
                    return new ViewModel([
                        'form' => $form,
                        'isError' => true,
                        'errorMessage' => $result->errorMessage
                    ]);
                }
            }
            else {

                $e = $form->get('email');

                return new ViewModel([
                    'form' => $form,
                    'isError' => true,
                    'errorMessage' => null
                ]);
            }
        }
        else {
            return new ViewModel([
                'form' => $form,
                'isError' => false,
            ]);
        }
    }

    public function passwordResetSuccessAction()
    {
        return new ViewModel();
    }

    public function getInvalidTokenResult($form)
    {
        return new ViewModel([
            'form' => $form,
            'isError' => true,
            'errorMessage' => "Invalid token. Please try reseting your password again."
        ]);
    }

    /**
     * This action displays the "Reset Password" page. 
     */
    public function setPasswordAction()
    {
        $token = $this->params()->fromQuery('token', null);

        $form = new PasswordChangeForm('reset');

        if (!$token) {
            return $this->getInvalidTokenResult($form);
        }

        if ($this->getRequest()->isPost()) {

            $data = $this->params()->fromPost();

            $form->setData($data);

            if ($form->isValid()) {

                $data = $form->getData();

                $result = $this->authService->setNewPasswordByToken($token, $data);

                if ($result->isSuccess) {
                    return $this->redirect()->toRoute('set-password-success');
                }
                else {
                    return $this->getInvalidTokenResult($form);
                }
            }
            else {
                return new ViewModel([
                    'form' => $form,
                    'isError' => true,
                    'errorMessage' => null
                ]);
            }
        }

        return new ViewModel([
            'form' => $form,
            'isError' => false,
        ]);
    }

    public function setPasswordSuccessAction()
    {
        return new ViewModel();
    }
}
