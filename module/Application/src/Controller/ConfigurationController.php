<?php

namespace Application\Controller;

use Zend\View\Model\JsonModel;

use Application\Controller\BaseRestfulController;

use Application\Controller\Form\AppSettingsForm;

use Application\Service\ConfigurationService;

use Application\Repository\VehicleTypeRepository;
use Application\Repository\VehicleYearRepository;
use Application\Repository\SettingRepository;
use Application\Repository\QuoteAmountRepository;
use Application\Repository\QuoteAmountOverrideRepository;
use Application\Repository\FaqRepository;

class ConfigurationController extends BaseRestfulController
{
    private $configurationService;

    private $vehicleTypeRepository;
    private $vehicleYearRepository;
    private $settingRepository;
    private $quoteAmountRepository;
    private $quoteAmountOverrideRepository;
    private $faqRepository;

    public function __construct(
        ConfigurationService $configurationService,
        VehicleTypeRepository $vehicleTypeRepository,
        VehicleYearRepository $vehicleYearRepository,
        SettingRepository $settingRepository,
        QuoteAmountRepository $quoteAmountRepository,
        QuoteAmountOverrideRepository $quoteAmountOverrideRepository,
        FaqRepository $faqRepository)
    {
        $this->configurationService = $configurationService;

        $this->vehicleTypeRepository = $vehicleTypeRepository;
        $this->vehicleYearRepository = $vehicleYearRepository;
        $this->settingRepository = $settingRepository;
        $this->quoteAmountRepository = $quoteAmountRepository;
        $this->quoteAmountOverrideRepository = $quoteAmountOverrideRepository;
        $this->faqRepository = $faqRepository;
    }

    // GET /configuration
    public function getList()
    {
        return new JsonModel([
            'vehicleYears' => $this->vehicleYearRepository->getAll(),
            'vehicleTypes' => $this->vehicleTypeRepository->getAll(),
            'vehicleDropOffAddress' => $this->settingRepository->get('VEHICLE_DROP_OFF_ADDRESS')->value,
            'contactPhoneNumber' => $this->settingRepository->get('CSAR_CONTACT_PHONE_NUMBER')->value,
            'quoteCalculationRules' => $this->quoteAmountRepository->getAll(),
            'quoteOverrides' => $this->quoteAmountOverrideRepository->getAll(),
            'faqs' => $this->faqRepository->getAll()
        ]);
    }

    // POST api/configuration
    public function create($data)
    {
        if (!$this->userIsRole('Administrator'))
        {
            return $this->notAuthorized();
        }

        $form = new AppSettingsForm();
        $form->setData($data);

        if ($form->isValid())
        {
            $data = $form->getData();

            $result = $this->configurationService->saveAppSettings($data);

            return new JsonModel(['result' => $result]);
        }
        else
        {
            return $this->badRequest("Input data invalid");
        }
    }
}
