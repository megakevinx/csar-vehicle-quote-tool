<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Http\Response\Stream;
use Zend\Http\Headers;

use Application\Service\LocationService;

class CoverageController extends AbstractActionController
{
    private $locationService;

    public function __construct(LocationService $locationService)
    {
        $this->locationService = $locationService;
    }

    public function indexAction()
    {
        return new ViewModel([
            'coverageAreas' => $this->locationService->getCoverageAreas()
        ]);
    }

    private function getAreasAsCsv(array $coverageAreas)
    {
        $lineSeparator = "\n";
        $responseContent = '"ZipCode","County"' . $lineSeparator;

        foreach ($coverageAreas as $zip)
        {
            $responseContent = $responseContent . vsprintf('"%s","%s"' . $lineSeparator, [ $zip->code, $zip->county->name ]);
        }

        return $responseContent;
    }

    public function downloadAction()
    {
        $coverageAreas = $this->locationService->getCoverageAreas();

        $responseContent = $this->getAreasAsCsv($coverageAreas);

        $response = $this->getResponse();

        $response->setStatusCode(200);
        $response->setContent($responseContent);
        $response
            ->getHeaders()
            ->addHeaderLine('Content-Disposition', 'attachment; filename="coverage.csv"')
            ->addHeaderLine('Content-Type', 'application/octet-stream')
            ->addHeaderLine('Content-Length', mb_strlen($responseContent));
    
        return $response;
    }
}
