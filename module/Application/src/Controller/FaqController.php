<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

use Application\Controller\Form\UpdateFaqForm;
use Application\Controller\Form\CreateFaqForm;

use Application\Repository\FaqRepository;

use Application\Model\Faq;

class FaqController extends BaseRestfulController
{
    private $repository;

    public function __construct(FaqRepository $repository)
    {
        $this->repository = $repository;
    }

    // POST api/faq
    public function create($data)
    {
        if (!$this->userIsRole('Administrator')) {
            return $this->notAuthorized();
        }

        $form = new CreateFaqForm();
        $form->setData($data);

        if ($form->isValid()) {
            $data = $form->getData();

            $faqToCreate = new Faq();
            $faqToCreate->question = $data["question"];
            $faqToCreate->answer = $data["answer"];

            $createdFaq = $this->repository->save($faqToCreate);

            return new JsonModel([
                'createdFaq' => $createdFaq,
                'faqs' => $this->repository->getAll()
            ]);
        }
        else {
            return $this->badRequest("Input data invalid");
        }
    }

    // PUT api/faq/{id}
    public function update($faqId, $data)
    {
        if (!$this->userIsRole('Administrator')) {
            return $this->notAuthorized();
        }

        $form = new UpdateFaqForm();
        $form->setData($data);

        if ($form->isValid()) {
            $data = $form->getData();

            $faqToUpdate = $this->repository->get($faqId);

            if ($faqToUpdate) {

                $faqToUpdate->question = $data["question"];
                $faqToUpdate->answer = $data["answer"];

                $updatedFaq = $this->repository->update($faqToUpdate);

                return new JsonModel([
                    'updatedFaq' => $updatedFaq
                ]);
            }
            else {
                return $this->badRequest("No FAQ found");
            }
        }
        else {
            return $this->badRequest("Input data invalid");
        }
    }

    // DELETE api/faq/{id}
    public function delete($faqId)
    {
        if (!$this->userIsRole('Administrator')) {
            return $this->notAuthorized();
        }

        $faqToDelete = $this->repository->get($faqId);

        if ($faqToDelete) {

            $deletedFaq = $this->repository->delete($faqToDelete->id);

            return new JsonModel([
                'deletedFaq' => $deletedFaq,
                'faqs' => $this->repository->getAll()
            ]);
        }
        else {
            return $this->badRequest("No FAQ found");
        }
    }
}
