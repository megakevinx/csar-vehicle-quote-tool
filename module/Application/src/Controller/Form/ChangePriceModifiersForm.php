<?php

namespace Application\Controller\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

class ChangePriceModifiersForm extends Form
{
    public function __construct()
    {
        parent::__construct('change-price-modifiers-form');

        $this->addElements();
        $this->addInputFilter();
    }

    protected function addElements() 
    {
        $this->add(['name' => 'basePriceChange']);
    }

    private function addInputFilter() 
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name' => 'basePriceChange',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
                [
                    'name' => 'PregReplace',
                    'options' => [
                        'pattern' => '/,/',
                        'replacement' => ''
                    ]
                ]
            ],
            'validators' => [
                [
                    'name' => 'IsInt',
                    'options' => [
                        'locale' => 'en'
                    ],
                ],
                [
                    'name' => 'LessThan',
                    'options' => [ 'max' => 100, 'inclusive' => true ],
                ],
                [
                    'name' => 'GreaterThan',
                    'options' => [ 'min' => -100, 'inclusive' => true ],
                ],
            ]
        ]);
    }
}