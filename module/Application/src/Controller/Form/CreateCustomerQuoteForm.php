<?php

namespace Application\Controller\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

class CreateCustomerQuoteForm extends Form
{
    public function __construct()
    {
        parent::__construct('create-customer-quote-form');

        $this->addElements();
        $this->addInputFilter();
    }

    protected function addElements() 
    {
        $this->add(['name' => 'vehicleYear']);
        $this->add(['name' => 'vehicleMake']);
        $this->add(['name' => 'vehicleModel']);
        $this->add(['name' => 'vehicleTypeId']);

        $this->add(['name' => 'vehicleHasKeys']);
        $this->add(['name' => 'vehicleHasTitle']);
        $this->add(['name' => 'vehicleWheelsDescription']);
        $this->add(['name' => 'vehicleWillBeDelivered']);
        $this->add(['name' => 'vehicleZipCode']);

        $this->add(['name' => 'sellerName']);
        $this->add(['name' => 'sellerPhoneNumber']);
        $this->add(['name' => 'sellerEmail']);
    }

    private function addInputFilter() 
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name' => 'vehicleYear',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'GreaterThan',
                    'options' => [ 'min' => 1990, 'inclusive' => true ],
                ],
                [
                    'name' => 'IsInt'
                ]
            ],
        ]);

        $inputFilter->add([
            'name' => 'vehicleMake',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [ 'min' => 1, 'max' => 25 ],
                ],
            ]
        ]);

        $inputFilter->add([
            'name' => 'vehicleModel',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [ 'min' => 1, 'max' => 25 ],
                ],
            ]
        ]);

        $inputFilter->add([
            'name' => 'vehicleTypeId',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'Digits'
                ]
            ],
        ]);

        $inputFilter->add([
            'name' => 'vehicleHasKeys',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'InArray',
                    'options' => [ 'haystack' => ['yes', 'no'] ],
                ]
            ],
        ]);

        $inputFilter->add([
            'name' => 'vehicleHasTitle',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'InArray',
                    'options' => [ 'haystack' => ['yes', 'no'] ],
                ]
            ],
        ]);

        $inputFilter->add([
            'name' => 'vehicleWheelsDescription',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'InArray',
                    'options' => [ 'haystack' => ['steel', 'aluminum', 'no wheels'] ],
                ]
            ],
        ]);

        $inputFilter->add([
            'name' => 'vehicleWillBeDelivered',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'InArray',
                    'options' => [ 'haystack' => ['yes', 'no'] ],
                ]
            ],
        ]);

        $inputFilter->add([
            'name' => 'vehicleZipCode',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'Digits'
                ]
            ],
        ]);

        $inputFilter->add([
            'name' => 'sellerName',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [ 'min' => 1, 'max' => 45 ],
                ],
            ]
        ]);

        $inputFilter->add([
            'name' => 'sellerPhoneNumber',
            'required' => false,
            'filters' => [
                ['name' => 'StringTrim'],
                [
                    'name' => 'PregReplace',
                    'options' => [
                        'pattern' => '/ /',
                        'replacement' => ''
                    ]
                ]
            ],
            'validators' => [
                [
                    'name' => 'StringLength',
                    'options' => [ 'min' => 10, 'max' => 15 ],
                ],
                [
                    'name' => 'Regex',
                    'options' => [ 'pattern' => '/^(\+?1-?)?(\([2-9]([02-9]\d|1[02-9])\)|[2-9]([02-9]\d|1[02-9]))-?[2-9]([02-9]\d|1[02-9])-?\d{4}$/' ],
                ],
            ]
        ]);

        $inputFilter->add([
            'name' => 'sellerEmail',
            'required' => false,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [ 'min' => 10, 'max' => 40 ],
                ],
                [
                    'name' => 'EmailAddress',
                    'options' => [
                        'allow' => \Zend\Validator\Hostname::ALLOW_DNS,
                        'useMxCheck'    => false
                    ],
                ],
            ]
        ]);
    }
}