<?php

namespace Application\Controller\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

class GetVehicleTypeForm extends Form
{
    public function __construct()
    {
        parent::__construct('get-vehicle-type-form');

        $this->addElements();
        $this->addInputFilter();
    }

    protected function addElements()
    {
        $this->add(['name' => 'year']);
        $this->add(['name' => 'make']);
        $this->add(['name' => 'model']);
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name' => 'year',
            'required' => false,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'GreaterThan',
                    'options' => [ 'min' => 1990, 'inclusive' => true ],
                ],
                [
                    'name' => 'IsInt'
                ]
            ],
        ]);

        $inputFilter->add([
            'name' => 'make',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [ 'min' => 1, 'max' => 25 ],
                ],
            ]
        ]);

        $inputFilter->add([
            'name' => 'model',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [ 'min' => 1, 'max' => 25 ],
                ],
            ]
        ]);
    }
}