<?php

namespace Application\Controller\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

class QuoteCalculationRuleForm extends Form
{
    public function __construct()
    {
        parent::__construct('quote-calculation-rule-form');

        $this->addElements();
        $this->addInputFilter();
    }

    protected function addElements() 
    {
        $this->add(['name' => 'id']);
        $this->add(['name' => 'priceModifier']);
    }

    private function addInputFilter() 
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name' => 'id',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim']
            ],
            'validators' => [
                [
                    'name' => 'IsInt',
                    'options' => [
                        'locale' => 'en'
                    ],
                ],
                [
                    'name' => 'GreaterThan',
                    'options' => [ 'min' => 1, 'inclusive' => true ],
                ],
            ]
        ]);

        $inputFilter->add([
            'name' => 'priceModifier',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
                [
                    'name' => 'PregReplace',
                    'options' => [
                        'pattern' => '/,/',
                        'replacement' => ''
                    ]
                ]
            ],
            'validators' => [
                [
                    'name' => 'IsInt',
                    'options' => [
                        'locale' => 'en'
                    ],
                ],
                [
                    'name' => 'LessThan',
                    'options' => [ 'max' => 500, 'inclusive' => true ],
                ],
                [
                    'name' => 'GreaterThan',
                    'options' => [ 'min' => -500, 'inclusive' => true ],
                ],
            ]
        ]);
    }
}