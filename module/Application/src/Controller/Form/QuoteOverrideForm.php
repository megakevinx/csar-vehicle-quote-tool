<?php

namespace Application\Controller\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

class QuoteOverrideForm extends Form
{
    const VEHICLE_YEAR_WILDCARD = 'Any';

    public function __construct()
    {
        parent::__construct('create-quote-form');

        $this->addElements();
        $this->addInputFilter();
    }

    protected function addElements() 
    {
        $this->add(['name' => 'fromVehicleYear']);
        $this->add(['name' => 'toVehicleYear']);
        $this->add(['name' => 'vehicleMake']);
        $this->add(['name' => 'vehicleModel']);
        $this->add(['name' => 'quote']);
    }

    private function validateVehicleYear($year, $yearToCompare, $comparerCallback)
    {
        if ((string)$year === self::VEHICLE_YEAR_WILDCARD)
        {
            return true;
        }
        else
        {
            $isIntValidator = new \Zend\I18n\Validator\IsInt();
            $greaterThanValidator = new \Zend\Validator\GreaterThan([ 'min' => 1990, 'inclusive' => true ]);

            if ($isIntValidator->isValid($year) && $greaterThanValidator->isValid($year))
            {
                if ($yearToCompare === self::VEHICLE_YEAR_WILDCARD)
                {
                    return true;
                }
                else
                {
                    return $comparerCallback($year, $yearToCompare);
                }
            }
            else
            {
                return false;
            }
        }
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name' => 'fromVehicleYear',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'Callback',
                    'options' =>
                    [
                        'callback' => function($value, $formValues)
                        {
                            return $this->validateVehicleYear(
                                $value,
                                $formValues["toVehicleYear"],
                                function($fromYear, $toYear)
                                {
                                    return $fromYear <= $toYear;
                                }
                            );
                        }
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name' => 'toVehicleYear',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'Callback',
                    'options' =>
                    [
                        'callback' => function($value, $formValues)
                        {
                            return $this->validateVehicleYear(
                                $value,
                                $formValues["fromVehicleYear"],
                                function($toYear, $formYear)
                                {
                                    return $toYear >= $formYear;
                                }
                            );
                        }
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name' => 'vehicleMake',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [ 'min' => 1, 'max' => 25 ],
                ],
            ]
        ]);

        $inputFilter->add([
            'name' => 'vehicleModel',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [ 'min' => 1, 'max' => 25 ],
                ],
            ]
        ]);

        $inputFilter->add([
            'name' => 'quote',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
                [
                    'name' => 'PregReplace',
                    'options' => [
                        'pattern' => '/,/',
                        'replacement' => ''
                    ]
                ],
            ],
            'validators' => [
                [
                    'name' => 'IsFloat',
                    'options' => [
                        'locale' => 'en'
                    ],
                ],
                [
                    'name' => 'LessThan',
                    'options' => [ 'max' => 999.99, 'inclusive' => true ],
                ],
                [
                    'name' => 'GreaterThan',
                    'options' => [ 'min' => 0, 'inclusive' => true ],
                ],
            ]
        ]);

    }
}