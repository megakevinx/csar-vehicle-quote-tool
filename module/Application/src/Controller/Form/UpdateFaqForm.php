<?php

namespace Application\Controller\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

class UpdateFaqForm extends Form
{
    public function __construct()
    {
        parent::__construct('update-faq-form');

        $this->addElements();
        $this->addInputFilter();
    }

    protected function addElements() 
    {
        $this->add(['name' => 'id']);
        $this->add(['name' => 'question']);
        $this->add(['name' => 'answer']);
    }

    private function addInputFilter() 
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name' => 'id',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'Digits'
                ]
            ]
        ]);

        $inputFilter->add([
            'name' => 'question',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
                ['name' => 'HtmlEntities'],
            ]
        ]);

        $inputFilter->add([
            'name' => 'answer',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
                ['name' => 'HtmlEntities'],
            ]
        ]);
    }
}