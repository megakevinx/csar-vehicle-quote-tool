<?php

namespace Application\Controller\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

class UpdateQuoteForm extends Form
{
    public function __construct()
    {
        parent::__construct('update-quote-form');

        $this->addElements();
        $this->addInputFilter();
    }

    protected function addElements() 
    {
        $this->add(['name' => 'vehicleStreetAddress']);
        $this->add(['name' => 'vehicleCity']);
        $this->add(['name' => 'sellerName']);
        $this->add(['name' => 'sellerPhoneNumber']);
        $this->add(['name' => 'sellerEmail']);
        $this->add(['name' => 'buyerPayout']);
    }

    private function addInputFilter() 
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name' => 'vehicleStreetAddress',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [ 'min' => 1, 'max' => 75 ],
                ],
            ]
        ]);

        $inputFilter->add([
            'name' => 'vehicleCity',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [ 'min' => 1, 'max' => 45 ],
                ],
            ]
        ]);

        $inputFilter->add([
            'name' => 'sellerName',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [ 'min' => 1, 'max' => 45 ],
                ],
            ]
        ]);

        $inputFilter->add([
            'name' => 'sellerPhoneNumber',
            'required' => false,
            'filters' => [
                ['name' => 'StringTrim'],
                [
                    'name' => 'PregReplace',
                    'options' => [
                        'pattern' => '/ /',
                        'replacement' => ''
                    ]
                ]
            ],
            'validators' => [
                [
                    'name' => 'StringLength',
                    'options' => [ 'min' => 10, 'max' => 15 ],
                ],
                [
                    'name' => 'Regex',
                    'options' => [ 'pattern' => '/^(\+?1-?)?(\([2-9]([02-9]\d|1[02-9])\)|[2-9]([02-9]\d|1[02-9]))-?[2-9]([02-9]\d|1[02-9])-?\d{4}$/' ],
                ],
            ]
        ]);

        $inputFilter->add([
            'name' => 'sellerEmail',
            'required' => false,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [ 'min' => 10, 'max' => 40 ],
                ],
                [
                    'name' => 'EmailAddress',
                    'options' => [
                        'allow' => \Zend\Validator\Hostname::ALLOW_DNS,
                        'useMxCheck'    => false
                    ],
                ],
            ]
        ]);

        $inputFilter->add([
            'name' => 'buyerPayout',
            'required' => true,
            'filters' => [
                [
                    'name' => 'StringTrim'
                ],
                [
                    'name' => 'PregReplace',
                    'options' => [
                        'pattern' => '/,/',
                        'replacement' => ''
                    ]
                ]
            ],
            'validators' => [
                [
                    'name' => 'IsFloat',
                    'options' => [
                        'locale' => 'en'
                    ],
                ],
                [
                    'name' => 'LessThan',
                    'options' => [ 'max' => 999.99, 'inclusive' => true ],
                ],
            ]
        ]);
    }
}