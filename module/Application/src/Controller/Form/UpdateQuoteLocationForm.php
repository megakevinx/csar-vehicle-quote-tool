<?php

namespace Application\Controller\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

class UpdateQuoteLocationForm extends Form
{
    public function __construct()
    {
        parent::__construct('update-quote-location-form');

        $this->addElements();
        $this->addInputFilter();
    }

    protected function addElements() 
    {
        $this->add(['name' => 'vehicleStreetAddress']);
        $this->add(['name' => 'vehicleCity']);
    }

    private function addInputFilter() 
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name' => 'vehicleStreetAddress',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [ 'min' => 1, 'max' => 75 ],
                ],
            ]
        ]);

        $inputFilter->add([
            'name' => 'vehicleCity',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [ 'min' => 1, 'max' => 45 ],
                ],
            ]
        ]);
    }
}