<?php

namespace Application\Controller\Form;

use Zend\Form\Form;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilter;

/**
 * This form is used to collect user's email, full name, password and status. The form 
 * can work in two scenarios - 'create' and 'update'. In 'create' scenario, user
 * enters password, in 'update' scenario he/she doesn't enter password.
 */
class UserForm extends Form
{
    const PAYMENT_METHODS = [
        'Check' => 'Check',
        'ACH' => 'ACH',
        'Other' => 'Other'
    ];

    public function __construct()
    {
        // Define form name
        parent::__construct('user-form');

        // Set POST method for this form
        $this->setAttribute('method', 'post');

        $this->addElements();
        $this->addInputFilter();
    }

    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements() 
    {
        // Add "email" field
        $this->add([
            'type'  => 'text',
            'name' => 'email',
            'options' => [
                'label' => 'E-mail',
            ],
        ]);

        // Add "first_name" field
        $this->add([
            'type'  => 'text',
            'name' => 'first_name',
            'options' => [
                'label' => 'First Name',
            ],
        ]);

        // Add "last_name" field
        $this->add([
            'type'  => 'text',
            'name' => 'last_name',
            'options' => [
                'label' => 'Last Name',
            ],
        ]);

        // Add "company_name" field
        $this->add([
            'type'  => 'text',
            'name' => 'company_name',
            'options' => [
                'label' => 'Company Name',
            ],
        ]);

        // Add "company_address" field
        $this->add([
            'type'  => 'text',
            'name' => 'company_address',
            'options' => [
                'label' => 'Company Address',
            ],
        ]);

        // Add "phone_number" field
        $this->add([
            'type'  => 'text',
            'name' => 'phone_number',
            'options' => [
                'label' => 'Phone Number',
            ],
        ]);

        // Add "payment_method" field
        $this->add([
            'type'  => 'Zend\Form\Element\Select',
            'name' => 'payment_method',
            'options' => [
                'label' => 'Preferred Payment Method',
                'empty_option' => '- Select -',
                'value_options' => self::PAYMENT_METHODS
            ],
        ]);

        // Add "password" field
        $this->add([
            'type'  => 'password',
            'name' => 'password',
            'options' => [
                'label' => 'Password',
            ],
        ]);

        // Add "confirm_password" field
        $this->add([
            'type'  => 'password',
            'name' => 'confirm_password',
            'options' => [
                'label' => 'Confirm password',
            ],
        ]);

        // Add the Submit button
        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Register'
            ],
        ]);
    }

    /**
     * This method creates input filter (used for form filtering/validation).
     */
    private function addInputFilter() 
    {
        // Create main input filter
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        // Add input for "email" field
        $inputFilter->add([
            'name'     => 'email',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'break_chain_on_failure' => true,
                    'options' => [
                        'messages' => [
                            \Zend\Validator\NotEmpty::IS_EMPTY => 'Please enter your Email' 
                        ],
                    ],
                ],
                [
                    'name'    => 'StringLength',
                    'options' => [
                        'min' => 4,
                        'max' => 128,
                        'messages' => [
                            \Zend\Validator\StringLength::TOO_LONG => 'Please enter an Email between 4 to 128 characters',
                            \Zend\Validator\StringLength::TOO_SHORT => 'Please enter an Email between 4 to 128 characters'
                        ],
                    ],
                ],
                [
                    'name' => 'EmailAddress',
                    'options' => [
                        'allow' => \Zend\Validator\Hostname::ALLOW_DNS,
                        'useMxCheck'    => false,
                        'message' => 'Please enter a valid Email address'
                    ],
                ],
            ],
        ]);
        
        // Add input for "full_name" field
        $inputFilter->add([
            'name'     => 'first_name',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'break_chain_on_failure' => true,
                    'options' => [
                        'messages' => [
                            \Zend\Validator\NotEmpty::IS_EMPTY => 'Please enter your First Name'
                        ],
                    ],
                ],
                [
                    'name'    => 'StringLength',
                    'options' => [
                        'min' => 1,
                        'max' => 45,
                        'messages' => [
                            \Zend\Validator\StringLength::TOO_LONG => 'Please enter a First Name shorter than 45 characters',
                            \Zend\Validator\StringLength::TOO_SHORT => 'Please enter your First Name'
                        ],
                    ],
                ],
            ],
        ]);

        // Add input for "full_name" field
        $inputFilter->add([
            'name'     => 'last_name',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'break_chain_on_failure' => true,
                    'options' => [
                        'messages' => [
                            \Zend\Validator\NotEmpty::IS_EMPTY => 'Please enter your Last Name'
                        ],
                    ],
                ],
                [
                    'name'    => 'StringLength',
                    'options' => [
                        'min' => 1,
                        'max' => 45,
                        'messages' => [
                            \Zend\Validator\StringLength::TOO_LONG => 'Please enter a Last Name shorter than 45 characters',
                            \Zend\Validator\StringLength::TOO_SHORT => 'Please enter your Last Name'
                        ],
                    ],
                ],
            ],
        ]);

        // Add input for "company_name" field
        $inputFilter->add([
            'name'     => 'company_name',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'break_chain_on_failure' => true,
                    'options' => [
                        'messages' => [
                            \Zend\Validator\NotEmpty::IS_EMPTY => 'Please enter your Company Name'
                        ],
                    ],
                ],
                [
                    'name'    => 'StringLength',
                    'options' => [
                        'min' => 1,
                        'max' => 45,
                        'messages' => [
                            \Zend\Validator\StringLength::TOO_LONG => 'Please enter a Company Name shorter than 45 characters',
                            \Zend\Validator\StringLength::TOO_SHORT => 'Please enter your Company Name'
                        ],
                    ],
                ],
            ],
        ]);

        // Add input for "company_address" field
        $inputFilter->add([
            'name'     => 'company_address',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'break_chain_on_failure' => true,
                    'options' => [
                        'messages' => [
                            \Zend\Validator\NotEmpty::IS_EMPTY => 'Please enter your Company Address'
                        ],
                    ],
                ],
                [
                    'name'    => 'StringLength',
                    'options' => [
                        'min' => 1,
                        'max' => 75,
                        'messages' => [
                            \Zend\Validator\StringLength::TOO_LONG => 'Please enter a Company Address shorter than 75 characters',
                            \Zend\Validator\StringLength::TOO_SHORT => 'Please enter your Company Address'
                        ],
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name' => 'phone_number',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
                [
                    'name' => 'PregReplace',
                    'options' => [
                        'pattern' => '/ /',
                        'replacement' => ''
                    ]
                ]
            ],
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'break_chain_on_failure' => true,
                    'options' => [
                        'message' => 'Please enter a Phone Number',
                    ],
                ],
                [
                    'name' => 'Regex',
                    'options' => [
                        'pattern' => '/^(\+?1-?)?(\([2-9]([02-9]\d|1[02-9])\)|[2-9]([02-9]\d|1[02-9]))-?[2-9]([02-9]\d|1[02-9])-?\d{4}$/',
                        'message' => 'Please enter a valid Phone Number',
                    ],
                ],
            ]
        ]);

        // Add input for "payment_method" field
        $inputFilter->add([
            'name'     => 'payment_method',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'break_chain_on_failure' => true,
                    'options' => [
                        'messages' => [
                            \Zend\Validator\NotEmpty::IS_EMPTY => 'Please enter your Preferred Payment Method'
                        ],
                    ],
                ],
                [
                    'name' => 'InArray',
                    'options' => [
                        'haystack' => array_keys(self::PAYMENT_METHODS),
                        'message' => 'Please choose a valid Preferred Payment Method'
                    ],
                ]
            ],
        ]);

        // Add input for "password" field
        $inputFilter->add([
            'name'     => 'password',
            'required' => true,
            'filters'  => [
            ],
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'break_chain_on_failure' => true,
                    'options' => [
                        'messages' => [
                            \Zend\Validator\NotEmpty::IS_EMPTY => 'Please enter a Password' 
                        ],
                    ],
                ],
                [
                    'name'    => 'StringLength',
                    'options' => [
                        'min' => 6,
                        'max' => 64,
                        'messages' => [
                            \Zend\Validator\StringLength::TOO_LONG => 'Your Password must be between 6 to 64 characters',
                            \Zend\Validator\StringLength::TOO_SHORT => 'Your Password must be between 6 to 64 characters'
                        ],
                    ],
                ],
            ],
        ]);
        
        // Add input for "confirm_password" field
        $inputFilter->add([
            'name'     => 'confirm_password',
            'required' => true,
            'filters'  => [
            ],
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'break_chain_on_failure' => true,
                    'options' => [
                        'messages' => [
                            \Zend\Validator\NotEmpty::IS_EMPTY => 'Please confirm your Password' 
                        ],
                    ],
                ],
                [
                    'name'    => 'Identical',
                    'options' => [
                        'token' => 'password',
                        'messages' => [
                            \Zend\Validator\Identical::NOT_SAME => 'The Passwords do not match'
                        ]
                    ],
                ],
            ],
        ]);
    }
}