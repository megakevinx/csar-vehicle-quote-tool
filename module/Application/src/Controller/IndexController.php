<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Application\Repository\FaqRepository;

class IndexController extends AbstractActionController
{
    const ADMIN_ROLE = 'Administrator';
    const VENDOR_ROLE = 'Vendor';
    const SALES_REP_ROLE = 'StreetSalesTeam';

    private $faqRepository;

    public function __construct(FaqRepository $faqRepository)
    {
        $this->faqRepository = $faqRepository;
    }

    public function indexAction()
    {
        $user = $this->identity();

        if ($user && $user->role !== self::ADMIN_ROLE) {
            return $this->redirect()->toRoute('login');
        }

        return new ViewModel();
    }

    public function aboutUsAction()
    {
        return new ViewModel();
    }

    public function termsAction()
    {
        return new ViewModel();
    }

    public function privacyAction()
    {
        return new ViewModel();
    }

    public function faqAction()
    {
        return new ViewModel([
            'faqs' => $this->faqRepository->getAll()
        ]);
    }

    public function adminAction()
    {
        $user = $this->identity();

        if (!$user || $user->role !== self::ADMIN_ROLE) {
            return $this->redirect()->toRoute('login');
        }

        return new ViewModel();
    }

    public function customerAction()
    {
        $user = $this->identity();

        if ($user && $user->role !== self::ADMIN_ROLE) {
            return $this->redirect()->toRoute('login');
        }

        return new ViewModel();
    }

    public function salesAction()
    {
        $user = $this->identity();

        if (!$user || !in_array($user->role, [self::SALES_REP_ROLE, self::ADMIN_ROLE])) {
            return $this->redirect()->toRoute('login');
        }

        return new ViewModel();
    }

    public function vendorAction()
    {
        $user = $this->identity();

        if (!$user || !in_array($user->role, [self::VENDOR_ROLE, self::ADMIN_ROLE])) {
            return $this->redirect()->toRoute('login');
        }

        return new ViewModel();
    }
}
