<?php

namespace Application\Controller;

use RuntimeException;
use Zend\View\Model\JsonModel;
use Zend\Mvc\Controller\AbstractRestfulController;

use Application\Controller\BaseRestfulController;

use Application\Service\NotificationService;

class NotificationController extends BaseRestfulController
{
    private $notificationService;

    public function __construct(NotificationService $notificationService)
    {
        $this->notificationService = $notificationService;
    }

    // POST api/notification/
    public function create($data)
    {
        if (!$this->identity())
        {
            return $this->notAuthorized();
        }

        $referenceNumber = $data['referenceNumber'] ?? null;

        if ($referenceNumber)
        {
            $callBackVendorResult = $this->notificationService->createCallbackVendorNotification($referenceNumber);
            $orderReceivedResult = $this->notificationService->createOrderReceivedWillCallBackNotification($referenceNumber);

            return  new JsonModel([
                'referenceNumber' => $referenceNumber,
                'createdNotifications' => (object)[
                    'callBackVendor' => $callBackVendorResult->createdNotifications,
                    'orderReceived' => $orderReceivedResult->createdNotifications
                ]
            ]);
        }
        else
        {
            return $this->badRequest("Must specify a reference number");
        }
    }

    private function handleCustomerNotificationRequest(callable $createNotificationsCallback)
    {
        if ($this->getRequest()->isPost())
        {
            $data = \Zend\Json\Json::decode($this->getRequest()->getContent(), \Zend\Json\Json::TYPE_ARRAY);
            $referenceNumber = $data['referenceNumber'] ?? null;

            if ($referenceNumber)
            {
                return  new JsonModel([
                    'referenceNumber' => $referenceNumber,
                    'createdNotifications' => (object)$createNotificationsCallback($referenceNumber)
                ]);
            }
            else
            {
                return $this->badRequest("Must specify a reference number");
            }
        }
        else
        {
            return $this->methodNotAllowed();
        }
    }

    // POST api/notification/callback-customer
    public function callbackCustomerAction()
    {
        return $this->handleCustomerNotificationRequest(function(string $referenceNumber)
        {
            $callBackCustomerResult = $this->notificationService->createCallBackCustomerNotification($referenceNumber);
            $orderReceivedResult = $this->notificationService->createOrderReceivedWillCallBackToCustomerNotification($referenceNumber);

            return [
                'callBackCustomer' => $callBackCustomerResult->createdNotifications,
                'orderReceived' => $orderReceivedResult->createdNotifications
            ];
        });
    }

    // POST api/notification/still-shopping
    public function stillShoppingAction()
    {
        return $this->handleCustomerNotificationRequest(function(string $referenceNumber)
        {
            $followUpCampaignResult = $this->notificationService->createStillShoppingFollowUpCampaignNotifications($referenceNumber);

            return [
                'followUpCampaign' => $followUpCampaignResult->createdNotifications
            ];
        });
    }

    // POST api/notification/not-ready
    public function notReadyAction()
    {
        return $this->handleCustomerNotificationRequest(function(string $referenceNumber)
        {
            $followUpCampaignResult = $this->notificationService->createNotYetFollowUpCampaignNotifications($referenceNumber);

            return [
                'followUpCampaign' => $followUpCampaignResult->createdNotifications
            ];
        });
    }
}
