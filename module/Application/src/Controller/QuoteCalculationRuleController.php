<?php

namespace Application\Controller;

use Zend\View\Model\JsonModel;

use Application\Controller\BaseRestfulController;

use Application\Controller\Form\QuoteCalculationRuleForm;
use Application\Controller\Form\ChangePriceModifiersForm;

use Application\Service\QuoteCalculationRuleService;

class QuoteCalculationRuleController extends BaseRestfulController
{
    private $service;

    public function __construct(QuoteCalculationRuleService $service)
    {
        $this->service = $service;
    }

    // POST api/quote-calculation-rules
    public function create($data)
    {
        if (!$this->userIsRole('Administrator'))
        {
            return $this->notAuthorized();
        }

        $rules = [];

        foreach ($data as $rule)
        {
            $form = new QuoteCalculationRuleForm();
            $form->setData($rule);

            if ($form->isValid())
            {
                $rules[] = $form->getData();
            }
            else
            {
                return $this->badRequest("Input data invalid");
            }
        }

        $result = $this->service->saveRules($rules);

        return new JsonModel(['result' => $result]);
    }

    // POST api/quote-calculation-rules/change
    public function changeAction()
    {
        if (!$this->userIsRole('Administrator'))
        {
            return $this->notAuthorized();
        }

        if ($this->getRequest()->isPost())
        {
            $data = \Zend\Json\Json::decode($this->getRequest()->getContent(), \Zend\Json\Json::TYPE_ARRAY);

            $form = new ChangePriceModifiersForm();
            $form->setData($data);

            if ($form->isValid())
            {
                $data = $form->getData();

                $this->service->changeVehicleTypesPriceModifiersBy($data);
                $quoteCalculationRules = $this->service->getAllRules();

                return new JsonModel([
                    'quoteCalculationRules' => $quoteCalculationRules
                ]);
            }
            else
            {
                return $this->badRequest("Input data invalid");
            }
        }
    }
}
