<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use Zend\Http\PhpEnvironment\Request;
use Zend\ServiceManager\ServiceManager;

use Application\Controller\BaseRestfulController;

use Application\Controller\Form\CreateQuoteForm;
use Application\Controller\Form\UpdateQuoteForm;
use Application\Controller\Form\CreateCustomerQuoteForm;
use Application\Controller\Form\UpdateQuoteLocationForm;

use Application\Service\TransactionService;
use Application\Service\QuoteService;
use Application\Service\LocationService;
use Application\Service\NotificationService;

use Application\Model\Transaction;

class QuoteController extends BaseRestfulController
{
    private $transactionService;
    private $quoteService;
    private $locationService;
    private $notificationService;

    public function __construct(TransactionService $transactionService, QuoteService $quoteService,
        LocationService $locationService, NotificationService $notificationService)
    {
        $this->transactionService = $transactionService;
        $this->quoteService = $quoteService;
        $this->locationService = $locationService;
        $this->notificationService = $notificationService;
    }

    // POST api/quote
    public function create($data)
    {
        if (!$this->identity())
        {
            return $this->notAuthorized();
        }

        if (!$this->locationService->isZipSupported($data['vehicleZipCode']))
        {
            return $this->badRequest("We don't offer our services in the vehicle's location");
        }

        $form = new CreateQuoteForm();
        $form->setData($data);

        if ($form->isValid())
        {
            $data = $form->getData();

            $county = $this->locationService->getCountyForZip($data["vehicleZipCode"]);
            $data["vehicleCounty"] = $county->name;
            $data["offeredQuote"] = $this->quoteService->calculateQuote($data);
            $data["createdByUserId"] = $this->identity()->id;

            $result = $this->transactionService->createTransaction($data);

            return new JsonModel([
                'quote' => number_format($result->savedTransaction->offeredQuote, 2, '.', ','),
                'referenceNumber' => $result->savedTransaction->inspectionId,
                'createdTransaction' => $result->savedTransaction
            ]);
        }
        else
        {
            return $this->badRequest("Input data invalid");
        }
    }

    // PUT api/quote/{referenceNumber}
    public function update($referenceNumber, $data)
    {
        if (!$this->identity())
        {
            return $this->notAuthorized();
        }

        if (!($data['sellerPhoneNumber'] ?? null) && !($data['sellerEmail'] ?? null))
        {
            return $this->badRequest("At least one of sellerEmail and sellerPhoneNumber is required.");
        }

        $form = new UpdateQuoteForm();
        $form->setData($data);

        if ($form->isValid())
        {
            $data = $form->getData();

            $transactionResult = $this->transactionService->updateTransaction($referenceNumber, $data);
            $contactSellerNotificationResult = $this->notificationService->createContactSellerNotification($referenceNumber);
            $orderReceivedNotificationResult = $this->notificationService->createOrderReceivedWillContactSellerNotification($referenceNumber);

            return new JsonModel([
                'referenceNumber' => $referenceNumber,
                'updatedTransaction' => $transactionResult->updatedTransaction,
                'createdNotifications' => (object)[
                    'contactSeller' => $contactSellerNotificationResult->createdNotifications,
                    'orderReceived' => $orderReceivedNotificationResult->createdNotifications
                ]
            ]);
        }
        else
        {
            return $this->badRequest("Input data invalid");
        }
    }

    // POST api/quote/customer
    public function customerAction()
    {
        if ($this->getRequest()->isPost())
        {
            $data = \Zend\Json\Json::decode($this->getRequest()->getContent(), \Zend\Json\Json::TYPE_ARRAY);

            if (!$this->locationService->isZipSupported($data['vehicleZipCode']))
            {
                return $this->badRequest("We don't offer our services in the vehicle's location");
            }

            if (!($data['sellerPhoneNumber'] ?? null) && !($data['sellerEmail'] ?? null))
            {
                return $this->badRequest("At least one of sellerEmail and sellerPhoneNumber is required.");
            }

            $form = new CreateCustomerQuoteForm();
            $form->setData($data);

            if ($form->isValid())
            {
                $data = $form->getData();

                $county = $this->locationService->getCountyForZip($data["vehicleZipCode"]);
                $data["vehicleCounty"] = $county->name;
                $data["offeredQuote"] = $this->quoteService->calculateQuote($data);

                $result = $this->transactionService->createTransaction($data);

                return new JsonModel([
                    'quote' => number_format($result->savedTransaction->offeredQuote, 2, '.', ','),
                    'referenceNumber' => $result->savedTransaction->inspectionId,
                    'createdTransaction' => $result->savedTransaction
                ]);
            }
            else
            {
                return $this->badRequest("Input data invalid");
            }
        }
        else
        {
            return $this->methodNotAllowed();
        }
    }

    // PUT api/quote/location/{reference_number}
    public function locationAction()
    {
        $referenceNumber = $this->params()->fromRoute('id', 0);

        if ($this->getRequest()->isPut())
        {
            $data = \Zend\Json\Json::decode($this->getRequest()->getContent(), \Zend\Json\Json::TYPE_ARRAY);

            $form = new UpdateQuoteLocationForm();
            $form->setData($data);

            if ($form->isValid())
            {
                $data = $form->getData();

                $transactionResult = $this->transactionService->updateTransaction($referenceNumber, $data);

                if ($transactionResult->isSuccess)
                {
                    $contactCustomerNotificationResult = $this->notificationService->createContactCustomerNotification($referenceNumber);
                    $orderReceivedNotificationResult = $this->notificationService->createOrderReceivedToCustomerNotification($referenceNumber);
                    $followUpCampaignResult = $this->notificationService->createAcceptedFollowUpCampaignNotifications($referenceNumber);

                    return new JsonModel([
                        'referenceNumber' => $referenceNumber,
                        'updatedTransaction' => $transactionResult->updatedTransaction,
                        'createdNotifications' => (object)[
                            'contactCustomer' => $contactCustomerNotificationResult->createdNotifications,
                            'orderReceived' => $orderReceivedNotificationResult->createdNotifications,
                            'followUpCampaign' => $followUpCampaignResult->createdNotifications
                        ]
                    ]);
                }
                else
                {
                    return $this->badRequest("Input data invalid");
                }
            }
            else
            {
                return $this->badRequest("Input data invalid");
            }
        }
        else
        {
            return $this->methodNotAllowed();
        }
    }

    // GET api/quote/calculate?params...
    public function calculateAction()
    {
        if (!$this->identity())
        {
            return $this->notAuthorized();
        }

        if ($this->getRequest()->isGet())
        {
            $data = $this->getRequest()->getQuery()->toArray();
            //$data = \Zend\Json\Json::decode($this->getRequest()->getContent(), \Zend\Json\Json::TYPE_ARRAY);

            if (!$this->locationService->isZipSupported($data['vehicleZipCode']))
            {
                return $this->badRequest("We don't offer our services in the vehicle's location");
            }

            $form = new CreateQuoteForm();
            $form->setData($data);

            if ($form->isValid())
            {
                $data = $form->getData();

                $county = $this->locationService->getCountyForZip($data["vehicleZipCode"]);
                $data["vehicleCounty"] = $county->name;
                $quote = $this->quoteService->calculateQuote($data);

                return new JsonModel([
                    'quote' => number_format($quote, 2, '.', ','),
                    'vehicleData' => $data
                ]);
            }
            else
            {
                return $this->badRequest("Input data invalid");
            }
        }
    }
}
