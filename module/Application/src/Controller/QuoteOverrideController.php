<?php

namespace Application\Controller;

use Application\Controller\QuoteOverrideController;
use Zend\View\Model\JsonModel;
use Zend\Db\Adapter\Exception\RuntimeException;

use Application\Controller\Form\QuoteOverrideForm;

use Application\Repository\QuoteAmountOverrideRepository;

use Application\Model\QuoteAmountOverride;

class QuoteOverrideController extends BaseRestfulController
{
    private $repository;

    public function __construct(QuoteAmountOverrideRepository $repository)
    {
        $this->repository = $repository;
    }

    // POST api/quote-override
    public function create($data)
    {
        if (!$this->userIsRole('Administrator'))
        {
            return $this->notAuthorized();
        }

        $form = new QuoteOverrideForm();
        $form->setData($data);

        if ($form->isValid())
        {
            $data = $form->getData();

            if ($this->repository->getBy($data["fromVehicleYear"], $data["toVehicleYear"], $data["vehicleMake"], $data["vehicleModel"]))
            {
                return $this->badRequest("There already exists an identical override");
            }

            $quoteAmountOverride = new QuoteAmountOverride();
            $quoteAmountOverride->fromVehicleYear = $data["fromVehicleYear"];
            $quoteAmountOverride->toVehicleYear = $data["toVehicleYear"];
            $quoteAmountOverride->vehicleMake = $data["vehicleMake"];
            $quoteAmountOverride->vehicleModel = $data["vehicleModel"];
            $quoteAmountOverride->quote = $data["quote"];

            $savedQuoteOverride = $this->repository->save($quoteAmountOverride);
            $quoteOverrides = $this->repository->getAll();

            return new JsonModel([
                'createdQuoteOverride' => $savedQuoteOverride,
                'quoteOverrides' => $quoteOverrides
            ]);
        }
        else
        {
           return $this->badRequest("Input data invalid");
        }
    }

    //DELETE api/quote-override/{id}
    public function delete($id)
    {
        if (!$this->userIsRole('Administrator'))
        {
            return $this->notAuthorized();
        }

        $this->repository->delete($id);
        $quoteOverrides = $this->repository->getAll();

        return new JsonModel([
            'quoteOverrides' => $quoteOverrides
        ]);
    }
}
