<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use Zend\Http\PhpEnvironment\Request;
use Zend\ServiceManager\ServiceManager;

use Application\Controller\BaseRestfulController;

use Application\Controller\Form\GetModelsForm;
use Application\Controller\Form\GetVehicleTypeForm;

use Application\Service\VehicleService;

use Application\Repository\VehicleMakeRepository;
use Application\Repository\VehicleModelRepository;

use Application\Model\VehicleMake;
use Application\Model\VehicleModel;

class VehicleController extends BaseRestfulController
{
    private $vehicleMakeRepository;
    private $vehicleModelRepository;
    private $vehicleService;

    public function __construct(VehicleMakeRepository $vehicleMakeRepository, VehicleModelRepository $vehicleModelRepository, VehicleService $vehicleService)
    {
        $this->vehicleMakeRepository = $vehicleMakeRepository;
        $this->vehicleModelRepository = $vehicleModelRepository;
        $this->vehicleService = $vehicleService;
    }

    // GET api/vehicle/makes
    public function makesAction()
    {
        if ($this->getRequest()->isGet())
        {
            return new JsonModel([
                'makes' => $this->vehicleMakeRepository->getAll()
            ]);
        }
        else
        {
            $this->methodNotAllowed();
        }
    }

    // GET api/vehicle/models?params...
    public function modelsAction()
    {
        if ($this->getRequest()->isGet())
        {
            $data = $this->getRequest()->getQuery()->toArray();

            $form = new GetModelsForm();
            $form->setData($data);

            if ($form->isValid())
            {
                $data = $form->getData();

                $models = [];

                if ($data['year'])
                {
                    $models = $this->vehicleModelRepository->getByYearAndMake($data['year'], $data['make']);
                }
                else
                {
                    $models = $this->vehicleModelRepository->getByMake($data['make']);
                }

                return new JsonModel([
                    'models' => $models
                ]);
            }
            else
            {
                return $this->badRequest("Input data invalid");
            }
        }
        else
        {
            $this->methodNotAllowed();
        }
    }

    // GET api/vehicle/type?params...
    public function typeAction()
    {
        if (! $this->getRequest()->isGet())
        {
            return $this->methodNotAllowed();
        }

        $data = $this->getRequest()->getQuery()->toArray();

        $form = new GetVehicleTypeForm();
        $form->setData($data);

        if (! $form->isValid())
        {
            return $this->badRequest("Input data invalid");
        }

        $data = $form->getData();

        $result = $this->vehicleService->getVehicleType($data);

        if (! $result->isSuccess) {
            error_log($result->message);
            return $this->badRequest("Not found");
        }

        return new JsonModel([
            'vehicleType' => $result->vehicleType
        ]);
    }
}
