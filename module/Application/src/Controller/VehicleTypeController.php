<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

use Application\Repository\VehicleTypeRepository;

class VehicleTypeController extends AbstractRestfulController
{
    private $repository;

    public function __construct(VehicleTypeRepository $repository)
    {
        $this->repository = $repository;
    }

    // GET /vehicle_type
    public function getList()
    {
        // return new JsonModel([
        //     'status' => 'SUCCESS',
        //     'message'=>'Here is your data',
        //     'data' => [
        //         'full_name' => 'John Doe',
        //         'address' => '51 Middle st.'
        //     ]
        // ]);

        return new JsonModel([
            'vehicle_types' => $this->repository->getAll()
        ]);
    }

    // GET /vehicle_type/{id}
    public function get($id)
    {
        return new JsonModel([
            'vehicle_type' => $this->repository->get($id)
        ]);
    }

    // POST /vehicle_type
    public function create($data)
    {
        return new JsonModel([
            'result' => $data
        ]);
    }

    // PUT /vehicle_type/{id}
    public function update($id, $data)
    {
        return new JsonModel([
            'id' => $id,
            'data' => $data
        ]);
    }

    //DELETE /vehicle_type/{id}
    public function delete($id)
    {
        return new JsonModel([
            'result' => $id
        ]);
    }
}
