<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

use RuntimeException;

use Application\Service\LocationService;

class ZipValidationController extends AbstractRestfulController
{
    private $locationService;

    public function __construct(LocationService $locationService)
    {
        $this->locationService = $locationService;
    }

    // GET /zip-validate/{id}
    public function get($zip)
    {
        return  new JsonModel([
            'isValid' => $this->locationService->isZipSupported($zip)
        ]);
    }
}
