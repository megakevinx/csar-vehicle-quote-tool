<?php

// /module/src/MyModule/View/Helper/SpecialPurpose.php
namespace Application\Helper;

use Zend\View\Helper\AbstractHelper;

use Application\Repository\SettingRepository;

class ContactPhoneNumber extends AbstractHelper
{
    private $settingRepository;

    public function __construct(SettingRepository $settingRepository)
    {
        $this->settingRepository = $settingRepository;
    }

    public function __invoke()
    {
        return $this->settingRepository->get('CSAR_CONTACT_PHONE_NUMBER')->value;
    }
}