<?php

namespace Application\Model;

class County
{
    public $id;
    public $name;

    // In order to work with zend-db's TableGateway class, we need to implement this method.
    public function exchangeArray(array $data)
    {
        $this->id = !empty($data['Area_ID']) ? $data['Area_ID'] : null;
        $this->name = !empty($data['AreaName']) ? $data['AreaName'] : null;
    }
}