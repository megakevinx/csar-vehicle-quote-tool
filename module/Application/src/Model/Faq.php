<?php

namespace Application\Model;

class Faq
{
    public $id;
    public $question;
    public $answer;

    // In order to work with zend-db's TableGateway class, we need to implement this method.
    public function exchangeArray(array $data)
    {
        $this->id = !empty($data['Faq_ID']) ? $data['Faq_ID'] : null;
        $this->question = !empty($data['Question']) ? $data['Question'] : null;
        $this->answer = !empty($data['Answer']) ? $data['Answer'] : null;
    }
}