<?php

namespace Application\Model;

abstract class FollowUpCampaign
{
    public static $ACCEPTED = "Accepted";
    public static $STILL_SHOPPING = "Still Shopping";
    public static $NOT_YET = "Not Yet";
}