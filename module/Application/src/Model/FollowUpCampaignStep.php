<?php

namespace Application\Model;

class FollowUpCampaignStep
{
    public $id;
    public $description;
    public $messageContent;
    public $messageSubject;
    public $triggerTime;

    public $campaign;
    public $notificationType;

    public function exchangeArray(array $data)
    {
        $this->id = !empty($data['FollowUpSequenceStep_ID']) ? $data['FollowUpSequenceStep_ID'] : null;
        $this->description = !empty($data['Description']) ? $data['Description'] : null;
        $this->messageContent = !empty($data['MessageContent']) ? $data['MessageContent'] : null;
        $this->messageSubject = !empty($data['MessageSubject']) ? $data['MessageSubject'] : null;
        $this->triggerTime = !empty($data['TriggerTime']) ? $data['TriggerTime'] : null;

        $this->campaign = !empty($data['Campaign']) ? $data['Campaign'] : null;
        $this->notificationType = !empty($data['NotificationType']) ? $data['NotificationType'] : null;
    }
}