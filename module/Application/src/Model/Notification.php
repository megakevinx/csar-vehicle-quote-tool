<?php

namespace Application\Model;

class Notification
{
    public $id;
    public $transactionId;
    public $type;
    public $status;
    public $destination;
    public $scheduled;
    public $subject;
    public $content;
    // TODO: - Add new application ID field to notification

    // In order to work with zend-db's TableGateway class, we need to implement this method.
    public function exchangeArray(array $data)
    {
        $this->id = !empty($data['Notification_ID']) ? $data['Notification_ID'] : null;
        $this->transactionId = !empty($data['Transaction_ID']) ? $data['Transaction_ID'] : null;
        $this->type = !empty($data['NotificationType']) ? $data['NotificationType'] : null;
        $this->status = !empty($data['NotificationStatus']) ? $data['NotificationStatus'] : null;
        $this->destination = !empty($data['Destination']) ? $data['Destination'] : null;
        $this->scheduled = !empty($data['Scheduled']) ? $data['Scheduled'] : null;
        $this->subject = !empty($data['Subject']) ? $data['Subject'] : null;
        $this->content = !empty($data['Content']) ? $data['Content'] : null;
    }
}