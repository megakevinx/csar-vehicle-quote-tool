<?php

namespace Application\Model;

abstract class NotificationStatus
{
    public static $PENDING = "PENDING";
    public static $DONE = "DONE";
    public static $FAILED = "FAILED";
    public static $CANCELLED = "CANCELLED";
}