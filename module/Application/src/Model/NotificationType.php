<?php

namespace Application\Model;

abstract class NotificationType
{
    public static $EMAIL = "EMAIL";
    public static $SMS = "SMS";
    public static $IN_APP_ALERT = "IN-APP ALERT";
}