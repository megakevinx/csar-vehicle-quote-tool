<?php

namespace Application\Model;

class QuoteAmount
{
    public $id;
    public $featureType;
    public $featureValue;
    public $priceModifier;

    // In order to work with zend-db's TableGateway class, we need to implement this method.
    public function exchangeArray(array $data)
    {
        $this->id = !empty($data['QuoteAmount_ID']) ? $data['QuoteAmount_ID'] : null;
        $this->featureType = !empty($data['FeatureType']) ? $data['FeatureType'] : null;
        $this->featureValue = !empty($data['FeatureValue']) ? $data['FeatureValue'] : null;

        // Just assign directly from DB. PHP's empty() function thinks null == 0 so this gets assigned
        // to null when it's 0 in the db if we run the code that the other fields use 
        $this->priceModifier = $data['PriceModifier'];
    }
}