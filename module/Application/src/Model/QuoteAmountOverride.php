<?php

namespace Application\Model;

class QuoteAmountOverride
{
    public $id;
    public $fromVehicleYear;
    public $toVehicleYear;
    public $vehicleMake;
    public $vehicleModel;
    public $quote;

    // In order to work with zend-db's TableGateway class, we need to implement this method.
    public function exchangeArray(array $data)
    {
        $this->id = !empty($data['QuoteAmountOverride_ID']) ? $data['QuoteAmountOverride_ID'] : null;
        $this->fromVehicleYear = !empty($data['FromVehicleYear']) ? $data['FromVehicleYear'] : null;
        $this->toVehicleYear = !empty($data['ToVehicleYear']) ? $data['ToVehicleYear'] : null;
        $this->vehicleMake = !empty($data['VehicleMake']) ? $data['VehicleMake'] : null;
        $this->vehicleModel = !empty($data['VehicleModel']) ? $data['VehicleModel'] : null;
        $this->quote = $data['Quote'];
    }
}