<?php

namespace Application\Model;

class Setting
{
    public $id;
    public $code;
    public $value;

    // In order to work with zend-db's TableGateway class, we need to implement this method.
    public function exchangeArray(array $data)
    {
        $this->id = !empty($data['Setting_ID']) ? $data['Setting_ID'] : null;
        $this->code = !empty($data['Code']) ? $data['Code'] : null;
        $this->value = !empty($data['Value']) ? $data['Value'] : null;
    }
}