<?php

namespace Application\Model;

class Transaction
{
    public $id;
    public $inspectionId;
    public $createdByUserId;

    public $vehicleTypeId;
    public $offeredQuote;

    public $vehicleCounty;
    public $vehicleHasTitle;
    public $vehicleWillBeDelivered;

    // Inspection fields
    public $vehicleYear;
    public $vehicleMake;
    public $vehicleModel;

    public $vehicleWheelsDescription;
    public $vehicleHasKeys;

    public $vehicleStreetAddress;
    public $vehicleCity;
    public $vehicleZipCode;

    public $sellerName;
    public $sellerPhoneNumber;
    public $sellerEmail;

    public $buyerPayout;

    // In order to work with zend-db's TableGateway class, we need to implement this method.
    public function exchangeArray(array $data)
    {
        $this->id = !empty($data['Transaction_ID']) ? $data['Transaction_ID'] : null;
        $this->inspectionId = !empty($data['Inspection_ID']) ? $data['Inspection_ID'] : null;
        $this->createdByUserId = !empty($data['CreatedBy_User_ID']) ? $data['CreatedBy_User_ID'] : null;

        $this->vehicleTypeId = !empty($data['VehicleType_ID']) ? $data['VehicleType_ID'] : null;
        $this->offeredQuote = !empty($data['OfferedQuoteMax']) ? $data['OfferedQuoteMax'] : null;
        $this->vehicleCounty = !empty($data['County']) ? $data['County'] : null;

        $this->vehicleHasTitle = $data['HasTitle'];
        $this->vehicleHasTitle = $this->vehicleHasTitle === 1 ? true : false;

        $this->vehicleWillBeDelivered = $data['IsSelfDelivery'];
        $this->vehicleWillBeDelivered = $this->vehicleWillBeDelivered === 1 ? true : false;
    }

    public function exchangeInspectionArray(array $data)
    {
        $this->vehicleYear = $data['_Year'];
        $this->vehicleMake = $data['Make'];
        $this->vehicleModel = $data['Model'];

        $this->vehicleWheelsDescription = $data['Wheels'];
        $this->vehicleHasKeys = $data['_Keys'] === 'yes' ? true : false;

        $this->vehicleStreetAddress = $data['StreetAddress'];
        $this->vehicleCity = $data['City'];
        $this->vehicleZipCode = $data['Zip'];

        $this->sellerName = $data['SellerName'];
        $this->sellerPhoneNumber = $data['Phone'];
        $this->sellerEmail = $data['Email'];

        $this->buyerPayout = $data['BuyerPay'];
    }
}