<?php

namespace Application\Model;

class VehicleModel
{
    public $id;
    public $name;
    public $vehicleMakeId;

    public function exchangeArray(array $data)
    {
        $this->id = !empty($data['VehicleModel_ID']) ? $data['VehicleModel_ID'] : null;
        $this->name = !empty($data['Name']) ? $data['Name'] : null;
        $this->vehicleMakeId = !empty($data['VehicleMake_ID']) ? $data['VehicleMake_ID'] : null;
    }
}
