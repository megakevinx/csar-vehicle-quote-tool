<?php

namespace Application\Model;

class VehicleModelSpec
{
    public $vehicleMakeId;
    public $vehicleModelId;
    public $year;
    public $bodyStyle;
    public $weight;
    public $length;
    public $width;

    public function exchangeArray(array $data)
    {
        $this->vehicleMakeId = $data['VehicleMake_ID'] ?? null;
        $this->vehicleModelId = $data['VehicleModel_ID'] ?? null;
        $this->year = $data['Year'] ?? null;
        $this->bodyStyle = $data['BodyStyle'] ?? null;
        $this->weight = $data['WeightInKg'] ?? null;
        $this->length = $data['LengthInMm'] ?? null;
        $this->width = $data['WidthInMm'] ?? null;
    }
}
