<?php

namespace Application\Model;

class VehicleType
{
    public $id;
    public $description;

    // In order to work with zend-db's TableGateway class, we need to implement this method.
    public function exchangeArray(array $data)
    {
        $this->id = !empty($data['VehicleType_ID']) ? $data['VehicleType_ID'] : null;
        $this->description = !empty($data['Description']) ? $data['Description'] : null;
    }
}