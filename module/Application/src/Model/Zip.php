<?php

namespace Application\Model;

class Zip
{
    public $id;
    public $code;
    public $countyId;

    // Navigational Property
    public $county;

    // In order to work with zend-db's TableGateway class, we need to implement this method.
    public function exchangeArray(array $data)
    {
        $this->id = !empty($data['Zip_ID']) ? $data['Zip_ID'] : null;
        $this->code = !empty($data['ZipCode']) ? $data['ZipCode'] : null;
        $this->countyId = !empty($data['Area_ID']) ? $data['Area_ID'] : null;
    }
}