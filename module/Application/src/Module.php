<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\Authentication\AuthenticationService;
use Zend\Session\SessionManager;
use Zend\Authentication\Storage\Session as SessionStorage;

use Zend\Mvc\MvcEvent;
use Zend\Session\AbstractManager;

class Module implements ConfigProviderInterface
{
    const VERSION = '3.0.3-dev';

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    public function onBootstrap(MvcEvent $e)
    {
        $sessionManager = $e->getApplication()->getServiceManager()->get('Zend\Session\SessionManager');
        $this->forgetInvalidSession($sessionManager);
    }

    protected function forgetInvalidSession(AbstractManager $sessionManager)
    {
        try {
            $sessionManager->start();
            return;
        } catch (\Exception $e) {
        }
        /**
         * Session validation failed: toast it and carry on.
         */
        // @codeCoverageIgnoreStart
        session_unset();
        // @codeCoverageIgnoreEnd
    }

    public function getViewHelperConfig()
    {
        return [
           'factories' => [
                'contactPhoneNumber' => function($container) {
                    return new Helper\ContactPhoneNumber(
                        $container->get(Repository\SettingRepository::class)
                    );
                },
            ],
        ];
    }

    public function getServiceConfig()
    {
        return [
            'factories' => [
                // Services
                Service\QuoteService::class => function($container) {
                    return new Service\QuoteService(
                        $container->get(Repository\QuoteAmountRepository::class),
                        $container->get(Repository\VehicleTypeRepository::class),
                        $container->get(Repository\ZipRepository::class),
                        $container->get(Repository\CountyRepository::class),
                        $container->get(Repository\QuoteAmountOverrideRepository::class)
                    );
                },
                Service\AuthService::class => function($container) {

                    // Get contents of 'access_filter' config key (the AuthManager service
                    // will use this data to determine whether to allow currently logged in user
                    // to execute the controller action or not.
                    // $config = $container->get('Config');
                    // if (isset($config['access_filter']))
                    //     $config = $config['access_filter'];
                    // else
                    //     $config = [];

                    return new Service\AuthService(
                        $container->get(\Zend\Authentication\AuthenticationService::class),
                        $container->get(\Zend\Session\SessionManager::class),
                        $container->get(Repository\UserRepository::class)
                    );
                },
                \Zend\Authentication\AuthenticationService::class => function($container) {
                    return new AuthenticationService(
                        new SessionStorage('Zend_Auth', 'session', $container->get(\Zend\Session\SessionManager::class)),
                        $container->get(Service\AuthAdapter::class)
                    );
                },
                Service\AuthAdapter::class => function($container) {
                    return new Service\AuthAdapter(
                        $container->get(Repository\UserRepository::class),
                        $container->get(Repository\BuyerRepository::class)
                    );
                },
                Service\LocationService::class => function($container) {
                    return new Service\LocationService(
                        $container->get(Repository\ZipRepository::class),
                        $container->get(Repository\CountyRepository::class)
                    );
                },
                Service\NotificationService::class => function($container) {
                    return new Service\NotificationService(
                        $container->get(Repository\NotificationRepository::class),
                        $container->get(Repository\TransactionRepository::class),
                        $container->get(Repository\UserRepository::class),
                        $container->get(Repository\VehicleTypeRepository::class),
                        $container->get(Repository\FollowUpCampaignRepository::class)
                    );
                },
                Service\TransactionService::class => function($container) {
                    return new Service\TransactionService(
                        $container->get(Repository\TransactionRepository::class)
                    );
                },
                Service\ConfigurationService::class => function($container) {
                    return new Service\ConfigurationService(
                        $container->get(Repository\SettingRepository::class)
                    );
                },
                Service\QuoteCalculationRuleService::class => function($container) {
                    return new Service\QuoteCalculationRuleService(
                        $container->get(Repository\QuoteAmountRepository::class)
                    );
                },
                Service\VehicleService::class => function($container) {
                    return new Service\VehicleService(
                        $container->get(Repository\VehicleModelSpecRepository::class),
                        $container->get(Repository\VehicleTypeRepository::class)
                    );
                },

                // Repositories
                Repository\VehicleTypeRepository::class => function($container) {
                    $tableGateway = $container->get(Model\VehicleTypeTableGateway::class);
                    return new Repository\VehicleTypeRepository($tableGateway);
                },
                Repository\VehicleYearRepository::class => function($container) {
                    return new Repository\VehicleYearRepository();
                },
                Repository\VehicleMakeRepository::class => function($container) {
                    $tableGateway = $container->get(Model\VehicleMakeTableGateway::class);
                    return new Repository\VehicleMakeRepository($tableGateway);
                },
                Repository\VehicleModelRepository::class => function($container) {
                    return new Repository\VehicleModelRepository(
                        $container->get(Model\VehicleModelTableGateway::class)
                    );
                },
                Repository\TransactionRepository::class => function($container) {
                    $transactionTableGateway = $container->get(Model\TransactionTableGateway::class);
                    $inspectionTableGateway = $container->get(Model\InspectionTableGateway::class);
                    return new Repository\TransactionRepository($transactionTableGateway, $inspectionTableGateway);
                },
                Repository\ZipRepository::class => function($container) {
                    $tableGateway = $container->get(Model\ZipTableGateway::class);
                    return new Repository\ZipRepository($tableGateway);
                },
                Repository\CountyRepository::class => function($container) {
                    $tableGateway = $container->get(Model\CountyTableGateway::class);
                    return new Repository\CountyRepository($tableGateway);
                },
                Repository\QuoteAmountRepository::class => function($container) {
                    $tableGateway = $container->get(Model\QuoteAmountTableGateway::class);
                    return new Repository\QuoteAmountRepository($tableGateway);
                },
                Repository\UserRepository::class => function($container) {
                    return new Repository\UserRepository(
                        $container->get(AdapterInterface::class),
                        $container->get(Model\UserTableGateway::class),
                        $container->get(Model\RoleTableGateway::class),
                        $container->get(Model\UserRoleAppTableGateway::class)
                    );
                },
                Repository\SettingRepository::class => function($container) {
                    $tableGateway = $container->get(Model\SettingTableGateway::class);
                    return new Repository\SettingRepository($tableGateway);
                },
                Repository\QuoteAmountOverrideRepository::class => function($container) {
                    $tableGateway = $container->get(Model\QuoteAmountOverrideTableGateway::class);
                    return new Repository\QuoteAmountOverrideRepository($tableGateway);
                },
                Repository\NotificationRepository::class => function($container) {
                    $tableGateway = $container->get(Model\NotificationTableGateway::class);
                    return new Repository\NotificationRepository($tableGateway);
                },
                Repository\BuyerRepository::class => function($container) {
                    return new Repository\BuyerRepository(
                        $container->get(AdapterInterface::class)
                    );
                },
                Repository\FollowUpCampaignRepository::class => function($container) {
                    return new Repository\FollowUpCampaignRepository(
                        $container->get(AdapterInterface::class)
                    );
                },
                Repository\FaqRepository::class => function($container) {
                    return new Repository\FaqRepository(
                        $container->get(Model\FaqTableGateway::class)
                    );
                },
                Repository\VehicleModelSpecRepository::class => function($container) {
                    return new Repository\VehicleModelSpecRepository(
                        $container->get(AdapterInterface::class)
                    );
                },

                // Table Gateways
                Model\VehicleTypeTableGateway::class => function($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\VehicleType());
                    return new TableGateway('VehicleType', $dbAdapter, null, $resultSetPrototype);
                },
                Model\VehicleMakeTableGateway::class => function($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\VehicleMake());
                    return new TableGateway('VehicleMake', $dbAdapter, null, $resultSetPrototype);
                },
                Model\VehicleModelTableGateway::class => function($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\VehicleModel());
                    return new TableGateway('VehicleModel', $dbAdapter, null, $resultSetPrototype);
                },
                Model\TransactionTableGateway::class => function($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\Transaction());
                    return new TableGateway('Transaction', $dbAdapter, null, $resultSetPrototype);
                },
                Model\InspectionTableGateway::class => function($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    return new TableGateway('Inspection', $dbAdapter, null, $resultSetPrototype);
                },
                Model\ZipTableGateway::class => function($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\Zip());
                    return new TableGateway('Zip', $dbAdapter, null, $resultSetPrototype);
                },
                Model\CountyTableGateway::class => function($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\County());
                    return new TableGateway('Area', $dbAdapter, null, $resultSetPrototype);
                },
                Model\QuoteAmountTableGateway::class => function($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\QuoteAmount());
                    return new TableGateway('QuoteAmount', $dbAdapter, null, $resultSetPrototype);
                },

                Model\UserTableGateway::class => function($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    return new TableGateway('User', $dbAdapter, null, null);
                },
                Model\RoleTableGateway::class => function($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    return new TableGateway('UserRole', $dbAdapter, null, null);
                },
                Model\UserRoleAppTableGateway::class => function($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    return new TableGateway('User_UserRole_Application', $dbAdapter, null, null);
                },
                Model\SettingTableGateway::class => function($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\Setting());
                    return new TableGateway('Setting', $dbAdapter, null, $resultSetPrototype);
                },
                Model\NotificationTableGateway::class => function($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\Notification());
                    return new TableGateway('Notification', $dbAdapter, null, $resultSetPrototype);
                },
                Model\QuoteAmountOverrideTableGateway::class => function($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\QuoteAmountOverride());
                    return new TableGateway('QuoteAmountOverride', $dbAdapter, null, $resultSetPrototype);
                },
                Model\FaqTableGateway::class => function($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\Faq());
                    return new TableGateway('Faq', $dbAdapter, null, $resultSetPrototype);
                }
            ],
        ];
    }

    public function getControllerConfig()
    {
        return [
            'factories' => [

                Controller\VehicleTypeController::class => function($container) {
                    return new Controller\VehicleTypeController(
                        $container->get(Repository\VehicleTypeRepository::class)
                    );
                },

                Controller\VehicleController::class => function($container) {
                    return new Controller\VehicleController(
                        $container->get(Repository\VehicleMakeRepository::class),
                        $container->get(Repository\VehicleModelRepository::class),
                        $container->get(Service\VehicleService::class)
                    );
                },

                Controller\ConfigurationController::class => function($container) {
                    return new Controller\ConfigurationController(
                        $container->get(Service\ConfigurationService::class),

                        $container->get(Repository\VehicleTypeRepository::class),
                        $container->get(Repository\VehicleYearRepository::class),
                        $container->get(Repository\SettingRepository::class),
                        $container->get(Repository\QuoteAmountRepository::class),
                        $container->get(Repository\QuoteAmountOverrideRepository::class),
                        $container->get(Repository\FaqRepository::class)
                    );
                },

                Controller\QuoteController::class => function($container) {
                    return new Controller\QuoteController(
                        $container->get(Service\TransactionService::class),
                        $container->get(Service\QuoteService::class),
                        $container->get(Service\LocationService::class),
                        $container->get(Service\NotificationService::class)
                    );
                },

                Controller\ZipValidationController::class => function($container) {
                    return new Controller\ZipValidationController(
                        $container->get(Service\LocationService::class)
                    );
                },

                Controller\AuthController::class => function($container) {
                    return new Controller\AuthController(
                        $container->get(Service\AuthService::class),
                        $container->get(Service\NotificationService::class)
                    );
                },

                Controller\NotificationController::class => function($container) {
                    return new Controller\NotificationController(
                        $container->get(Service\NotificationService::class)
                    );
                },

                Controller\QuoteCalculationRuleController::class => function($container) {
                    return new Controller\QuoteCalculationRuleController(
                        $container->get(Service\QuoteCalculationRuleService::class)
                    );
                },

                Controller\QuoteOverrideController::class => function($container) {
                    return new Controller\QuoteOverrideController(
                        $container->get(Repository\QuoteAmountOverrideRepository::class)
                    );
                },

                Controller\CoverageController::class => function($container) {
                    return new Controller\CoverageController(
                        $container->get(Service\LocationService::class)
                    );
                },

                Controller\IndexController::class => function($container) {
                    return new Controller\IndexController(
                        $container->get(Repository\FaqRepository::class)
                    );
                },

                Controller\FaqController::class => function($container) {
                    return new Controller\FaqController(
                        $container->get(Repository\FaqRepository::class)
                    );
                },
            ],
        ];
    }
}
