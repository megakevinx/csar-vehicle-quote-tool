<?php

namespace Application\Repository;

use RuntimeException;
use Zend\Db\TableGateway\TableGatewayInterface;
//use Zend\Db\Sql\Select;

use Application\Model\Faq;

class FaqRepository
{
    private $tableGateway;

    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function getAll()
    {
        $resultSet = $this->tableGateway->select();

        $entities = [];

        foreach ($resultSet as $row) {
            $entities[] = $row;
        }

        return $entities;
    }

    public function get(int $faqId)
    {
        $resultSet = $this->tableGateway->select(['Faq_ID' => $faqId]);

        $row = $resultSet->current();

        return $row;
    }

    public function save(Faq $faq)
    {
        $faqData = [
            'Question' => $faq->question,
            'Answer' => $faq->answer
        ];

        $this->tableGateway->insert($faqData);
        $faq->id = $this->tableGateway->lastInsertValue;

        return $faq;
    }

    public function update(Faq $faq)
    {
        $faqData = [
            'Question' => $faq->question,
            'Answer' => $faq->answer
        ];

        $this->tableGateway->update($faqData, ['Faq_ID' => $faq->id]);

        return $faq;
    }

    public function delete(int $faqId)
    {
        return $this->tableGateway->delete(['Faq_ID' => (int) $faqId]);
    }
}