<?php

namespace Application\Repository;

use RuntimeException;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGatewayInterface;

use Application\Model\FollowUpCampaignStep;

class FollowUpCampaignRepository
{
    private $dbAdapter;

    public function __construct(Adapter $dbAdapter)
    {
        $this->dbAdapter = $dbAdapter;
    }

    public function getStepsFor(string $followUpCampaignType)
    {
        $sql = new Sql($this->dbAdapter);

        $select = $sql->select()
            ->from('FollowUpSequenceStep')->columns(['FollowUpSequenceStep_ID', 'Description', 'MessageContent', 'MessageSubject', 'TriggerTime'])
            ->join('FollowUpSequenceStepType', 'FollowUpSequenceStep.FollowUpSequenceStepType_ID = FollowUpSequenceStepType.FollowUpSequenceStepType_ID', ['NotificationType' => 'Description'])
            ->join('TransactionStatus', 'FollowUpSequenceStep.TransactionStatus_ID = TransactionStatus.TransactionStatus_ID', ['Campaign' => 'Description'])
            ->where(['TransactionStatus.Description' => $followUpCampaignType]);

        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $this->dbAdapter->query($selectString, Adapter::QUERY_MODE_EXECUTE)->toArray();

        if ($results)
        {
            $steps = [];

            foreach ($results as $row)
            {
                $followUpStep = new FollowUpCampaignStep();
                $followUpStep->exchangeArray($row);

                $steps[] = $followUpStep;
            }

            return $steps;
        }
        else
        {
            return null;
        }
    }
}