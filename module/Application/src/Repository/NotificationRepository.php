<?php

namespace Application\Repository;

use RuntimeException;
use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGatewayInterface;

use Application\Model\Notification;

class NotificationRepository
{
    private $tableGateway;

    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveNotification(Notification $notification)
    {
        $notificationData = [
            'Transaction_ID' => $notification->transactionId,

            'NotificationType' => $notification->type,
            'NotificationStatus' => $notification->status,

            'Destination' => $notification->destination,
            'Scheduled' => $notification->scheduled,

            'Subject' => $notification->subject,
            'Content' => $notification->content
        ];

        $this->tableGateway->insert($notificationData);
        $notification->id = $this->tableGateway->lastInsertValue;

        return $notification;
    }
}