<?php

namespace Application\Repository;

use RuntimeException;
use Zend\Db\TableGateway\TableGatewayInterface;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;

use Application\Model\QuoteAmountOverride;

class QuoteAmountOverrideRepository
{
    const WILDCARD = 'Any';

    private $tableGateway;

    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function getAll()
    {
        $resultSet = $this->tableGateway->select(function(Select $select)
        {
            $select->order('QuoteAmountOverride_ID');
        });

        $quoteAmountOverride = [];

        foreach ($resultSet as $row)
        {
            $quoteAmountOverride[] = $row;
        }

        return $quoteAmountOverride;
    }

    public function getBy(string $fromVehicleYear, string $toVehicleYear, string $vehicleMake, string $vehicleModel)
    {
        $resultSet = $this->tableGateway->select([
            'FromVehicleYear' => $fromVehicleYear,
            'ToVehicleYear' => $toVehicleYear,
            'VehicleMake' => $vehicleMake,
            'VehicleModel' => $vehicleModel
        ]);

        $row = $resultSet->current();

        if (!$row)
        {
            return null;
        }

        return $row;
    }

    private function prepareSelectStatement(Select $select, string $vehicleMake, string $vehicleModel)
    {
        $select->where->equalTo('VehicleMake', $vehicleMake);
        $select->where->equalTo('VehicleModel', $vehicleModel);

        $select->order('QuoteAmountOverride_ID')->limit(1);
    }

    public function getWithFromYearLessThanOrEqualAndToYearGreaterThanOrEqual(string $fromVehicleYear, string $toVehicleYear, string $vehicleMake, string $vehicleModel)
    {
        // Helpful for debugging. Include $gateway in use statement below.
        //$gateway = $this->tableGateway;

        $resultSet = $this->tableGateway->select(function (Select $select) use ($fromVehicleYear, $toVehicleYear, $vehicleMake, $vehicleModel) {
            $select->where->notEqualTo('FromVehicleYear', self::WILDCARD);
            $select->where->lessThanOrEqualTo('FromVehicleYear', $fromVehicleYear);

            $select->where->notEqualTo('ToVehicleYear', self::WILDCARD);
            $select->where->greaterThanOrEqualTo('ToVehicleYear', $toVehicleYear);

            $this->prepareSelectStatement($select, $vehicleMake, $vehicleModel);

            //$sql = new Sql($gateway->getAdapter());
            //$selectString = $sql->getSqlStringForSqlObject($select);
        });

        $row = $resultSet->current();

        if (!$row)
        {
            return null;
        }

        return $row;
    }

    public function getWithFromYearLessThanOrEqual(string $fromVehicleYear, string $toVehicleYear, string $vehicleMake, string $vehicleModel)
    {
        $resultSet = $this->tableGateway->select(function (Select $select) use ($fromVehicleYear, $toVehicleYear, $vehicleMake, $vehicleModel) {
            $select->where->notEqualTo('FromVehicleYear', self::WILDCARD);
            $select->where->lessThanOrEqualTo('FromVehicleYear', $fromVehicleYear);

            $select->where->equalTo('ToVehicleYear', $toVehicleYear);

            $this->prepareSelectStatement($select, $vehicleMake, $vehicleModel);
        });

        $row = $resultSet->current();

        if (!$row)
        {
            return null;
        }

        return $row;
    }

    public function getWithToYearGreaterThanOrEqual(string $fromVehicleYear, string $toVehicleYear, string $vehicleMake, string $vehicleModel)
    {
        $resultSet = $this->tableGateway->select(function (Select $select) use ($fromVehicleYear, $toVehicleYear, $vehicleMake, $vehicleModel) {
            $select->where->equalTo('FromVehicleYear', $fromVehicleYear);

            $select->where->notEqualTo('ToVehicleYear', self::WILDCARD);
            $select->where->greaterThanOrEqualTo('ToVehicleYear', $toVehicleYear);

            $this->prepareSelectStatement($select, $vehicleMake, $vehicleModel);
        });

        $row = $resultSet->current();

        if (!$row)
        {
            return null;
        }

        return $row;
    }

    public function save(QuoteAmountOverride $quoteAmountOverride)
    {
        $quoteAmountOverrideData = [
            'FromVehicleYear' => $quoteAmountOverride->fromVehicleYear,
            'ToVehicleYear' => $quoteAmountOverride->toVehicleYear,
            'VehicleMake' => $quoteAmountOverride->vehicleMake,
            'VehicleModel' => $quoteAmountOverride->vehicleModel,
            'Quote' => $quoteAmountOverride->quote,
        ];

        $this->tableGateway->insert($quoteAmountOverrideData);
        $quoteAmountOverride->id = $this->tableGateway->lastInsertValue;

        return $quoteAmountOverride;
    }

    public function delete(int $id)
    {
        return $this->tableGateway->delete(['QuoteAmountOverride_ID' => (int) $id]);
    }
}