<?php

namespace Application\Repository;

use RuntimeException;
use Zend\Db\TableGateway\TableGatewayInterface;
use Zend\Db\Sql\Select;

use Application\Model\QuoteAmount;

class QuoteAmountRepository
{
    private $tableGateway;

    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function get(int $id)
    {
        $resultSet = $this->tableGateway->select(['QuoteAmount_ID' => $id]);

        $row = $resultSet->current();

        return $row;
    }

    public function getAll()
    {
        $resultSet = $this->tableGateway->select(
            function (Select $select) {
                $select->order(['FeatureType DESC', 'FeatureValue ASC']);
            }
        );

        $quoteAmounts = [];

        foreach ($resultSet as $row)
        {
            $quoteAmounts[] = $row;
        }

        return $quoteAmounts;
    }

    public function getAllMap()
    {
        $resultSet =  $this->tableGateway->select();
        
        $quoteAmounts = [];

        foreach ($resultSet as $row) {
            $quoteAmounts[$row->featureType][strtolower($row->featureValue)] = $row->priceModifier;
        }

        return $quoteAmounts;
    }

    public function getByFeatureType(string $featureType)
    {
        $resultSet = $this->tableGateway->select(['FeatureType' => $featureType]);

        $quoteAmounts = [];

        foreach ($resultSet as $row)
        {
            $quoteAmounts[] = $row;
        }

        return $quoteAmounts;
    }

    public function update(QuoteAmount $quoteAmount)
    {
        $quoteAmountData = [
            'PriceModifier' => $quoteAmount->priceModifier,
        ];

        $this->tableGateway->update($quoteAmountData, ['QuoteAmount_ID' => $quoteAmount->id]);

        return $quoteAmount;
    }
}