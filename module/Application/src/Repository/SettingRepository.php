<?php

namespace Application\Repository;

use RuntimeException;
use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGatewayInterface;

use Application\Model\Setting;

class SettingRepository
{
    private $tableGateway;

    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function get(string $settingCode)
    {
        $resultSet = $this->tableGateway->select(['Code' => $settingCode]);

        $row = $resultSet->current();

        return $row;
    }

    public function update(Setting $setting)
    {
        $settingData = [
            'Value' => $setting->value,
        ];

        $this->tableGateway->update($settingData, ['Setting_ID' => $setting->id]);

        return $setting;
    }
}