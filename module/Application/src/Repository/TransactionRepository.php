<?php

namespace Application\Repository;

use RuntimeException;
use Zend\Db\TableGateway\TableGatewayInterface;

use Application\Model\Transaction;

// TODO: Refactor magic strings for column names
class TransactionRepository
{
    // TODO: put this stuff in config
    private static $DATE_TIME_FORMAT = 'Y-m-d H:i:s';
    private static $DEFAULT_BUYER_ID = 88; // This is Frank's Buyer Id
    private static $DEFAULT_STATE = 'FL'; // This is the default State
    private static $APPLICATION_ID = 1;

    private $transactionTableGateway;
    private $inspectionTableGateway;

    public function __construct(TableGatewayInterface $transactionTableGateway, TableGatewayInterface $inspectionTableGateway)
    {
        $this->transactionTableGateway = $transactionTableGateway;
        $this->inspectionTableGateway = $inspectionTableGateway;
    }

    public function get(string $inspectionId)
    {
        // We call to array here to close the statement so that mysql does not complain that
        // we're executing a query without using the results of the previous one
        $inspectionResultSet = $this->inspectionTableGateway->select(['Inspection_ID' => $inspectionId])->toArray();

        if (!$inspectionResultSet)
        {
            return null;
        }

        $inspection = $inspectionResultSet[0];

        $transactionResultSet = $this->transactionTableGateway->select(['Inspection_ID' => $inspectionId]);
        $transaction = $transactionResultSet->current();

        if (!$transaction)
        {
            return null;
        }

        $transaction->exchangeInspectionArray($inspection);

        return $transaction;
    }

    private function getFullAddress(Transaction $transaction)
    {
        return
            $transaction->vehicleStreetAddress . " " .
            $transaction->vehicleCity . ", " .
            TransactionRepository::$DEFAULT_STATE . " " .
            $transaction->vehicleZipCode;
    }

    public function saveTransaction(Transaction $transaction)
    {
        $inspectionData = [
            '_Year' => $transaction->vehicleYear,
            'Make' => $transaction->vehicleMake,
            'Model' => $transaction->vehicleModel,
            'Wheels' => $transaction->vehicleWheelsDescription,
            '_Keys' => $transaction->vehicleHasKeys ? 'yes' : 'no',
            'Title' => $transaction->vehicleHasTitle ? 'yes' : 'no',

            'Buyer_ID' => TransactionRepository::$DEFAULT_BUYER_ID,
            'DateVal' => date(TransactionRepository::$DATE_TIME_FORMAT),

            'Zip' => $transaction->vehicleZipCode,
            'State' => TransactionRepository::$DEFAULT_STATE,

            'SellerName' => $transaction->sellerName,
            'Phone' => $transaction->sellerPhoneNumber,
            'Email' => $transaction->sellerEmail,

            'FullAddress' => '',

            'PurchAmt' => 0.0,
            'PickupNotes' => '',
            'LeadSource' => '',
            'KeysLoc' => '',
            'DatePmtRecd' => '0000-00-00 00:00:00',
            'DMV_Fees' => 0.0,
            'OtherFeesNotes' => 0,
            'OtherFeesAmt' => 0,
            'LeadCost' => 0,
            'BuyerPay' => 0.0,
            'DateTowPaid' => '0000-00-00 00:00:00',
            'TowPaidCode' => '',
            'TowCompany' => '',
            'TowStatus' => '',
            'TowCost' => 0.0,
            'TowID' => '',
            'EndBuyer' => '',
            'SellPrice' => 0.0,
            'NOI' => 0.0,
            'NOI_pct' => 0.0,
            'Incomplete' => '1',
        ];

        $this->inspectionTableGateway->insert($inspectionData);
        $transaction->inspectionId = $this->inspectionTableGateway->lastInsertValue;

        $transactionData = [
            'Inspection_ID' => $transaction->inspectionId,
            'ReferenceNumber' => $transaction->inspectionId,
            'CreatedBy_User_ID' => $transaction->createdByUserId,

            'VehicleType_ID' => $transaction->vehicleTypeId,
            'OfferedQuoteMin' => $transaction->offeredQuote,
            'OfferedQuoteMax' => $transaction->offeredQuote,

            'County' => $transaction->vehicleCounty,

            'HasTitle' => $transaction->vehicleHasTitle ? 1 : 0,
            'IsSelfDelivery' => $transaction->vehicleWillBeDelivered ? 1 : 0,

            'CreatedBy_Application_ID' => TransactionRepository::$APPLICATION_ID,
        ];

        $this->transactionTableGateway->insert($transactionData);
        $transaction->id = $this->transactionTableGateway->lastInsertValue;

        return $transaction;
    }

    public function updateTransaction(Transaction $transaction)
    {
        // Only update these fields because that's all we need at the moment.
        $inspectionData = [
            'StreetAddress' => $transaction->vehicleStreetAddress,
            'City' => $transaction->vehicleCity,

            'SellerName' => $transaction->sellerName,
            'Phone' => $transaction->sellerPhoneNumber,
            'Email' => $transaction->sellerEmail,

            'BuyerPay' => $transaction->buyerPayout,

            'FullAddress' => $this->getFullAddress($transaction)
        ];

        $this->inspectionTableGateway->update($inspectionData, ['Inspection_ID' => $transaction->inspectionId]);

        return $transaction;
    }
}