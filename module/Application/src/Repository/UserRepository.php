<?php

namespace Application\Repository;

use RuntimeException;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGatewayInterface;

use Application\Model\User;

class UserRepository
{
    const APP_CODE = 'VEHICLE_QUOTE_TOOL';

    private $dbAdapter;
    private $userTableGateway;
    private $roleTableGateway;
    private $userRoleAppTableGateway;

    public function __construct(Adapter $dbAdapter, TableGatewayInterface $userTableGateway, TableGatewayInterface $roleTableGateway, TableGatewayInterface $userRoleAppTableGateway)
    {
        $this->dbAdapter = $dbAdapter;
        $this->userTableGateway = $userTableGateway;
        $this->roleTableGateway = $roleTableGateway;
        $this->userRoleAppTableGateway = $userRoleAppTableGateway;
    }

    private function select($where)
    {
        $sql = new Sql($this->dbAdapter);

        $select = $sql->select()
            ->from('User')->columns(['User_ID', 'Email', 'Password', 'FirstName', 'LastName', 'PhoneNumber', 'PasswordResetToken', 'PasswordResetTokenCreationDate'])
            ->join('User_UserRole_Application', 'User.User_ID = User_UserRole_Application.User_ID')
            ->join('UserRole', 'User_UserRole_Application.UserRole_ID = UserRole.UserRole_ID', ['Role' => 'Description'])
            ->where($where);

        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $this->dbAdapter->query($selectString, Adapter::QUERY_MODE_EXECUTE)->toArray();

        return $results;
    }

    private function createUserObject($userData)
    {
        if ($userData)
        {
            $user = new User();
            $user->exchangeArray($userData[0]);

            return $user;
        }
        else
        {
            return null;
        }
    }

    public function get(int $id)
    {
        $results = $this->select(['User.User_ID' => $id]);
        return $this->createUserObject($results);
    }

    public function getByEmail(string $email)
    {
        $results = $this->select(['User.Email' => $email, 'User_UserRole_Application.Application_Code' => 'VEHICLE_QUOTE_TOOL']);
        return $this->createUserObject($results);
    }

    public function getByPasswordResetToken(string $passwordResetToken)
    {
        $results = $this->select(['User.PasswordResetToken' => $passwordResetToken, 'User_UserRole_Application.Application_Code' => 'VEHICLE_QUOTE_TOOL']);
        return $this->createUserObject($results);
    }

    public function update(User $user)
    {
        $userData = [
            'Password' => $user->password,
            'PasswordResetToken' => $user->passwordResetToken,
            'PasswordResetTokenCreationDate' => $user->passwordResetTokenCreationDate,
        ];

        $this->userTableGateway->update($userData, ['User_ID' => $user->id]);

        return $user;
    }

    public function saveUser(User $user)
    {
        $role = $this->roleTableGateway->select(['Description' => $user->role])->toArray();

        if (!$role)
        {
            throw new RuntimeException('Cannot create a User with a Role that does not exist');
        }

        $roleId = $role[0]['UserRole_ID'];

        $userData = [
            'Email' => $user->email,
            'Password' => $user->password,

            'FirstName' => $user->firstName,
            'LastName' => $user->lastName,
            'PhoneNumber' => $user->phoneNumber,

            'CompanyName' => $user->companyName,
            'CompanyAddress' => $user->companyAddress,
            'PaymentMethod' => $user->paymentMethod,

            // Legacy stuff. Not used by this app but the db requires it.
            'UserRole_ID' => $roleId
        ];

        $this->userTableGateway->insert($userData);
        $user->id = $this->userTableGateway->lastInsertValue;

        $userRoleAppData = [
            'User_ID' => $user->id,
            'UserRole_ID' => $roleId,
            'Application_Code' => self::APP_CODE,
        ];

        $this->userRoleAppTableGateway->insert($userRoleAppData);

        return $user;
    }
}