<?php

namespace Application\Repository;

use RuntimeException;
use Zend\Db\TableGateway\TableGatewayInterface;
use Zend\Db\Sql\Select;

class VehicleMakeRepository
{
    private $tableGateway;

    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function getAll()
    {
        $resultSet = $this->tableGateway->select();

        $vehicleMakes = [];

        foreach ($resultSet as $row) {
            $vehicleMakes[] = $row;
        }

        return $vehicleMakes;
    }
}