<?php

namespace Application\Repository;

use RuntimeException;
use Zend\Db\Adapter\Adapter;
use Zend\Db\TableGateway\TableGatewayInterface;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;

use Application\Model\VehicleModelSpec;

class VehicleModelSpecRepository
{
    private $dbAdapter;

    public function __construct(Adapter $dbAdapter)
    {
        $this->dbAdapter = $dbAdapter;
    }

    private function getSpecSelect($sql, $year, $make, $model, $queryIndex, $exactMatch, $allInfo)
    {
        $select = $sql->select()
            ->from('VehicleModelSpec')
            ->columns([
                'VehicleMake_ID', 'VehicleModel_ID', 'Year', 'BodyStyle',
                'WeightInKg', 'LengthInMm', 'WidthInMm',
                'QueryIndex' => new Expression($queryIndex)
            ]);

        if ($exactMatch) {
            $select->where->equalTo('Year', $year);
        }

        $select->where->equalTo('VehicleMake_ID', $make);
        $select->where->equalTo('VehicleModel_ID', $model);

        if ($allInfo) {
            $select->where->isNotNull('BodyStyle');
        }

        $select->where->isNotNull('WeightInKg');
        $select->where->isNotNull('LengthInMm');
        $select->where->isNotNull('WidthInMm');

        return $select;
    }

    public function get(string $year, string $make, string $model)
    {
        $sql = new Sql($this->dbAdapter);

        $exactMatchAllInfo = $this->getSpecSelect(
            $sql,
            $year, $make, $model, "1",
            true, true
        );

        $partialMatchAllInfo = $this->getSpecSelect(
            $sql,
            $year, $make, $model, "2",
            false, true
        );

        $exactMatchSomeInfo = $this->getSpecSelect(
            $sql,
            $year, $make, $model, "3",
            true, false
        );

        $partialMatchSomeInfo = $this->getSpecSelect(
            $sql,
            $year, $make, $model, "4",
            false, false
        )
        ->order([
            'QueryIndex',
            new Expression('ABS (Year - ?)', $year)
        ])
        ->limit(1);

        $selectString = sprintf(
            '%s UNION %s UNION %s UNION %s',
            $sql->getSqlStringForSqlObject($exactMatchAllInfo),
            $sql->getSqlStringForSqlObject($partialMatchAllInfo),
            $sql->getSqlStringForSqlObject($exactMatchSomeInfo),
            $sql->getSqlStringForSqlObject($partialMatchSomeInfo)
        );

        $results = $this->dbAdapter->query($selectString, Adapter::QUERY_MODE_EXECUTE)->toArray();

        if ($results)
        {
            $spec = new VehicleModelSpec();
            $spec->exchangeArray($results[0]);

            return $spec;
        }
        else
        {
            return null;
        }
    }
}