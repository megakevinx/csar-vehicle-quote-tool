<?php

namespace Application\Repository;

use RuntimeException;
use Zend\Db\TableGateway\TableGatewayInterface;
use Zend\Db\Sql\Select;

class VehicleTypeRepository
{
    private $tableGateway;

    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function getAll()
    {
        $resultSet = $this->tableGateway->select(
            function (Select $select) {
                $select->order('Sequence');
            }
        );

        $vehicleTypes = [];

        foreach ($resultSet as $row) {
            $vehicleTypes[] = $row;
        }

        return $vehicleTypes;
    }

    public function getAllMap()
    {
        $resultSet = $this->tableGateway->select();

        $vehicleTypes = [];

        foreach ($resultSet as $row) {
            $vehicleTypes[$row->id] = $row->description;
        }

        return $vehicleTypes;
    }

    public function get(int $id)
    {
        $resultSet = $this->tableGateway->select(['VehicleType_ID' => $id]);

        $row = $resultSet->current();

        if (!$row) {
            throw new RuntimeException(sprintf('Could not find row with identifier %d', $id));
        }

        return $row;
    }

    public function getByDescription(string $description)
    {
        $resultSet = $this->tableGateway->select(['Description' => $description]);

        $row = $resultSet->current();

        return $row;
    }
}