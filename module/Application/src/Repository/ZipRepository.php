<?php

namespace Application\Repository;

use RuntimeException;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGatewayInterface;

use Application\Model\Zip;
use Application\Model\County;

class ZipRepository
{
    private $tableGateway;

    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function getByCode(string $code)
    {
        $resultSet = $this->tableGateway->select(['ZipCode' => $code]);

        $row = $resultSet->current();

        return $row;
    }

    public function getAll()
    {
        $dbAdapter = $this->tableGateway->getAdapter();
        $sql = new Sql($dbAdapter);

        $select = $sql->select()
            ->from('Zip')->columns(['ZipCode', 'Area_ID'])
            ->join('Area', 'Zip.Area_ID = Area.Area_ID', ['AreaName'])
            ->order('Zip.ZipCode');

        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $dbAdapter->query($selectString, Adapter::QUERY_MODE_EXECUTE)->toArray();

        if ($results)
        {
            $zipCodes = [];

            foreach ($results as $row)
            {
                $zip = new Zip();
                $zip->exchangeArray($row);

                $county = new County();
                $county->id = $zip->countyId;
                $county->name = $row['AreaName'];

                $zip->county = $county;

                $zipCodes[] = $zip;
            }

            return $zipCodes;
        }
        else
        {
            return null;
        }
    }
}