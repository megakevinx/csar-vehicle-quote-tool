<?php

namespace Application\Service;

use Zend\Authentication\Adapter\AdapterInterface;
use Zend\Authentication\Result;
//use Zend\Crypt\Password\Bcrypt;

use Application\Repository\UserRepository;
use Application\Repository\BuyerRepository;

/**
 * Adapter used for authenticating user. It takes login and password on input
 * and checks the database if there is a user with such login (email) and password.
 * If such user exists, the service returns its identity (email). The identity
 * is saved to session and can be retrieved later with Identity view helper provided
 * by ZF3.
 */
class AuthAdapter implements AdapterInterface
{
    private $userRepository;
    private $buyerRepository;

    /**
     * User email.
     * @var string 
     */
    private $email;

    /**
     * Password
     * @var string 
     */
    private $password;

    public function __construct(UserRepository $userRepository, BuyerRepository $buyerRepository)
    {
        $this->userRepository = $userRepository;
        $this->buyerRepository = $buyerRepository;
    }
    
    /**
     * Sets user email.
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }
    
    /**
     * Sets password.
     */
    public function setPassword($password)
    {
        $this->password = (string)$password;
    }
    
    /**
     * Performs an authentication attempt.
     */
    public function authenticate()
    {
        $user = $this->userRepository->getByEmail($this->email);
        $validatePassword = function($input, $stored) { return $input === $stored; };

        if (!$user)
        {
            $user = $this->buyerRepository->getByName($this->email);
            $validatePassword = function($input, $stored) { return $this->isCorrectBuyerPassword($input, $stored); };
        }

        if (!$user)
        {
            return new Result(
                Result::FAILURE_IDENTITY_NOT_FOUND,
                null,
                ['Invalid credentials.']
            );
        }

        if ($validatePassword($this->password, $user->password))
        {
            if ($user->role)
            {
                return new Result(
                    Result::SUCCESS,
                    $user,
                    ['Authenticated successfully.']
                );
            }
            else
            {
                return new Result(
                    Result::FAILURE,
                    null,
                    ['No role configured.']
                );
            }
        }
        else
        {
            return new Result(
                Result::FAILURE_CREDENTIAL_INVALID,
                null, 
                ['Invalid credentials.']
            );
        }
    }

    private function isCorrectBuyerPassword($passwordPlainText, $passwordHash)
    {
        $salt = substr($passwordHash, 0, 8);
        return $passwordHash == $this->getPasswordHash($salt, $passwordPlainText);
    }

    // calculate the hash from a salt and the passwordPlainText
    private function getPasswordHash($salt, $passwordPlainText)
    {
        return $salt . (hash('whirlpool', $salt . $passwordPlainText));
    }
}


