<?php

namespace Application\Service;

use Zend\Authentication\AuthenticationService;
use Zend\Session\SessionManager;
use Zend\Authentication\Result;
use Zend\Math\Rand;

use Application\Repository\UserRepository;

use Application\Model\User;

/**
 * The AuthService service is responsible for user's login/logout and simple access 
 * filtering. The access filtering feature checks whether the current visitor 
 * is allowed to see the given page or not.  
 */
class AuthService
{
    const DATE_FORMAT = 'Y-m-d H:i:s';
    const TOKEN_EXPIRATION_TIME = 24*60*60;
    const TOKEN_LENGTH = 32;
    /**
     * Authentication service.
     * @var \Zend\Authentication\AuthenticationService
     */
    private $authProvider;
    
    /**
     * Session manager.
     * @var Zend\Session\SessionManager
     */
    private $sessionManager;

    private $userRepository;

    public function __construct(AuthenticationService $authProvider, SessionManager $sessionManager, UserRepository $userRepository)
    {
        $this->authProvider = $authProvider;
        $this->sessionManager = $sessionManager;
        $this->userRepository = $userRepository;
    }
    
    /**
     * Performs a login attempt. If $rememberMe argument is true, it forces the session
     * to last for one month (otherwise the session expires on one hour).
     */
    public function login($email, $password, $rememberMe)
    {
        $isSuccess = false;

        // Check if user has already logged in. If so, do not allow to log in 
        // twice.
        if ($this->authProvider->getIdentity() != null)
        {
            //throw new \Exception('Already logged in');
            // Remove identity from session.
            $this->authProvider->clearIdentity();
        }

        // Authenticate with login/password.
        $authAdapter = $this->authProvider->getAdapter();
        $authAdapter->setEmail($email);
        $authAdapter->setPassword($password);

        $result = $this->authProvider->authenticate();

        if ($result->getCode() == Result::SUCCESS)
        {
            $isSuccess = true;

            if ($rememberMe)
            {
                $this->sessionManager->rememberMe(60*60*24*30);
            }
        }

        return (object)[
            'isSuccess' => $isSuccess,
            'identity' => $this->authProvider->getIdentity()
        ];
    }
    
    /**
     * Performs user logout.
     */
    public function logout()
    {
        // Allow to log out only when user is logged in.
        if ($this->authProvider->getIdentity() == null)
        {
            throw new \Exception('The user is not logged in');
        }
        
        // Remove identity from session.
        $this->authProvider->clearIdentity();
    }

    public function register(array $data)
    {
        $user = $this->userRepository->getByEmail($data['email']);

        if ($user)
        {
            return (object)[
                'isSuccess' => false,
                'errorMessage' => "There's already a registered user with that email."
            ];
        }
        else
        {
            $userToSave = new User();
            $userToSave->email = $data['email'];
            $userToSave->password = $data['password'];
            $userToSave->firstName = $data['first_name'];
            $userToSave->lastName = $data['last_name'];
            $userToSave->companyName = $data['company_name'];
            $userToSave->companyAddress = $data['company_address'];
            $userToSave->phoneNumber = $data['phone_number'];
            $userToSave->paymentMethod = $data['payment_method'];
            // TODO: Attention here. This assumes that all the users created are vendors
            $userToSave->role = 'Vendor';
    
            $savedUser = $this->userRepository->saveUser($userToSave);
    
            return (object)[
                'isSuccess' => true,
                'savedUser' => $savedUser
            ];
        }
    }

    public function beginResetPassword(array $data)
    {
        // Look for the user with such email.
        $user = $this->userRepository->getByEmail($data['email']);

        if ($user != null) {

            $token = $this->calculatePasswordResetToken();

            $user->passwordResetToken = $token;
            $user->passwordResetTokenCreationDate = date(self::DATE_FORMAT);

            $this->userRepository->update($user);

            return (object)[
                'isSuccess' => true,
                'updatedUser' => $user
            ];
        }
        else {
            return (object)[
                'isSuccess' => false,
                'errorMessage' => "There's no registered user with that email."
            ];
        }
    }

    private function calculatePasswordResetToken()
    {
        return Rand::getString(self::TOKEN_LENGTH, '0123456789abcdefghijklmnopqrstuvwxyz', true);
    }

    public function setNewPasswordByToken(string $token, array $data)
    {
        if (strlen($token) != self::TOKEN_LENGTH) {
            return (object)[
                'isSuccess' => false,
                'errorMessage' => "Invalid token. Please try reseting your password again."
            ];
        }

        $user = $this->userRepository->getByPasswordResetToken($token);

        if (!$user) {
            return (object)[
                'isSuccess' => false,
                'errorMessage' => "Invalid token. Please try reseting your password again."
            ];
        }

        $tokenCreationDate = strtotime($user->passwordResetTokenCreationDate);
        $currentDate = strtotime('now');

        if ($currentDate - $tokenCreationDate > self::TOKEN_EXPIRATION_TIME) {
            return (object)[
                'isSuccess' => false,
                'errorMessage' => "Token expired. Please try reseting your password again."
            ];
        }

        $user->password = $data['new_password'];
        $user->passwordResetToken = null;
        $user->passwordResetTokenCreationDate = null;

        $this->userRepository->update($user);

        return (object)[
            'isSuccess' => true
        ];
    }
}