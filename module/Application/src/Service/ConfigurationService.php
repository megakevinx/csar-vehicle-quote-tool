<?php

namespace Application\Service;

use Application\Repository\SettingRepository;

use Application\Model\Setting;

class ConfigurationService
{
    private static $SETTING_CODE_MAP = [
        'vehicleDropOffAddress' => 'VEHICLE_DROP_OFF_ADDRESS',
        'contactPhoneNumber' => 'CSAR_CONTACT_PHONE_NUMBER'
    ]; 

    private $settingRepository;

    public function __construct(SettingRepository $settingRepository)
    {
        $this->settingRepository = $settingRepository;
    }

    public function saveAppSettings(array $settings)
    {
        $updatedSettings = [];

        foreach ($settings as $settingCode => $settingValue)
        {
            $settingCode = ConfigurationService::$SETTING_CODE_MAP[$settingCode];

            $settingToUpdate = $this->settingRepository->get($settingCode);
            $settingToUpdate->value = $settingValue;

            $updatedSettings[] = $this->settingRepository->update($settingToUpdate);
        }

        return (object)[
            'isSuccess' => true,
            'updatedSettings' => $updatedSettings
        ];
    }
}