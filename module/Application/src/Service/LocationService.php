<?php

namespace Application\Service;

use Application\Repository\ZipRepository;
use Application\Repository\CountyRepository;

class LocationService
{
    // TODO: Move to configuration
    private $supportedCounties = [
        'hernando',
        'polk',
        'pasco',
        'hillsborough',
        'pinellas'
    ];

    private $zipRepository;
    private $countyRepository;

    public function __construct(ZipRepository $zipRepository, CountyRepository $countyRepository)
    {
        $this->zipRepository = $zipRepository;
        $this->countyRepository = $countyRepository;
    }

    private function isSupportedCounty($countyName)
    {
        return in_array(strtolower($countyName), $this->supportedCounties);
    }

    public function getCoverageAreas()
    {
        $zips = $this->zipRepository->getAll();

        $coverageAreas = array_filter($zips, function($zip) {
            return $this->isSupportedCounty($zip->county->name);
        });

        return $coverageAreas;
    }

    public function getCountyForZip(string $zip)
    {
        $zipCode = $this->zipRepository->getByCode($zip);
        $county = null;

        if ($zipCode)
        {
            $county = $this->countyRepository->get($zipCode->countyId);
        }
        
        return $county;
    }

    public function isZipSupported(string $zip)
    {
        $isSupported = false;

        $county = $this->getCountyForZip($zip);

        if ($county)
        {
            if ($this->isSupportedCounty($county->name))
            {
                $isSupported = true;
            }
        }

        return $isSupported;
    }
}