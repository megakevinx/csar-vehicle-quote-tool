<?php

namespace Application\Service;

use Application\Repository\TransactionRepository;
use Application\Repository\NotificationRepository;
use Application\Repository\UserRepository;
use Application\Repository\VehicleTypeRepository;
use Application\Repository\FollowUpCampaignRepository;

use Application\Model\Notification;
use Application\Model\NotificationType;
use Application\Model\NotificationStatus;
use Application\Model\Transaction;
use Application\Model\User;
use Application\Model\VehicleType;
use Application\Model\FollowUpCampaignStep;
use Application\Model\FollowUpCampaign;

class NotificationService
{
    const DATE_TIME_FORMAT = 'Y-m-d H:i:s';

    const VENDOR_NOTIFICATION_ID = "VDR";
    // const SALES_NOTIFICATION_ID = "SLS"; Sales Reps don't create new transactions, they only get calculated quotes
    const CUSTOMER_NOTIFICATION_ID = "CST";

    const CALLBACK_VENDOR_NOTIFICATION_EMAIL_DESTINATION = "wbcfast@gmail.com, cscallcenter2018@gmail.com";
    const CALLBACK_VENDOR_NOTIFICATION_EMAIL_SUBJECT_FORMAT = "New deal captured by a vendor. Please call them back. Ref. Number: %s";
    const CALLBACK_VENDOR_NOTIFICATION_EMAIL_CONTENT_FORMAT = 
        "A new deal has been created by vendor %s %s.\n" .
        "Email: %s, Phone number: %s\n" .
        "\n" .
        "Please call them back to get the rest of the information regarding the pickup and the seller.\n" .
        "\n" .
        "Vehicle: %s %s %s (%s)\n" .
        "Location: %s\n" .
        "Does it have Title? %s\n" .
        "Does it have Keys? %s\n" .
        "Wheels: %s\n" .
        "Will it be delivered to Core Support? %s\n" .
        "\n".
        "Quoted: %s\n".
        "Ref. Number: %s\n";

    const CALLBACK_VENDOR_NOTIFICATION_SMS_DESTINATION = "813-528-3607";
    const CALLBACK_VENDOR_NOTIFICATION_SMS_CONTENT_FORMAT = 
        self::VENDOR_NOTIFICATION_ID . " New deal by vendor %s %s\n" .
        "Contact them at %s %s\n" .
        "%s %s %s\n" .
        "Zip: %s\n" .
        "Quoted $%s\n" .
        "Ref# %s\n";

    const CONTACT_SELLER_NOTIFICATION_EMAIL_DESTINATION = "wbcfast@gmail.com, cscallcenter2018@gmail.com";
    const CONTACT_SELLER_NOTIFICATION_EMAIL_SUBJECT_FORMAT = "New deal captured by a vendor. Ready for inspection. Ref. Number: %s";
    const CONTACT_SELLER_NOTIFICATION_EMAIL_CONTENT_FORMAT = 
        "A new deal has been created by vendor %s %s.\n" .
        "Email: %s, Phone number: %s\n" .
        "\n" .
        "They have already provided the seller and pickup info. " . 
        "Please contact the seller to schedule an inspection.\n" .
        "\n" .
        "Vehicle: %s %s %s (%s)\n" .
        "Location: %s %s, FL %s\n" .
        "Does it have Title? %s\n" .
        "Does it have Keys? %s\n" .
        "Wheels: %s\n" .
        "Will it be delivered to Core Support? %s\n" .
        "\n" .
        "Seller: %s\n" . 
        "Contact them at %s or %s\n" .
        "\n" .
        "Quoted: $%s\n" .
        "Pay to seller: $%s\n".
        "Ref. Number: %s\n";

    const CONTACT_SELLER_NOTIFICATION_SMS_DESTINATION = "813-528-3607";
    const CONTACT_SELLER_NOTIFICATION_SMS_CONTENT_FORMAT = 
        self::VENDOR_NOTIFICATION_ID . " New deal\n" .
        "%s %s %s\n" .
        "%s %s, FL %s\n" .
        "%s\n" .
        "%s %s\n" .
        "%s\n" .
        "Quoted $%s\n" .
        "Pay to seller $%s\n" .
        "Ref# %s\n";

    const ORDER_RECEIVED_EMAIL_SUBJECT_FORMAT = "Your pickup order has been received. Ref. Number: %s";
    const ORDER_RECEIVED_WILL_CONTACT_SELLER_EMAIL_CONTENT_FORMAT =
        "Your pickup order has been received.\n" .
        "We'll take it from here and contact the vehicle's seller and arrange the pickup if needed.\n" .
        "\n" .
        "Vehicle: %s %s %s (%s)\n" .
        "Location: %s %s, FL %s\n" .
        "Does it have Title? %s\n" .
        "Does it have Keys? %s\n" .
        "Wheels: %s\n" .
        "Will it be delivered to Core Support? %s\n" .
        "\n" .
        "Seller: %s\n" . 
        "Seller Contact info: %s or %s\n" .
        "\n" .
        "Quoted: $%s\n" .
        "Payout to seller: $%s\n" .
        "Ref. Number: %s\n" .
        "\n" .
        "Thank you for choosing Core Support!";

    const ORDER_RECEIVED_WILL_CALL_BACK_EMAIL_CONTENT_FORMAT =
        "Your pickup order has been received.\n" .
        "We'll call you back as soon as we can to get the rest of the pickup and seller info.\n" .
        "\n" .
        "Vehicle: %s %s %s (%s)\n" .
        "Location Zip code: %s\n" .
        "Does it have Title? %s\n" .
        "Does it have Keys? %s\n" .
        "Wheels: %s\n" .
        "Will it be delivered to Core Support? %s\n" .
        "\n".
        "Quoted: %s\n".
        "Ref. Number: %s\n" .
        "\n" .
        "Thank you for choosing Core Support!";

    const CONTACT_CUSTOMER_NOTIFICATION_EMAIL_DESTINATION = "wbcfast@gmail.com, cscallcenter2018@gmail.com";
    const CONTACT_CUSTOMER_NOTIFICATION_EMAIL_SUBJECT_FORMAT = "New deal captured by a customer. Ready for inspection. Ref. Number: %s";
    const CONTACT_CUSTOMER_NOTIFICATION_EMAIL_CONTENT_FORMAT = 
        "A new deal has been created by customer %s.\n" .
        "Email: %s, Phone number: %s\n" .
        "\n" .
        "They have already provided the contact and pickup info. Please contact them to schedule an inspection.\n" .
        "\n" .
        "Vehicle: %s %s %s (%s)\n" .
        "Location: %s %s, FL %s\n" .
        "Does it have Title? %s\n" .
        "Does it have Keys? %s\n" .
        "Wheels: %s\n" .
        "Will it be delivered to Core Support? %s\n" .
        "\n" .
        "Quoted: %s\n" .
        "Ref. Number: %s\n";

    const CONTACT_CUSTOMER_NOTIFICATION_SMS_DESTINATION = "813-528-3607";
    const CONTACT_CUSTOMER_NOTIFICATION_SMS_CONTENT_FORMAT = 
        self::CUSTOMER_NOTIFICATION_ID . " New deal\n" .
        "%s %s %s\n" .
        "%s %s, FL %s\n" .
        "%s\n" .
        "%s %s\n" .
        "%s\n" .
        "Quoted $%s\n" .
        "Ref# %s\n";

    const ORDER_RECEIVED_TO_CUSTOMER_EMAIL_SUBJECT_FORMAT= "Thank you for your business, your order has been received. Ref. Number: %s";
    const ORDER_RECEIVED_TO_CUSTOMER_NOTIFICATION_EMAIL_CONTENT_FORMAT =
        "Thank you for your business, your order has been received.\n" .
        "We'll call you back as soon as we can to schedule the inspection of your vehicle.\n" .
        "\n" .
        "Vehicle: %s %s %s (%s)\n" .
        "Location: %s %s, FL %s\n" .
        "Does it have Title? %s\n" .
        "Does it have Keys? %s\n" .
        "Wheels: %s\n" .
        "Will it be delivered to Core Support? %s\n" .
        "\n".
        "Quoted: %s\n".
        "Ref. Number: %s\n" .
        "\n" .
        "Thank you for choosing Core Support!";

    const CALLBACK_CUSTOMER_NOTIFICATION_EMAIL_DESTINATION = "wbcfast@gmail.com, cscallcenter2018@gmail.com";
    const CALLBACK_CUSTOMER_NOTIFICATION_EMAIL_SUBJECT_FORMAT = "New deal captured by a customer. Please call them back. Ref. Number: %s";
    const CALLBACK_CUSTOMER_NOTIFICATION_EMAIL_CONTENT_FORMAT = 
        "A new deal has been created by customer %s.\n" .
        "Email: %s, Phone number: %s\n" .
        "\n" .
        "Please call them back to get the rest of the information regarding their location.\n" .
        "\n" .
        "Vehicle: %s %s %s (%s)\n" .
        "Location Zip code: %s\n" .
        "Does it have Title? %s\n" .
        "Does it have Keys? %s\n" .
        "Wheels: %s\n" .
        "Will it be delivered to Core Support? %s\n" .
        "\n".
        "Quoted: %s\n".
        "Ref. Number: %s\n";

    const CALLBACK_CUSTOMER_NOTIFICATION_SMS_DESTINATION = "813-528-3607";
    const CALLBACK_CUSTOMER_NOTIFICATION_SMS_CONTENT_FORMAT = 
        self::CUSTOMER_NOTIFICATION_ID . " New deal by customer %s\n" .
        "Contact them at %s %s\n" .
        "%s %s %s\n" .
        "Zip: %s\n" .
        "Quoted $%s\n" .
        "Ref# %s\n";

    const ORDER_RECEIVED_WILL_CALLBACK_TO_CUSTOMER_EMAIL_SUBJECT_FORMAT= "Thank you for your business, your order has been received. Ref. Number: %s";
    const ORDER_RECEIVED_WILL_CALLBACK_TO_CUSTOMER_NOTIFICATION_EMAIL_CONTENT_FORMAT =
        "Thank you for your business, your order has been received.\n" .
        "We'll call you back as soon as we can to get the vehicle's location info.\n" .
        "\n" .
        "Vehicle: %s %s %s (%s)\n" .
        "Location Zip code: %s\n" .
        "Does it have Title? %s\n" .
        "Does it have Keys? %s\n" .
        "Wheels: %s\n" .
        "Will it be delivered to Core Support? %s\n" .
        "\n".
        "Quoted: %s\n".
        "Ref. Number: %s\n" .
        "\n" .
        "Thank you for choosing Core Support!";

    const PASSWORD_RESET_EMAIL_SUBJECT = "CSAR Quote Tool Password Reset";
    const PASSWORD_RESET_EMAIL_CONTENT_FORMAT =
        "Please follow the link below to reset your password:\n" .
        "%s?token=%s\n" .
        "If you haven't asked to reset your password, please ignore this message.\n";

    private $notificationRepository;
    private $transactionRepository;
    private $userRepository;
    private $vehicleTypeRepository;
    private $followUpCampaignRepository;

    public function __construct(NotificationRepository $notificationRepository, TransactionRepository $transactionRepository,
        UserRepository $userRepository, VehicleTypeRepository $vehicleTypeRepository, FollowUpCampaignRepository $followUpCampaignRepository)
    {
        $this->notificationRepository = $notificationRepository;
        $this->transactionRepository = $transactionRepository;
        $this->userRepository = $userRepository;
        $this->vehicleTypeRepository = $vehicleTypeRepository;
        $this->followUpCampaignRepository = $followUpCampaignRepository;
    }

    private function getSuccessResult(array $createdNotifications)
    {
        return (object)[
            'isSuccess' => true,
            'createdNotifications' => $createdNotifications
        ];
    }

    private function getTransactionDoesNotExistResult()
    {
        return (object)[
            'isSuccess' => false,
            'createdNotifications' => null,
            'errorMessage' => "The Transaction to notify about does not exist."
        ];
    }

    private function createNotification(Transaction $transaction, string $type, string $destination, string $subject, string $content)
    {
        $notification = new Notification();

        $notification->transactionId = $transaction->id;
        $notification->status = NotificationStatus::$PENDING;
        $notification->scheduled = date(self::DATE_TIME_FORMAT);

        $notification->type = $type;
        $notification->destination = $destination;
        $notification->subject = $subject;
        $notification->content = $content;

        return $this->notificationRepository->saveNotification($notification);
    }

    public function createCallbackVendorNotification(string $referenceNumber)
    {
        $transaction = $this->transactionRepository->get($referenceNumber);

        if ($transaction)
        {
            $vendor = $this->userRepository->get($transaction->createdByUserId);
            $vehicleType = $this->vehicleTypeRepository->get($transaction->vehicleTypeId);

            $savedEmail = $this->createNotification(
                $transaction,
                NotificationType::$EMAIL,
                self::CALLBACK_VENDOR_NOTIFICATION_EMAIL_DESTINATION,
                vsprintf(self::CALLBACK_VENDOR_NOTIFICATION_EMAIL_SUBJECT_FORMAT, [ $notification->transactionId ]),
                vsprintf(
                    self::CALLBACK_VENDOR_NOTIFICATION_EMAIL_CONTENT_FORMAT,
                    [
                        $vendor->firstName,
                        $vendor->lastName,
                        $vendor->email,
                        $vendor->phoneNumber,

                        $transaction->vehicleYear,
                        $transaction->vehicleMake,
                        $transaction->vehicleModel,
                        $vehicleType->description,

                        $transaction->vehicleZipCode,

                        $transaction->vehicleHasTitle ? "Yes" : "No",
                        $transaction->vehicleHasKeys ? "Yes" : "No",
                        $transaction->vehicleWheelsDescription,
                        $transaction->vehicleWillBeDelivered ? "Yes" : "No",

                        $transaction->offeredQuote,
                        $transaction->inspectionId,
                    ]
                )
            );

            $savedSms = $this->createNotification(
                $transaction,
                NotificationType::$SMS,
                self::CALLBACK_VENDOR_NOTIFICATION_SMS_DESTINATION,
                "",
                vsprintf(
                    self::CALLBACK_VENDOR_NOTIFICATION_SMS_CONTENT_FORMAT,
                    [
                        $vendor->firstName,
                        $vendor->lastName,
                        $vendor->email,
                        $vendor->phoneNumber,

                        $transaction->vehicleYear,
                        $transaction->vehicleMake,
                        $transaction->vehicleModel,

                        $transaction->vehicleZipCode,

                        $transaction->offeredQuote,
                        $transaction->inspectionId
                    ]
                )
            );

            return $this->getSuccessResult([$savedEmail, $savedSms]);
        }
        else
        {
            return $this->getTransactionDoesNotExistResult();
        }
    }

    public function createContactSellerNotification(string $referenceNumber)
    {
        $transaction = $this->transactionRepository->get($referenceNumber);

        if ($transaction)
        {
            $vendor = $this->userRepository->get($transaction->createdByUserId);
            $vehicleType = $this->vehicleTypeRepository->get($transaction->vehicleTypeId);

            $savedEmail = $this->createNotification(
                $transaction,
                NotificationType::$EMAIL,
                self::CONTACT_SELLER_NOTIFICATION_EMAIL_DESTINATION,
                vsprintf(self::CONTACT_SELLER_NOTIFICATION_EMAIL_SUBJECT_FORMAT, [ $transaction->inspectionId ]),
                vsprintf(
                    self::CONTACT_SELLER_NOTIFICATION_EMAIL_CONTENT_FORMAT,
                    [
                        $vendor->firstName,
                        $vendor->lastName,
                        $vendor->email,
                        $vendor->phoneNumber,

                        $transaction->vehicleYear,
                        $transaction->vehicleMake,
                        $transaction->vehicleModel,
                        $vehicleType->description,

                        $transaction->vehicleStreetAddress,
                        $transaction->vehicleCity,
                        $transaction->vehicleZipCode,

                        $transaction->vehicleHasTitle ? "Yes" : "No",
                        $transaction->vehicleHasKeys ? "Yes" : "No",
                        $transaction->vehicleWheelsDescription,
                        $transaction->vehicleWillBeDelivered ? "Yes" : "No",

                        $transaction->sellerName,
                        $transaction->sellerPhoneNumber,
                        $transaction->sellerEmail,

                        $transaction->offeredQuote,
                        $transaction->buyerPayout,
                        $transaction->inspectionId,
                    ]
                )
            );

            $savedSms = $this->createNotification(
                $transaction,
                NotificationType::$SMS,
                self::CONTACT_SELLER_NOTIFICATION_SMS_DESTINATION,
                "",
                vsprintf(
                    self::CONTACT_SELLER_NOTIFICATION_SMS_CONTENT_FORMAT,
                    [
                        $transaction->vehicleYear,
                        $transaction->vehicleMake,
                        $transaction->vehicleModel,

                        $transaction->vehicleStreetAddress,
                        $transaction->vehicleCity,
                        $transaction->vehicleZipCode,

                        $transaction->sellerName,
                        $transaction->sellerPhoneNumber,
                        $transaction->sellerEmail,

                        $transaction->vehicleHasTitle ? 'title' : 'no title',

                        $transaction->offeredQuote,
                        $transaction->buyerPayout,
                        $transaction->inspectionId
                    ]
                )
            );

            return $this->getSuccessResult([$savedEmail, $savedSms]);
        }
        else
        {
            return $this->getTransactionDoesNotExistResult();
        }
    }

    public function createOrderReceivedWillContactSellerNotification(string $referenceNumber)
    {
        $transaction = $this->transactionRepository->get($referenceNumber);

        if ($transaction)
        {
            $vendor = $this->userRepository->get($transaction->createdByUserId);
            $vehicleType = $this->vehicleTypeRepository->get($transaction->vehicleTypeId);

            $savedEmail = $this->createNotification(
                $transaction,
                NotificationType::$EMAIL,
                $vendor->email,
                vsprintf(self::ORDER_RECEIVED_EMAIL_SUBJECT_FORMAT, [$transaction->inspectionId]),
                vsprintf(
                    self::ORDER_RECEIVED_WILL_CONTACT_SELLER_EMAIL_CONTENT_FORMAT,
                    [
                        $transaction->vehicleYear,
                        $transaction->vehicleMake,
                        $transaction->vehicleModel,
                        $vehicleType->description,

                        $transaction->vehicleStreetAddress,
                        $transaction->vehicleCity,
                        $transaction->vehicleZipCode,

                        $transaction->vehicleHasTitle ? "Yes" : "No",
                        $transaction->vehicleHasKeys ? "Yes" : "No",
                        $transaction->vehicleWheelsDescription,
                        $transaction->vehicleWillBeDelivered ? "Yes" : "No",

                        $transaction->sellerName,
                        $transaction->sellerPhoneNumber,
                        $transaction->sellerEmail,

                        $transaction->offeredQuote,
                        $transaction->buyerPayout,
                        $transaction->inspectionId,
                    ]
                )
            );

            return $this->getSuccessResult([$savedEmail]);
        }
        else
        {
            return $this->getTransactionDoesNotExistResult();
        }
    }

    public function createOrderReceivedWillCallBackNotification(string $referenceNumber)
    {
        $transaction = $this->transactionRepository->get($referenceNumber);

        if ($transaction)
        {
            $vendor = $this->userRepository->get($transaction->createdByUserId);
            $vehicleType = $this->vehicleTypeRepository->get($transaction->vehicleTypeId);

            $savedEmail = $this->createNotification(
                $transaction,
                NotificationType::$EMAIL,
                $vendor->email,
                vsprintf(self::ORDER_RECEIVED_EMAIL_SUBJECT_FORMAT, [$transaction->inspectionId]),
                vsprintf(
                    self::ORDER_RECEIVED_WILL_CALL_BACK_EMAIL_CONTENT_FORMAT,
                    [
                        $transaction->vehicleYear,
                        $transaction->vehicleMake,
                        $transaction->vehicleModel,
                        $vehicleType->description,
    
                        $transaction->vehicleZipCode,
    
                        $transaction->vehicleHasTitle ? "Yes" : "No",
                        $transaction->vehicleHasKeys ? "Yes" : "No",
                        $transaction->vehicleWheelsDescription,
                        $transaction->vehicleWillBeDelivered ? "Yes" : "No",
    
                        $transaction->offeredQuote,
                        $transaction->inspectionId,
                    ]
                )
            );

            return $this->getSuccessResult([$savedEmail]);
        }
        else
        {
            return $this->getTransactionDoesNotExistResult();
        }
    }

    public function createContactCustomerNotification(string $referenceNumber)
    {
        $transaction = $this->transactionRepository->get($referenceNumber);

        if ($transaction)
        {
            $vehicleType = $this->vehicleTypeRepository->get($transaction->vehicleTypeId);

            $savedEmail = $this->createNotification(
                $transaction,
                NotificationType::$EMAIL,
                self::CONTACT_CUSTOMER_NOTIFICATION_EMAIL_DESTINATION,
                vsprintf(self::CONTACT_CUSTOMER_NOTIFICATION_EMAIL_SUBJECT_FORMAT, [ $transaction->inspectionId ]),
                vsprintf(
                    self::CONTACT_CUSTOMER_NOTIFICATION_EMAIL_CONTENT_FORMAT,
                    [
                        $transaction->sellerName,
                        $transaction->sellerEmail ?? "N/A",
                        $transaction->sellerPhoneNumber ?? "N/A",

                        $transaction->vehicleYear,
                        $transaction->vehicleMake,
                        $transaction->vehicleModel,
                        $vehicleType->description,

                        $transaction->vehicleStreetAddress,
                        $transaction->vehicleCity,
                        $transaction->vehicleZipCode,

                        $transaction->vehicleHasTitle ? "Yes" : "No",
                        $transaction->vehicleHasKeys ? "Yes" : "No",
                        $transaction->vehicleWheelsDescription,
                        $transaction->vehicleWillBeDelivered ? "Yes" : "No",

                        $transaction->offeredQuote,
                        $transaction->inspectionId,
                    ]
                )
            );

            $savedSms = $this->createNotification(
                $transaction,
                NotificationType::$SMS,
                self::CONTACT_CUSTOMER_NOTIFICATION_SMS_DESTINATION,
                "",
                vsprintf(
                    self::CONTACT_CUSTOMER_NOTIFICATION_SMS_CONTENT_FORMAT,
                    [
                        $transaction->vehicleYear,
                        $transaction->vehicleMake,
                        $transaction->vehicleModel,

                        $transaction->vehicleStreetAddress,
                        $transaction->vehicleCity,
                        $transaction->vehicleZipCode,

                        $transaction->sellerName,
                        $transaction->sellerPhoneNumber,
                        $transaction->sellerEmail,
        
                        $transaction->vehicleHasTitle ? 'title' : 'no title',

                        $transaction->offeredQuote,
                        $transaction->inspectionId
                    ]
                )
            );

            return $this->getSuccessResult([$savedEmail, $savedSms]);
        }
        else
        {
            return $this->getTransactionDoesNotExistResult();
        }
    }

    public function createOrderReceivedToCustomerNotification(string $referenceNumber)
    {
        $transaction = $this->transactionRepository->get($referenceNumber);

        if ($transaction)
        {
            $vehicleType = $this->vehicleTypeRepository->get($transaction->vehicleTypeId);

            if ($transaction->sellerEmail)
            {
                $savedEmail = $this->createNotification(
                    $transaction,
                    NotificationType::$EMAIL,
                    $transaction->sellerEmail,
                    vsprintf(self::ORDER_RECEIVED_TO_CUSTOMER_EMAIL_SUBJECT_FORMAT, [ $transaction->inspectionId ]),
                    vsprintf(
                        self::ORDER_RECEIVED_TO_CUSTOMER_NOTIFICATION_EMAIL_CONTENT_FORMAT,
                        [
                            $transaction->vehicleYear,
                            $transaction->vehicleMake,
                            $transaction->vehicleModel,
                            $vehicleType->description,

                            $transaction->vehicleStreetAddress,
                            $transaction->vehicleCity,
                            $transaction->vehicleZipCode,

                            $transaction->vehicleHasTitle ? "Yes" : "No",
                            $transaction->vehicleHasKeys ? "Yes" : "No",
                            $transaction->vehicleWheelsDescription,
                            $transaction->vehicleWillBeDelivered ? "Yes" : "No",

                            $transaction->offeredQuote,
                            $transaction->inspectionId,
                        ]
                    )
                );

                return $this->getSuccessResult([$savedEmail]);
            }
            else
            {
                return (object)[
                    'isSuccess' => false,
                    'createdNotifications' => null,
                    'errorMessage' => "No email registered for the seller."
                ];
            }
        }
        else
        {
            return $this->getTransactionDoesNotExistResult();
        }
    }

    public function createCallBackCustomerNotification(string $referenceNumber)
    {
        $transaction = $this->transactionRepository->get($referenceNumber);

        if ($transaction)
        {
            $vehicleType = $this->vehicleTypeRepository->get($transaction->vehicleTypeId);

            $savedEmail = $this->createNotification(
                $transaction,
                NotificationType::$EMAIL,
                self::CALLBACK_CUSTOMER_NOTIFICATION_EMAIL_DESTINATION,
                vsprintf(self::CALLBACK_CUSTOMER_NOTIFICATION_EMAIL_SUBJECT_FORMAT, [ $transaction->inspectionId ]),
                vsprintf(
                    self::CALLBACK_CUSTOMER_NOTIFICATION_EMAIL_CONTENT_FORMAT,
                    [
                        $transaction->sellerName,
                        $transaction->sellerEmail ?? "N/A",
                        $transaction->sellerPhoneNumber ?? "N/A",

                        $transaction->vehicleYear,
                        $transaction->vehicleMake,
                        $transaction->vehicleModel,
                        $vehicleType->description,

                        $transaction->vehicleZipCode,

                        $transaction->vehicleHasTitle ? "Yes" : "No",
                        $transaction->vehicleHasKeys ? "Yes" : "No",
                        $transaction->vehicleWheelsDescription,
                        $transaction->vehicleWillBeDelivered ? "Yes" : "No",

                        $transaction->offeredQuote,
                        $transaction->inspectionId,
                    ]
                )
            );

            $savedSms = $this->createNotification(
                $transaction,
                NotificationType::$SMS,
                self::CALLBACK_CUSTOMER_NOTIFICATION_SMS_DESTINATION,
                "",
                vsprintf(
                    self::CALLBACK_CUSTOMER_NOTIFICATION_SMS_CONTENT_FORMAT,
                    [
                        $transaction->sellerName,
                        $transaction->sellerPhoneNumber,
                        $transaction->sellerEmail,
        
                        $transaction->vehicleYear,
                        $transaction->vehicleMake,
                        $transaction->vehicleModel,
        
                        $transaction->vehicleZipCode,
        
                        $transaction->offeredQuote,
                        $transaction->inspectionId
                    ]
                )
            );

            return $this->getSuccessResult([$savedEmail, $savedSms]);
        }
        else
        {
            return $this->getTransactionDoesNotExistResult();
        }
    }

    public function createOrderReceivedWillCallBackToCustomerNotification(string $referenceNumber)
    {
        $transaction = $this->transactionRepository->get($referenceNumber);

        if ($transaction)
        {
            $vehicleType = $this->vehicleTypeRepository->get($transaction->vehicleTypeId);

            if ($transaction->sellerEmail)
            {
                $savedEmail = $this->createNotification(
                    $transaction,
                    NotificationType::$EMAIL,
                    $transaction->sellerEmail,
                    vsprintf(self::ORDER_RECEIVED_WILL_CALLBACK_TO_CUSTOMER_EMAIL_SUBJECT_FORMAT, [ $transaction->inspectionId ]),
                    vsprintf(
                        self::ORDER_RECEIVED_WILL_CALLBACK_TO_CUSTOMER_NOTIFICATION_EMAIL_CONTENT_FORMAT,
                        [
                            $transaction->vehicleYear,
                            $transaction->vehicleMake,
                            $transaction->vehicleModel,
                            $vehicleType->description,
            
                            $transaction->vehicleZipCode,
            
                            $transaction->vehicleHasTitle ? "Yes" : "No",
                            $transaction->vehicleHasKeys ? "Yes" : "No",
                            $transaction->vehicleWheelsDescription,
                            $transaction->vehicleWillBeDelivered ? "Yes" : "No",
            
                            $transaction->offeredQuote,
                            $transaction->inspectionId,
                        ]
                    )
                );

                return $this->getSuccessResult([$savedEmail]);
            }
            else
            {
                return (object)[
                    'isSuccess' => false,
                    'createdNotifications' => null,
                    'errorMessage' => "No email registered for the seller."
                ];
            }
        }
        else
        {
            return $this->getTransactionDoesNotExistResult();
        }
    }

    private function createFollowUpCampaignNotifications(string $referenceNumber, string $followUpCampaign)
    {
        $transaction = $this->transactionRepository->get($referenceNumber);
        $followUpCampaignSteps = $this->followUpCampaignRepository->getStepsFor($followUpCampaign);

        $createdNotifications = [];

        foreach ($followUpCampaignSteps as $step)
        {
            $destination = "";

            if ($step->notificationType == NotificationType::$EMAIL)
            {
                $destination = $transaction->sellerEmail;
            }
            else if ($step->notificationType == NotificationType::$SMS)
            {
                $destination = $transaction->sellerPhoneNumber;
            }
            else
            {
                continue;
            }

            $notification = new Notification();

            $notification->transactionId = $transaction->id;

            $notification->type = $step->notificationType;
            $notification->status = NotificationStatus::$PENDING;

            $notification->destination = $destination;

            $notification->subject = $step->messageSubject ?? "";
            $notification->content = $step->messageContent;

            $scheduleTime = strtotime("+" . $step->triggerTime  . " minutes"); // gets a time representing $step->triggerTime minutes in the future
            $notification->scheduled = date(self::DATE_TIME_FORMAT, $scheduleTime);

            $createdNotifications[] = $this->notificationRepository->saveNotification($notification);
        }

        return $this->getSuccessResult($createdNotifications);
    }

    public function createAcceptedFollowUpCampaignNotifications(string $referenceNumber)
    {
        return $this->createFollowUpCampaignNotifications($referenceNumber, FollowUpCampaign::$ACCEPTED);
    }

    public function createStillShoppingFollowUpCampaignNotifications(string $referenceNumber)
    {
        return $this->createFollowUpCampaignNotifications($referenceNumber, FollowUpCampaign::$STILL_SHOPPING);
    }

    public function createNotYetFollowUpCampaignNotifications(string $referenceNumber)
    {
        return $this->createFollowUpCampaignNotifications($referenceNumber, FollowUpCampaign::$NOT_YET);
    }

    public function createPasswordResetNotification(User $user, string $passwordResetBaseUrl)
    {
        $notification = new Notification();

        $notification->type = NotificationType::$EMAIL;
        $notification->status = NotificationStatus::$PENDING;
        $notification->scheduled = date(self::DATE_TIME_FORMAT);

        $notification->destination = $user->email;

        $notification->subject = self::PASSWORD_RESET_EMAIL_SUBJECT;

        $notification->content = vsprintf(
            self::PASSWORD_RESET_EMAIL_CONTENT_FORMAT,
            [
                $passwordResetBaseUrl,
                $user->passwordResetToken
            ]
        );

        return $this->notificationRepository->saveNotification($notification);
    }
}