<?php

namespace Application\Service;

use Application\Repository\QuoteAmountRepository;

use Application\Model\QuoteAmount;

class QuoteCalculationRuleService
{
    private static $VEHICLE_TYPE_FEATURE_TYPE = "TYPE";

    private $repository;

    public function __construct(QuoteAmountRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getAllRules()
    {
        return $this->repository->getAll();
    }

    public function saveRules(array $rules)
    {
        $updatedRules = [];

        foreach ($rules as $rule)
        {
            $ruleToUpdate = $this->repository->get($rule["id"]);

            if ($ruleToUpdate->priceModifier != $rule["priceModifier"])
            {
                $ruleToUpdate->priceModifier = $rule["priceModifier"];
                $updatedRules[] = $this->repository->update($ruleToUpdate);
            }
        }

        return (object)[
            'isSuccess' => true,
            'updatedRules' => $updatedRules
        ];
    }

    public function changeVehicleTypesPriceModifiersBy(array $data)
    {
        $priceChange = $data["basePriceChange"];
        $updatedRules = [];

        $rulesToUpdate = $this->repository->getByFeatureType(QuoteCalculationRuleService::$VEHICLE_TYPE_FEATURE_TYPE);

        foreach ($rulesToUpdate as $rule)
        {
            $rule->priceModifier = $rule->priceModifier + $priceChange;
            $updatedRules[] = $this->repository->update($rule);
        }

        return (object)[
            'isSuccess' => true,
            'updatedRules' => $updatedRules
        ];
    }
}