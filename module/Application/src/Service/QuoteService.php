<?php

namespace Application\Service;

use Application\Repository\QuoteAmountRepository;
use Application\Repository\VehicleTypeRepository;
use Application\Repository\ZipRepository;
use Application\Repository\CountyRepository;
use Application\Repository\QuoteAmountOverrideRepository;

class QuoteService
{
    const WILDCARD = 'Any';

    private $quoteAmountRepository;
    private $vehicleTypeRepository;
    private $zipRepository;
    private $countyRepository;
    private $quoteAmountOverrideRepository;

    public function __construct(QuoteAmountRepository $quoteAmountRepository, VehicleTypeRepository $vehicleTypeRepository,
        ZipRepository $zipRepository, CountyRepository $countyRepository, QuoteAmountOverrideRepository $quoteAmountOverrideRepository)
    {
        $this->quoteAmountRepository = $quoteAmountRepository;
        $this->vehicleTypeRepository = $vehicleTypeRepository;
        $this->zipRepository = $zipRepository;
        $this->countyRepository = $countyRepository;
        $this->quoteAmountOverrideRepository = $quoteAmountOverrideRepository;
    }

    public function calculateQuote(array $data)
    {
        $getOverrideFuncs = [
            function($data) { // Looks for an override that matches this exact vehicle
                return $this->quoteAmountOverrideRepository->getBy(
                    $data['vehicleYear'], $data['vehicleYear'], $data['vehicleMake'], $data['vehicleModel']
                );
            },
            function($data) { // Looks for a ranged override that includes this vehicle
                return $this->quoteAmountOverrideRepository
                    ->getWithFromYearLessThanOrEqualAndToYearGreaterThanOrEqual(
                        $data['vehicleYear'], $data['vehicleYear'], $data['vehicleMake'], $data['vehicleModel']
                    );
            },
            function($data) { // Looks for a year-or-newer override that includes this vehicle
                return $this->quoteAmountOverrideRepository
                    ->getWithFromYearLessThanOrEqual(
                        $data['vehicleYear'], self::WILDCARD, $data['vehicleMake'], $data['vehicleModel']
                    );
            },
            function($data) { // Looks for a year-or-older override that includes this vehicle
                return $this->quoteAmountOverrideRepository
                    ->getWithToYearGreaterThanOrEqual(
                        self::WILDCARD, $data['vehicleYear'], $data['vehicleMake'], $data['vehicleModel']
                    );
            },
            function($data) { // Looks for an any-year override that includes this vehicle
                return $this->quoteAmountOverrideRepository->getBy(
                    self::WILDCARD, self::WILDCARD, $data['vehicleMake'], $data['vehicleModel']
                );
            },

            function($data) { // Looks for an override that matches any model of this vehicle's make and year
                return $this->quoteAmountOverrideRepository->getBy(
                    $data['vehicleYear'], $data['vehicleYear'], $data['vehicleMake'], self::WILDCARD
                );
            },
            function($data) { // Looks for a ranged override that includes any model of this vehicle's make
                return $this->quoteAmountOverrideRepository
                    ->getWithFromYearLessThanOrEqualAndToYearGreaterThanOrEqual(
                        $data['vehicleYear'], $data['vehicleYear'], $data['vehicleMake'], self::WILDCARD
                    );
            },
            function($data) { // Looks for a year-or-newer override that includes any model of this vehicle's make
                return $this->quoteAmountOverrideRepository
                    ->getWithFromYearLessThanOrEqual(
                        $data['vehicleYear'], self::WILDCARD, $data['vehicleMake'], self::WILDCARD
                    );
            },
            function($data) { // Looks for a year-or-older override that includes any model of this vehicle's make
                return $this->quoteAmountOverrideRepository
                    ->getWithToYearGreaterThanOrEqual(
                        self::WILDCARD, $data['vehicleYear'], $data['vehicleMake'], self::WILDCARD
                    );
            },
            function($data) { // Looks for an any-year override that includes any model of this vehicle's make
                return $this->quoteAmountOverrideRepository->getBy(
                    self::WILDCARD, self::WILDCARD, $data['vehicleMake'], self::WILDCARD
                );
            },
        ];

        foreach ($getOverrideFuncs as $getOverride)
        {
            $override = $getOverride($data);

            if ($override)
            {
                return $override->quote;
            }
        }

        return $this->calculateUsingRules($data);
    }

    private function calculateUsingRules(array $data)
    {
        $fieldToFeatureTypeMap = array(
            'vehicleTypeId' => 'TYPE',
            'vehicleWheelsDescription' => 'RIMS',
            'vehicleHasTitle' => 'HAS_TITLE',
            'vehicleZipCode' => 'LOCATION',
            'vehicleWillBeDelivered' => 'SELF_DELIVERY',
            'vehicleHasKeys' => 'HAS_KEYS'
        );

        $fieldsForQuoteCalculation = [];

        foreach ($fieldToFeatureTypeMap as $field => $featureType)
        {
            $fieldsForQuoteCalculation[$field] = $data[$field];
        }

        $vehicleTypes = $this->vehicleTypeRepository->getAllMap();
        $quoteAmounts = $this->quoteAmountRepository->getAllMap();

        $calculatedQuote = 0;

        foreach ($fieldsForQuoteCalculation as $field => $fieldValue)
        {
            if ($field == "vehicleTypeId")
            {
                $fieldValue = $vehicleTypes[$fieldValue];
            }
            else if ($field == "vehicleZipCode")
            {
                // If the seller is NOT delivering the vehicle, we have to calculate the amount deducted from the quote
                if ($data['vehicleWillBeDelivered'] == "no")
                {
                    //Calculate county based on zipcode
                    $zip = $this->zipRepository->getByCode($fieldValue);
                    $county = $this->countyRepository->get($zip->countyId);
                    $fieldValue = $county->name;
                }
                else if ($data['vehicleWillBeDelivered'] == "yes")
                {
                    // if the vehicle will be delivered then we don't include location into the calculation
                    continue;
                }
            }

            $amount = $quoteAmounts[$fieldToFeatureTypeMap[$field]][strtolower($fieldValue)];

            $calculatedQuote = $calculatedQuote + $amount;
        }

        return $calculatedQuote;
    }
}