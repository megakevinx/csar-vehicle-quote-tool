<?php

namespace Application\Service;

use Application\Repository\TransactionRepository;

use Application\Model\Transaction;

class TransactionService
{
    private $transactionRepository;

    public function __construct(TransactionRepository $transactionRepository)
    {
        $this->transactionRepository = $transactionRepository;
    }

    public function createTransaction(array $data)
    {
        $transactionToSave = new Transaction();

        // TODO: This is assuming customers don't have login information
        $transactionToSave->createdByUserId = $data['createdByUserId'] ?? null;

        $transactionToSave->vehicleYear = $data['vehicleYear'];
        $transactionToSave->vehicleMake = $data['vehicleMake'];
        $transactionToSave->vehicleModel = $data['vehicleModel'];
        $transactionToSave->vehicleTypeId = $data['vehicleTypeId'];

        $transactionToSave->vehicleHasKeys = $data['vehicleHasKeys'] == 'yes' ? true : false;
        $transactionToSave->vehicleHasTitle = $data['vehicleHasTitle'] == 'yes' ? true : false;
        $transactionToSave->vehicleWheelsDescription = $data['vehicleWheelsDescription'];
        $transactionToSave->vehicleWillBeDelivered = $data['vehicleWillBeDelivered'] == 'yes' ? true : false;
        $transactionToSave->vehicleZipCode = $data['vehicleZipCode'];

        $transactionToSave->vehicleCounty = $data['vehicleCounty'];

        $transactionToSave->offeredQuote = $data['offeredQuote'];

        $transactionToSave->sellerName = $data['sellerName'] ?? null;
        $transactionToSave->sellerPhoneNumber = $data['sellerPhoneNumber'] ?? null;
        $transactionToSave->sellerEmail = $data['sellerEmail'] ?? null;

        $savedTransaction = $this->transactionRepository->saveTransaction($transactionToSave);

        return (object)[
            'isSuccess' => true,
            'savedTransaction' => $savedTransaction
        ];
    }

    public function updateTransaction(string $inspectionId, array $data)
    {
        // TODO: validate that there is a transaction to update
        $transactionToUpdate = $this->transactionRepository->get($inspectionId);

        if ($transactionToUpdate)
        {
            $transactionToUpdate->vehicleStreetAddress = $data['vehicleStreetAddress'];
            $transactionToUpdate->vehicleCity = $data['vehicleCity'];

            $transactionToUpdate->sellerName = $data['sellerName'] ?? $transactionToUpdate->sellerName;
            $transactionToUpdate->sellerPhoneNumber = $data['sellerPhoneNumber'] ?? $transactionToUpdate->sellerPhoneNumber;
            $transactionToUpdate->sellerEmail = $data['sellerEmail'] ?? $transactionToUpdate->sellerEmail;

            $transactionToUpdate->buyerPayout = $data['buyerPayout'] ?? $transactionToUpdate->buyerPayout;

            $updatedTransaction = $this->transactionRepository->updateTransaction($transactionToUpdate);

            return (object)[
                'isSuccess' => true,
                'updatedTransaction' => $updatedTransaction
            ];
        }
        else
        {
            return (object)[
                'isSuccess' => false
            ];
        }
    }
}