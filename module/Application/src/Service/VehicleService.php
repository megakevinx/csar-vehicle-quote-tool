<?php

namespace Application\Service;

use Application\Repository\VehicleModelSpecRepository;
use Application\Repository\VehicleTypeRepository;

class VehicleBodyClasses
{
    public const CAR = "Car";
    public const TRUCK = "Truck";
    public const SUV = "SUV";
    public const VAN = "Van";
    public const MINIVAN = "Minivan";
}

class VehicleSizeClasses
{
    public const COMPACT = "Compact";
    public const MIDSIZE = "Midsize";
    public const LARGE = "Large";
}

class VehicleService
{
    const VEHICLE_BODY_TYPE_MAP = [
        "Coupe" => VehicleBodyClasses::CAR,
        "Convertible" => VehicleBodyClasses::CAR,
        "Roadster" => VehicleBodyClasses::CAR,
        "Sedan" => VehicleBodyClasses::CAR,
        "Hatchback" => VehicleBodyClasses::CAR,
        "Station Wagon" => VehicleBodyClasses::CAR,
        "Midsize Cars" => VehicleBodyClasses::CAR,
        "Compact Cars" => VehicleBodyClasses::CAR,
        "Small Station Wagons" => VehicleBodyClasses::CAR,
        "Midsize Station Wagons" => VehicleBodyClasses::CAR,
        "Subcompact Cars" => VehicleBodyClasses::CAR,
        "Mini Compact Cars" => VehicleBodyClasses::CAR,
        "Two Seaters" => VehicleBodyClasses::CAR,
        "Large Cars" => VehicleBodyClasses::CAR,

        "SUV" => VehicleBodyClasses::SUV,
        "Crossover" => VehicleBodyClasses::SUV,
        "Small Sport Utility Vehicles" => VehicleBodyClasses::SUV,
        "Standard Sport Utility Vehicles" => VehicleBodyClasses::SUV,
        "Sport Utility Vehicles" => VehicleBodyClasses::SUV,

        "Minivan" => VehicleBodyClasses::MINIVAN,
        "Passenger Vans" => VehicleBodyClasses::MINIVAN,

        "Van" => VehicleBodyClasses::VAN,
        "Panel Van" => VehicleBodyClasses::VAN,
        "Cargo Vans" => VehicleBodyClasses::VAN,

        "Standard Pickup Trucks" => VehicleBodyClasses::TRUCK,
        "Small Pickup Trucks" => VehicleBodyClasses::TRUCK,
        "Pickup" => VehicleBodyClasses::TRUCK
    ];

    // https://en.wikipedia.org/wiki/Car_classification
    // US Insurance Institute for Highway Safety | Highway Loss Data Institute 'Guide to car size groups' (includes minivans)
    const CAR_SIZE_GROUP_SPEC = [
        [ "areaGroup" => 1, "weightGroup" => 1, "sizeClass" => VehicleSizeClasses::COMPACT ],
        [ "areaGroup" => 1, "weightGroup" => 2, "sizeClass" => VehicleSizeClasses::COMPACT ],
        [ "areaGroup" => 1, "weightGroup" => 3, "sizeClass" => VehicleSizeClasses::COMPACT ],
        [ "areaGroup" => 1, "weightGroup" => 4, "sizeClass" => VehicleSizeClasses::COMPACT ],
        [ "areaGroup" => 1, "weightGroup" => 5, "sizeClass" => VehicleSizeClasses::MIDSIZE ],

        [ "areaGroup" => 2, "weightGroup" => 1, "sizeClass" => VehicleSizeClasses::COMPACT ],
        [ "areaGroup" => 2, "weightGroup" => 2, "sizeClass" => VehicleSizeClasses::COMPACT ],
        [ "areaGroup" => 2, "weightGroup" => 3, "sizeClass" => VehicleSizeClasses::MIDSIZE ],
        [ "areaGroup" => 2, "weightGroup" => 4, "sizeClass" => VehicleSizeClasses::MIDSIZE ],
        [ "areaGroup" => 2, "weightGroup" => 5, "sizeClass" => VehicleSizeClasses::MIDSIZE ],

        [ "areaGroup" => 3, "weightGroup" => 1, "sizeClass" => VehicleSizeClasses::COMPACT ],
        [ "areaGroup" => 3, "weightGroup" => 2, "sizeClass" => VehicleSizeClasses::MIDSIZE ],
        [ "areaGroup" => 3, "weightGroup" => 3, "sizeClass" => VehicleSizeClasses::MIDSIZE ],
        [ "areaGroup" => 3, "weightGroup" => 4, "sizeClass" => VehicleSizeClasses::LARGE ],
        [ "areaGroup" => 3, "weightGroup" => 5, "sizeClass" => VehicleSizeClasses::LARGE ],

        [ "areaGroup" => 4, "weightGroup" => 1, "sizeClass" => VehicleSizeClasses::COMPACT ],
        [ "areaGroup" => 4, "weightGroup" => 2, "sizeClass" => VehicleSizeClasses::MIDSIZE ],
        [ "areaGroup" => 4, "weightGroup" => 3, "sizeClass" => VehicleSizeClasses::LARGE ],
        [ "areaGroup" => 4, "weightGroup" => 4, "sizeClass" => VehicleSizeClasses::LARGE ],
        [ "areaGroup" => 4, "weightGroup" => 5, "sizeClass" => VehicleSizeClasses::LARGE ],

        [ "areaGroup" => 5, "weightGroup" => 1, "sizeClass" => VehicleSizeClasses::MIDSIZE ],
        [ "areaGroup" => 5, "weightGroup" => 2, "sizeClass" => VehicleSizeClasses::MIDSIZE ],
        [ "areaGroup" => 5, "weightGroup" => 3, "sizeClass" => VehicleSizeClasses::LARGE ],
        [ "areaGroup" => 5, "weightGroup" => 4, "sizeClass" => VehicleSizeClasses::LARGE ],
        [ "areaGroup" => 5, "weightGroup" => 5, "sizeClass" => VehicleSizeClasses::LARGE ]
    ];

    private $vehicleModelSpecRepository;
    private $vehicleTypeRepository;

    public function __construct(VehicleModelSpecRepository $vehicleModelSpecRepository, VehicleTypeRepository $vehicleTypeRepository)
    {
        $this->vehicleModelSpecRepository = $vehicleModelSpecRepository;
        $this->vehicleTypeRepository = $vehicleTypeRepository;
    }

    public function getVehicleType(array $data)
    {
        $year = $data['year'];
        $make = $data['make'];
        $model = $data['model'];

        $vehicleSpec = $this->vehicleModelSpecRepository->get($year, $make, $model);

        if (! $vehicleSpec) {
            return (object)[
                "isSuccess" => false,
                "message" => "Could not find vehicle type: " . $year . " " . $make . " " . $model
            ];
        }

        $bodyClass = $this->getVehicleBodyClass($vehicleSpec->bodyStyle);
        $sizeClass = $this->getVehicleSizeClass($bodyClass, $vehicleSpec->weight, $vehicleSpec->length, $vehicleSpec->width);

        $type = $this->vehicleTypeRepository->getByDescription($sizeClass . " " . $bodyClass);

        return (object)[
            "isSuccess" => true,
            "vehicleType" => $type
        ];
    }

    private function getVehicleBodyClass($bodyStyle)
    {
        if (! $bodyStyle || ! array_key_exists($bodyStyle, self::VEHICLE_BODY_TYPE_MAP)) {
            return VehicleBodyClasses::CAR;
        }

        return self::VEHICLE_BODY_TYPE_MAP[$bodyStyle];
    }

    // https://en.wikipedia.org/wiki/Car_classification
    // US Insurance Institute for Highway Safety | Highway Loss Data Institute 'Guide to car size groups' (includes minivans)
    // US IIHS|HLDI Guide to SUV size groups
    private function getVehicleSizeClass($bodyClass, $weight, $lenght, $width)
    {
        if (in_array($bodyClass, [VehicleBodyClasses::CAR, VehicleBodyClasses::MINIVAN])) {

            if (! $weight || ! $lenght || ! $width) {
                return VehicleSizeClasses::MIDSIZE;
            }

            $areaGroup = $this->getAreaGroup($lenght * $width);
            $weightGroup = $this->getWeightGroup($weight);

            $sizeClass = array_filter(
                self::CAR_SIZE_GROUP_SPEC,
                function ($item) use ($areaGroup, $weightGroup) {
                    return $item["areaGroup"] == $areaGroup && $item["weightGroup"] == $weightGroup;
                }
            );

            return array_values($sizeClass)[0]["sizeClass"];

        } else if (in_array($bodyClass, [VehicleBodyClasses::TRUCK, VehicleBodyClasses::SUV, VehicleBodyClasses::VAN])) {

            if (! $weight) {
                return VehicleSizeClasses::MIDSIZE;
            }

            if ($weight <= 1700) {
                return VehicleSizeClasses::COMPACT;
            } else if ($weight <= 2150) {
                return VehicleSizeClasses::MIDSIZE;
            } else {
                return VehicleSizeClasses::LARGE;
            }
        }
    }

    private function getAreaGroup($area)
    {
        if ($area <= 7400000) {
            return 1;
        } else if ($area <= 8400000) {
            return 2;
        } else if ($area <= 9300000) {
            return 3;
        } else if ($area <= 10200000) {
            return 4;
        } else {
            return 5;
        }
    }

    private function getWeightGroup($weight)
    {
        if ($weight <= 1150) {
            return 1;
        } else if ($weight <= 1350) {
            return 2;
        } else if ($weight <= 1600) {
            return 3;
        } else if ($weight <= 1800) {
            return 4;
        } else {
            return 5;
        }
    }
}
