<?php

namespace ApplicationTest\Controller\Form;

use \PHPUnit\Framework\TestCase;

use Zend\Form\Form;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilter;

use Application\Controller\Form\AppSettingsForm;

class AppSettingsFormTest extends BaseFormTestCase
{
    protected function getFormInstance()
    {
        return new AppSettingsForm();
    }

    public function dataProvider_TestValidationIsCorrect()
    {
        return [
            'All data valid' => [
                true, [ 'vehicleDropOffAddress' => '10 Main St Tampa, FL 33503', 'contactPhoneNumber' => '555-555-5555' ]
            ],
            'Vehicle drop off address missing' => [
                false, [ 'vehicleDropOffAddress' => '', 'contactPhoneNumber' => '555-555-5555' ]
            ],
            'Vehicle drop off address too long' => [
                false, [
                    'vehicleDropOffAddress' => '10 Main St Tampa, FL 33503 10 Main St Tampa, FL 33503 10 Main St Tampa, FL 33503 10 Main St Tampa, FL 33503 10 Main St Tampa, FL 33503',
                    'contactPhoneNumber' => '555-555-5555'
                ]
            ],
            'Contact phone number missing' => [
                false, [ 'vehicleDropOffAddress' => '10 Main St Tampa, FL 33503', 'contactPhoneNumber' => '' ]
            ],
            'Contact phone number invalid' => [
                false, [ 'vehicleDropOffAddress' => '10 Main St Tampa, FL 33503', 'contactPhoneNumber' => 'asdqwe321' ]
            ],
            'Contact phone number too long' => [
                false, [
                    'vehicleDropOffAddress' => '10 Main St Tampa, FL 33503',
                    'contactPhoneNumber' => '555555555555555555555555555555'
                ]
            ],
            'Contact phone number with spaces' => [
                true, [ 'vehicleDropOffAddress' => '10 Main St Tampa, FL 33503', 'contactPhoneNumber' => '555 555 5555' ]
            ],
            'Contact phone number with parens and spaces' => [
                true, [ 'vehicleDropOffAddress' => '10 Main St Tampa, FL 33503', 'contactPhoneNumber' => '(555) 555 5555' ]
            ],
            'Contact phone number with parens and dashes' => [
                true, [ 'vehicleDropOffAddress' => '10 Main St Tampa, FL 33503', 'contactPhoneNumber' => '(555)-555-5555' ]
            ],
            'Contact phone number with parens and dashes and spaces' => [
                true, [ 'vehicleDropOffAddress' => '10 Main St Tampa, FL 33503', 'contactPhoneNumber' => '(555) 555-5555' ]
            ],
            'Contact phone number all together' => [
                true, [ 'vehicleDropOffAddress' => '10 Main St Tampa, FL 33503', 'contactPhoneNumber' => '5555555555' ]
            ],
        ];
    }
}