<?php

namespace ApplicationTest\Controller\Form;

use \PHPUnit\Framework\TestCase;

use Zend\Form\Form;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilter;

abstract class BaseFormTestCase extends TestCase
{
    protected abstract function getFormInstance();

    /**
    * @dataProvider dataProvider_TestValidationIsCorrect
    */
    public function testValidationIsCorrect($expected, $data)
    {
        // Arrange
        $form = $this->getFormInstance();
        $form->setData($data);

        // Act
        $actual = $form->isValid();

        // Assert
        $this->assertEquals($expected, $actual);
    }
}