<?php

namespace ApplicationTest\Controller\Form;

use \PHPUnit\Framework\TestCase;

use Zend\Form\Form;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilter;

use Application\Controller\Form\ChangePriceModifiersForm;

class ChangePriceModifiersFormTest extends BaseFormTestCase
{
    protected function getFormInstance()
    {
        return new ChangePriceModifiersForm();
    }

    public function dataProvider_TestValidationIsCorrect()
    {
        return [
            'All data valid' => [
                true, [ 'basePriceChange' => '10' ]
            ],
            'Base price change not a number' => [
                false, [ 'basePriceChange' => 'asd' ]
            ],
            'Base price change too big' => [
                false, [ 'basePriceChange' => '101' ]
            ],
            'Base price change too low' => [
                false, [ 'basePriceChange' => '-101' ]
            ],
            'Base price change is float' => [
                false, [ 'basePriceChange' => '50.50' ]
            ],
        ];
    }
}