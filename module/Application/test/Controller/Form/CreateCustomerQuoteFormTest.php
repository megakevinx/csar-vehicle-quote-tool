<?php

namespace ApplicationTest\Controller\Form;

use \PHPUnit\Framework\TestCase;

use Zend\Form\Form;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilter;

use Application\Controller\Form\CreateCustomerQuoteForm;

class CreateCustomerQuoteFormTest extends BaseFormTestCase
{
    protected function getFormInstance()
    {
        return new CreateCustomerQuoteForm();
    }

    public function dataProvider_TestValidationIsCorrect()
    {
        return [
            'All data valid' => [
                true, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",

                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",

                    'sellerName' => "Seller Name",
                    'sellerPhoneNumber' => "555-555-5555",
                    'sellerEmail' => "text@gmail.com",
                ]
            ],
            'Vehicle year missing' => [
                false, [
                    'vehicleYear' => "0",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",
                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",

                    'sellerName' => "Seller Name",
                    'sellerPhoneNumber' => "555-555-5555",
                    'sellerEmail' => "text@gmail.com",
                ]
            ],
            'Vehicle year too old' => [
                false, [
                    'vehicleYear' => "1980",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",
                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",

                    'sellerName' => "Seller Name",
                    'sellerPhoneNumber' => "555-555-5555",
                    'sellerEmail' => "text@gmail.com",
                ]
            ],
            'Vehicle year just old enough' => [
                true, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",
                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",

                    'sellerName' => "Seller Name",
                    'sellerPhoneNumber' => "555-555-5555",
                    'sellerEmail' => "text@gmail.com",
                ]
            ],
            'Vehicle year not a number' => [
                false, [
                    'vehicleYear' => "abcd",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",
                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",

                    'sellerName' => "Seller Name",
                    'sellerPhoneNumber' => "555-555-5555",
                    'sellerEmail' => "text@gmail.com",
                ]
            ],
            'Vehicle make missing' => [
                false, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",
                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",

                    'sellerName' => "Seller Name",
                    'sellerPhoneNumber' => "555-555-5555",
                    'sellerEmail' => "text@gmail.com",
                ]
            ],
            'Vehicle make too long' => [
                false, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyotatoyotatoyotatoyotatoyotatoyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",
                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",

                    'sellerName' => "Seller Name",
                    'sellerPhoneNumber' => "555-555-5555",
                    'sellerEmail' => "text@gmail.com",
                ]
            ],
            'Vehicle model missing' => [
                false, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "",
                    'vehicleTypeId' => "1",
                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",

                    'sellerName' => "Seller Name",
                    'sellerPhoneNumber' => "555-555-5555",
                    'sellerEmail' => "text@gmail.com",
                ]
            ],
            'Vehicle model too long' => [
                false, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "suprasuprasuprasuprasuprasuprasuprasuprasupra",
                    'vehicleTypeId' => "1",
                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",
                    'sellerName' => "Seller Name",
                    'sellerPhoneNumber' => "555-555-5555",
                    'sellerEmail' => "text@gmail.com",
                ]
            ],
            'Vehicle type id missing' => [
                false, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "",
                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",
                    'sellerName' => "Seller Name",
                    'sellerPhoneNumber' => "555-555-5555",
                    'sellerEmail' => "text@gmail.com",
                ]
            ],
            'Vehicle type id not a number' => [
                false, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "lol",
                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",
                    'sellerName' => "Seller Name",
                    'sellerPhoneNumber' => "555-555-5555",
                    'sellerEmail' => "text@gmail.com",
                ]
            ],
            'Vehicle has keys missing' => [
                false, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",
                    'vehicleHasKeys' => "",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",
                    'sellerName' => "Seller Name",
                    'sellerPhoneNumber' => "555-555-5555",
                    'sellerEmail' => "text@gmail.com",
                ]
            ],
            'Vehicle does not have keys' => [
                true, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",
                    'vehicleHasKeys' => "no",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",
                    'sellerName' => "Seller Name",
                    'sellerPhoneNumber' => "555-555-5555",
                    'sellerEmail' => "text@gmail.com",
                ]
            ],
            'Vehicle has keys invalid' => [
                false, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",
                    'vehicleHasKeys' => "whatever",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",
                    'sellerName' => "Seller Name",
                    'sellerPhoneNumber' => "555-555-5555",
                    'sellerEmail' => "text@gmail.com",
                ]
            ],
            'Vehicle has title missing' => [
                false, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",
                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",
                    'sellerName' => "Seller Name",
                    'sellerPhoneNumber' => "555-555-5555",
                    'sellerEmail' => "text@gmail.com",
                ]
            ],
            'Vehicle does not have title' => [
                true, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",
                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "no",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",
                    'sellerName' => "Seller Name",
                    'sellerPhoneNumber' => "555-555-5555",
                    'sellerEmail' => "text@gmail.com",
                ]
            ],
            'Vehicle has title invalid' => [
                false, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",
                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "whatever",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",
                    'sellerName' => "Seller Name",
                    'sellerPhoneNumber' => "555-555-5555",
                    'sellerEmail' => "text@gmail.com",
                ]
            ],
            'Vehicle wheels missing' => [
                false, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",
                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",
                    'sellerName' => "Seller Name",
                    'sellerPhoneNumber' => "555-555-5555",
                    'sellerEmail' => "text@gmail.com",
                ]
            ],
            'Vehicle has steel wheels' => [
                true, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",
                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",
                    'sellerName' => "Seller Name",
                    'sellerPhoneNumber' => "555-555-5555",
                    'sellerEmail' => "text@gmail.com",
                ]
            ],
            'Vehicle has aluminum wheels' => [
                true, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",
                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "aluminum",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",
                    'sellerName' => "Seller Name",
                    'sellerPhoneNumber' => "555-555-5555",
                    'sellerEmail' => "text@gmail.com",
                ]
            ],
            'Vehicle has no wheels' => [
                true, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",
                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "no wheels",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",
                    'sellerName' => "Seller Name",
                    'sellerPhoneNumber' => "555-555-5555",
                    'sellerEmail' => "text@gmail.com",
                ]
            ],
            'Vehicle wheels invalid' => [
                false, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",
                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "rotary rims",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",
                    'sellerName' => "Seller Name",
                    'sellerPhoneNumber' => "555-555-5555",
                    'sellerEmail' => "text@gmail.com",
                ]
            ],
            'Vehicle will be delivered missing' => [
                false, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",
                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "",
                    'vehicleZipCode' => "02453",
                    'sellerName' => "Seller Name",
                    'sellerPhoneNumber' => "555-555-5555",
                    'sellerEmail' => "text@gmail.com",
                ]
            ],
            'Vehicle will not be delivered' => [
                true, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",
                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "no",
                    'vehicleZipCode' => "02453",
                    'sellerName' => "Seller Name",
                    'sellerPhoneNumber' => "555-555-5555",
                    'sellerEmail' => "text@gmail.com",
                ]
            ],
            'Vehicle will be delivered invalid' => [
                false, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",
                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "maybe",
                    'vehicleZipCode' => "02453",
                    'sellerName' => "Seller Name",
                    'sellerPhoneNumber' => "555-555-5555",
                    'sellerEmail' => "text@gmail.com",
                ]
            ],
            'Vehicle zip code missing' => [
                false, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",
                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "",
                    'sellerName' => "Seller Name",
                    'sellerPhoneNumber' => "555-555-5555",
                    'sellerEmail' => "text@gmail.com",
                ]
            ],
            'Vehicle zip code invalid' => [
                false, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",
                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "asdasd",
                    'sellerName' => "Seller Name",
                    'sellerPhoneNumber' => "555-555-5555",
                    'sellerEmail' => "text@gmail.com",
                ]
            ],
            'Seller name missing' => [
                false, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",

                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",

                    'sellerName' => null,
                    'sellerPhoneNumber' => "555-555-5555",
                    'sellerEmail' => "text@gmail.com",
                ]
            ],
            'Seller name empty' => [
                false, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",

                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",

                    'sellerName' => "",
                    'sellerPhoneNumber' => "555-555-5555",
                    'sellerEmail' => "text@gmail.com",
                ]
            ],
            'Seller name whitespace' => [
                false, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",

                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",

                    'sellerName' => "       ",
                    'sellerPhoneNumber' => "555-555-5555",
                    'sellerEmail' => "text@gmail.com",
                ]
            ],
            'Seller name too long' => [
                false, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",

                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",

                    'sellerName' => "Seller NameSeller NameSeller NameSeller NameSeller NameSeller NameSeller Name",
                    'sellerPhoneNumber' => "555-555-5555",
                    'sellerEmail' => "text@gmail.com",
                ]
            ],
            'phone number too long' => [
                false, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",

                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",

                    'sellerName' => "Seller Name",
                    'sellerPhoneNumber' => "55555555555555555555555555",
                    'sellerEmail' => "text@gmail.com",
                ]
            ],
            'phone number not valid' => [
                false, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",

                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",

                    'sellerName' => "Seller Name",
                    'sellerPhoneNumber' => "asda 555kjasd",
                    'sellerEmail' => "text@gmail.com",
                ]
            ],
            'phone number no dash' => [
                true, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",

                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",

                    'sellerName' => "Seller Name",
                    'sellerPhoneNumber' => "5555555555",
                    'sellerEmail' => "text@gmail.com",
                ]
            ],
            'phone number spaces' => [
                true, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",

                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",

                    'sellerName' => "Seller Name",
                    'sellerPhoneNumber' => "555 555 5555",
                    'sellerEmail' => "text@gmail.com",
                ]
            ],
            'phone number dashes' => [
                true, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",

                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",

                    'sellerName' => "Seller Name",
                    'sellerPhoneNumber' => "555-555-5555",
                    'sellerEmail' => "text@gmail.com",
                ]
            ],
            'phone number parens' => [
                true, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",

                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",

                    'sellerName' => "Seller Name",
                    'sellerPhoneNumber' => "(555) 555-5555",
                    'sellerEmail' => "text@gmail.com",
                ]
            ],
            'phone number missing' => [
                true, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",

                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",

                    'sellerName' => "Seller Name",
                    'sellerPhoneNumber' => null,
                    'sellerEmail' => "text@gmail.com",
                ]
            ],
            'email missing' => [
                true, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",

                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",

                    'sellerName' => "Seller Name",
                    'sellerPhoneNumber' => "5555555555",
                    'sellerEmail' => null,
                ]
            ],
            'email empty' => [
                true, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",

                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",

                    'sellerName' => "Seller Name",
                    'sellerPhoneNumber' => "5555555555",
                    'sellerEmail' => "",
                ]
            ],
            'email whitespace' => [
                true, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",

                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",

                    'sellerName' => "Seller Name",
                    'sellerPhoneNumber' => "5555555555",
                    'sellerEmail' => "         ",
                ]
            ],
            'email invalid' => [
                false, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",

                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",

                    'sellerName' => "Seller Name",
                    'sellerPhoneNumber' => "5555555555",
                    'sellerEmail' => "adkalksdal",
                ]
            ],
        ];
    }
}