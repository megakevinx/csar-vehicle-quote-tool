<?php

namespace ApplicationTest\Controller\Form;

use \PHPUnit\Framework\TestCase;

use Zend\Form\Form;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilter;

use Application\Controller\Form\CreateFaqForm;

class CreateFaqFormTest extends BaseFormTestCase
{
    protected function getFormInstance()
    {
        return new CreateFaqForm();
    }

    public function dataProvider_TestValidationIsCorrect()
    {
        return [
            'All data valid' => [
                true, [ 'question' => "this is a question", 'answer' => "this is an answer" ]
            ],
            'Question missing' => [
                false, [ 'question' => null, 'answer' => "this is an answer" ]
            ],
            'Question empty' => [
                false, [ 'question' => "", 'answer' => "this is an answer" ]
            ],
            'Question whitespace' => [
                false, [ 'question' => "        ", 'answer' => "this is an answer" ]
            ],
            'Answer missing' => [
                false, [ 'question' => "this is a question", 'answer' => null ]
            ],
            'Answer empty' => [
                false, [ 'question' => "this is a question", 'answer' => "" ]
            ],
            'Answer whitespace' => [
                false, [ 'question' => "this is a question", 'answer' => "           " ]
            ]
        ];
    }
}