<?php

namespace ApplicationTest\Controller\Form;

use \PHPUnit\Framework\TestCase;

use Zend\Form\Form;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilter;

use Application\Controller\Form\CreateQuoteForm;

class CreateQuoteFormTest extends BaseFormTestCase
{
    protected function getFormInstance()
    {
        return new CreateQuoteForm();
    }

    public function dataProvider_TestValidationIsCorrect()
    {
        return [
            'All data valid' => [
                true, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",
                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",
                ]
            ],
            'Vehicle year missing' => [
                false, [
                    'vehicleYear' => "0",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",
                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",
                ]
            ],
            'Vehicle year too old' => [
                false, [
                    'vehicleYear' => "1980",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",
                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",
                ]
            ],
            'Vehicle year just old enough' => [
                true, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",
                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",
                ]
            ],
            'Vehicle year not a number' => [
                false, [
                    'vehicleYear' => "abcd",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",
                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",
                ]
            ],
            'Vehicle make missing' => [
                false, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",
                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",
                ]
            ],
            'Vehicle make too long' => [
                false, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyotatoyotatoyotatoyotatoyotatoyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",
                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",
                ]
            ],
            'Vehicle model missing' => [
                false, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "",
                    'vehicleTypeId' => "1",
                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",
                ]
            ],
            'Vehicle model too long' => [
                false, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "suprasuprasuprasuprasuprasuprasuprasuprasupra",
                    'vehicleTypeId' => "1",
                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",
                ]
            ],
            'Vehicle type id missing' => [
                false, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "",
                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",
                ]
            ],
            'Vehicle type id not a number' => [
                false, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "lol",
                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",
                ]
            ],
            'Vehicle has keys missing' => [
                false, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",
                    'vehicleHasKeys' => "",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",
                ]
            ],
            'Vehicle does not have keys' => [
                true, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",
                    'vehicleHasKeys' => "no",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",
                ]
            ],
            'Vehicle has keys invalid' => [
                false, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",
                    'vehicleHasKeys' => "whatever",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",
                ]
            ],
            'Vehicle has title missing' => [
                false, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",
                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",
                ]
            ],
            'Vehicle does not have title' => [
                true, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",
                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "no",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",
                ]
            ],
            'Vehicle has title invalid' => [
                false, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",
                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "whatever",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",
                ]
            ],
            'Vehicle wheels missing' => [
                false, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",
                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",
                ]
            ],
            'Vehicle has steel wheels' => [
                true, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",
                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",
                ]
            ],
            'Vehicle has aluminum wheels' => [
                true, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",
                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "aluminum",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",
                ]
            ],
            'Vehicle has no wheels' => [
                true, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",
                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "no wheels",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",
                ]
            ],
            'Vehicle wheels invalid' => [
                false, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",
                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "rotary rims",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "02453",
                ]
            ],
            'Vehicle will be delivered missing' => [
                false, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",
                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "",
                    'vehicleZipCode' => "02453",
                ]
            ],
            'Vehicle will not be delivered' => [
                true, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",
                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "no",
                    'vehicleZipCode' => "02453",
                ]
            ],
            'Vehicle will be delivered invalid' => [
                false, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",
                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "maybe",
                    'vehicleZipCode' => "02453",
                ]
            ],
            'Vehicle zip code missing' => [
                false, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",
                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "",
                ]
            ],
            'Vehicle zip code invalid' => [
                false, [
                    'vehicleYear' => "1990",
                    'vehicleMake' => "toyota",
                    'vehicleModel' => "supra",
                    'vehicleTypeId' => "1",
                    'vehicleHasKeys' => "yes",
                    'vehicleHasTitle' => "yes",
                    'vehicleWheelsDescription' => "steel",
                    'vehicleWillBeDelivered' => "yes",
                    'vehicleZipCode' => "asdasd",
                ]
            ]
        ];
    }
}