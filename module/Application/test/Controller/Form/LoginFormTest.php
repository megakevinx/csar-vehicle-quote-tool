<?php

namespace ApplicationTest\Controller\Form;

use \PHPUnit\Framework\TestCase;

use Zend\Form\Form;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilter;

use Application\Controller\Form\LoginForm;

class LoginFormTest extends TestCase
{
    public function dataProvider_TestValidationIsCorrect()
    {
        return [
            'All data valid' => [
                true, [ 'email' => 'kevincampusano@gmail.com', 'password' => 'password', 'remember_me' => "1" ]
            ],
            'All data valid with whitespace' => [
                true, [ 'email' => '     kevincampusano@gmail.com        ', 'password' => '    password   ', 'remember_me' => "1" ]
            ],
            'Email invalid' => [
                true, [ 'email' => 'some invalid string', 'password' => 'password', 'remember_me' => "1" ]
            ],
            'Email missing' => [
                false, [ 'email' => '', 'password' => 'password', 'remember_me' => "1" ]
            ],
            'Password missing' => [
                false, [ 'email' => 'kevincampusano@gmail.com', 'password' => '', 'remember_me' => "1" ]
            ],
            'Remember Me invalid' => [
                false, [ 'email' => 'kevincampusano@gmail.com', 'password' => 'password', 'remember_me' => 'yes' ]
            ],
        ];
    }

    /**
    * @dataProvider dataProvider_TestValidationIsCorrect
    */
    public function testValidationIsCorrect($expected, $data)
    {
        // Arrange
        $form = new LoginForm();
        $csrf = $form->get('csrf')->getValue();

        $data['csrf'] = $csrf;

        $form->setData($data);

        // Act
        $actual = $form->isValid();

        // Assert
        $this->assertEquals($expected, $actual);
    }
}