<?php

namespace ApplicationTest\Controller\Form;

use \PHPUnit\Framework\TestCase;

use Zend\Form\Form;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilter;

use Application\Controller\Form\PasswordChangeForm;

class PasswordChangeFormTest extends TestCase
{
    public function dataProvider_TestValidationIsCorrect()
    {
        return [
            'All data valid' => [
                true, [ 'password' => 'abc12345', 'confirm_new_password' => 'abc12345', ]
            ],

            'Password missing' => [
                false, [ 'password' => null, 'confirm_new_password' => 'abc12345', ]
            ],
            'Password confirm missing' => [
                false, [ 'password' => 'abc12345', 'confirm_new_password' => null, ]
            ],

            'Password empty' => [
                false, [ 'password' => "", 'confirm_new_password' => 'abc12345', ]
            ],
            'Password confirm empty' => [
                false, [ 'password' => 'abc12345', 'confirm_new_password' => "", ]
            ],

            'Both missing' => [
                false, [ 'password' => null, 'confirm_new_password' => null, ]
            ],
            'Both whitespace' => [
                false, [ 'password' => "      ", 'confirm_new_password' => "      ", ]
            ],
            'Both empty' => [
                false, [ 'password' => "", 'confirm_new_password' => "", ]
            ],

            'Both different' => [
                false, [ 'password' => 'abcdefg', 'confirm_new_password' => "123456", ]
            ],

            'Both too long' => [
                false, [ 'password' => '123456123456123456123456123456123456123456123456123456123456123456123456', 'confirm_new_password' => "123456123456123456123456123456123456123456123456123456123456123456123456", ]
            ],
        ];
    }

    /**
    * @dataProvider dataProvider_TestValidationIsCorrect
    */
    public function testValidationIsCorrect($expected, $data)
    {
        // Arrange
        $form = new PasswordChangeForm();
        $csrf = $form->get('csrf')->getValue();

        $data['csrf'] = $csrf;

        $form->setData($data);

        // Act
        $actual = $form->isValid();

        // Assert
        $this->assertEquals($expected, $actual);
    }
}