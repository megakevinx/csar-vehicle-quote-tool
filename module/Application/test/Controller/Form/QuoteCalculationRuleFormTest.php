<?php

namespace ApplicationTest\Controller\Form;

use \PHPUnit\Framework\TestCase;

use Zend\Form\Form;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilter;

use Application\Controller\Form\QuoteCalculationRuleForm;

class QuoteCalculationRuleFormTest extends BaseFormTestCase
{
    protected function getFormInstance()
    {
        return new QuoteCalculationRuleForm();
    }

    public function dataProvider_TestValidationIsCorrect()
    {
        return [
            'All data valid' => [
                true, [ 'id' => '1', 'priceModifier' => '50' ]
            ],
            'Id is not a number' => [
                false, [ 'id' => 'asdasd', 'priceModifier' => '50' ]
            ],
            'Id is a float' => [
                false, [ 'id' => '1.2', 'priceModifier' => '50' ]
            ],
            'Id is a negative number' => [
                false, [ 'id' => '-10', 'priceModifier' => '50' ]
            ],
            'priceModifier is not a number' => [
                false, [ 'id' => '1', 'priceModifier' => 'asd' ]
            ],
            'priceModifier is a float' => [
                false, [ 'id' => '1', 'priceModifier' => '50.50' ]
            ],
            'priceModifier is too big' => [
                false, [ 'id' => '1', 'priceModifier' => '501' ]
            ],
            'priceModifier is too low' => [
                false, [ 'id' => '1', 'priceModifier' => '-501' ]
            ]
        ];
    }
}