<?php

namespace ApplicationTest\Controller\Form;

use \PHPUnit\Framework\TestCase;

use Zend\Form\Form;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilter;

use Application\Controller\Form\QuoteOverrideForm;

class QuoteOverrideFormTest extends BaseFormTestCase
{
    protected function getFormInstance()
    {
        return new QuoteOverrideForm();
    }

    private function getMockInputData($overrides)
    {
        return array_merge([
            'fromVehicleYear' => '1990',
            'toVehicleYear' => '1990',
            'vehicleMake' => 'acura',
            'vehicleModel' => 'nsx',
            'quote' => '149.99'
        ], $overrides);
    }

    public function dataProvider_TestValidationIsCorrect()
    {
        return [
            'All data valid' => [
                true, $this->getMockInputData([])
            ],
            'Any from year' => [
                true, $this->getMockInputData(['fromVehicleYear' => 'Any',])
            ],
            'from year not a number' => [
                false, $this->getMockInputData(['fromVehicleYear' => 'asdasd'])
            ],
            'from year not an int' => [
                false, $this->getMockInputData(['fromVehicleYear' => '1990.51'])
            ],
            'from year too low' => [
                false, $this->getMockInputData(['fromVehicleYear' => '1988'])
            ],
            'Any to year' => [
                true, $this->getMockInputData(['toVehicleYear' => 'Any'])
            ],
            'To year not a number' => [
                false, $this->getMockInputData(['toVehicleYear' => 'zxcxzc'])
            ],
            'To year not an int' => [
                false, $this->getMockInputData(['toVehicleYear' => '19.90',])
            ],
            'To year too low' => [
                false, $this->getMockInputData(['toVehicleYear' => '1989'])
            ],
            'From year greater than to year' => [
                false, $this->getMockInputData(['fromVehicleYear' => '2000', 'toVehicleYear' => '1995'])
            ],
            'Any model' => [
                true, $this->getMockInputData(['vehicleModel' => 'Any'])
            ],
            'Any model and year' => [
                true, $this->getMockInputData(['fromVehicleYear' => 'Any', 'toVehicleYear' => 'Any','vehicleModel' => 'Any'])
            ],
            'Make too long' => [
                false, $this->getMockInputData(['vehicleMake' => 'acuraacuraacuraacuraacuraacuraacuraacuraacuraacuraacuraacuraacura'])
            ],
            'Model too long' => [
                false, $this->getMockInputData(['vehicleModel' => 'nsxnsxnsxnsxnsxnsxnsxnsxnsxnsxnsxnsxnsxnsxnsxnsx'])
            ],
            'Quote not a number' => [
                false, $this->getMockInputData(['quote' => 'asdasd'])
            ],
            'Quote is int' => [
                true, $this->getMockInputData(['quote' => '100'])
            ],
            'Quote too high' => [
                false, $this->getMockInputData(['quote' => '1000'])
            ],
            'Quote too low' => [
                false, $this->getMockInputData(['quote' => '-1'])
            ],
            'Quote is nothing' => [
                true, $this->getMockInputData(['quote' => '0'])
            ],
        ];
    }
}