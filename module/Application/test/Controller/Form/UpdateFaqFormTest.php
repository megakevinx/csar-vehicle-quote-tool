<?php

namespace ApplicationTest\Controller\Form;

use \PHPUnit\Framework\TestCase;

use Zend\Form\Form;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilter;

use Application\Controller\Form\UpdateFaqForm;

class UpdateFaqFormTest extends BaseFormTestCase
{
    protected function getFormInstance()
    {
        return new UpdateFaqForm();
    }

    public function dataProvider_TestValidationIsCorrect()
    {
        return [
            'All data valid' => [
                true, [ 'id' => 1, 'question' => "this is a question", 'answer' => "this is an answer" ]
            ],
            'Question missing' => [
                false, [ 'question' => null, 'answer' => "this is an answer" ]
            ],
            'Question empty' => [
                false, [ 'question' => "", 'answer' => "this is an answer" ]
            ],
            'Question whitespace' => [
                false, [ 'question' => "        ", 'answer' => "this is an answer" ]
            ],
            'Answer missing' => [
                false, [ 'question' => "this is a question", 'answer' => null ]
            ],
            'Answer empty' => [
                false, [ 'question' => "this is a question", 'answer' => "" ]
            ],
            'Answer whitespace' => [
                false, [ 'question' => "this is a question", 'answer' => "           " ]
            ],
            'Id string' => [
                true, [ 'id' => "20", 'question' => "this is a question", 'answer' => "this is an answer" ]
            ],
            'Id missing' => [
                false, [ 'id' => null, 'question' => "this is a question", 'answer' => "this is an answer" ]
            ],
            'Id not a number' => [
                false, [ 'id' => "asdasd", 'question' => "this is a question", 'answer' => "this is an answer" ]
            ],
            'Id empty' => [
                false, [ 'id' => "", 'question' => "this is a question", 'answer' => "this is an answer" ]
            ],
            'Id whitespace' => [
                false, [ 'id' => "    ", 'question' => "this is a question", 'answer' => "this is an answer" ]
            ],
        ];
    }
}