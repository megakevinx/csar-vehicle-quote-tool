<?php

namespace ApplicationTest\Controller\Form;

use \PHPUnit\Framework\TestCase;

use Zend\Form\Form;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilter;

use Application\Controller\Form\UpdateQuoteForm;

class UpdateQuoteFormTest extends BaseFormTestCase
{
    protected function getFormInstance()
    {
        return new UpdateQuoteForm();
    }

    public function dataProvider_TestValidationIsCorrect()
    {
        return [
            'All data valid' => [
                true, [
                    'vehicleStreetAddress' => "123 Main st",
                    'vehicleCity' => "Gotham",
                    'sellerName' => "Test Sellerson",
                    'sellerPhoneNumber' => "555-555-5555",
                    'sellerEmail' => "test@gmail.com",
                    'buyerPayout' => "100.50"
                ]
            ],
            'Vehicle street address missing' => [
                false, [
                    'vehicleStreetAddress' => "",
                    'vehicleCity' => "Gotham",
                    'sellerName' => "Test Sellerson",
                    'sellerPhoneNumber' => "555-555-5555",
                    'sellerEmail' => "test@gmail.com",
                    'buyerPayout' => "100.50"
                ]
            ],
            'Vehicle street address too long' => [
                false, [
                    'vehicleStreetAddress' => "123 Main st123 Main st123 Main st123 Main st123 Main st123 Main st123 Main st123 Main st123 Main st",
                    'vehicleCity' => "Gotham",
                    'sellerName' => "Test Sellerson",
                    'sellerPhoneNumber' => "555-555-5555",
                    'sellerEmail' => "test@gmail.com",
                    'buyerPayout' => "100.50"
                ]
            ],
            'Vehicle city missing' => [
                false, [
                    'vehicleStreetAddress' => "123 Main st",
                    'vehicleCity' => "",
                    'sellerName' => "Test Sellerson",
                    'sellerPhoneNumber' => "555-555-5555",
                    'sellerEmail' => "test@gmail.com",
                    'buyerPayout' => "100.50"
                ]
            ],
            'Vehicle city too long' => [
                false, [
                    'vehicleStreetAddress' => "123 Main st",
                    'vehicleCity' => "GothamGothamGothamGothamGothamGothamGothamGothamGotham",
                    'sellerName' => "Test Sellerson",
                    'sellerPhoneNumber' => "555-555-5555",
                    'sellerEmail' => "test@gmail.com",
                    'buyerPayout' => "100.50"
                ]
            ],
            'Seller name missing' => [
                false, [
                    'vehicleStreetAddress' => "123 Main st",
                    'vehicleCity' => "Gotham",
                    'sellerName' => "",
                    'sellerPhoneNumber' => "555-555-5555",
                    'sellerEmail' => "test@gmail.com",
                    'buyerPayout' => "100.50"
                ]
            ],
            'Vehicle city too long' => [
                false, [
                    'vehicleStreetAddress' => "123 Main st",
                    'vehicleCity' => "Gotham",
                    'sellerName' => "Test SellersonTest SellersonTest SellersonTest Sellerson",
                    'sellerPhoneNumber' => "555-555-5555",
                    'sellerEmail' => "test@gmail.com",
                    'buyerPayout' => "100.50"
                ]
            ],
            'Seller phone number missing' => [
                true, [
                    'vehicleStreetAddress' => "123 Main st",
                    'vehicleCity' => "Gotham",
                    'sellerName' => "Test Sellerson",
                    'sellerPhoneNumber' => "",
                    'sellerEmail' => "test@gmail.com",
                    'buyerPayout' => "100.50"
                ]
            ],
            'Seller phone number with spaces valid' => [
                true, [
                    'vehicleStreetAddress' => "123 Main st",
                    'vehicleCity' => "Gotham",
                    'sellerName' => "Test Sellerson",
                    'sellerPhoneNumber' => "555 555 5555",
                    'sellerEmail' => "test@gmail.com",
                    'buyerPayout' => "100.50"
                ]
            ],
            'Seller phone number with parens valid' => [
                true, [
                    'vehicleStreetAddress' => "123 Main st",
                    'vehicleCity' => "Gotham",
                    'sellerName' => "Test Sellerson",
                    'sellerPhoneNumber' => "(555) 555 5555",
                    'sellerEmail' => "test@gmail.com",
                    'buyerPayout' => "100.50"
                ]
            ],
            'Seller phone number with parens and dashes valid' => [
                true, [
                    'vehicleStreetAddress' => "123 Main st",
                    'vehicleCity' => "Gotham",
                    'sellerName' => "Test Sellerson",
                    'sellerPhoneNumber' => "(555)-555-5555",
                    'sellerEmail' => "test@gmail.com",
                    'buyerPayout' => "100.50"
                ]
            ],
            'Seller phone number invalid' => [
                false, [
                    'vehicleStreetAddress' => "123 Main st",
                    'vehicleCity' => "Gotham",
                    'sellerName' => "Test Sellerson",
                    'sellerPhoneNumber' => "asdasdasd",
                    'sellerEmail' => "test@gmail.com",
                    'buyerPayout' => "100.50"
                ]
            ],
            'Seller email missing' => [
                true, [
                    'vehicleStreetAddress' => "123 Main st",
                    'vehicleCity' => "Gotham",
                    'sellerName' => "Test Sellerson",
                    'sellerPhoneNumber' => "555-555-5555",
                    'sellerEmail' => "",
                    'buyerPayout' => "100.50"
                ]
            ],
            'Seller email too long' => [
                false, [
                    'vehicleStreetAddress' => "123 Main st",
                    'vehicleCity' => "Gotham",
                    'sellerName' => "Test Sellerson",
                    'sellerPhoneNumber' => "555-555-5555",
                    'sellerEmail' => "test@gmail.comtest@gmail.comtest@gmail.comtest@gmail.comtest@gmail.com",
                    'buyerPayout' => "100.50"
                ]
            ],
            'Buyer payout missing' => [
                false, [
                    'vehicleStreetAddress' => "123 Main st",
                    'vehicleCity' => "Gotham",
                    'sellerName' => "Test Sellerson",
                    'sellerPhoneNumber' => "555-555-5555",
                    'sellerEmail' => "test@gmail.com",
                    'buyerPayout' => ""
                ]
            ],
            'Buyer payout too high' => [
                false, [
                    'vehicleStreetAddress' => "123 Main st",
                    'vehicleCity' => "Gotham",
                    'sellerName' => "Test Sellerson",
                    'sellerPhoneNumber' => "555-555-5555",
                    'sellerEmail' => "test@gmail.com",
                    'buyerPayout' => "2000.00"
                ]
            ],
            'Buyer payout almost too high' => [
                true, [
                    'vehicleStreetAddress' => "123 Main st",
                    'vehicleCity' => "Gotham",
                    'sellerName' => "Test Sellerson",
                    'sellerPhoneNumber' => "555-555-5555",
                    'sellerEmail' => "test@gmail.com",
                    'buyerPayout' => "999.99"
                ]
            ],
            'Buyer payout just too high' => [
                false, [
                    'vehicleStreetAddress' => "123 Main st",
                    'vehicleCity' => "Gotham",
                    'sellerName' => "Test Sellerson",
                    'sellerPhoneNumber' => "555-555-5555",
                    'sellerEmail' => "test@gmail.com",
                    'buyerPayout' => "1000.00"
                ]
            ],
            'Buyer payout with no cents' => [
                true, [
                    'vehicleStreetAddress' => "123 Main st",
                    'vehicleCity' => "Gotham",
                    'sellerName' => "Test Sellerson",
                    'sellerPhoneNumber' => "555-555-5555",
                    'sellerEmail' => "test@gmail.com",
                    'buyerPayout' => "800"
                ]
            ],
            'Buyer payout with too high with commas' => [
                false, [
                    'vehicleStreetAddress' => "123 Main st",
                    'vehicleCity' => "Gotham",
                    'sellerName' => "Test Sellerson",
                    'sellerPhoneNumber' => "555-555-5555",
                    'sellerEmail' => "test@gmail.com",
                    'buyerPayout' => "1,000.00"
                ]
            ],
            'Buyer payout not a number' => [
                false, [
                    'vehicleStreetAddress' => "123 Main st",
                    'vehicleCity' => "Gotham",
                    'sellerName' => "Test Sellerson",
                    'sellerPhoneNumber' => "555-555-5555",
                    'sellerEmail' => "test@gmail.com",
                    'buyerPayout' => "abc.de"
                ]
            ],
        ];
    }
}