<?php

namespace ApplicationTest\Controller\Form;

use \PHPUnit\Framework\TestCase;

use Zend\Form\Form;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilter;

use Application\Controller\Form\UpdateQuoteLocationForm;

class UpdateQuoteLocationFormTest extends BaseFormTestCase
{
    protected function getFormInstance()
    {
        return new UpdateQuoteLocationForm();
    }

    public function dataProvider_TestValidationIsCorrect()
    {
        return [
            'All data valid' => [
                true, [ 'vehicleStreetAddress' => '10 Main St', 'vehicleCity' => 'Tampa' ]
            ],
            'Street address missing' => [
                false, [ 'vehicleStreetAddress' => null, 'vehicleCity' => 'Tampa' ]
            ],
            'Street address too long' => [
                false, [ 'vehicleStreetAddress' => "vehicleStreetAddressvvehicleStreetAddressvehicleStreetAddressvehicleStreetAddressvehicleStreetAddress", 'vehicleCity' => 'Tampa' ]
            ],
            'Street address whitespace' => [
                false, [ 'vehicleStreetAddress' => "             ", 'vehicleCity' => 'Tampa' ]
            ],
            'Street address empty' => [
                false, [ 'vehicleStreetAddress' => "", 'vehicleCity' => 'Tampa' ]
            ],
            'City missing' => [
                false, [ 'vehicleStreetAddress' => "10 Main St", 'vehicleCity' => null ]
            ],
            'City too long' => [
                false, [ 'vehicleStreetAddress' => "10 Main St", 'vehicleCity' => 'TampaTampaTampaTampaTampaTampaTampaTampaTampaTampaTampaTampaTampaTampaTampaTampaTampaTampaTampaTampaTampaTampaTampaTampaTampaTampa' ]
            ],
            'City whitespace' => [
                false, [ 'vehicleStreetAddress' => "10 Main St", 'vehicleCity' => '        ' ]
            ],
            'City empty' => [
                false, [ 'vehicleStreetAddress' => "10 Main St", 'vehicleCity' => '' ]
            ],
        ];
    }
}