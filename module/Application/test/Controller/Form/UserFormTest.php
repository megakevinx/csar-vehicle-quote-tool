<?php

namespace ApplicationTest\Controller\Form;

use \PHPUnit\Framework\TestCase;

use Zend\Form\Form;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilter;

use Application\Controller\Form\UserForm;

class UserFormTest extends BaseFormTestCase
{
    protected function getFormInstance()
    {
        return new UserForm();
    }

    private function getMockInputData($overrides)
    {
        return array_merge([
            'email' => "kevincampusano@gmail.com",
            'first_name' => "Kevin",
            'last_name' => "Campusano",
            'phone_number' => "555-555-5555",
            'company_name' => "My Company Co.",
            'company_address' => "123 main st tampa, FL 12345",
            'payment_method' => "Check",
            'password' => "123456",
            'confirm_password' => "123456"
        ], $overrides);
    }

    public function dataProvider_TestValidationIsCorrect()
    {
        return [
            'All data valid' => [
                true, $this->getMockInputData([])
            ],
            'Email missing' => [
                false, $this->getMockInputData(['email' => ""])
            ],
            'Email too short' => [
                false, $this->getMockInputData(['email' => "a@com"])
            ],
            'Email too long' => [
                false, $this->getMockInputData(['email' => "abcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcde@gmail.com"])
            ],
            'First name missing' => [
                false, $this->getMockInputData(['first_name' => ""])
            ],
            'First name too long' => [
                false, $this->getMockInputData(['first_name' => "kevinkevinkevinkevinkevinkevinkevinkevinkevinkevin"])
            ],
            'Last name missing' => [
                false, $this->getMockInputData(['last_name' => ""])
            ],
            'Last name too long' => [
                false, $this->getMockInputData(['last_name' => "CampusanoCampusanoCampusanoCampusanoCampusanoCampusanoCampusano"])
            ],
            'Phone number digits only' => [
                true, $this->getMockInputData(['phone_number' => "5555555555"])
            ],
            'Phone number with parens and dash' => [
                true, $this->getMockInputData(['phone_number' => "(555)555-5555"])
            ],
            'Phone number with parens and dashes' => [
                true, $this->getMockInputData(['phone_number' => "(555)-555-5555"])
            ],
            'Phone number with parens and space' => [
                true, $this->getMockInputData(['phone_number' => "(555) 555 5555"])
            ],
            'Phone number with parens, dash and space' => [
                true, $this->getMockInputData(['phone_number' => "(555) 555-5555"])
            ],
            'Phone number with spaces' => [
                true, $this->getMockInputData(['phone_number' => "555 555 5555"])
            ],
            'Phone number missing' => [
                false, $this->getMockInputData(['phone_number' => ""])
            ],
            'Phone number invalid' => [
                false, $this->getMockInputData(['phone_number' => "asdasdasda"])
            ],
            'Company name missing' => [
                false, $this->getMockInputData(['company_name' => ""])
            ],
            'Company name too long' => [
                false, $this->getMockInputData(['company_name' => "My Company Co.My Company Co.My Company Co.My Company Co.My Company Co."])
            ],
            'Company address missing' => [
                false, $this->getMockInputData(['company_address' => ""])
            ],
            'Company address too long' => [
                false, $this->getMockInputData(['company_address' => "123 main st tampa, FL 12345123 main st tampa, FL 12345123 main st tampa, FL 12345123 main st tampa, FL 12345123 main st tampa, FL 12345"])
            ],
            'Payment method is missing' => [
                false, $this->getMockInputData(['payment_method' => ""])
            ],
            'Payment method is invalid' => [
                false, $this->getMockInputData(['payment_method' => "asdasd"])
            ],
            'Payment method is Check' => [
                true, $this->getMockInputData(['payment_method' => "Check"])
            ],
            'Payment method is ACH' => [
                true, $this->getMockInputData(['payment_method' => "ACH"])
            ],
            'Payment method is Other' => [
                true, $this->getMockInputData(['payment_method' => "Other"])
            ],
            'Password missing' => [
                false, $this->getMockInputData([
                    'password' => "",
                    'confirm_password' => ""
                ])
            ],
            'Password too short' => [
                false, $this->getMockInputData([
                    'password' => "123",
                    'confirm_password' => "123"
                ])
            ],
            'Password too long' => [
                false, $this->getMockInputData([
                    'password' => "1234512345123451234512345123451234512345123451234512345123451234512345",
                    'confirm_password' => "1234512345123451234512345123451234512345123451234512345123451234512345"
                ])
            ],
            'Confirm Password missing' => [
                false, $this->getMockInputData([
                    'password' => "123456",
                    'confirm_password' => ""
                ])
            ],
            'Confirm Password mismatch' => [
                false, $this->getMockInputData([
                    'password' => "123456",
                    'confirm_password' => "abcdef"
                ])
            ],
        ];
    }
}