<?php

namespace ApplicationTest\Service;

use \PHPUnit\Framework\TestCase;

use Zend\Authentication\Result;

use Application\Service\AuthAdapter;
use Application\Repository\UserRepository;
use Application\Repository\BuyerRepository;
use Application\Model\User;

class AuthAdapterTest extends TestCase
{
    private function createMockUser()
    {
        $mockUser = new User();
        $mockUser->id = 1;
        $mockUser->email = "mockemail";
        $mockUser->password = "mockpassword";
        $mockUser->firstName = "mockfirstname";
        $mockUser->lastName = "mocklastname";
        $mockUser->role = "Vendor";

        return $mockUser;
    }

    public function testValidUserAuthenticatesSuccessfully()
    {
        // Arrange
        $mockUser = $this->createMockUser();

        $mockUserRepository = $this->createMock(UserRepository::class);
        $mockUserRepository->method('getByEmail')->willReturn($mockUser);

        $mockBuyerRepository = $this->createMock(BuyerRepository::class);
        $mockBuyerRepository->method('getByName')->willReturn(null);

        $authAdapter = new AuthAdapter($mockUserRepository, $mockBuyerRepository);
        $authAdapter->setEmail($mockUser->email);
        $authAdapter->setPassword($mockUser->password);

        $expected = Result::SUCCESS;

        // Act
        $actual = $authAdapter->authenticate()->getCode();

        // Assert
        $this->assertEquals($expected, $actual);
    }

    public function testValidBuyerAuthenticatesSuccessfully()
    {
        // Arrange
        $mockUser = $this->createMockUser();
        $mockUser->password = "36685e8b1d30c8c1adfa6d80fc7ddc2cf88f5645774c6457fb745d3c14358c26cbdb68110ea32ccd10bf09d053c0d518819d7b75334fb9ae2f9aa49c82ca0d479bbfeab3";

        $mockUserRepository = $this->createMock(UserRepository::class);
        $mockUserRepository->method('getByEmail')->willReturn(null);

        $mockBuyerRepository = $this->createMock(BuyerRepository::class);
        $mockBuyerRepository->method('getByName')->willReturn($mockUser);

        $authAdapter = new AuthAdapter($mockUserRepository, $mockBuyerRepository);
        $authAdapter->setEmail($mockUser->email);
        $authAdapter->setPassword("password");

        $expected = Result::SUCCESS;

        // Act
        $actual = $authAdapter->authenticate()->getCode();

        // Assert
        $this->assertEquals($expected, $actual);
    }

    public function testMissingUserAndBuyerFailsAuthentication()
    {
        // Arrange
        $mockUserRepository = $this->createMock(UserRepository::class);
        $mockUserRepository->method('getByEmail')->willReturn(null);

        $mockBuyerRepository = $this->createMock(BuyerRepository::class);
        $mockBuyerRepository->method('getByName')->willReturn(null);

        $authAdapter = new AuthAdapter($mockUserRepository, $mockBuyerRepository);
        $authAdapter->setEmail("doesnot@exist.com");
        $authAdapter->setPassword("password");

        $expected = Result::FAILURE_IDENTITY_NOT_FOUND;

        // Act
        $actual = $authAdapter->authenticate()->getCode();

        // Assert
        $this->assertEquals($expected, $actual);
    }

    public function testWrongUserPasswordFailsAuthentication()
    {
        // Arrange
        $mockUser = $this->createMockUser();
        $mockUser->password = "amockpassword";

        $mockUserRepository = $this->createMock(UserRepository::class);
        $mockUserRepository->method('getByEmail')->willReturn($mockUser);

        $mockBuyerRepository = $this->createMock(BuyerRepository::class);
        $mockBuyerRepository->method('getByName')->willReturn(null);

        $authAdapter = new AuthAdapter($mockUserRepository, $mockBuyerRepository);
        $authAdapter->setEmail($mockUser->email);
        $authAdapter->setPassword("adifferentmockpassword");

        $expected = Result::FAILURE_CREDENTIAL_INVALID;

        // Act
        $actual = $authAdapter->authenticate()->getCode();

        // Assert
        $this->assertEquals($expected, $actual);
    }

    public function testWrongBuyerPasswordFailsAuthentication()
    {
        // Arrange
        $mockUser = $this->createMockUser();
        $mockUser->password = "36685e8b1d30c8c1adfa6d80fc7ddc2cf88f5645774c6457fb745d3c14358c26cbdb68110ea32ccd10bf09d053c0d518819d7b75334fb9ae2f9aa49c82ca0d479bbfeab3";

        $mockUserRepository = $this->createMock(UserRepository::class);
        $mockUserRepository->method('getByEmail')->willReturn(null);

        $mockBuyerRepository = $this->createMock(BuyerRepository::class);
        $mockBuyerRepository->method('getByName')->willReturn($mockUser);

        $authAdapter = new AuthAdapter($mockUserRepository, $mockBuyerRepository);
        $authAdapter->setEmail($mockUser->email);
        $authAdapter->setPassword("adifferentmockpassword");

        $expected = Result::FAILURE_CREDENTIAL_INVALID;

        // Act
        $actual = $authAdapter->authenticate()->getCode();

        // Assert
        $this->assertEquals($expected, $actual);
    }

    public function testUserWithNoRoleFailsAuthentication()
    {
        // Arrange
        $mockUser = $this->createMockUser();
        $mockUser->role = null;

        $mockUserRepository = $this->createMock(UserRepository::class);
        $mockUserRepository->method('getByEmail')->willReturn($mockUser);

        $mockBuyerRepository = $this->createMock(BuyerRepository::class);
        $mockBuyerRepository->method('getByName')->willReturn(null);

        $authAdapter = new AuthAdapter($mockUserRepository, $mockBuyerRepository);
        $authAdapter->setEmail($mockUser->email);
        $authAdapter->setPassword($mockUser->password);

        $expected = Result::FAILURE;

        // Act
        $actual = $authAdapter->authenticate()->getCode();

        // Assert
        $this->assertEquals($expected, $actual);
    }

    public function testBuyerWithNoRoleFailsAuthentication()
    {
        // Arrange
        $mockUser = $this->createMockUser();
        $mockUser->role = null;
        $mockUser->password = "36685e8b1d30c8c1adfa6d80fc7ddc2cf88f5645774c6457fb745d3c14358c26cbdb68110ea32ccd10bf09d053c0d518819d7b75334fb9ae2f9aa49c82ca0d479bbfeab3";

        $mockUserRepository = $this->createMock(UserRepository::class);
        $mockUserRepository->method('getByEmail')->willReturn(null);

        $mockBuyerRepository = $this->createMock(BuyerRepository::class);
        $mockBuyerRepository->method('getByName')->willReturn($mockUser);

        $authAdapter = new AuthAdapter($mockUserRepository, $mockBuyerRepository);
        $authAdapter->setEmail($mockUser->email);
        $authAdapter->setPassword("password");

        $expected = Result::FAILURE;

        // Act
        $actual = $authAdapter->authenticate()->getCode();

        // Assert
        $this->assertEquals($expected, $actual);
    }

    public function testUsesProvidedUserEmailForAuthentication()
    {
        // Arrange
        $mockUser = $this->createMockUser();

        $mockUserRepository = $this->createMock(UserRepository::class);
        $mockUserRepository->method('getByEmail')->willReturn($mockUser);
        $mockUserRepository->expects($this->once())
                           ->method('getByEmail')
                           ->with($this->equalTo($mockUser->email));

        $mockBuyerRepository = $this->createMock(BuyerRepository::class);

        $authAdapter = new AuthAdapter($mockUserRepository, $mockBuyerRepository);
        $authAdapter->setEmail($mockUser->email);
        $authAdapter->setPassword($mockUser->password);

        // Act & Assert
        $actual = $authAdapter->authenticate();
    }

    public function testUsesProvidedBuyerEmailForAuthentication()
    {
        // Arrange
        $mockUser = $this->createMockUser();

        $mockUserRepository = $this->createMock(UserRepository::class);
        $mockUserRepository->method('getByEmail')->willReturn(null);

        $mockBuyerRepository = $this->createMock(BuyerRepository::class);
        $mockBuyerRepository->method('getByName')->willReturn($mockUser);
        $mockBuyerRepository->expects($this->once())
                            ->method('getByName')
                            ->with($this->equalTo($mockUser->email));

        $authAdapter = new AuthAdapter($mockUserRepository, $mockBuyerRepository);
        $authAdapter->setEmail($mockUser->email);
        $authAdapter->setPassword($mockUser->password);

        // Act & Assert
        $actual = $authAdapter->authenticate();
    }
}