<?php

namespace ApplicationTest\Service;

use \PHPUnit\Framework\TestCase;

use Zend\Authentication\AuthenticationService;
use Zend\Session\SessionManager;
use Zend\Authentication\Result;

use Application\Service\AuthService;
use Application\Service\AuthAdapter;
use Application\Repository\UserRepository;
use Application\Model\User;

class AuthServiceTest extends TestCase
{
    private function createMockUser()
    {
        $mockUser = new User();
        $mockUser->id = 1;
        $mockUser->email = "mockemail";
        $mockUser->password = "mockpassword";
        $mockUser->firstName = "mockfirstname";
        $mockUser->lastName = "mocklastname";
        $mockUser->phoneNumber = "555-555-5555";
        $mockUser->role = "Vendor";

        return $mockUser;
    }

    private function createMockAuthProvider($mockUser)
    {
        $mockAuthAdapter = $this->createMock(AuthAdapter::class);
        $mockAuthAdapter->method('setEmail')->willReturn(null);
        $mockAuthAdapter->method('setPassword')->willReturn(null);

        $mockAuthProvider = $this->createMock(AuthenticationService::class);
        $mockAuthProvider->method('getIdentity')->willReturn($mockUser);
        $mockAuthProvider->method('clearIdentity')->willReturn(null);
        $mockAuthProvider->method('getAdapter')->willReturn($mockAuthAdapter);

        return $mockAuthProvider;
    }

    private function createMockSessionManager()
    {
        $mockSessionManager = $this->createMock(SessionManager::class);
        $mockSessionManager->method('rememberMe')->willReturn(null);

        return $mockSessionManager;
    }

    public function testUserLogsInSuccessfully()
    {
        // Arrange
        $mockUser = $this->createMockUser();
        $mockAuthProvider = $this->createMockAuthProvider($mockUser);
        $mockAuthProvider->method('authenticate')->willReturn(
            new Result(Result::SUCCESS, $mockUser, ['Authenticated successfully.'])
        );

        $mockSessionManager = $this->createMockSessionManager();
        $mockSessionManager->expects($this->never())->method('rememberMe');

        $mockUserRepository = $this->createMock(UserRepository::class);

        $authService = new AuthService($mockAuthProvider, $mockSessionManager, $mockUserRepository);

        $expected = (object)[
            'isSuccess' => true,
            'identity' => $mockUser
        ];

        // Act
        $actual = $authService->login("mockemail", "mockpassword", false);

        // Assert
        $this->assertTrue($actual->isSuccess);
        $this->assertEquals($expected->identity->id, $actual->identity->id);
    }

    public function testUserLogsInSuccessfullyAndSessionIsRememebered()
    {
        // Arrange
        $mockUser = $this->createMockUser();
        $mockAuthProvider = $this->createMockAuthProvider($mockUser);
        $mockAuthProvider->method('authenticate')->willReturn(
            new Result(Result::SUCCESS, $mockUser, ['Authenticated successfully.'])
        );

        $mockSessionManager = $this->createMockSessionManager();
        $mockSessionManager->expects($this->once())->method('rememberMe');

        $mockUserRepository = $this->createMock(UserRepository::class);

        $authService = new AuthService($mockAuthProvider, $mockSessionManager, $mockUserRepository);

        $expected = (object)[
            'isSuccess' => true,
            'identity' => $mockUser
        ];

        // Act
        $actual = $authService->login("mockemail", "mockpassword", true);

        // Assert
        $this->assertTrue($actual->isSuccess);
        $this->assertEquals($expected->identity->id, $actual->identity->id);
    }

    public function testUserFailsLogIn()
    {
        // Arrange
        $mockAuthProvider = $this->createMockAuthProvider(null);
        $mockAuthProvider->method('authenticate')->willReturn(
            new Result(Result::FAILURE, null, ['No role configured.'])
        );

        $mockSessionManager = $this->createMockSessionManager();

        $mockUserRepository = $this->createMock(UserRepository::class);

        $authService = new AuthService($mockAuthProvider, $mockSessionManager, $mockUserRepository);

        // Act
        $actual = $authService->login("mockemail", "mockpassword", false);

        // Assert
        $this->assertFalse($actual->isSuccess);
        $this->assertNull($actual->identity);
    }

    public function testUserLogsOut()
    {
        // Arrange
        $mockUser = $this->createMockUser();
        $mockAuthProvider = $this->createMockAuthProvider($mockUser);
        $mockAuthProvider->expects($this->once())->method('clearIdentity');

        $mockSessionManager = $this->createMockSessionManager();
        $mockUserRepository = $this->createMock(UserRepository::class);

        $authService = new AuthService($mockAuthProvider, $mockSessionManager, $mockUserRepository);

        // Act & Assert
        $actual = $authService->logout();
    }

    public function testExceptionThrownWhenLoggingOutWithNoUserLogedIn()
    {
        // Arrange
        $mockUser = $this->createMockUser();
        $mockAuthProvider = $this->createMockAuthProvider(null);
        $mockAuthProvider->expects($this->never())->method('clearIdentity');

        $mockSessionManager = $this->createMockSessionManager();
        $mockUserRepository = $this->createMock(UserRepository::class);

        $authService = new AuthService($mockAuthProvider, $mockSessionManager, $mockUserRepository);

        $this->expectException(\Exception::class);

        // Act & Assert
        $actual = $authService->logout();
    }

    public function testRegisterUser()
    {
        // Arrange
        $mockUser = $this->createMockUser();

        $mockAuthProvider = $this->createMock(AuthenticationService::class);
        $mockSessionManager = $this->createMock(SessionManager::class);

        $mockUserRepository = $this->createMock(UserRepository::class);
        $mockUserRepository->method('getByEmail')->willReturn(null);
        $mockUserRepository->expects($this->once())->method('getByEmail')->with($this->equalTo($mockUser->email));

        $mockUserRepository->method('saveUser')->willReturn($mockUser);
        $mockUserRepository->expects($this->once())->method('saveUser');

        $authService = new AuthService($mockAuthProvider, $mockSessionManager, $mockUserRepository);

        $expected = (object)[
            'isSuccess' => true,
            'savedUser' => $mockUser
        ];

        // Act
        $actual = $authService->register([
            'email' => $mockUser->email,
            'password' => $mockUser->password,
            'first_name' => $mockUser->firstName,
            'last_name' => $mockUser->lastName,
            'phone_number' => $mockUser->phoneNumber,
            'company_name' => $mockUser->companyName,
            'company_address' => $mockUser->companyAddress,
            'payment_method' => $mockUser->paymentMethod,
            'role' => $mockUser->role,
        ]);

        // Assert
        $this->assertTrue($actual->isSuccess);

        $this->assertEquals($expected->savedUser->id, $actual->savedUser->id);
        $this->assertEquals($expected->savedUser->email, $actual->savedUser->email);
        $this->assertEquals($expected->savedUser->password, $actual->savedUser->password);
        $this->assertEquals($expected->savedUser->firstName, $actual->savedUser->firstName);
        $this->assertEquals($expected->savedUser->lastName, $actual->savedUser->lastName);
        $this->assertEquals($expected->savedUser->role, $actual->savedUser->role);
    }

    public function testDoesNotRegisterUserWithExistingEmail()
    {
        // Arrange
        $mockUser = $this->createMockUser();

        $mockAuthProvider = $this->createMock(AuthenticationService::class);
        $mockSessionManager = $this->createMock(SessionManager::class);

        $mockUserRepository = $this->createMock(UserRepository::class);
        $mockUserRepository->method('getByEmail')->willReturn($mockUser);
        $mockUserRepository->expects($this->once())->method('getByEmail')->with($this->equalTo($mockUser->email));

        $mockUserRepository->method('saveUser');
        $mockUserRepository->expects($this->never())->method('saveUser');

        $authService = new AuthService($mockAuthProvider, $mockSessionManager, $mockUserRepository);

        $expected = (object)[
            'isSuccess' => false,
            'errorMessage' => "There's already a registered user with that email."
        ];

        // Act
        $actual = $authService->register([
            'email' => $mockUser->email,
            'password' => $mockUser->password,
            'first_name' => $mockUser->firstName,
            'last_name' => $mockUser->lastName,
            'company_name' => $mockUser->companyName,
            'company_address' => $mockUser->companyAddress,
            'payment_method' => $mockUser->paymentMethod,
            'role' => $mockUser->role,
        ]);

        // Assert
        $this->assertFalse($actual->isSuccess);
        $this->assertEquals($expected->errorMessage, $actual->errorMessage);
    }
}