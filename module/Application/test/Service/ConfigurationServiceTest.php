<?php

namespace ApplicationTest\Service;

use \PHPUnit\Framework\TestCase;

use Application\Service\ConfigurationService;

use Application\Repository\SettingRepository;

use Application\Model\Setting;

class ConfigurationServiceTest extends TestCase
{
    public function testSaveAppSettings()
    {
        // Arrange
        $setting1 = new Setting();
        $setting1->id = 1;
        $setting1->code = 'VEHICLE_DROP_OFF_ADDRESS';
        $setting1->value = 'some value';

        $setting2 = new Setting();
        $setting2->id = 2;
        $setting2->code = 'CSAR_CONTACT_PHONE_NUMBER';
        $setting2->value = 'some value';

        $mockSettingRepository = $this->createMock(SettingRepository::class);
        $mockSettingRepository
            ->expects($this->exactly(2))
            ->method('get')
            ->withConsecutive(
                [$this->equalTo("VEHICLE_DROP_OFF_ADDRESS")],
                [$this->equalTo("CSAR_CONTACT_PHONE_NUMBER")]
            )
            ->will($this->onConsecutiveCalls(
                $setting1,
                $setting2
            ));

        $mockSettingRepository
            ->expects($this->exactly(2))
            ->method('update')
            ->will($this->returnArgument(0));

        $service = new ConfigurationService($mockSettingRepository);

        // Act
        $actual = $service->saveAppSettings([
            "vehicleDropOffAddress" => "a random address",
            "contactPhoneNumber" => "555-555-5555"
        ]);

        // Assert
        $this->assertTrue($actual->isSuccess);
        $this->assertNotEmpty($actual->updatedSettings);
        $this->assertEquals($actual->updatedSettings[0]->value, "a random address");
        $this->assertEquals($actual->updatedSettings[1]->value, "555-555-5555");
    }
}