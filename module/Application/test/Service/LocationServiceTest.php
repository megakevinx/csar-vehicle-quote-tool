<?php

namespace ApplicationTest\Service;

use \PHPUnit\Framework\TestCase;

use Application\Service\LocationService;

use Application\Repository\ZipRepository;
use Application\Repository\CountyRepository;

use Application\Model\Zip;
use Application\Model\County;

class LocationServiceTest extends TestCase
{
    public function testGetCountyForZip()
    {
        // Arrange
        $mockZip = new Zip();
        $mockZip->code = '12345';
        $mockZip->countyId = 1;

        $mockZipRepository = $this->createMock(ZipRepository::class);
        $mockZipRepository->method('getByCode')->willReturn($mockZip);
        $mockZipRepository->expects($this->once())->method('getByCode')->with($this->equalTo($mockZip->code));

        $mockCounty = new County();
        $mockCounty->id = 1;
        $mockCounty->name = "Pasco";

        $mockCountyRepository = $this->createMock(CountyRepository::class);
        $mockCountyRepository->method('get')->willReturn($mockCounty);
        $mockCountyRepository->expects($this->once())->method('get')->with($this->equalTo($mockZip->countyId));

        $service = new LocationService($mockZipRepository, $mockCountyRepository);

        $expected = $mockCounty;

        // Act
        $actual = $service->getCountyForZip($mockZip->code);

        // Assert
        $this->assertEquals($expected->id, $actual->id);
        $this->assertEquals($expected->name, $actual->name);
    }

    public function testReturnNullForInvalidZip()
    {
        // Arrange
        $mockZipRepository = $this->createMock(ZipRepository::class);
        $mockZipRepository->method('getByCode')->willReturn(null);
        $mockZipRepository->expects($this->once())->method('getByCode')->with($this->anything());

        $mockCountyRepository = $this->createMock(CountyRepository::class);
        $mockCountyRepository->method('get');
        $mockCountyRepository->expects($this->never())->method('get');

        $service = new LocationService($mockZipRepository, $mockCountyRepository);

        // Act
        $actual = $service->getCountyForZip('99999');

        // Assert
        $this->assertNull($actual);
    }

    public function dataProvider_TestIsZipSupported()
    {
        return [
            'Hillsborough' => [true, 'Hillsborough'],
            'Pinellas' => [true, 'Pinellas'],
            'Pasco' => [true, 'Pasco'],
            'Hernando' => [true, 'Hernando'],
            'Polk' => [true, 'Polk'],
            'Sarasota' => [false, 'Sarasota'],
            'Bradenton' => [false, 'Bradenton'],
            'Whatever' => [false, 'Whatever'],
        ];
    }

    /**
    * @dataProvider dataProvider_TestIsZipSupported
    */
    public function testIsZipSupported($expected, $county)
    {
        // Arrange
        $mockZip = new Zip();
        $mockZip->code = '12345';
        $mockZip->countyId = 1;

        $mockZipRepository = $this->createMock(ZipRepository::class);
        $mockZipRepository->method('getByCode')->willReturn($mockZip);
        $mockZipRepository->expects($this->once())->method('getByCode')->with($this->equalTo($mockZip->code));

        $mockCounty = new County();
        $mockCounty->id = 1;
        $mockCounty->name = $county;

        $mockCountyRepository = $this->createMock(CountyRepository::class);
        $mockCountyRepository->method('get')->willReturn($mockCounty);
        $mockCountyRepository->expects($this->once())->method('get')->with($this->equalTo($mockZip->countyId));

        $service = new LocationService($mockZipRepository, $mockCountyRepository);

        // Act
        $actual = $service->isZipSupported($mockZip->code);

        // Assert
        $this->assertEquals($expected, $actual);
    }

    public function testIsZipSupportedReturnsFalseWhenZipCodeIsNotFound()
    {
        // Arrange
        $mockZipRepository = $this->createMock(ZipRepository::class);
        $mockZipRepository->method('getByCode')->willReturn(null);
        $mockZipRepository->expects($this->once())->method('getByCode')->with($this->anything());

        $mockCountyRepository = $this->createMock(CountyRepository::class);
        $mockCountyRepository->method('get');
        $mockCountyRepository->expects($this->never())->method('get');

        $service = new LocationService($mockZipRepository, $mockCountyRepository);

        // Act
        $actual = $service->isZipSupported('321asd');

        // Assert
        $this->assertFalse($actual);
    }
}