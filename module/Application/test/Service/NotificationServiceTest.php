<?php

namespace ApplicationTest\Service;

use \PHPUnit\Framework\TestCase;

use Application\Service\NotificationService;

use Application\Repository\NotificationRepository;
use Application\Repository\TransactionRepository;
use Application\Repository\UserRepository;
use Application\Repository\VehicleTypeRepository;
use Application\Repository\followUpCampaignRepository;

use Application\Model\Transaction;
use Application\Model\User;
use Application\Model\VehicleType;
use Application\Model\Notification;
use Application\Model\NotificationType;
use Application\Model\NotificationStatus;
use Application\Model\FollowUpCampaignStep;
use Application\Model\FollowUpCampaign;

class NotificationServiceTest extends TestCase
{
    private function createNotificationServiceToTest($referenceNumber, $createdByUserEmail, $saveNotificationCallsAmount)
    {
        $mockTransaction = new Transaction();
        $mockTransaction->id = 1;
        $mockTransaction->createdByUserId = 1;
        $mockTransaction->vehicleTypeId = 1;

        $transactionRepository = $this->createMock(TransactionRepository::class);
        $transactionRepository
            ->expects($this->once())
            ->method('get')
            ->with($this->equalTo($referenceNumber))
            ->willReturn($mockTransaction);

        $mockUser = new User();
        $mockUser->email = $createdByUserEmail;

        $userRepository = $this->createMock(UserRepository::class);
        $userRepository
            ->expects($this->once())
            ->method('get')
            ->with($this->equalTo($mockTransaction->createdByUserId))
            ->willReturn($mockUser);

        $mockVehicleType = new VehicleType();

        $vehicleTypeRepository = $this->createMock(VehicleTypeRepository::class);
        $vehicleTypeRepository
            ->expects($this->once())
            ->method('get')
            ->with($this->equalTo($mockTransaction->vehicleTypeId))
            ->willReturn($mockVehicleType);

        $notificationRepository = $this->createMock(NotificationRepository::class);
        $notificationRepository
            ->expects($this->exactly($saveNotificationCallsAmount))
            ->method('saveNotification')
            ->with($this->anything())
            ->will($this->returnArgument(0));

        $followUpCampaignRepository = $this->createMock(FollowUpCampaignRepository::class);

        $service = new NotificationService($notificationRepository, $transactionRepository, $userRepository, $vehicleTypeRepository, $followUpCampaignRepository);

        return $service;
    }

    public function testCreateCallbackVendorNotification()
    {
        // Arrange
        $service = $this->createNotificationServiceToTest("12345", "test@gmail.com", 2);

        // Act
        $actual = $service->createCallbackVendorNotification("12345");

        // Assert
        $this->assertTrue($actual->isSuccess);
        $this->assertNotEmpty($actual->createdNotifications);
        $this->assertEquals(count($actual->createdNotifications), 2);

        $this->assertEquals(
            count(array_filter($actual->createdNotifications, function($n) {
                return $n->type == NotificationType::$EMAIL;
            })),
            1
        );
        $this->assertEquals(
            count(array_filter($actual->createdNotifications, function($n) {
                return $n->type == NotificationType::$SMS;
            })),
            1
        );
        $this->assertEquals(
            count(array_filter($actual->createdNotifications, function($n) {
                return $n->status == NotificationStatus::$PENDING;
            })),
            2
        );
    }

    public function testCreateCallbackVendorNotificationUnsusccessfulWhenNoTransactionIsFound()
    {
        // Arrange
        $transactionRepository = $this->createMock(TransactionRepository::class);
        $transactionRepository
            ->expects($this->once())
            ->method('get')
            ->with($this->equalTo("12345"))
            ->willReturn(null);

        $userRepository = $this->createMock(UserRepository::class);
        $vehicleTypeRepository = $this->createMock(VehicleTypeRepository::class);
        $notificationRepository = $this->createMock(NotificationRepository::class);
        $followUpCampaignRepository = $this->createMock(FollowUpCampaignRepository::class);

        $service = new NotificationService($notificationRepository, $transactionRepository, $userRepository, $vehicleTypeRepository, $followUpCampaignRepository);

        // Act
        $actual = $service->createCallbackVendorNotification("12345");

        // Assert
        $this->assertFalse($actual->isSuccess);
        $this->assertNotEmpty($actual->errorMessage);
    }

    public function testCreateContactSellerNotification()
    {
        // Arrange
        $service = $this->createNotificationServiceToTest("12345", "test@gmail.com", 2);

        // Act
        $actual = $service->createContactSellerNotification("12345");

        // Assert
        $this->assertTrue($actual->isSuccess);
        $this->assertNotEmpty($actual->createdNotifications);
        $this->assertEquals(count($actual->createdNotifications), 2);

        $this->assertEquals(
            count(array_filter($actual->createdNotifications, function($n) {
                return $n->type == NotificationType::$EMAIL;
            })),
            1
        );
        $this->assertEquals(
            count(array_filter($actual->createdNotifications, function($n) {
                return $n->type == NotificationType::$SMS;
            })),
            1
        );
        $this->assertEquals(
            count(array_filter($actual->createdNotifications, function($n) {
                return $n->status == NotificationStatus::$PENDING;
            })),
            2
        );
    }

    public function testCreateContactSellerNotificationUnsusccessfulWhenNoTransactionIsFound()
    {
        // Arrange
        $transactionRepository = $this->createMock(TransactionRepository::class);
        $transactionRepository
            ->expects($this->once())
            ->method('get')
            ->with($this->equalTo("12345"))
            ->willReturn(null);

        $userRepository = $this->createMock(UserRepository::class);
        $vehicleTypeRepository = $this->createMock(VehicleTypeRepository::class);
        $notificationRepository = $this->createMock(NotificationRepository::class);
        $followUpCampaignRepository = $this->createMock(FollowUpCampaignRepository::class);

        $service = new NotificationService($notificationRepository, $transactionRepository, $userRepository, $vehicleTypeRepository, $followUpCampaignRepository);

        // Act
        $actual = $service->createContactSellerNotification("12345");

        // Assert
        $this->assertFalse($actual->isSuccess);
        $this->assertNotEmpty($actual->errorMessage);
    }

    public function testCreateOrderReceivedWillContactSellerNotification()
    {
        // Arrange
        $referenceNumber = "12345";
        $destination = "test@gmail.com";
        $service = $this->createNotificationServiceToTest($referenceNumber, $destination, 1);

        // Act
        $actual = $service->createOrderReceivedWillContactSellerNotification($referenceNumber);

        // Assert
        $this->assertTrue($actual->isSuccess);
        $this->assertNotEmpty($actual->createdNotifications);
        $this->assertEquals(count($actual->createdNotifications), 1);

        $this->assertEquals($actual->createdNotifications[0]->type, NotificationType::$EMAIL);
        $this->assertEquals($actual->createdNotifications[0]->status, NotificationStatus::$PENDING);
        $this->assertEquals($actual->createdNotifications[0]->destination, $destination);
    }

    public function testCreateOrderReceivedWillContactSellerUnsusccessfulWhenNoTransactionIsFound()
    {
        // Arrange
        $transactionRepository = $this->createMock(TransactionRepository::class);
        $transactionRepository
            ->expects($this->once())
            ->method('get')
            ->with($this->equalTo("12345"))
            ->willReturn(null);

        $userRepository = $this->createMock(UserRepository::class);
        $vehicleTypeRepository = $this->createMock(VehicleTypeRepository::class);
        $notificationRepository = $this->createMock(NotificationRepository::class);
        $followUpCampaignRepository = $this->createMock(FollowUpCampaignRepository::class);

        $service = new NotificationService($notificationRepository, $transactionRepository, $userRepository, $vehicleTypeRepository, $followUpCampaignRepository);

        // Act
        $actual = $service->createOrderReceivedWillContactSellerNotification("12345");

        // Assert
        $this->assertFalse($actual->isSuccess);
        $this->assertNotEmpty($actual->errorMessage);
    }

    public function testCreateOrderReceivedWillCallBackNotification()
    {
        // Arrange
        $referenceNumber = "12345";
        $destination = "test@gmail.com";
        $service = $this->createNotificationServiceToTest($referenceNumber, $destination, 1);

        // Act
        $actual = $service->createOrderReceivedWillCallBackNotification($referenceNumber);

        // Assert
        $this->assertTrue($actual->isSuccess);
        $this->assertNotEmpty($actual->createdNotifications);
        $this->assertEquals(count($actual->createdNotifications), 1);

        $this->assertEquals($actual->createdNotifications[0]->type, NotificationType::$EMAIL);
        $this->assertEquals($actual->createdNotifications[0]->status, NotificationStatus::$PENDING);
        $this->assertEquals($actual->createdNotifications[0]->destination, $destination);
    }

    public function testCreateOrderReceivedWillCallBackUnsusccessfulWhenNoTransactionIsFound()
    {
        // Arrange
        $transactionRepository = $this->createMock(TransactionRepository::class);
        $transactionRepository
            ->expects($this->once())
            ->method('get')
            ->with($this->equalTo("12345"))
            ->willReturn(null);

        $userRepository = $this->createMock(UserRepository::class);
        $vehicleTypeRepository = $this->createMock(VehicleTypeRepository::class);
        $notificationRepository = $this->createMock(NotificationRepository::class);
        $followUpCampaignRepository = $this->createMock(FollowUpCampaignRepository::class);

        $service = new NotificationService($notificationRepository, $transactionRepository, $userRepository, $vehicleTypeRepository, $followUpCampaignRepository);

        // Act
        $actual = $service->createOrderReceivedWillCallBackNotification("12345");

        // Assert
        $this->assertFalse($actual->isSuccess);
        $this->assertNotEmpty($actual->errorMessage);
    }

    public function dataProvider_TestCreateFollowUpCampaignNotifications()
    {
        return [
            'Accepted' => ['createAcceptedFollowUpCampaignNotifications', FollowUpCampaign::$ACCEPTED],
            'still Shopping' => ['createStillShoppingFollowUpCampaignNotifications', FollowUpCampaign::$STILL_SHOPPING],
            'Not Yet' => ['createNotYetFollowUpCampaignNotifications', FollowUpCampaign::$NOT_YET],
        ];
    }

    /**
    * @dataProvider dataProvider_TestCreateFollowUpCampaignNotifications
    */
    public function testCreateFollowUpCampaignNotifications(string $methodName, string $campaignName)
    {
        // Arrange
        $mockTransaction = new Transaction();
        $mockTransaction->id = 1;
        $mockTransaction->createdByUserId = 1;
        $mockTransaction->vehicleTypeId = 1;
        $mockTransaction->sellerEmail = "test@email.com";
        $mockTransaction->sellerPhoneNumber = "1234567890";

        $transactionRepository = $this->createMock(TransactionRepository::class);
        $transactionRepository
            ->expects($this->once())
            ->method('get')
            ->with($this->equalTo("12345"))
            ->willReturn($mockTransaction);

        $userRepository = $this->createMock(UserRepository::class);
        $vehicleTypeRepository = $this->createMock(VehicleTypeRepository::class);

        $notificationRepository = $this->createMock(NotificationRepository::class);
        $notificationRepository
            ->expects($this->exactly(2))
            ->method('saveNotification')
            ->with($this->anything())
            ->will($this->returnArgument(0));

        $mockEmailStep = new FollowUpCampaignStep();
        $mockEmailStep->messageContent = "test email content";
        $mockEmailStep->messageSubject = "test email subject";
        $mockEmailStep->notificationType = NotificationType::$EMAIL;
        $mockEmailStep->triggerTime = 10;

        $mockSmsStep = new FollowUpCampaignStep();
        $mockSmsStep->messageContent = "test sms content";
        $mockSmsStep->messageSubject = null;
        $mockSmsStep->notificationType = NotificationType::$SMS;
        $mockSmsStep->triggerTime = 10;

        $followUpCampaignRepository = $this->createMock(FollowUpCampaignRepository::class);
        $followUpCampaignRepository
            ->expects($this->once())
            ->method('getStepsFor')
            ->with($this->equalTo($campaignName))
            ->willReturn([$mockEmailStep, $mockSmsStep]);

        $service = new NotificationService($notificationRepository, $transactionRepository, $userRepository, $vehicleTypeRepository, $followUpCampaignRepository);

        // Act
        $actual = $service->$methodName("12345");

        // Assert
        $this->assertTrue($actual->isSuccess);
        $this->assertNotEmpty($actual->createdNotifications);
        $this->assertEquals(count($actual->createdNotifications), 2);

        $this->assertEquals($actual->createdNotifications[0]->type, NotificationType::$EMAIL);
        $this->assertEquals($actual->createdNotifications[0]->status, NotificationStatus::$PENDING);
        $this->assertEquals($actual->createdNotifications[0]->destination, "test@email.com");
        $this->assertEquals($actual->createdNotifications[0]->subject, "test email subject");
        $this->assertEquals($actual->createdNotifications[0]->content, "test email content");

        $this->assertEquals($actual->createdNotifications[1]->type, NotificationType::$SMS);
        $this->assertEquals($actual->createdNotifications[1]->status, NotificationStatus::$PENDING);
        $this->assertEquals($actual->createdNotifications[1]->destination, "1234567890");
        $this->assertEquals($actual->createdNotifications[1]->subject, "");
        $this->assertEquals($actual->createdNotifications[1]->content, "test sms content");
    }
}