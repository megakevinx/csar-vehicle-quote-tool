<?php

namespace ApplicationTest\Service;

use \PHPUnit\Framework\TestCase;

use Application\Service\QuoteCalculationRuleService;

use Application\Repository\QuoteAmountRepository;

use Application\Model\QuoteAmount;

class QuoteCalculationRuleServiceTest extends TestCase
{
    public function testSaveRules()
    {
        // Arrange
        $quoteAmount1 = new QuoteAmount();
        $quoteAmount1->id = 1;
        $quoteAmount1->priceModifier = 10;

        $quoteAmount2 = new QuoteAmount();
        $quoteAmount2->id = 2;
        $quoteAmount2->priceModifier = 20;

        $mockQuoteAmountRepository = $this->createMock(QuoteAmountRepository::class);
        $mockQuoteAmountRepository
            ->expects($this->exactly(2))
            ->method('get')
            ->withConsecutive(
                [$this->equalTo(1)],
                [$this->equalTo(2)]
            )
            ->will($this->onConsecutiveCalls(
                $quoteAmount1,
                $quoteAmount2
            ));

        $mockQuoteAmountRepository
            ->expects($this->exactly(2))
            ->method('update')
            ->will($this->returnArgument(0));

        $service = new QuoteCalculationRuleService($mockQuoteAmountRepository);

        // Act
        $actual = $service->saveRules([
            ["id" => 1, "priceModifier" => 30],
            ["id" => 2, "priceModifier" => 40],
        ]);

        // Assert
        $this->assertTrue($actual->isSuccess);
        $this->assertNotEmpty($actual->updatedRules);
        $this->assertEquals($actual->updatedRules[0]->priceModifier, 30);
        $this->assertEquals($actual->updatedRules[1]->priceModifier, 40);
    }

    public function testChangeVehicleTypesPriceModifiersBy()
    {
        // Arrange
        $quoteAmount1 = new QuoteAmount();
        $quoteAmount1->id = 1;
        $quoteAmount1->priceModifier = 10;

        $quoteAmount2 = new QuoteAmount();
        $quoteAmount2->id = 2;
        $quoteAmount2->priceModifier = 20;

        $mockQuoteAmountRepository = $this->createMock(QuoteAmountRepository::class);
        $mockQuoteAmountRepository
            ->expects($this->once())
            ->method('getByFeatureType')
            ->with($this->equalTo("TYPE"))
            ->willReturn([$quoteAmount1, $quoteAmount2]);

        $mockQuoteAmountRepository
            ->expects($this->exactly(2))
            ->method('update')
            ->will($this->returnArgument(0));

        $service = new QuoteCalculationRuleService($mockQuoteAmountRepository);

        // Act
        $actual = $service->changeVehicleTypesPriceModifiersBy(["basePriceChange" => 100]);

        // Assert
        $this->assertTrue($actual->isSuccess);
        $this->assertNotEmpty($actual->updatedRules);
        $this->assertEquals($actual->updatedRules[0]->priceModifier, 110);
        $this->assertEquals($actual->updatedRules[1]->priceModifier, 120);
    }
}