<?php

namespace ApplicationTest\Service;

use \PHPUnit\Framework\TestCase;

use Application\Service\QuoteService;

use Application\Repository\QuoteAmountRepository;
use Application\Repository\VehicleTypeRepository;
use Application\Repository\ZipRepository;
use Application\Repository\CountyRepository;
use Application\Repository\QuoteAmountOverrideRepository;

use Application\Model\Zip;
use Application\Model\County;
use Application\Model\QuoteAmountOverride;

class QuoteServiceTest extends TestCase
{
    // Helpers
    private function createQuoteAmountRepositoryMock($quoteAmountRules)
    {
        $quoteAmountRepository = $this->createMock(QuoteAmountRepository::class);
        $quoteAmountRepository->method('getAllMap')->willReturn($quoteAmountRules);

        return $quoteAmountRepository;
    }

    private function createVehicleTypeRepositoryMock()
    {
        $vehicleTypeRepository = $this->createMock(VehicleTypeRepository::class);
        $vehicleTypeRepository->method('getAllMap')->willReturn([
            1 => 'Compact Car',
            2 => 'Midsize Car',
            3 => 'Fullsize Car',
            4 => 'Pickup',
            5 => 'Mini Van',
            6 => 'Small SUV',
            7 => 'Fullsize SUV',
        ]);

        return $vehicleTypeRepository;
    }

    private function createZipRepositoryMock()
    {
        $zipRepository = $this->createMock(ZipRepository::class);

        $mockZip = new Zip();
        $mockZip->id = 1;
        $mockZip->code = '33503';
        $mockZip->countyId = 1;

        $zipRepository->method('getByCode')->willReturn($mockZip);

        return $zipRepository;
    }

    private function createCountyRepositoryMock($county)
    {
        $countyRepository = $this->createMock(CountyRepository::class);

        $mockCounty = new County();
        $mockCounty->id = 1;
        $mockCounty->name = $county;

        $countyRepository->method('get')->willReturn($mockCounty);

        return $countyRepository;
    }

    private function createQuoteAmountOverrideRepositoryMock($quoteOverride)
    {
        $quoteAmountOverrideRepository = $this->createMock(QuoteAmountOverrideRepository::class);

        if ($quoteOverride)
        {
            $override = new QuoteAmountOverride();
            $override->quote = $quoteOverride;
            $quoteAmountOverrideRepository->method('getBy')->willReturn($override);
        }
        else
        {
            $quoteAmountOverrideRepository->method('getBy')->willReturn(null);
        }

        return $quoteAmountOverrideRepository;
    }

    private function createInputDataMock()
    {
        $inputData = [
            'vehicleYear' => '1991',
            'vehicleMake' => 'toyota',
            'vehicleModel' => 'supra',
            'vehicleTypeId' => '1',
            'vehicleHasKeys' => 'no',
            'vehicleHasTitle' => 'yes',
            'vehicleWheelsDescription' => 'steel',
            'vehicleWillBeDelivered' => 'yes',
            'vehicleZipCode' => '33503',
        ];

        return $inputData;
    }

    private function createQuoteAmountRuleMock()
    {
        return [
            'RIMS' => [ 'steel' => 0 ],
            'HAS_TITLE' => [ 'yes' => 0, 'no' => 0 ],
            'LOCATION' => [ 'hillsborough' => 0 ],
            'SELF_DELIVERY' => [ 'yes' => 0, 'no' => 0 ],
            'HAS_KEYS' => [ 'yes' => 0, 'no' => 0 ],
            'TYPE' => [ 'compact car' => 0 ],
        ];
    }

    // Test Cases
    public function testCalculationUsesAllRulesWhenNoOverride()
    {
        // Arrange
        $quoteAmountRepository = $this->createQuoteAmountRepositoryMock([
            'RIMS' => [
                'no wheels' => -40,
                'steel' => 0,
                'aluminum' => 10,
            ],
            'HAS_TITLE' => [
                'yes' => 10,
                'no' => -10,
            ],
            'LOCATION' => [
                'hillsborough' => 0,
                'pinellas' => 0,
                'pasco' => -20,
                'hernando' => -20,
                'polk' => -20,
            ],
            'SELF_DELIVERY' => [
                'yes' => 0,
                'no' => 0,
            ],
            'HAS_KEYS' => [
                'yes' => 0,
                'no' => -20,
            ],
            'TYPE' => [
                'compact car' => 100,
                'midsize car' => 160,
                'fullsize car' => 180,
                'pickup' => 220,
                'mini van' => 200,
                'small suv' => 200,
                'fullsize suv' => 220,
            ],
        ]);

        $vehicleTypeRepository = $this->createVehicleTypeRepositoryMock();
        $zipRepository = $this->createZipRepositoryMock();
        $countyRepository = $this->createCountyRepositoryMock('Hillsborough');
        $quoteAmountOverrideRepository = $this->createQuoteAmountOverrideRepositoryMock(null);

        $service = new QuoteService($quoteAmountRepository, $vehicleTypeRepository, $zipRepository, $countyRepository, $quoteAmountOverrideRepository);

        $inputData = $this->createInputDataMock();

        $expected = 100 - 20 + 10 + 0 ;

        // Act
        $actual = $service->calculateQuote($inputData);

        // Assert
        $this->assertEquals($expected, $actual);
    }

    public function dataProvider_TestCalculationUsesVehicleType()
    {
        return [
            'Compact Car' => [100, "1"],
            'Midsize Car' => [101, "2"],
            'Fullsize Car' => [102, "3"],
            'Pickup' => [103, "4"],
            'Mini Van' => [104, "5"],
            'Small SUV' => [105, "6"],
            'Fullsize SUV' => [106, "7"],
        ];
    }

    /**
    * @dataProvider dataProvider_TestCalculationUsesVehicleType
    */
    public function testCalculationUsesVehicleType($expected, $inputVehicleTypeId)
    {
        // Arrange
        $quoteAmountRules = $this->createQuoteAmountRuleMock();
        $quoteAmountRules['TYPE'] = [
            'compact car' => 100,
            'midsize car' => 101,
            'fullsize car' => 102,
            'pickup' => 103,
            'mini van' => 104,
            'small suv' => 105,
            'fullsize suv' => 106,
        ];

        $quoteAmountRepository = $this->createQuoteAmountRepositoryMock($quoteAmountRules);
        $vehicleTypeRepository = $this->createVehicleTypeRepositoryMock();
        $zipRepository = $this->createZipRepositoryMock();
        $countyRepository = $this->createCountyRepositoryMock('Hillsborough');
        $quoteAmountOverrideRepository = $this->createQuoteAmountOverrideRepositoryMock(null);

        $service = new QuoteService($quoteAmountRepository, $vehicleTypeRepository, $zipRepository, $countyRepository, $quoteAmountOverrideRepository);

        $inputData = $this->createInputDataMock();
        $inputData['vehicleTypeId'] = $inputVehicleTypeId;

        // Act
        $actual = $service->calculateQuote($inputData);

        // Assert
        $this->assertEquals($expected, $actual);
    }

    public function dataProvider_TestCalculationUsesVehicleLocation()
    {
        return [
            'Hillsborough' => [11, 'Hillsborough'],
            'Pinellas' => [12, 'Pinellas'],
            'Pasco' => [13, 'Pasco'],
            'Hernando' => [14, 'Hernando'],
            'Polk' => [15, 'Polk'],
        ];
    }

    /**
    * @dataProvider dataProvider_TestCalculationUsesVehicleLocation
    */
    public function testCalculationUsesVehicleLocation($expected, $county)
    {
        // Arrange
        $quoteAmountRules = $this->createQuoteAmountRuleMock();
        $quoteAmountRules['LOCATION'] = [
            'hillsborough' => 11,
            'pinellas' => 12,
            'pasco' => 13,
            'hernando' => 14,
            'polk' => 15,
        ];

        $quoteAmountRepository = $this->createQuoteAmountRepositoryMock($quoteAmountRules);
        $vehicleTypeRepository = $this->createVehicleTypeRepositoryMock();
        $zipRepository = $this->createZipRepositoryMock();
        $countyRepository = $this->createCountyRepositoryMock($county);
        $quoteAmountOverrideRepository = $this->createQuoteAmountOverrideRepositoryMock(null);

        $service = new QuoteService($quoteAmountRepository, $vehicleTypeRepository, $zipRepository, $countyRepository, $quoteAmountOverrideRepository);

        $inputData = $this->createInputDataMock();
        $inputData['vehicleWillBeDelivered'] = "no";

        // Act
        $actual = $service->calculateQuote($inputData);

        // Assert
        $this->assertEquals($expected, $actual);
    }

    public function dataProvider_TestCalculationIgnoresVehicleLocationWhenSelfDelivery()
    {
        return [
            'Hillsborough' => ['Hillsborough'],
            'Pinellas' => ['Pinellas'],
            'Pasco' => ['Pasco'],
            'Hernando' => ['Hernando'],
            'Polk' => ['Polk'],
        ];
    }

    /**
    * @dataProvider dataProvider_TestCalculationIgnoresVehicleLocationWhenSelfDelivery
    */
    public function testCalculationIgnoresVehicleLocationWhenSelfDelivery($county)
    {
        // Arrange
        $quoteAmountRules = $this->createQuoteAmountRuleMock();
        $quoteAmountRules['LOCATION'] = [
            'hillsborough' => 11,
            'pinellas' => 12,
            'pasco' => 13,
            'hernando' => 14,
            'polk' => 15,
        ];
        $quoteAmountRules['SELF_DELIVERY'] = [
            'yes' => 10,
            'no' => 0,
        ];

        $quoteAmountRepository = $this->createQuoteAmountRepositoryMock($quoteAmountRules);
        $vehicleTypeRepository = $this->createVehicleTypeRepositoryMock();
        $zipRepository = $this->createZipRepositoryMock();
        $countyRepository = $this->createCountyRepositoryMock($county);
        $quoteAmountOverrideRepository = $this->createQuoteAmountOverrideRepositoryMock(null);

        $service = new QuoteService($quoteAmountRepository, $vehicleTypeRepository, $zipRepository, $countyRepository, $quoteAmountOverrideRepository);

        $inputData = $this->createInputDataMock();
        $inputData['vehicleWillBeDelivered'] = "yes";

        $expected = 10;

        // Act
        $actual = $service->calculateQuote($inputData);

        // Assert
        $this->assertEquals($expected, $actual);
    }

    public function testCalculationUsesVehicleHasTitle()
    {
        // Arrange
        $quoteAmountRules = $this->createQuoteAmountRuleMock();
        $quoteAmountRules['HAS_TITLE'] = [ 'yes' => 100, 'no' => 0 ];

        $quoteAmountRepository = $this->createQuoteAmountRepositoryMock($quoteAmountRules);
        $vehicleTypeRepository = $this->createVehicleTypeRepositoryMock();
        $zipRepository = $this->createZipRepositoryMock();
        $countyRepository = $this->createCountyRepositoryMock('Hillsborough');
        $quoteAmountOverrideRepository = $this->createQuoteAmountOverrideRepositoryMock(null);

        $service = new QuoteService($quoteAmountRepository, $vehicleTypeRepository, $zipRepository, $countyRepository, $quoteAmountOverrideRepository);

        $inputData = $this->createInputDataMock();
        $inputData['vehicleHasTitle'] = "yes";

        $expected = 100;

        // Act
        $actual = $service->calculateQuote($inputData);

        // Assert
        $this->assertEquals($expected, $actual);
    }

    public function testCalculationUsesVehicleDoesNotHaveTitle()
    {
        // Arrange
        $quoteAmountRules = $this->createQuoteAmountRuleMock();
        $quoteAmountRules['HAS_TITLE'] = [ 'yes' => 0, 'no' => 100 ];

        $quoteAmountRepository = $this->createQuoteAmountRepositoryMock($quoteAmountRules);
        $vehicleTypeRepository = $this->createVehicleTypeRepositoryMock();
        $zipRepository = $this->createZipRepositoryMock();
        $countyRepository = $this->createCountyRepositoryMock('Hillsborough');
        $quoteAmountOverrideRepository = $this->createQuoteAmountOverrideRepositoryMock(null);

        $service = new QuoteService($quoteAmountRepository, $vehicleTypeRepository, $zipRepository, $countyRepository, $quoteAmountOverrideRepository);

        $inputData = $this->createInputDataMock();
        $inputData['vehicleHasTitle'] = "no";

        $expected = 100;

        // Act
        $actual = $service->calculateQuote($inputData);

        // Assert
        $this->assertEquals($expected, $actual);
    }

    public function testCalculationUsesVehicleHasKeys()
    {
        // Arrange
        $quoteAmountRules = $this->createQuoteAmountRuleMock();
        $quoteAmountRules['HAS_KEYS'] = [ 'yes' => 99, 'no' => 0 ];

        $quoteAmountRepository = $this->createQuoteAmountRepositoryMock($quoteAmountRules);
        $vehicleTypeRepository = $this->createVehicleTypeRepositoryMock();
        $zipRepository = $this->createZipRepositoryMock();
        $countyRepository = $this->createCountyRepositoryMock('Hillsborough');
        $quoteAmountOverrideRepository = $this->createQuoteAmountOverrideRepositoryMock(null);

        $service = new QuoteService($quoteAmountRepository, $vehicleTypeRepository, $zipRepository, $countyRepository, $quoteAmountOverrideRepository);

        $inputData = $this->createInputDataMock();
        $inputData['vehicleHasKeys'] = "yes";

        $expected = 99;

        // Act
        $actual = $service->calculateQuote($inputData);

        // Assert
        $this->assertEquals($expected, $actual);
    }

    public function testCalculationUsesVehicleDoesNotHaveKeys()
    {
        // Arrange
        $quoteAmountRules = $this->createQuoteAmountRuleMock();
        $quoteAmountRules['HAS_KEYS'] = [ 'yes' => 0, 'no' => 99 ];

        $quoteAmountRepository = $this->createQuoteAmountRepositoryMock($quoteAmountRules);
        $vehicleTypeRepository = $this->createVehicleTypeRepositoryMock();
        $zipRepository = $this->createZipRepositoryMock();
        $countyRepository = $this->createCountyRepositoryMock('Hillsborough');
        $quoteAmountOverrideRepository = $this->createQuoteAmountOverrideRepositoryMock(null);

        $service = new QuoteService($quoteAmountRepository, $vehicleTypeRepository, $zipRepository, $countyRepository, $quoteAmountOverrideRepository);

        $inputData = $this->createInputDataMock();
        $inputData['vehicleHasKeys'] = "no";

        $expected = 99;

        // Act
        $actual = $service->calculateQuote($inputData);

        // Assert
        $this->assertEquals($expected, $actual);
    }

    public function dataProvider_TestCalculationUsesVehicleWheels()
    {
        return [
            'No Wheels' => [10, 'no wheels'],
            'Steel' => [20, 'steel'],
            'Aluminum' => [30, 'aluminum']
        ];
    }

    /**
    * @dataProvider dataProvider_TestCalculationUsesVehicleWheels
    */
    public function testCalculationUsesVehicleWheels($expected, $wheels)
    {
        // Arrange
        $quoteAmountRules = $this->createQuoteAmountRuleMock();
        $quoteAmountRules['RIMS'] = [ 'no wheels' => 10, 'steel' => 20, 'aluminum' => 30 ];

        $quoteAmountRepository = $this->createQuoteAmountRepositoryMock($quoteAmountRules);
        $vehicleTypeRepository = $this->createVehicleTypeRepositoryMock();
        $zipRepository = $this->createZipRepositoryMock();
        $countyRepository = $this->createCountyRepositoryMock('Hillsborough');
        $quoteAmountOverrideRepository = $this->createQuoteAmountOverrideRepositoryMock(null);

        $service = new QuoteService($quoteAmountRepository, $vehicleTypeRepository, $zipRepository, $countyRepository, $quoteAmountOverrideRepository);

        $inputData = $this->createInputDataMock();
        $inputData['vehicleWheelsDescription'] = $wheels;

        //$expected = 99;

        // Act
        $actual = $service->calculateQuote($inputData);

        // Assert
        $this->assertEquals($expected, $actual);
    }

    public function testCalculationUsesOverride()
    {
        // Arrange
        $quoteAmountRepository = $this->createQuoteAmountRepositoryMock($this->createQuoteAmountRuleMock());

        $vehicleTypeRepository = $this->createVehicleTypeRepositoryMock();
        $zipRepository = $this->createZipRepositoryMock();
        $countyRepository = $this->createCountyRepositoryMock('Hillsborough');
        $quoteAmountOverrideRepository = $this->createQuoteAmountOverrideRepositoryMock(99.99);

        $service = new QuoteService($quoteAmountRepository, $vehicleTypeRepository, $zipRepository, $countyRepository, $quoteAmountOverrideRepository);

        $inputData = $this->createInputDataMock();

        $expected = 99.99;

        // Act
        $actual = $service->calculateQuote($inputData);

        // Assert
        $this->assertEquals($expected, $actual);
    }

    public function testCalculationUsesSpecificOverride()
    {
        // Arrange
        $quoteAmountRepository = $this->createQuoteAmountRepositoryMock($this->createQuoteAmountRuleMock());

        $vehicleTypeRepository = $this->createVehicleTypeRepositoryMock();
        $zipRepository = $this->createZipRepositoryMock();
        $countyRepository = $this->createCountyRepositoryMock('Hillsborough');
        $quoteAmountOverrideRepository = $this->createQuoteAmountOverrideRepositoryMock(99.99);

        $override = new QuoteAmountOverride();
        $override->quote = 99.99;

        $quoteAmountOverrideRepository = $this->createMock(QuoteAmountOverrideRepository::class);
        $quoteAmountOverrideRepository
            ->expects($this->once())
            ->method('getBy')
            ->with(
                $this->equalTo("1990"),
                $this->equalTo("1990"),
                $this->equalTo("acura"),
                $this->equalTo("integra")
            )
            ->willReturn($override);

        $service = new QuoteService($quoteAmountRepository, $vehicleTypeRepository, $zipRepository, $countyRepository, $quoteAmountOverrideRepository);

        $inputData = $this->createInputDataMock();
        $inputData['vehicleYear'] = "1990";
        $inputData['vehicleMake'] = "acura";
        $inputData['vehicleModel'] = "integra";

        $expected = 99.99;

        // Act
        $actual = $service->calculateQuote($inputData);

        // Assert
        $this->assertEquals($expected, $actual);
    }

    public function testCalculationUsesRangedOverride()
    {
        // Arrange
        $quoteAmountRepository = $this->createQuoteAmountRepositoryMock($this->createQuoteAmountRuleMock());

        $vehicleTypeRepository = $this->createVehicleTypeRepositoryMock();
        $zipRepository = $this->createZipRepositoryMock();
        $countyRepository = $this->createCountyRepositoryMock('Hillsborough');
        $quoteAmountOverrideRepository = $this->createQuoteAmountOverrideRepositoryMock(99.99);

        $override = new QuoteAmountOverride();
        $override->quote = 99.99;

        $quoteAmountOverrideRepository = $this->createMock(QuoteAmountOverrideRepository::class);

        $quoteAmountOverrideRepository
            ->expects($this->once())
            ->method('getBy')
            ->willReturn(null);

        $quoteAmountOverrideRepository
            ->expects($this->once())
            ->method('getWithFromYearLessThanOrEqualAndToYearGreaterThanOrEqual')
            ->with(
                $this->equalTo("1990"),
                $this->equalTo("1990"),
                $this->equalTo("acura"),
                $this->equalTo("integra")
            )
            ->willReturn($override);

        $service = new QuoteService($quoteAmountRepository, $vehicleTypeRepository, $zipRepository, $countyRepository, $quoteAmountOverrideRepository);

        $inputData = $this->createInputDataMock();
        $inputData['vehicleYear'] = "1990";
        $inputData['vehicleMake'] = "acura";
        $inputData['vehicleModel'] = "integra";

        $expected = 99.99;

        // Act
        $actual = $service->calculateQuote($inputData);

        // Assert
        $this->assertEquals($expected, $actual);
    }

    public function testCalculationUsesGivenYearAndNewerOverride()
    {
        // Arrange
        $quoteAmountRepository = $this->createQuoteAmountRepositoryMock($this->createQuoteAmountRuleMock());

        $vehicleTypeRepository = $this->createVehicleTypeRepositoryMock();
        $zipRepository = $this->createZipRepositoryMock();
        $countyRepository = $this->createCountyRepositoryMock('Hillsborough');
        $quoteAmountOverrideRepository = $this->createQuoteAmountOverrideRepositoryMock(99.99);

        $override = new QuoteAmountOverride();
        $override->quote = 99.99;

        $quoteAmountOverrideRepository = $this->createMock(QuoteAmountOverrideRepository::class);

        $quoteAmountOverrideRepository
            ->expects($this->once())
            ->method('getBy')
            ->willReturn(null);

        $quoteAmountOverrideRepository
            ->expects($this->once())
            ->method('getWithFromYearLessThanOrEqualAndToYearGreaterThanOrEqual')
            ->willReturn(null);

        $quoteAmountOverrideRepository
            ->expects($this->once())
            ->method('getWithFromYearLessThanOrEqual')
            ->with(
                $this->equalTo("1990"),
                $this->equalTo("Any"),
                $this->equalTo("acura"),
                $this->equalTo("integra")
            )
            ->willReturn($override);

        $service = new QuoteService($quoteAmountRepository, $vehicleTypeRepository, $zipRepository, $countyRepository, $quoteAmountOverrideRepository);

        $inputData = $this->createInputDataMock();
        $inputData['vehicleYear'] = "1990";
        $inputData['vehicleMake'] = "acura";
        $inputData['vehicleModel'] = "integra";

        $expected = 99.99;

        // Act
        $actual = $service->calculateQuote($inputData);

        // Assert
        $this->assertEquals($expected, $actual);
    }

    public function testCalculationUsesGivenYearAndOlderOverride()
    {
        // Arrange
        $quoteAmountRepository = $this->createQuoteAmountRepositoryMock($this->createQuoteAmountRuleMock());

        $vehicleTypeRepository = $this->createVehicleTypeRepositoryMock();
        $zipRepository = $this->createZipRepositoryMock();
        $countyRepository = $this->createCountyRepositoryMock('Hillsborough');
        $quoteAmountOverrideRepository = $this->createQuoteAmountOverrideRepositoryMock(99.99);

        $override = new QuoteAmountOverride();
        $override->quote = 99.99;

        $quoteAmountOverrideRepository = $this->createMock(QuoteAmountOverrideRepository::class);

        $quoteAmountOverrideRepository
            ->expects($this->once())
            ->method('getBy')
            ->willReturn(null);

        $quoteAmountOverrideRepository
            ->expects($this->once())
            ->method('getWithFromYearLessThanOrEqualAndToYearGreaterThanOrEqual')
            ->willReturn(null);

        $quoteAmountOverrideRepository
            ->expects($this->once())
            ->method('getWithFromYearLessThanOrEqual')
            ->willReturn(null);

        $quoteAmountOverrideRepository
            ->expects($this->once())
            ->method('getWithToYearGreaterThanOrEqual')
            ->with(
                $this->equalTo("Any"),
                $this->equalTo("1990"),
                $this->equalTo("acura"),
                $this->equalTo("integra")
            )
            ->willReturn($override);

        $service = new QuoteService($quoteAmountRepository, $vehicleTypeRepository, $zipRepository, $countyRepository, $quoteAmountOverrideRepository);

        $inputData = $this->createInputDataMock();
        $inputData['vehicleYear'] = "1990";
        $inputData['vehicleMake'] = "acura";
        $inputData['vehicleModel'] = "integra";

        $expected = 99.99;

        // Act
        $actual = $service->calculateQuote($inputData);

        // Assert
        $this->assertEquals($expected, $actual);
    }

    public function testCalculationUsesAnyYearOverride()
    {
        // Arrange
        $quoteAmountRepository = $this->createQuoteAmountRepositoryMock($this->createQuoteAmountRuleMock());

        $vehicleTypeRepository = $this->createVehicleTypeRepositoryMock();
        $zipRepository = $this->createZipRepositoryMock();
        $countyRepository = $this->createCountyRepositoryMock('Hillsborough');

        $override = new QuoteAmountOverride();
        $override->quote = 88.88;

        $quoteAmountOverrideRepository = $this->createMock(QuoteAmountOverrideRepository::class);

        $quoteAmountOverrideRepository
            ->expects($this->once())
            ->method('getWithFromYearLessThanOrEqualAndToYearGreaterThanOrEqual')
            ->willReturn(null);

        $quoteAmountOverrideRepository
            ->expects($this->once())
            ->method('getWithFromYearLessThanOrEqual')
            ->willReturn(null);

        $quoteAmountOverrideRepository
            ->expects($this->once())
            ->method('getWithToYearGreaterThanOrEqual')
            ->willReturn(null);

        $quoteAmountOverrideRepository
            ->expects($this->exactly(2))
            ->method('getBy')
            ->willReturn($override)
            ->withConsecutive(
                [$this->equalTo("1990"), $this->equalTo("1990"), $this->equalTo("acura"), $this->equalTo("integra")],
                [$this->equalTo("Any"), $this->equalTo("Any"), $this->equalTo("acura"), $this->equalTo("integra")]
            )
            ->will($this->onConsecutiveCalls(
                null,
                $override
            ));

        $service = new QuoteService($quoteAmountRepository, $vehicleTypeRepository, $zipRepository, $countyRepository, $quoteAmountOverrideRepository);

        $inputData = $this->createInputDataMock();
        $inputData['vehicleYear'] = "1990";
        $inputData['vehicleMake'] = "acura";
        $inputData['vehicleModel'] = "integra";

        $expected = 88.88;

        // Act
        $actual = $service->calculateQuote($inputData);

        // Assert
        $this->assertEquals($expected, $actual);
    }

    public function testCalculationUsesAnyModelOverride()
    {
        // Arrange
        $quoteAmountRepository = $this->createQuoteAmountRepositoryMock($this->createQuoteAmountRuleMock());

        $vehicleTypeRepository = $this->createVehicleTypeRepositoryMock();
        $zipRepository = $this->createZipRepositoryMock();
        $countyRepository = $this->createCountyRepositoryMock('Hillsborough');

        $override = new QuoteAmountOverride();
        $override->quote = 77.77;

        $quoteAmountOverrideRepository = $this->createMock(QuoteAmountOverrideRepository::class);

        $quoteAmountOverrideRepository
            ->expects($this->once())
            ->method('getWithFromYearLessThanOrEqualAndToYearGreaterThanOrEqual')
            ->willReturn(null);

        $quoteAmountOverrideRepository
            ->expects($this->once())
            ->method('getWithFromYearLessThanOrEqual')
            ->willReturn(null);

        $quoteAmountOverrideRepository
            ->expects($this->once())
            ->method('getWithToYearGreaterThanOrEqual')
            ->willReturn(null);

        $quoteAmountOverrideRepository
            ->expects($this->exactly(3))
            ->method('getBy')
            ->withConsecutive(
                [$this->equalTo("1990"), $this->equalTo("1990"), $this->equalTo("acura"), $this->equalTo("integra")],
                [$this->equalTo("Any"), $this->equalTo("Any"), $this->equalTo("acura"), $this->equalTo("integra")],
                [$this->equalTo("1990"), $this->equalTo("1990"), $this->equalTo("acura"), $this->equalTo("Any")]
            )
            ->will($this->onConsecutiveCalls(
                null,
                null,
                $override
            ));

        $service = new QuoteService($quoteAmountRepository, $vehicleTypeRepository, $zipRepository, $countyRepository, $quoteAmountOverrideRepository);

        $inputData = $this->createInputDataMock();
        $inputData['vehicleYear'] = "1990";
        $inputData['vehicleMake'] = "acura";
        $inputData['vehicleModel'] = "integra";

        $expected = 77.77;

        // Act
        $actual = $service->calculateQuote($inputData);

        // Assert
        $this->assertEquals($expected, $actual);
    }

    public function testCalculationUsesRangedAnyModelOverride()
    {
        // Arrange
        $quoteAmountRepository = $this->createQuoteAmountRepositoryMock($this->createQuoteAmountRuleMock());

        $vehicleTypeRepository = $this->createVehicleTypeRepositoryMock();
        $zipRepository = $this->createZipRepositoryMock();
        $countyRepository = $this->createCountyRepositoryMock('Hillsborough');

        $override = new QuoteAmountOverride();
        $override->quote = 77.77;

        $quoteAmountOverrideRepository = $this->createMock(QuoteAmountOverrideRepository::class);

        $quoteAmountOverrideRepository
            ->expects($this->exactly(3))
            ->method('getBy')
            ->willReturn(null);

        $quoteAmountOverrideRepository
            ->expects($this->once())
            ->method('getWithFromYearLessThanOrEqual')
            ->willReturn(null);

        $quoteAmountOverrideRepository
            ->expects($this->once())
            ->method('getWithToYearGreaterThanOrEqual')
            ->willReturn(null);

        $quoteAmountOverrideRepository
            ->expects($this->exactly(2))
            ->method('getWithFromYearLessThanOrEqualAndToYearGreaterThanOrEqual')
            ->withConsecutive(
                [$this->equalTo("1990"), $this->equalTo("1990"), $this->equalTo("acura"), $this->equalTo("integra")],
                [$this->equalTo("1990"), $this->equalTo("1990"), $this->equalTo("acura"), $this->equalTo("Any")]
            )
            ->will($this->onConsecutiveCalls(
                null,
                $override
            ));

        $service = new QuoteService($quoteAmountRepository, $vehicleTypeRepository, $zipRepository, $countyRepository, $quoteAmountOverrideRepository);

        $inputData = $this->createInputDataMock();
        $inputData['vehicleYear'] = "1990";
        $inputData['vehicleMake'] = "acura";
        $inputData['vehicleModel'] = "integra";

        $expected = 77.77;

        // Act
        $actual = $service->calculateQuote($inputData);

        // Assert
        $this->assertEquals($expected, $actual);
    }

    public function testCalculationUsesGivenYearAndNewerAnyModelOverride()
    {
        // Arrange
        $quoteAmountRepository = $this->createQuoteAmountRepositoryMock($this->createQuoteAmountRuleMock());

        $vehicleTypeRepository = $this->createVehicleTypeRepositoryMock();
        $zipRepository = $this->createZipRepositoryMock();
        $countyRepository = $this->createCountyRepositoryMock('Hillsborough');

        $override = new QuoteAmountOverride();
        $override->quote = 77.77;

        $quoteAmountOverrideRepository = $this->createMock(QuoteAmountOverrideRepository::class);

        $quoteAmountOverrideRepository
            ->expects($this->exactly(3))
            ->method('getBy')
            ->willReturn(null);

        $quoteAmountOverrideRepository
            ->expects($this->exactly(2))
            ->method('getWithFromYearLessThanOrEqualAndToYearGreaterThanOrEqual')
            ->willReturn(null);

        $quoteAmountOverrideRepository
            ->expects($this->once())
            ->method('getWithToYearGreaterThanOrEqual')
            ->willReturn(null);

        $quoteAmountOverrideRepository
            ->expects($this->exactly(2))
            ->method('getWithFromYearLessThanOrEqual')
            ->withConsecutive(
                [$this->equalTo("1990"), $this->equalTo("Any"), $this->equalTo("acura"), $this->equalTo("integra")],
                [$this->equalTo("1990"), $this->equalTo("Any"), $this->equalTo("acura"), $this->equalTo("Any")]
            )
            ->will($this->onConsecutiveCalls(
                null,
                $override
            ));

        $service = new QuoteService($quoteAmountRepository, $vehicleTypeRepository, $zipRepository, $countyRepository, $quoteAmountOverrideRepository);

        $inputData = $this->createInputDataMock();
        $inputData['vehicleYear'] = "1990";
        $inputData['vehicleMake'] = "acura";
        $inputData['vehicleModel'] = "integra";

        $expected = 77.77;

        // Act
        $actual = $service->calculateQuote($inputData);

        // Assert
        $this->assertEquals($expected, $actual);
    }

    public function testCalculationUsesGivenYearAndOlderAnyModelOverride()
    {
        // Arrange
        $quoteAmountRepository = $this->createQuoteAmountRepositoryMock($this->createQuoteAmountRuleMock());

        $vehicleTypeRepository = $this->createVehicleTypeRepositoryMock();
        $zipRepository = $this->createZipRepositoryMock();
        $countyRepository = $this->createCountyRepositoryMock('Hillsborough');

        $override = new QuoteAmountOverride();
        $override->quote = 77.77;

        $quoteAmountOverrideRepository = $this->createMock(QuoteAmountOverrideRepository::class);

        $quoteAmountOverrideRepository
            ->expects($this->exactly(3))
            ->method('getBy')
            ->willReturn(null);

        $quoteAmountOverrideRepository
            ->expects($this->exactly(2))
            ->method('getWithFromYearLessThanOrEqualAndToYearGreaterThanOrEqual')
            ->willReturn(null);

        $quoteAmountOverrideRepository
            ->expects($this->exactly(2))
            ->method('getWithFromYearLessThanOrEqual')
            ->willReturn(null);

        $quoteAmountOverrideRepository
            ->expects($this->exactly(2))
            ->method('getWithToYearGreaterThanOrEqual')
            ->withConsecutive(
                [$this->equalTo("Any"), $this->equalTo("1990"), $this->equalTo("acura"), $this->equalTo("integra")],
                [$this->equalTo("Any"), $this->equalTo("1990"), $this->equalTo("acura"), $this->equalTo("Any")]
            )
            ->will($this->onConsecutiveCalls(
                null,
                $override
            ));

        $service = new QuoteService($quoteAmountRepository, $vehicleTypeRepository, $zipRepository, $countyRepository, $quoteAmountOverrideRepository);

        $inputData = $this->createInputDataMock();
        $inputData['vehicleYear'] = "1990";
        $inputData['vehicleMake'] = "acura";
        $inputData['vehicleModel'] = "integra";

        $expected = 77.77;

        // Act
        $actual = $service->calculateQuote($inputData);

        // Assert
        $this->assertEquals($expected, $actual);
    }

    public function testCalculationUsesAnyYearAnyModelOverride()
    {
        // Arrange
        $quoteAmountRepository = $this->createQuoteAmountRepositoryMock($this->createQuoteAmountRuleMock());

        $vehicleTypeRepository = $this->createVehicleTypeRepositoryMock();
        $zipRepository = $this->createZipRepositoryMock();
        $countyRepository = $this->createCountyRepositoryMock('Hillsborough');

        $override = new QuoteAmountOverride();
        $override->quote = 66.66;

        $quoteAmountOverrideRepository = $this->createMock(QuoteAmountOverrideRepository::class);

        $quoteAmountOverrideRepository
            ->expects($this->exactly(2))
            ->method('getWithFromYearLessThanOrEqualAndToYearGreaterThanOrEqual')
            ->willReturn(null);

        $quoteAmountOverrideRepository
            ->expects($this->exactly(2))
            ->method('getWithFromYearLessThanOrEqual')
            ->willReturn(null);

        $quoteAmountOverrideRepository
            ->expects($this->exactly(2))
            ->method('getWithToYearGreaterThanOrEqual')
            ->willReturn(null);

        $quoteAmountOverrideRepository
            ->expects($this->exactly(4))
            ->method('getBy')
            ->withConsecutive(
                [$this->equalTo("1990"), $this->equalTo("1990"), $this->equalTo("acura"), $this->equalTo("integra")],
                [$this->equalTo("Any"), $this->equalTo("Any"), $this->equalTo("acura"), $this->equalTo("integra")],
                [$this->equalTo("1990"), $this->equalTo("1990"), $this->equalTo("acura"), $this->equalTo("Any")],
                [$this->equalTo("Any"), $this->equalTo("Any"), $this->equalTo("acura"), $this->equalTo("Any")]
            )
            ->will($this->onConsecutiveCalls(
                null,
                null,
                null,
                $override
            ));

        $service = new QuoteService($quoteAmountRepository, $vehicleTypeRepository, $zipRepository, $countyRepository, $quoteAmountOverrideRepository);

        $inputData = $this->createInputDataMock();
        $inputData['vehicleYear'] = "1990";
        $inputData['vehicleMake'] = "acura";
        $inputData['vehicleModel'] = "integra";

        $expected = 66.66;

        // Act
        $actual = $service->calculateQuote($inputData);

        // Assert
        $this->assertEquals($expected, $actual);
    }
}
?>